name = ExoRedOne
scale = 1
filename = exo_red.fbx
marketplaceID = {00000000-0000-0000-0000-000000000000}
texdir = textures
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointRoot = Hips
joint = jointLean = Spine
joint = jointHead = Head
joint = jointLeftHand = LeftHand
joint = jointEyeLeft = LeftEye
joint = jointEyeRight = RightEye
bs = EyeSquint_L = Squint_Left = 1
bs = MouthRight = Midmouth_Right = 1
bs = EyeOpen_R = EyesWide_Right = 1
bs = BrowsU_L = BrowsUp_Left = 1
bs = EyeBlink_R = Blink_Right = 1
bs = LipsUpperOpen = UpperLipOut = 1
bs = BrowsD_L = BrowsDown_Left = 1
bs = Sneer = Squint_Right = 0.5
bs = Sneer = Squint_Left = 0.5
bs = Sneer = NoseScrunch_Right = 0.75
bs = Sneer = NoseScrunch_Left = 0.75
bs = EyeSquint_R = Squint_Right = 1
bs = LipsLowerClose = LowerLipIn = 1
bs = MouthLeft = Midmouth_Left = 1
bs = ChinLowerRaise = Jaw_Up = 1
bs = BrowsU_C = BrowsUp_Right = 1
bs = BrowsU_C = BrowsUp_Left = 1
bs = BrowsD_R = BrowsDown_Right = 1
bs = MouthFrown_L = Frown_Left = 1
bs = LipsLowerDown = LowerLipDown_Right = 0.7
bs = LipsLowerDown = LowerLipDown_Left = 0.7
bs = EyeBlink_L = Blink_Left = 1
bs = MouthSmile_R = Smile_Right = 1
bs = JawFwd = JawForeward = 1
bs = ChinUpperRaise = UpperLipUp_Right = 0.5
bs = ChinUpperRaise = UpperLipUp_Left = 0.5
bs = LipsUpperUp = UpperLipUp_Right = 0.7
bs = LipsUpperUp = UpperLipUp_Left = 0.7
bs = MouthDimple_R = Smile_Right = 0.25
bs = LipsLowerOpen = LowerLipOut = 1
bs = JawOpen = MouthOpen = 0.7
bs = BrowsU_R = BrowsUp_Right = 1
bs = MouthSmile_L = Smile_Left = 1
bs = EyeOpen_L = EyesWide_Left = 1
bs = LipsUpperClose = UpperLipIn = 1
bs = LipsPucker = MouthNarrow_Right = 1
bs = LipsPucker = MouthNarrow_Left = 1
bs = LipsFunnel = TongueUp = 1
bs = LipsFunnel = MouthWhistle_NarrowAdjust_Right = 0.5
bs = LipsFunnel = MouthWhistle_NarrowAdjust_Left = 0.5
bs = LipsFunnel = MouthNarrow_Right = 1
bs = LipsFunnel = MouthNarrow_Left = 1
bs = LipsFunnel = Jaw_Down = 0.36
bs = LipsFunnel = JawForeward = 0.39
bs = MouthFrown_R = Frown_Right = 1
bs = JawRight = Jaw_Right = 1
bs = MouthDimple_L = Smile_Left = 0.25
bs = JawLeft = JawRotateY_Left = 0.5
bs = Puff = CheekPuff_Right = 1
bs = Puff = CheekPuff_Left = 1
jointIndex = RightHandPinky2 = 27
jointIndex = RightHandPinky4 = 29
jointIndex = RightHandIndex2 = 39
jointIndex = L_LipCornerLowTweak = 102
jointIndex = LeftHandPinky4 = 53
jointIndex = LeftUpLeg = 14
jointIndex = EXO_Eyes = 3
jointIndex = Neck = 70
jointIndex = LeftHand = 49
jointIndex = L_LipCorner = 100
jointIndex = L_CheekFold = 95
jointIndex = RightHandThumb2 = 43
jointIndex = Jaw = 112
jointIndex = L_InnerBrow = 108
jointIndex = LeftFoot = 16
jointIndex = LeftForeArm = 48
jointIndex = RightHandMiddle1 = 34
jointIndex = Spine1 = 20
jointIndex = LeftHandMiddle4 = 61
jointIndex = R_Eye = 78
jointIndex = RightUpLeg = 9
jointIndex = LeftShoulder = 46
jointIndex = RightHandRing2 = 31
jointIndex = Head = 72
jointIndex = R_gLipCorner = 85
jointIndex = LeftHandIndex2 = 63
jointIndex = LeftArm = 47
jointIndex = RightLeg = 10
jointIndex = EXO_BrowsLashes = 0
jointIndex = TongueBack = 115
jointIndex = RightHandMiddle4 = 37
jointIndex = RightHandThumb1 = 42
jointIndex = L_InnerCheek = 104
jointIndex = R_Ear = 74
jointIndex = LeftHandRing2 = 55
jointIndex = LeftHandPinky2 = 51
jointIndex = LeftHandMiddle1 = 58
jointIndex = LeftHandMiddle3 = 60
jointIndex = L_Ear = 118
jointIndex = LipMidLower = 97
jointIndex = LeftHandMiddle2 = 59
jointIndex = L_EyelidLower = 106
jointIndex = RightHandPinky3 = 28
jointIndex = R_IOuterBrow = 79
jointIndex = RightHandRing4 = 33
jointIndex = RightHandIndex1 = 38
jointIndex = RightFoot = 11
jointIndex = HeadTop_End = 119
jointIndex = RightHandRing1 = 30
jointIndex = L_Temple = 111
jointIndex = TongueTip = 117
jointIndex = RightHandMiddle3 = 36
jointIndex = L_LipUpper = 98
jointIndex = R_OuterCheek = 83
jointIndex = RightNostril = 90
jointIndex = L_OuterCheek = 92
jointIndex = L_LipCornerUpTweak = 101
jointIndex = EXO_Teeth = 6
jointIndex = EXO_Body = 5
jointIndex = LeftHandPinky1 = 50
jointIndex = LeftHandIndex4 = 65
jointIndex = EXO_EyesSpec = 7
jointIndex = LeftHandIndex3 = 64
jointIndex = L_IOuterBrow = 110
jointIndex = RightHandThumb4 = 45
jointIndex = LeftHandThumb2 = 67
jointIndex = RightShoulder = 22
jointIndex = R_InnerBrow = 81
jointIndex = LeftHandIndex1 = 62
jointIndex = LeftHandThumb4 = 69
jointIndex = EXO_HeadMask = 1
jointIndex = L_Eye = 107
jointIndex = RightHandMiddle2 = 35
jointIndex = Scalp = 93
jointIndex = LeftHandRing1 = 54
jointIndex = RightToe_End = 13
jointIndex = TongueMid = 116
jointIndex = LeftHandThumb3 = 68
jointIndex = R_Temple = 75
jointIndex = L_MidBrow = 109
jointIndex = LowerChin = 113
jointIndex = LeftHandRing4 = 57
jointIndex = LeftHandPinky3 = 52
jointIndex = R_LipUpper = 84
jointIndex = LeftToeBase = 17
jointIndex = RightForeArm = 24
jointIndex = EXO_Caruncula = 2
jointIndex = RightHandThumb3 = 44
jointIndex = R_MidBrow = 80
jointIndex = L_Nostril = 94
jointIndex = R_gLipCornerUpTweak = 86
jointIndex = RightToeBase = 12
jointIndex = RightHandRing3 = 32
jointIndex = LeftToe_End = 18
jointIndex = Spine2 = 21
jointIndex = Throat = 71
jointIndex = MidBrows = 82
jointIndex = L_LowerCheek = 103
jointIndex = Hips = 8
jointIndex = R_InnerCheek = 73
jointIndex = L_EyelidUpper = 105
jointIndex = LeftHandThumb1 = 66
jointIndex = RightHand = 25
jointIndex = R_CheekFold = 89
jointIndex = Chin = 114
jointIndex = R_EyelidUpper = 76
jointIndex = R_gLipCornerLowTweak = 87
jointIndex = R_LowerCheek = 88
jointIndex = RightHandPinky1 = 26
jointIndex = R_EyelidLower = 77
jointIndex = LipMidUpper = 96
jointIndex = Spine = 19
jointIndex = Exo_Suit = 4
jointIndex = LeftLeg = 15
jointIndex = RightHandIndex3 = 40
jointIndex = R_LipLower = 91
jointIndex = RightHandIndex4 = 41
jointIndex = L_LipLower = 99
jointIndex = LeftHandRing3 = 56
jointIndex = RightArm = 23
