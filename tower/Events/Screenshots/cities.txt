aachen	https://ccc.ac/post/2022-12-23-jev22
aalen	https://pretalx.hackwerk.fun/jev-2022/schedule/#0
athen	https://www.hackerspace.gr/wiki/Hackanaskopisi_2022_at_hsgr
berlin	https://hip-berlin.de/
bern	https://hackbar.ch/
chemnitz	https://chaoschemnitz.de/Hauptseite
copenhagen	https://labitat.dk/w/index.php?title=RemoteCCC.local_2022
duesseldorf	https://chaosdorf.de/2022/12/dezentrale-jahresendveranstaltung/
duisburg	https://hackspaceduisburg.hopto.org/enter_the_space47
enschede	https://tkkrlab.nl/blog/blog_nieuws_2022_12_13_ccc_remote/
erlangen	https://w.icmp.camp/
giessen	https://chilyconchaos.de/
hamburg_01	Manually, left out a slot
hamburg_02	see 01
hamburg_03	see 01
hamburg_04	see 01
heilbronn	https://codeforheilbronn.de/2022/11/29/dlc3-announcement.html
karlsruhe	https://entropia.de/Hauptseite
kassel	https://flipdot.org/
koeln	https://discuss.kalk.space/t/chaos-im-space-2022/888
konstanz	https://www.hacknology.de/
lausanne	https://fixme.ch/wiki/RC3_2022

leipzig!    https://dezentrale.space/posts/2022/12/localverse2022-fahrplan/
linz!   https://devlol.org/
luzern!   https://laborluxeria.github.io/winterchaos2022/
mannheim	https://raumzeitlabor.de/blog/Jahresendveranstaltung/
muenchen_01	Manually, left out a slot
muenchen_02	see 01
muenster	https://www.warpzone.ms/kamphack/
newyork	https://woodbine.nyc/2022/12/dweb-jev/
oslo	https://oslohack.no/
potsdam	https://cfp.ccc-p.org/rtc22/
reetzer	https://pretalx.c3voc.de/hacking-in-hell-2022/cfp
stuttgart	https://www.shackspace.de/
unna	https://www.un-hack-bar.de/2022/12/11/chaos-reboot-23/
wien	https://metalab.at/wiki/Caf%C3%A9_Dezentral
wiesbaden	https://www.cccwi.de/events/2022/12/20/JEV/
wuelfrath	https://events.haecksen.org/fireshonks/