---
title: PPP for Opening
tags: xrelog22, postprod, opening
description: PPP for Opening
---

# PPP for Session 01 - Opening

| Task | Date| Who| State
| -------- | -------- | -------- | -------- |
| PPPad created    | 23-01-02     | cf23     | done
| Asking for Feedback    | 23-01-02     | cf23     | started



### Links:
- [Pretalx](https://pretalx.c3voc.de/xrelog-2022/talk/M7TWWN/)
- [Prelive](https://tube.lab.nrw/w/ozJb749bt2dMnDihrz4ugQ)
- [Brainstorm](https://lab.nrw/hedgedoc/R-QNtOr1Qcqbi4pc6IGtJw#)
- [Back to PPP-Meta](https://lab.nrw/hedgedoc/mYUj3O3GTuWb-Svc0moiyQ#)


### Chapters
00:00 - 00:21 xrelog22 Intro
00:21 - xx:xx Part One
xx:xx - xx:xx Part Two
xx:xx - xx:xx Part Three
xx:xx - 19:25 Outro

### Glitches

- 00:02 - ...
- 


### Purpose of this Pad
- A Kind of a Devlog
- Reporting Glitches & Errors with Timestamp
- Chapterize Video
- Collect more Attributes
    - XML-ID
    - Slug
    - ...
- Collecting Status
    - Repare
    - Translate
    - Subtitle
    - Tracker-Production