1
00:00:30,000 --> 00:00:40,800
Ласкаво просимо на відкриття XRelog22!

2
00:00:40,800 --> 00:00:51,920
Привіт Анчоусу і привіт зовнішньому світу! Ми хочемо познайомити вас з тим, що таке XRelog22

3
00:00:51,920 --> 00:01:01,760
 приготував для вас у цьому році. Що таке XRelog22 взагалі?

4
00:01:01,760 --> 00:01:11,360
 Які цілі? Хочемо розібратися з SocialVR і SocialVR без дурниць 

5
00:01:11,360 --> 00:01:17,000
І це те, що ми винесли в наш девіз. Ми називаємо це 
"Бачення незалежних мультивсесвітів".

6
00:01:17,000 --> 00:01:23,960
Я чув, що ви також хочете інтегрувати хакерську етику в метасвіт і в мультивсесвіт.

7
00:01:23,960 --> 00:01:29,960
Простори мета і мультивсесвіту. Безумовно! У нас просто не вистачає його в достатній кількості.

8
00:01:29,960 --> 00:01:35,480
Метапростору не існує. Скажімо, я щойно помітив, що взагалі не бачу логотипу каналу.

9
00:01:35,480 --> 00:01:39,680
Чи є у вас логотип каналу? Логотип каналу?

10
00:01:39,680 --> 00:01:49,440
Що це було? Що це був за шум?

11
00:01:49,440 --> 00:01:56,960
Так, я теж не знаю. Так, на жаль, саме стрічка не витримала.

12
00:01:56,960 --> 00:02:04,960
Так, ми не супер-відеоряд. І не суперпрофесіонал з ОБС.

13
00:02:04,960 --> 00:02:11,680
Ми не ідеальні люди, тому іноді логотип каналу зникає.

14
00:02:11,680 --> 00:02:17,440
Скотч був просто поганий. Вона має бути поганої якості.

15
00:02:17,440 --> 00:02:21,760
Я думаю, що нам ще треба трохи скоригувати наші очікування. Я серйозно,

16
00:02:21,760 --> 00:02:27,760
що люди не повинні думати, що їх будуть обслуговувати ідеально, не повинні думати, що вони отримають якісну продукцію ARD, ZDF.

17
00:02:27,760 --> 00:02:30,520
ARD і ZDF нікому не потрібні.

18
00:02:30,520 --> 00:02:35,960
Але так чи інакше, багато чого є незавершеного, але привабливого.

19
00:02:35,960 --> 00:02:44,960
Він не недобудований, він у виробництві. Він спритний. Це гнучка розробка.

20
00:02:44,960 --> 00:02:47,560
Ви маєте на увазі, що "Work in progress" - це новий стандарт, так?

21
00:02:47,560 --> 00:02:48,560
Так, звичайно.

22
00:02:48,560 --> 00:02:54,320
Ага, добре. Так що готуйтеся до багатьох перевертань... перевертань... перевертань...

23
00:02:54,320 --> 00:02:58,720
Так, бачите, знову починається. Приготуйтеся побачити багато розлитого.

24
00:02:58,720 --> 00:03:02,320
Пляшки мате і розлиті речі, але ми будемо веселитися.

25
00:03:02,320 --> 00:03:04,720
Заклеюємо їх скотчем.

26
00:03:04,720 --> 00:03:05,400
Саме так.

27
00:03:05,400 --> 00:03:24,360
Але насправді воно вимовляється як XRelog22.

28
00:03:24,360 --> 00:03:31,960
Так, і це походить від операційної групи XR.Labs.

29
00:03:31,960 --> 00:03:39,800
Я тут, в ICCBS, дивись. Ми будуємо віртуальні світи.

30
00:03:39,800 --> 00:03:47,800
Одним з них, транспортним засобом, що мандрує віртуальним світом, є наш МЦКБС, 

31
00:03:47,800 --> 00:03:55,000
Студія міжгалактичної комунікації "Хаос". І для цієї події він пришвартувався в Оверте.

32
00:03:55,000 --> 00:04:02,760
Ось так, бачите, бачите, він досить побитий. Це тому, що у нас тут сталося кілька нещасних випадків.

33
00:04:02,760 --> 00:04:10,600
І вся гарна внутрішня фурнітура була погано закріплена, тому трохи постраждала. 

34
00:04:10,600 --> 00:04:17,120
Але ми це красиво виправляємо.

35
00:04:17,120 --> 00:04:23,920
Ходімо, я тобі дещо покажу. Тому що ми хочемо зробити якісь фірмові страви в куточках.

36
00:04:23,920 --> 00:04:31,960
Я не знаю, хто це впізнає, ті, хто бував на

37
00:04:31,960 --> 00:04:39,680
 База "С" розпізнає його. Це стіни світла, зроблені з пляшок мате.

38
00:04:39,680 --> 00:04:49,760
І тут ми розглядаємо, наприклад, створення так званого цифрового двійника цього пристрою.

39
00:04:49,760 --> 00:04:55,120
А ось інструкція зі створення інтерфейсу прикладного програмування, щоб ви знали, як ним користуватися. 

40
00:04:55,120 --> 00:05:01,480
Ось цей сайт, ви можете на нього подивитися і навіть почитати. І ось приклад того, як можна використати цей простір.

41
00:05:01,480 --> 00:05:08,920
Поставимо сюди цей світлодіодний екран.

42
00:05:08,920 --> 00:05:15,120
Я трохи літаю. Тут теж можна літати. Треба трохи потренуватися літати. 

43
00:05:15,120 --> 00:05:21,880
Ого, тепер я лечу сюди. Поглянь сюди.  Тут ще є дірка, бачиш? Знову зламався. 

44
00:05:21,880 --> 00:05:26,680
Тут треба ще трохи підправити. Це вже інше завдання. Розумієте, це все через аварії при посадці.

45
00:05:26,680 --> 00:05:32,520
Знову тут. Я щойно пройшов портал, а це наш зал вильоту.

46
00:05:32,520 --> 00:05:42,920
Тепер ми можемо піднятися до дирижабля. Агов, земля обертається. Круто...

47
00:05:42,920 --> 00:05:51,920
 ми можемо це зробити... дуже круто. Тепер ми можемо побачити МЦКБС ззовні. Тоді я

48
00:05:51,920 --> 00:05:58,600
тут, всередині дирижабля. А ще є портал. Подивимося, до чого це призведе? Ходімо всередину.

49
00:05:58,600 --> 00:06:08,400
Так, це повертає нас назад. А, ось де вони поміняли портали. Добре, що ми про це поговорили.

50
00:06:08,400 --> 00:06:13,720
Власне, портал, яким я щойно пройшов, мав би вести сюди. 

51
00:06:13,720 --> 00:06:23,080
Давайте подивимось сюди. Що це? Поглянь, темна... Поглянь, це Пил Феї.

52
00:06:23,080 --> 00:06:33,160
Я не можу в це повірити. Я це бачу вперше. Ого, зовсім непогано. Насправді це так, 

53
00:06:33,160 --> 00:06:39,880
Там все ще все крутиться? Вони більше не повертаються. Так, вони є, вони дуже повільно обертаються. 

54
00:06:39,880 --> 00:06:44,960
Тут ми бачимо вузол даних у різних художніх репрезентаціях. І у нас є багаття.

55
00:06:44,960 --> 00:06:56,560
І він тріщить. Тут дуже гостинно. Сподіваюся, ми будемо часто тут зустрічатися.

56
00:06:56,560 --> 00:07:02,720
Ну, якщо я тут гуляю, то можу зайти і в підвал до Jev22.

57
00:07:02,720 --> 00:07:13,960
А підвал виглядає так. Стоп, стоп, стоп, стоп, стоп. Погляньмо сюди. 

58
00:07:13,960 --> 00:07:20,520
Це всі події, головні сторінки подій. Ви можете подивитися на них усіх.

59
00:07:20,520 --> 00:07:27,240
Їх дуже багато. Так, давайте подивимося сюди. Паралельно хакерство. І коли ви підходите ближче, там дійсно інтерактивний сайт.

60
00:07:27,240 --> 00:07:33,000
Ви можете натиснути на цей і погортати його. І це працює для всіх 38 подій.

61
00:07:33,000 --> 00:07:42,640
Ви можете переглянути їх усі та взяти участь у конкурсі.

62
00:07:42,640 --> 00:07:54,120
Чилі з Хаосом. Так, у нас ви можете дивитися Чилі з хаосом. Програма дуже цікава. 

63
00:07:54,120 --> 00:08:01,480
Ви можете використовувати Місця, щоб прилетіти до нашого мейкерського простору.  

64
00:08:01,480 --> 00:08:11,680
Makerspace називається просто XRelog22. І цей мейкерспейс зараз є своєрідною колекцією незавершених речей. 

65
00:08:11,680 --> 00:08:17,120
незавершених справ. Ми починали з цього, в розрізненому порядку. 

66
00:08:17,120 --> 00:08:25,840
Наприклад, у мене є ідея для 15 треків, тобто 15 тем, які ми маємо. А що, якщо ми зробимо коло по треках.

67
00:08:25,840 --> 00:08:38,800
побудувати 15 поверхів. Так, дуже круто. А ще я хочу вміти літати. Так, літати - це хороша ідея.

68
00:08:38,800 --> 00:08:45,840
При ближчому розгляді ці поверхи трохи схожі на маленькі зали очікування. 

69
00:08:45,840 --> 00:08:53,800
Дуже маленький і дуже мініатюрний. І на кожному з цих поверхів - окрема тема. 

70
00:08:53,800 --> 00:09:02,200
Ми піднялися на третій поверх.

71
00:09:02,200 --> 00:09:09,480
Чи це подкастинг, чи це SocialVR, чи це XR-спільноти, їх дуже багато і так далі.

72
00:09:09,480 --> 00:09:16,680
І, звичайно, на перших кількох поверхах - (Н)Активізм.

73
00:09:16,680 --> 00:09:28,240
Це, безумовно, наша головна тема.

74
00:09:28,240 --> 00:09:36,160
Зачекай, я лечу туди. Там написано, але лампадка не горить.

75
00:09:36,160 --> 00:09:42,040
Я кудись переставив лампу. Зараз вона взагалі не горить.

76
00:09:42,040 --> 00:09:48,800
Загалом, тут все дуже зрозуміло. Ми можемо це прочитати?
Ні, гективізм знаходиться в темряві. 
про етику хакерів.

77
00:09:48,800 --> 00:09:55,160
Озирніться навколо. Це одна з перших речей, яку я зробив.

78
00:09:55,160 --> 00:10:02,920
Я намалював хакерську етику в 3D у віртуальній реальності за допомогою мульти пензля. 

79
00:10:02,920 --> 00:10:11,240
А потім я інтегрував цю картину як 3D-модель сюди. І я навіть інтегрував синтез голосу, німецькою та англійською мовами.

80
00:10:11,240 --> 00:10:18,160
Ну, те, що я з'ясував, досліджуючи хакерську етику,

81
00:10:18,160 --> 00:10:24,600
полягає в тому, що перші шість правил хакерів старіші,

82
00:10:24,600 --> 00:10:30,440
датуються 1960-ми роками і лише два правила були додані Клубом комп'ютерного хаосу

83
00:10:30,440 --> 00:10:36,760
Зокрема, не жартуйте з чужими даними.

84
00:10:36,760 --> 00:10:43,840
Використовуйте публічні дані, захищайте приватні дані. Я думаю, що ми це вже чули. 

85
00:10:43,840 --> 00:10:50,520
Це два доповнення до Комп'ютерного Хаос-Клубу. Ми взяли на себе сміливість додати дев'яте правило.

86
00:10:50,520 --> 00:11:00,720
Завжди думайте про шматочки і дерева. Тому що екологічний аспект поки що не є частиною етики хакерів. 

87
00:11:00,720 --> 00:11:07,280
Тому він має бути завершений. 

88
00:11:07,280 --> 00:11:14,320
Це була наша хакерська етика. А то бачите, так це збірка всякої всячини.

89
00:11:14,320 --> 00:11:17,560
Вгорі видно дирижабль.

90
00:11:17,560 --> 00:11:24,880
Над дирижаблем встановлена перша версія ICCBS. 

91
00:11:24,880 --> 00:11:30,440
Тут стоять лотки. Ми будемо використовувати ці лотки під час проведення майстер-класів.

92
00:11:30,440 --> 00:11:36,280
Тоді ми зможемо літати на теми і чути речі. 

93
00:11:36,280 --> 00:11:43,160
Ми так просто майструємо. Це наш віртуальний мейкерспейс.

94
00:11:43,160 --> 00:11:52,040
Він не має тематичного зв'язку. 

95
00:11:52,040 --> 00:11:59,000
Коли ми завершимо роботу над Tracktower, вона буде перенесена на власний доменний сервер. 

96
00:11:59,000 --> 00:12:07,320
Потім ми виносимо його з мейкерспейсу і будуємо щось нове. 

97
00:12:07,320 --> 00:12:20,400
Зараз я повертаюся в студію міжгалактичного зв'язку "Міжгалактичний Хаос". Скорочено ICCBS. 

98
00:12:20,400 --> 00:12:31,080
Поглянь. Там, звідки я родом. Тобто будь-хто може просто змінити місцезнаходження, хто є в системі?

99
00:12:31,080 --> 00:12:37,960
Так, все відкрито. Вам навіть не потрібно реєструватися. 

100
00:12:37,960 --> 00:12:44,560
Вам навіть не потрібно створювати обліковий запис. Вам достатньо завантажити клієнт і запустити його, щоб увійти в цей світ.

101
00:12:44,560 --> 00:12:49,320
Ви отримаєте коротке пояснення, як це працює.

102
00:12:49,320 --> 00:12:55,960
Тоді вже можна користуватися функцією "Місця". Тоді ви зможете відкрити для себе ще більше світів.

103
00:12:55,960 --> 00:13:07,080
Отже, ICCBS і Fairy Dust і XRelog або пізніше також Tracktower.

104
00:13:07,080 --> 00:13:14,040
Це лише три світи з десятків світів.

105
00:13:14,040 --> 00:13:18,240
Напевно, незабаром їх будуть сотні.

106
00:13:18,240 --> 00:13:27,520
Звичайно, є також світи, які не відкриті для громадськості, як, наприклад, Darling VR. 

107
00:13:27,520 --> 00:13:32,520
Він трохи більш сексуально забарвлений. Ми не хочемо, щоб будь-хто міг туди потрапити. 

108
00:13:32,520 --> 00:13:39,280
Але в іншому більшість світів відкриті для відвідування, 

109
00:13:39,280 --> 00:13:49,200
Вільно, без фігні, без NFT, смарт-контракту, блокчейну, крипто-траку. 

110
00:13:49,200 --> 00:13:52,560
Всього цього тут немає. Це так гарно.

111
00:13:52,560 --> 00:14:09,880
Анчоусе, ти показав нам так багато речей цього світу. 

112
00:14:09,880 --> 00:14:12,960
Але що ви маєте намір робити в Конгресі? Ви нічого про це не сказали.

113
00:14:12,960 --> 00:14:25,080
Так, саме так. У нас 15 сесій, 15 тем.

114
00:14:25,080 --> 00:14:36,440
і ми також розглядаємо інші 38 подій JEV22. Наша головна тема - критика технологій. 

115
00:14:36,440 --> 00:14:42,240
Чітка критика технологій. Ми не хочемо, щоб це зійшло з рук Facebook.

116
00:14:42,240 --> 00:14:48,800
Ми хочемо поговорити про Web 3.0. Ми хочемо критично подивитися на те, як це розвивається.

117
00:14:48,800 --> 00:14:53,520
Ми підготували для вас дуже цікаві сесії.

118
00:14:53,520 --> 00:14:58,280
Тоді ми хочемо зазирнути далі свого носа і подивитися на інші заходи JEV.

119
00:14:58,280 --> 00:15:04,920
Є абсолютно фантастичні ініціативи, від Нью-Йорка до Афін. 

120
00:15:04,920 --> 00:15:12,560
У Мюнхені - два, у Гамбурзі - чотири. У Берліні, в Ерлангені, в Дюссельдорфі, в Аахені.

121
00:15:12,560 --> 00:15:15,800
Нам є що відкрити для себе.

122
00:15:15,800 --> 00:15:17,280
Але чи буде так само весело?

123
00:15:17,280 --> 00:15:23,640
О, весело! Так, звичайно. Це правда, що у нас немає Jeopardy,

124
00:15:23,640 --> 00:15:29,080
Ми не встигли підготувати для нього більше гарних запитань. 

125
00:15:29,080 --> 00:15:38,400
Але ми намагаємося зробити щось настільки божевільне. Ми назвемо його Чемпіонат світу з мафії 2030. Ми переграємо жеребкування чемпіонату світу з футболу 2030 року.

126
00:15:38,400 --> 00:15:44,920
Дозвольте собі здивуватися. Щоразу об 11 годині вечора у середу, четвер та п'ятницю.

127
00:15:44,920 --> 00:15:47,400
Це буде цікаво.

128
00:15:47,400 --> 00:15:50,600
Ви, мабуть, робите це в дуже корумпований спосіб.

129
00:15:50,600 --> 00:15:57,840
Чи вважається мат валютою?

130
00:15:57,840 --> 00:16:05,720
Якщо це не вважається валютою, то ми теж не корумповані. 

131
00:16:05,720 --> 00:16:12,880
Важливо те, що у нас дуже і дуже мало фронтальних презентацій.

132
00:16:12,880 --> 00:16:19,640
Майже все у нас - це партисипативний воркшоп, побудований як Barcamp. 

133
00:16:19,640 --> 00:16:28,080
Одразу після цього відкриття буде баркемп XR. Але є ще й "Bits and Trees".

134
00:16:28,080 --> 00:16:34,200
Fireside Chat, до участі в якому Вас щиро запрошуємо.

135
00:16:34,200 --> 00:16:39,760
Також майстер-класи Оверте, де ми досліджуємо цей світ, 

136
00:16:39,760 --> 00:16:41,080
також розраховані на семінари, а не на лекції.

137
00:16:41,080 --> 00:16:50,760
Ви хочете ще раз сказати, як ми виходимо на світ? 

138
00:16:50,760 --> 00:16:57,800
Тому що ми бачимо цей дійсно великий світ, але як нам у нього потрапити? 

139
00:16:57,800 --> 00:16:58,800
Ми можемо до вас приїхати?

140
00:16:58,800 --> 00:17:04,520
Так, звичайно. Тож головне, що ми в "Матриці" як вдома. 

141
00:17:04,520 --> 00:17:11,960
У розкладі Pretalx для кожної презентації вказується матричний канал. 

142
00:17:11,960 --> 00:17:16,760
Саме тут ми зустрічаємося, а також обмінюємося всією інформацією. 

143
00:17:16,760 --> 00:17:29,820
Інша справа - це Метасвіт, це Мультисвіт Оверте.

144
00:17:29,820 --> 00:17:35,520
Overte.org вже є домашньою сторінкою.
 Там можна завантажити клієнт.

145
00:17:35,520 --> 00:17:42,880
Він існує як для Linux, так і для Windows. Цікаво, що він не доступний для Apple. 

146
00:17:42,880 --> 00:17:51,840
Ви також можете скористатися ним, щоб завітати до нас в ICCBS, в ліс Казкового пилу. 

147
00:17:51,840 --> 00:17:57,160
або у віртуальному хакерському просторі на xrelog з Track Tower. 

148
00:17:57,160 --> 00:17:58,160
Ви можете дослідити все це самостійно.

149
00:17:58,160 --> 00:18:08,760
Це звучить захоплююче, і я думаю, 

150
00:18:08,760 --> 00:18:11,600
деяким слухачам також буде дуже цікаво зустрітися з вами у 3D-просторі.

151
00:18:11,600 --> 00:18:17,640
Мені цікаво знати. У скількох глядачів обваляться сервери? 

152
00:18:17,640 --> 00:18:19,040
ми дізнаємося разом.

153
00:18:19,040 --> 00:18:20,040
Тестування, тестування.

154
00:18:20,040 --> 00:18:26,200
Ми також можемо надати інші сервери. Але ми ще не автоматизовані. 

155
00:18:26,200 --> 00:18:32,280
надання інших серверів. Але ми ще не дуже автоматизовані. 

156
00:18:32,280 --> 00:18:36,840
Це займе трохи часу, але ми готові. 

157
00:18:36,840 --> 00:18:38,080
Якщо ми зараз впадемо, ми ще можемо щось зробити.

158
00:18:38,080 --> 00:18:45,520
Насамкінець, я хотів би тепло попрощатися. 

159
00:18:45,520 --> 00:18:51,240
До речі, теж від, е-е-е, а ви хто такий?

160
00:18:51,240 --> 00:18:56,560
Вибач, Анчоусе, не можу тобі сказати.

161
00:18:56,560 --> 00:19:25,560
Тож нам доведеться розгадувати цей ребус наступного разу. Побачимося пізніше. До побачення.
