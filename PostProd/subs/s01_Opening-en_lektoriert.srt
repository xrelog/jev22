1
00:00:30,000 --> 00:00:40,800
Welcome to the opening of XRelog22!

2
00:00:40,800 --> 00:00:51,920
Hello Anchoray and hello to the outside world! We want to introduce you here what XRelog22

3
00:00:51,920 --> 00:01:01,760
 has in store for you this year. What is XRelog22 in general?

4
00:01:01,760 --> 00:01:11,360
 What are the objectives? We want to deal with SocialVR and SocialVR without bullshit 

5
00:01:11,360 --> 00:01:17,000
and that's what we put in our motto. We call it 
''Visions for Independent Multiverses.''

6
00:01:17,000 --> 00:01:23,960
I heard that you also want to integrate the hacker ethic into the metaverse and into the multiverse.

7
00:01:23,960 --> 00:01:29,960
The expanses of the meta and multiverse. Absolutely! There just aren't enough of them in the

8
00:01:29,960 --> 00:01:35,480
Metaverse does not exist. Say, I just noticed that I don't see the channel logo at all.

9
00:01:35,480 --> 00:01:39,680
Do you have a channel logo? Channel logo?

10
00:01:39,680 --> 00:01:49,440
What was that? What was that noise?

11
00:01:49,440 --> 00:01:56,960
Yes, I don't know either. Yes, unfortunately, it was the tape that didn't hold.

12
00:01:56,960 --> 00:02:04,960
Yes, we are not the super videasts. Nor really super professional with OBS.

13
00:02:04,960 --> 00:02:11,680
We are not perfect people, so sometimes the channel logo disappears.

14
00:02:11,680 --> 00:02:17,440
The scotch, it was just bad. It must be of bad quality.

15
00:02:17,440 --> 00:02:21,760
I think we still need to adjust our expectations a little bit. I mean by that,

16
00:02:21,760 --> 00:02:27,760
that people shouldn't think that they are going to be served perfectly, nor should they expect ARD or ZDF quality.

17
00:02:27,760 --> 00:02:30,520
Nobody wants ARD and ZDF.

18
00:02:30,520 --> 00:02:35,960
But anyway, there is a lot of unfinished business at home, but it is attractive.

19
00:02:35,960 --> 00:02:44,960
It's not unfinished, it's in production. It's agile. It's agile development.

20
00:02:44,960 --> 00:02:47,560
You mean 'Work in Progress' is the new standard, right?

21
00:02:47,560 --> 00:02:48,560
Yes, of course.

22
00:02:48,560 --> 00:02:54,320
Aha, okay. So get ready to get a lot of spills ... spills ...

23
00:02:54,320 --> 00:02:58,720
Yes, you see, here we go again. Get ready to see a lot of spilled stuff.

24
00:02:58,720 --> 00:03:02,320
Bottles of mate and spilled stuff, but we're going to have fun.

25
00:03:02,320 --> 00:03:04,720
We stick them with the tape.

26
00:03:04,720 --> 00:03:05,400
Exactly.

27
00:03:05,400 --> 00:03:24,360
But it's really pronounced XRelog22.

28
00:03:24,360 --> 00:03:31,960
So this is derived from XR.Labs Operation Group.

29
00:03:31,960 --> 00:03:39,800
I'm here at ICCBS, look. We build virtual worlds.

30
00:03:39,800 --> 00:03:47,800
One of them, a vehicle traveling through the virtual world, is our ICCBS, 

31
00:03:47,800 --> 00:03:55,000
Intergalactical Chaos Communication Broadcast Studio. And it docked in Overte for this event.

32
00:03:55,000 --> 00:04:02,760
There, you see, you see, it's pretty beat up. That's because we've had a few accidents here before.

33
00:04:02,760 --> 00:04:10,600
And all the nice interior fittings were not well fixed and so they got a bit damaged. 

34
00:04:10,600 --> 00:04:17,120
But we are fixing it beautifully.

35
00:04:17,120 --> 00:04:23,920
Come on, I'll show you something. Because we want to make some specialties in the corners.

36
00:04:23,920 --> 00:04:31,960
I don't know who's going to recognize this, the ones that have been to

37
00:04:31,960 --> 00:04:39,680
 the C-Base will recognize it. It's walls of light, made up of bottles of mate.

38
00:04:39,680 --> 00:04:49,760
And here we are considering, for example, to build what is called a Digital Twin of this device.

39
00:04:49,760 --> 00:04:55,120
Here are the instructions on how to build the application programming interface to know how to use it. 

40
00:04:55,120 --> 00:05:01,480
Here is this web site, you can look at it and even read it. And here's an example of how we could use this space.

41
00:05:01,480 --> 00:05:08,920
Let's put this LED screen here.

42
00:05:08,920 --> 00:05:15,120
I fly a little bit. Here, we can fly here too. I have to practice flying a little bit. 

43
00:05:15,120 --> 00:05:21,880
Wow, now I'm flying here. Look over here.  There's still a hole here, see? Oh, it's broken again. 

44
00:05:21,880 --> 00:05:26,680
Here we have to fix it a little bit more. That's another task. You see, it's like that because of the landing accidents.

45
00:05:26,680 --> 00:05:32,520
Once again here. I just passed a portal and this is our departure hall.

46
00:05:32,520 --> 00:05:42,920
Now we can go up to the zeppelin. Hey, the earth, there, it turns. Cool...

47
00:05:42,920 --> 00:05:51,920
 we can do this... Very cool. Here we see the ICCBS from the outside. Then I'm

48
00:05:51,920 --> 00:05:58,600
here, inside the zeppelin. And there is also a portal. Let's see where it leads? Let's go inside.

49
00:05:58,600 --> 00:06:08,400
Ah yes, that takes us back. Ah, that's where they changed the portals. It's good that we talked about it.

50
00:06:08,400 --> 00:06:13,720
Actually, the portal I just went through should lead here. 

51
00:06:13,720 --> 00:06:23,080
Let's have a look here. What's that? Look, a dark... Look, it's the Fairy Dust.

52
00:06:23,080 --> 00:06:33,160
Amazing! This is the first time I've seen it. Wow, it's not bad at all. In fact, 

53
00:06:33,160 --> 00:06:39,880
Are things still spinning up there? They don't turn anymore. Yes, they do, very slowly. 

54
00:06:39,880 --> 00:06:44,960
Here we see the data node in different artistic representations. And we have a campfire.

55
00:06:44,960 --> 00:06:56,560
And it's crackling. It's really welcoming here. I hope we'll meet here often.

56
00:06:56,560 --> 00:07:02,720
Well, if I walk around here, I can also go to Jev22's basement.

57
00:07:02,720 --> 00:07:13,960
And the basement looks like this. Wow, wow, wow, wow. Let's take a look here. 

58
00:07:13,960 --> 00:07:20,520
These are all the events, that is the event home pages. You can look at all of them.

59
00:07:20,520 --> 00:07:27,240
There are a lot of them. Yes, let's look here. Hacking in parallel. And when you get close to it, there's a really interactive site.

60
00:07:27,240 --> 00:07:33,000
You can click on this one and flip through it. And it works for all 38 events.

61
00:07:33,000 --> 00:07:42,640
You can look at them all and participate.

62
00:07:42,640 --> 00:07:54,120
Chili con Chaos. Yes, here you can watch Chili con Chaos. The program is very interesting. 

63
00:07:54,120 --> 00:08:01,480
You can fly into our Makerspace via Places.  

64
00:08:01,480 --> 00:08:11,680
The Makerspace is simply called XRelog22. And this makerspace is now a kind of collection of unfinished things. 

65
00:08:11,680 --> 00:08:17,120
of unfinished things. We started things there, in a scattered order. 

66
00:08:17,120 --> 00:08:25,840
for example, I have the idea, for the 15 tracks, so 15 themes, that we have. What if we made a tower of tracks.

67
00:08:25,840 --> 00:08:38,800
build 15 floors. Yes, very cool. I also want to be able to fly. Yes, flying is a good idea.

68
00:08:38,800 --> 00:08:45,840
On closer inspection, these floors look a bit like little boarding lobbies. 

69
00:08:45,840 --> 00:08:53,800
Very small and very mini. And on each of these floors, there is a dedicated theme. 

70
00:08:53,800 --> 00:09:02,200
We started up to the third floor.

71
00:09:02,200 --> 00:09:09,480
Whether it's podcasting, whether it's SocialVR, whether it's XR communities, there's a number of them and so on.

72
00:09:09,480 --> 00:09:16,680
And of course, in the first few floors, there's (H)Activism.

73
00:09:16,680 --> 00:09:28,240
That's of course our main theme.

74
00:09:28,240 --> 00:09:36,160
Wait, I'm flying there. It's written there, but the lamp is not lit.

75
00:09:36,160 --> 00:09:42,040
I moved the lamp somewhere. Now it doesn't light at all.

76
00:09:42,040 --> 00:09:48,800
In general, it lights up well here. Can we read this.
No, hectivism is in the dark. 
about hacker ethics.

77
00:09:48,800 --> 00:09:55,160
Take a look around here. This is one of the first things I did.

78
00:09:55,160 --> 00:10:02,920
I drew hacker ethics in 3D in virtual reality with a multibrush. 

79
00:10:02,920 --> 00:10:11,240
And then I integrated this painting as a 3D model here. And I even integrated a voice synthesis, in German and English.

80
00:10:11,240 --> 00:10:18,160
Well, what I found out in research on hacker ethics,

81
00:10:18,160 --> 00:10:24,600
is that the first six rules of hackers are older,

82
00:10:24,600 --> 00:10:30,440
date back to the 60's and only two rules were added by the Computer Chaos Club

83
00:10:30,440 --> 00:10:36,760
In particular, Don't litter other people's data.

84
00:10:36,760 --> 00:10:43,840
Make public data available, protect private data. I think we've heard that before. 

85
00:10:43,840 --> 00:10:50,520
These are the two complements of the Computer-Chaos-Club. We took the liberty of adding a ninth rule.

86
00:10:50,520 --> 00:11:00,720
Always think about bits and trees. Because the ecological aspect, does not appear yet in the hackers' ethics. 

87
00:11:00,720 --> 00:11:07,280
That's why it should necessarily be completed. 

88
00:11:07,280 --> 00:11:14,320
That was our hacker ethic. Otherwise you see, so it's a collection of all sorts of things.

89
00:11:14,320 --> 00:11:17,560
You see at the top, you can see the zeppelin there.

90
00:11:17,560 --> 00:11:24,880
Above the zeppelin, there is a first version of the ICCBS. 

91
00:11:24,880 --> 00:11:30,440
Here there are trays. We will use these trays when we hold workshops.

92
00:11:30,440 --> 00:11:36,280
Then we can fly to themes and hear things. 

93
00:11:36,280 --> 00:11:43,160
We tinker so simply. This is our virtual makerspace.

94
00:11:43,160 --> 00:11:52,040
It has no thematic link. 

95
00:11:52,040 --> 00:11:59,000
When we finish the Tracktower, it will be transferred to its own domain server. 

96
00:11:59,000 --> 00:12:07,320
Then we remove it from the makerspace and build something new. 

97
00:12:07,320 --> 00:12:20,400
Now I'm going back to the Intergalactic Chaos Communication Broadcast Studio. ICCBS for short. 

98
00:12:20,400 --> 00:12:31,080
Take a look. Where I'm coming from. So anyone can just change the location, who is in the system?

99
00:12:31,080 --> 00:12:37,960
Yes, everything is open. You don't even have to register. 

100
00:12:37,960 --> 00:12:44,560
You don't even need to create an account. Just download the client and launch it to enter this world.

101
00:12:44,560 --> 00:12:49,320
You will receive a short explanation on how it works.

102
00:12:49,320 --> 00:12:55,960
Then you can already use the Places function. Now you can discover even more worlds.

103
00:12:55,960 --> 00:13:07,080
So ICCBS and Fairy Dust and XRelog or later also Tracktower.

104
00:13:07,080 --> 00:13:14,040
These are only three worlds out of dozens of worlds.

105
00:13:14,040 --> 00:13:18,240
There will probably be hundreds of them soon that we can visit.

106
00:13:18,240 --> 00:13:27,520
Of course, there are also worlds that are not open to the public, such as a Darling VR. 

107
00:13:27,520 --> 00:13:32,520
It's a bit more sexually colored. We don't want just anyone to be able to get in. 

108
00:13:32,520 --> 00:13:39,280
But otherwise, most worlds are open to the public, 

109
00:13:39,280 --> 00:13:49,200
freely, without bullshit, without NFT, smart contract, blockchain, crypto-truc. 

110
00:13:49,200 --> 00:13:52,560
All of this is not found here. It's so beautiful.

111
00:13:52,560 --> 00:14:09,880
Anchoray, you have shown us so many things of this world. 

112
00:14:09,880 --> 00:14:12,960
But what do you intend to do on Congress? You didn't say anything about it.

113
00:14:12,960 --> 00:14:25,080
Yes, that's right. We have 15 sessions, and 15 topics.

114
00:14:25,080 --> 00:14:36,440
and we're also looking at the other 38 JEV22 events. Our main theme is the critique of technology. 

115
00:14:36,440 --> 00:14:42,240
Clear criticism of technology. We don't want to let Facebook off the hook.

116
00:14:42,240 --> 00:14:48,800
We want to talk about Web 3.0. We want to look critically at how this is developing.

117
00:14:48,800 --> 00:14:53,520
We have prepared some very interesting sessions for you.

118
00:14:53,520 --> 00:14:58,280
Then we want to look further than the end of our nose and also look at other JEV events.

119
00:14:58,280 --> 00:15:04,920
There are some absolutely fantastic initiatives, from New York to Athens. 

120
00:15:04,920 --> 00:15:12,560
In Munich there are two, in Hamburg there are four in total. In Berlin, in Erlangen, in Düsseldorf, in Aachen.

121
00:15:12,560 --> 00:15:15,800
There is a lot to discover.

122
00:15:15,800 --> 00:15:17,280
But, will it be as much fun?

123
00:15:17,280 --> 00:15:23,640
Oh, fun! Yes, of course. It's true that we don't have Jeopardy,

124
00:15:23,640 --> 00:15:29,080
we haven't been able to prepare any more nice questions for it. 

125
00:15:29,080 --> 00:15:38,400
But we're trying to do something this crazy. We're going to call it the Mafifa World Cup 2030. We're going to replay the 2030 World Cup draw.

126
00:15:38,400 --> 00:15:44,920
Let yourself be surprised. Each time at 11 p.m. on Wednesday, Thursday and Friday.

127
00:15:44,920 --> 00:15:47,400
This is going to be interesting.

128
00:15:47,400 --> 00:15:50,600
You have to do this in a very corrupt way.

129
00:15:50,600 --> 00:15:57,840
Does mate count as currency?

130
00:15:57,840 --> 00:16:05,720
If it doesn't count as currency, then we're not corrupt either. 

131
00:16:05,720 --> 00:16:12,880
What's important is that with us, there are very, very few frontal presentations.

132
00:16:12,880 --> 00:16:19,640
With us, almost everything is a participatory workshop, designed like a Barcamp. 

133
00:16:19,640 --> 00:16:28,080
There's an XR barcamp, right after this opening. But there's also a Bits and Trees.

134
00:16:28,080 --> 00:16:34,200
Fireside Chat, where you are cordially invited to participate.

135
00:16:34,200 --> 00:16:39,760
Also Overte's workshops, where we explore this world, 

136
00:16:39,760 --> 00:16:41,080
are also designed to be workshops, not lectures.

137
00:16:41,080 --> 00:16:50,760
You want to say again how we access the world? 

138
00:16:50,760 --> 00:16:57,800
Because we see this really great world, but how do we get into it. 

139
00:16:57,800 --> 00:16:58,800
Can we visit you?

140
00:16:58,800 --> 00:17:04,520
Yes, of course. So the main thing is that we're in the Matrix. 

141
00:17:04,520 --> 00:17:11,960
In the schedule on Pretalx, a Matrix channel is indicated for each presentation. 

142
00:17:11,960 --> 00:17:16,760
This is where we meet and also exchange all information. 

143
00:17:16,760 --> 00:17:29,820
The other thing is this Metaverse, this Multiverse Overte.

144
00:17:29,820 --> 00:17:35,520
Overte.org, this is already the home page.
 You can download a client there.

145
00:17:35,520 --> 00:17:42,880
It exists for Linux and for Windows. Curiously, it is not available for Apple. 

146
00:17:42,880 --> 00:17:51,840
You can also use it to visit us at ICCBS, in the Fairy Dust forest. 

147
00:17:51,840 --> 00:17:57,160
or in a virtual hackerspace on the xrelog with the Track Tower. 

148
00:17:57,160 --> 00:17:58,160
You can explore all this yourself.

149
00:17:58,160 --> 00:18:08,760
That sounds exciting and I think, 

150
00:18:08,760 --> 00:18:11,600
some listeners should also find it really exciting to meet you in 3D space.

151
00:18:11,600 --> 00:18:17,640
I'm curious to know. How many viewers will the servers crash? 

152
00:18:17,640 --> 00:18:19,040
we'll find out together.

153
00:18:19,040 --> 00:18:20,040
Testing, testing.

154
00:18:20,040 --> 00:18:26,200
We can also provision other servers. But we are not yet automated. 

155
00:18:26,200 --> 00:18:32,280
provision other servers. But we're not very automated yet. 

154
00:18:20,040 --> 00:18:26,200
It takes a little time, but we are ready. 

157
00:18:36,840 --> 00:18:38,080
If we fall now, we can still do something.

158
00:18:38,080 --> 00:18:45,520
Finally, I would like to say a very warm goodbye to you. 

159
00:18:45,520 --> 00:18:51,240
By the way also from, uh, uh, who are you exactly?

160
00:18:51,240 --> 00:18:56,560
Sorry Anchoray, I can't tell you.

161
00:18:56,560 --> 00:19:25,560
So we'll have to solve this riddle next time. See you later. Bye.