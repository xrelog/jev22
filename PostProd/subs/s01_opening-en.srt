1
00:00:30,000 --> 00:00:41,000
Welcome to the opening of XRELOG22!

2
00:00:41,000 --> 00:00:47,000
Hello Anchoray and hello world out there!

3
00:00:47,000 --> 00:00:56,000
Today we want to introduce you to what XRELOG22 is preparing for you this year.

4
00:00:56,000 --> 00:01:03,000
What is XRELOG22 in general? What are the goals?

5
00:01:03,000 --> 00:01:11,000
We want to take care of SocialVR, and that is SocialVR without Bullshit.

6
00:01:11,000 --> 00:01:18,000
We poured that into our motto. We call it "Visions for Independent Multiverses".

7
00:01:18,000 --> 00:01:27,000
I heard you also want to bring the hacker ethics into the meta and multiverse, the infinite widths of the meta and multiverse.

8
00:01:27,000 --> 00:01:31,000
Absolutely! There is simply too little of that in the metaverse.

9
00:01:31,000 --> 00:01:38,000
Tell me what I just noticed. I don't see a sender logo at all. Do you actually have a sender logo?

10
00:01:38,000 --> 00:01:40,000
Sender logo?

11
00:01:40,000 --> 00:01:50,000
What was that? What kind of crap was that?

12
00:01:50,000 --> 00:01:57,000
I don't know either. Unfortunately, I don't think the tesa-crep held up that well.

13
00:01:57,000 --> 00:02:12,000
We are not the great super video OBS super perfect people, but there is already a sender logo in the bud.

14
00:02:12,000 --> 00:02:18,000
The tesa was just bad. It's just bad quality.

15
00:02:18,000 --> 00:02:28,000
I think we have to adjust the expectation management a bit. People don't think they're getting super perfection.

16
00:02:28,000 --> 00:02:31,000
Nobody wants ARD and ZDF.

17
00:02:31,000 --> 00:02:36,000
But as it should be. A lot is unfinished for us, but sexy.

18
00:02:36,000 --> 00:02:45,000
That's not unfinished, that's in production. That's agile. It's an agile development.

19
00:02:45,000 --> 00:02:48,000
You mean work in progress is the new normal?

20
00:02:48,000 --> 00:02:50,000
Of course.

21
00:02:50,000 --> 00:02:54,000
So be careful of many flipping...

22
00:02:54,000 --> 00:03:02,000
We say it again. Be careful of many flipping mate bottles and stuff. But we're going to have fun.

23
00:03:02,000 --> 00:03:05,000
We'll stick to the tesa.

24
00:03:05,000 --> 00:03:20,000
Exactly.

25
00:03:20,000 --> 00:03:25,000
But it's really pronounced XR Relog 22.

26
00:03:25,000 --> 00:03:35,000
And that's derived from XR Labs Operation Group. I'm standing here in the icc|bs. Look here.

27
00:03:35,000 --> 00:03:40,000
We're building virtual worlds.

28
00:03:40,000 --> 00:03:50,000
One of them, a vehicle to travel through the virtual worlds, is our icc|bs Intergalactical Chaos Communication Broadcast Studio.

29
00:03:50,000 --> 00:03:55,000
And that's hosting this event in Overte.

30
00:03:55,000 --> 00:04:00,000
You see, it's pretty broken.

31
00:04:00,000 --> 00:04:05,000
That's because we've already had a few crashes on the package.

32
00:04:05,000 --> 00:04:16,000
And the whole beautiful interior was not properly adjusted and therefore a bit broken.

33
00:04:16,000 --> 00:04:18,000
But we're building it up again beautifully.

34
00:04:18,000 --> 00:04:21,000
Come with me, I'll show you something.

35
00:04:21,000 --> 00:04:27,000
We want to put a few specialties together in the corners.

36
00:04:27,000 --> 00:04:31,000
I don't know who recognizes that.

37
00:04:31,000 --> 00:04:34,000
Who has been to the C-Base will recognize that.

38
00:04:34,000 --> 00:04:42,000
This is a light wall consisting of mate bottles.

39
00:04:42,000 --> 00:04:50,000
And there is, for example, a consideration that we build a so-called digital twin of this device.

40
00:04:50,000 --> 00:04:54,000
Here, by the way, is the handicraft manual for API, how to use it.

41
00:04:54,000 --> 00:04:59,000
This is this website here, you can look at it and read it.

42
00:04:59,000 --> 00:05:06,000
And that's an idea, for example, how we can play this room.

43
00:05:06,000 --> 00:05:09,000
That we build this LED wall here.

44
00:05:09,000 --> 00:05:12,000
Let's fly, wait a minute, flying is also possible.

45
00:05:12,000 --> 00:05:14,000
I have to practice a bit, fly.

46
00:05:14,000 --> 00:05:18,000
Oh, now I'm flying around here, look at this.

47
00:05:18,000 --> 00:05:22,000
There is still a hole here, you see, oh, that's still broken.

48
00:05:22,000 --> 00:05:24,000
Here we still have to fix a bit, that's another task.

49
00:05:24,000 --> 00:05:28,000
You see, that's like the crash landing.

50
00:05:28,000 --> 00:05:34,000
I just went through a portal and here is our departure hall.

51
00:05:34,000 --> 00:05:40,000
You can go up to the Zeppelin here.

52
00:05:40,000 --> 00:05:43,000
Say, the earth there, it's spinning, right?

53
00:05:43,000 --> 00:05:45,000
Cool, yes, that's very cool.

54
00:05:45,000 --> 00:05:49,000
There you can see a bit of the icc|bs from the outside.

55
00:05:49,000 --> 00:05:53,000
Yes, so, and then I'm in the Zeppelin here.

56
00:05:53,000 --> 00:05:57,000
So, and there is also a portal.

57
00:05:57,000 --> 00:05:58,000
Look, where does that lead?

58
00:05:58,000 --> 00:05:59,000
Let's go in.

59
00:05:59,000 --> 00:06:03,000
Oops, ah, so, that leads back again.

60
00:06:03,000 --> 00:06:06,000
Ah, they changed the portals.

61
00:06:06,000 --> 00:06:09,000
Well, good that we talked about it.

62
00:06:09,000 --> 00:06:13,000
Actually, the portal where I just went through should lead here.

63
00:06:13,000 --> 00:06:16,000
Look here, oh, what is that here?

64
00:06:16,000 --> 00:06:19,000
Look, a dark...

65
00:06:19,000 --> 00:06:23,000
Oh, look there, there is the "Fairy Dust".

66
00:06:23,000 --> 00:06:26,000
Unbelievable, I haven't seen that yet.

67
00:06:26,000 --> 00:06:31,000
Wow, that's not bad.

68
00:06:31,000 --> 00:06:34,000
Do the things up here actually still turn?

69
00:06:34,000 --> 00:06:36,000
They don't turn anymore.

70
00:06:36,000 --> 00:06:38,000
Yes, they turn very slowly.

71
00:06:38,000 --> 00:06:42,000
Here you can see the "Datenknoten" in various artistic expressions.

72
00:06:42,000 --> 00:06:45,000
Oh, and we have a campfire.

73
00:06:45,000 --> 00:06:49,000
And it's crackling, it's crackling.

74
00:06:49,000 --> 00:06:51,000
Oh, it's really cozy here.

75
00:06:51,000 --> 00:06:53,000
I hope that we...

76
00:06:53,000 --> 00:06:54,000
It's really great.

77
00:06:54,000 --> 00:06:57,000
... we'll see each other again here often.

78
00:06:57,000 --> 00:07:04,000
Well, if I walk around here like this, then I can go into the basement of JEV22 again.

79
00:07:04,000 --> 00:07:07,000
And the basement looks like this.

80
00:07:07,000 --> 00:07:10,000
Wow, wow, wow, wow, wow.

81
00:07:10,000 --> 00:07:16,000
Look here, these are all events, so the starting pages of the events.

82
00:07:16,000 --> 00:07:19,000
You can go and look at all of them now.

83
00:07:19,000 --> 00:07:21,000
There are some of them.

84
00:07:21,000 --> 00:07:24,000
Yes, look here, Hacking in Parallel.

85
00:07:24,000 --> 00:07:29,000
And if you get closer, you'll get a really interactive website.

86
00:07:29,000 --> 00:07:31,000
You can click on it and read it through.

87
00:07:31,000 --> 00:07:34,000
And that goes with all...

88
00:07:34,000 --> 00:07:38,000
In total there are 38 events.

89
00:07:38,000 --> 00:07:41,000
You can now look at all of them and join in.

90
00:07:41,000 --> 00:07:44,000
Chili con Chaos.

91
00:07:44,000 --> 00:07:48,000
Zoom, you can look at Chili con Chaos.

92
00:07:48,000 --> 00:07:51,000
Ah, the program is very interesting.

93
00:07:51,000 --> 00:08:00,000
You can fly over places here beautifully into our makerspace.

94
00:08:00,000 --> 00:08:06,000
The makerspace is called xrelog.

95
00:08:06,000 --> 00:08:13,000
And this makerspace is now a collection of unfinished things.

96
00:08:13,000 --> 00:08:16,000
So we just started crosswise and crosswise.

97
00:08:16,000 --> 00:08:23,000
I had the vision here, for example, we have 15 tracks, so 15 theme areas.

98
00:08:23,000 --> 00:08:28,000
Let's build a track tower with 15 floors.

99
00:08:28,000 --> 00:08:31,000
Yes.

100
00:08:31,000 --> 00:08:33,000
Very cool.

101
00:08:33,000 --> 00:08:37,000
I want to be able to fly there too and not run.

102
00:08:37,000 --> 00:08:40,000
Yes, but flying is not such a bad idea.

103
00:08:40,000 --> 00:08:48,000
If you look closely, these rooms look a bit like little mini departure halls.

104
00:08:48,000 --> 00:08:51,000
So very small and very mini.

105
00:08:51,000 --> 00:08:59,000
And in each of these rooms, we have only started on the third floor, there is a theme.

106
00:08:59,000 --> 00:09:03,000
And we want to devote ourselves to this topic.

107
00:09:03,000 --> 00:09:12,000
Whether it's podcasting, whether it's social VR, whether it's the XR communities, there are some or, or, or.

108
00:09:12,000 --> 00:09:19,000
And of course one of the first floors, the first floor is hacktivism.

109
00:09:19,000 --> 00:09:22,000
That is of course our main topic.

110
00:09:22,000 --> 00:09:24,000
Oops.

111
00:09:24,000 --> 00:09:27,000
Okay, hacktivism is our main topic.

112
00:09:27,000 --> 00:09:30,000
And wait, I'll fly over there again.

113
00:09:30,000 --> 00:09:37,000
That's so to speak, it's written in letters.

114
00:09:37,000 --> 00:09:39,000
Ah, the lamp is not on.

115
00:09:39,000 --> 00:09:41,000
Ah, I moved the lamp somewhere.

116
00:09:41,000 --> 00:09:43,000
Now it doesn't even light up here.

117
00:09:43,000 --> 00:09:45,000
Actually, it lights up very nicely, you can stick it there.

118
00:09:45,000 --> 00:09:48,000
No, the hacktivism is in the dark.

119
00:09:48,000 --> 00:09:52,000
Oh, speaking of hacker ethics, look here.

120
00:09:52,000 --> 00:09:55,000
This is one of the first things I did.

121
00:09:55,000 --> 00:10:01,000
I painted the hacktivism in 3D in virtual reality with Multibrush.

122
00:10:01,000 --> 00:10:07,000
And I then built this painting here as a 3D model.

123
00:10:07,000 --> 00:10:15,000
And not only that, I even built in a language edition in German and English.

124
00:10:15,000 --> 00:10:16,000
Number one.

125
00:10:16,000 --> 00:10:24,000
Well, what I found out during the hacktivism research is that the first six hacker rules are actually older.

126
00:10:24,000 --> 00:10:26,000
They date back to the 1960s.

127
00:10:26,000 --> 00:10:31,000
And that only two rules have been added by Computer Chaos Club.

128
00:10:31,000 --> 00:10:34,000
Namely, do not litter in the data of others.

129
00:10:34,000 --> 00:10:37,000
And the next one is very famous, I think.

130
00:10:37,000 --> 00:10:41,000
Use public data, protect private data.

131
00:10:41,000 --> 00:10:43,000
I think you've heard that before.

132
00:10:43,000 --> 00:10:46,000
And these are two additions from Computer Chaos Club.

133
00:10:46,000 --> 00:10:54,000
And we were so free and added a ninth addition.

134
00:10:54,000 --> 00:10:57,000
Always think of "bits & bäume" (trees).

135
00:10:57,000 --> 00:11:07,000
Because the ecological aspect is due to the fact that they are so old, not yet represented in the hacker ethics.

136
00:11:07,000 --> 00:11:10,000
That's why you would have to add that.

137
00:11:10,000 --> 00:11:12,000
That's the hacker ethics.

138
00:11:12,000 --> 00:11:16,000
And as you can see, it's a collection of all kinds of things.

139
00:11:16,000 --> 00:11:19,000
You see above, we can look around, you can see the Zeppelin.

140
00:11:19,000 --> 00:11:24,000
And above the Zeppelin is already a first version of the icc|bs.

141
00:11:24,000 --> 00:11:27,000
And here are so-called plateaus.

142
00:11:27,000 --> 00:11:32,000
We will use these plateaus again when we hold workshops here.

143
00:11:32,000 --> 00:11:38,000
Then you can fly to these topics and hear things.

144
00:11:38,000 --> 00:11:41,000
And yes, we just do our work here.

145
00:11:41,000 --> 00:11:44,000
This is our virtual makerspace.

146
00:11:44,000 --> 00:11:51,000
And it doesn't have a thematic connection yet.

147
00:11:51,000 --> 00:11:58,000
Because when we have the track tower ready, it will move to its own domain server.

148
00:11:58,000 --> 00:12:02,000
Then we throw it out of the makerspace again and build something new.

149
00:12:02,000 --> 00:12:05,000
Very cool.

150
00:12:05,000 --> 00:12:13,000
Now I'm flying back to the International Chaos Communication Broadcast Studio.

151
00:12:13,000 --> 00:12:16,000
Short icc|bs.

152
00:12:16,000 --> 00:12:19,000
icc|bs.

153
00:12:19,000 --> 00:12:22,000
Let's see how I get there.

154
00:12:22,000 --> 00:12:31,000
And so can everyone change who is in the system?

155
00:12:31,000 --> 00:12:32,000
Yes, yes, yes.

156
00:12:32,000 --> 00:12:33,000
It's all open.

157
00:12:33,000 --> 00:12:37,000
You don't even have to register.

158
00:12:37,000 --> 00:12:39,000
So you don't even have to create an account.

159
00:12:39,000 --> 00:12:45,000
You can just download the client, start it and then you're in the world.

160
00:12:45,000 --> 00:12:48,000
Then you get a little explanation of how everything works.

161
00:12:48,000 --> 00:12:54,000
And then you can use this places function.

162
00:12:54,000 --> 00:12:57,000
And then you can... there are many more worlds.

163
00:12:57,000 --> 00:13:05,000
So this icc|bs and the Fairy Dust and the XR Relog.

164
00:13:05,000 --> 00:13:08,000
Or later the Track Tower 15.

165
00:13:08,000 --> 00:13:12,000
These are now only three worlds of hundreds of worlds.

166
00:13:12,000 --> 00:13:14,000
Or let's say dozens of worlds.

167
00:13:14,000 --> 00:13:16,000
There will probably be hundreds soon.

168
00:13:16,000 --> 00:13:18,000
Which you can visit directly openly.

169
00:13:18,000 --> 00:13:20,000
Of course there are also worlds that are not open.

170
00:13:20,000 --> 00:13:27,000
Because... for example a DarlingVR.

171
00:13:27,000 --> 00:13:30,000
That's a bit more colorful from a sexual point of view.

172
00:13:30,000 --> 00:13:35,000
But you don't necessarily want everyone to come in directly.

173
00:13:35,000 --> 00:13:38,000
But otherwise most worlds are open.

174
00:13:38,000 --> 00:13:48,000
Accessible, free, open, without bullshit, without NFT, smart contract, blockchain, crypto stuff.

175
00:13:48,000 --> 00:13:50,000
Everything is not to be found here.

176
00:13:50,000 --> 00:13:52,000
That's not here.

177
00:13:52,000 --> 00:13:54,000
That's so beautiful.

178
00:14:04,000 --> 00:14:08,000
So, Anchoray, you have now shown so much about the world.

179
00:14:08,000 --> 00:14:11,000
But what are you actually planning to do with Conquest?

180
00:14:11,000 --> 00:14:13,000
Somehow you haven't said anything about that yet.

181
00:14:13,000 --> 00:14:16,000
That's right. I haven't said anything yet.

182
00:14:16,000 --> 00:14:20,000
So we have 15 sessions.

183
00:14:20,000 --> 00:14:23,000
We have 15 theme tracks.

184
00:14:23,000 --> 00:14:31,000
And we also look at over 35 other JEV22 events.

185
00:14:31,000 --> 00:14:36,000
And our focus on this is technical criticism.

186
00:14:36,000 --> 00:14:38,000
Clearly and clearly technical criticism.

187
00:14:38,000 --> 00:14:41,000
We don't want to let Facebook get away with it.

188
00:14:41,000 --> 00:14:44,000
We want to let ourselves go over Web 3.0.

189
00:14:44,000 --> 00:14:48,000
We really want to see how all this develops critically.

190
00:14:48,000 --> 00:14:52,000
And we have prepared some very interesting sessions for you.

191
00:14:52,000 --> 00:14:56,000
And as I said, we also want to look beyond the edge of the plate.

192
00:14:56,000 --> 00:14:59,000
And also deal with the other JEV events.

193
00:14:59,000 --> 00:15:07,000
There are fantastic initiatives from New York, Athens, Munich, Hamburg, four in total.

194
00:15:07,000 --> 00:15:15,000
In Berlin, in Erlangen, in Düsseldorf, in Aachen. There is a lot to discover.

195
00:15:15,000 --> 00:15:18,000
But is there also a little fun?

196
00:15:18,000 --> 00:15:20,000
Oh, fun.

197
00:15:20,000 --> 00:15:22,000
Yes, of course. We don't have a Jeopadry.

198
00:15:22,000 --> 00:15:25,000
We didn't manage to put together nice questions.

199
00:15:25,000 --> 00:15:29,000
But we try to do something similar.

200
00:15:29,000 --> 00:15:32,000
We call it FIFA World Cup 2030.

201
00:15:32,000 --> 00:15:39,000
We are playing the World Cup or the draw for the World Cup 2030.

202
00:15:39,000 --> 00:15:41,000
Let yourself be surprised.

203
00:15:41,000 --> 00:15:45,000
At 23 o'clock on Wednesday, Thursday and Friday.

204
00:15:45,000 --> 00:15:48,000
Will be interesting.

205
00:15:48,000 --> 00:15:51,000
You're probably doing it very corrupt.

206
00:15:51,000 --> 00:15:54,000
You've probably already made everything.

207
00:15:54,000 --> 00:15:58,000
But does Mate count as currency?

208
00:15:58,000 --> 00:16:04,000
If that doesn't count as currency, then we're not corrupt either.

209
00:16:04,000 --> 00:16:08,000
But in the end, what is important to know?

210
00:16:08,000 --> 00:16:13,000
We actually only have very, very few front presentations.

211
00:16:13,000 --> 00:16:18,000
With us, almost everything is designed like a participation workshop, like a barcamp.

212
00:16:18,000 --> 00:16:24,000
Express advertising is available in XR Barcamp, right after this opening.

213
00:16:24,000 --> 00:16:32,000
But there is also a "Bits & Bäume" fireside chat, where you are also cordially invited to participate.

214
00:16:32,000 --> 00:16:41,000
And the workshops by Overte, where we explore these worlds, are also designed to be workshops and not lectures.

215
00:16:44,000 --> 00:16:49,000
Would you like to say how to get to the world?

216
00:16:49,000 --> 00:16:57,000
Because at the moment we see the really cool world, but how to get there?

217
00:16:57,000 --> 00:16:59,000
Can you visit you there?

218
00:16:59,000 --> 00:17:00,000
Yes, of course.

219
00:17:00,000 --> 00:17:03,000
The most important thing for us is in the matrix.

220
00:17:03,000 --> 00:17:11,000
If you look at the schedule in the pretalx, then a matrix channel is selected for each lecture.

221
00:17:11,000 --> 00:17:15,000
That's where we meet and that's where we exchange all the information.

222
00:17:15,000 --> 00:17:18,000
That's one thing that's really interesting.

223
00:17:18,000 --> 00:17:24,000
The other thing is this metaverse, this multiverse, Overte.

224
00:17:24,000 --> 00:17:31,000
Overte.org, that's the homepage.

225
00:17:31,000 --> 00:17:35,000
There you can download a client.

226
00:17:35,000 --> 00:17:38,000
It is available for Linux and Windows.

227
00:17:38,000 --> 00:17:41,000
Interestingly, not for Apple.

228
00:17:41,000 --> 00:17:55,000
And with that you can also visit us here in the icc|bs, in the Fairy Dust forest or in our virtual hackerspace on the xrelog with the Track Tower.

229
00:17:55,000 --> 00:17:58,000
You can explore all of this yourself.

230
00:17:58,000 --> 00:18:11,000
That sounds exciting in any case and I think some listeners will find it really exciting to meet you in 3D.

231
00:18:11,000 --> 00:18:13,000
I'm curious.

232
00:18:13,000 --> 00:18:19,000
From how many viewers the servers break down, we have to find out together.

233
00:18:19,000 --> 00:18:21,000
Test, test.

234
00:18:21,000 --> 00:18:23,000
Yes, let's see.

235
00:18:23,000 --> 00:18:25,000
We have prepared a bit for Hetzner.

236
00:18:25,000 --> 00:18:29,000
We can also provide more servers.

237
00:18:29,000 --> 00:18:32,000
However, they are not yet super automated.

238
00:18:32,000 --> 00:18:35,000
It takes a while, but we will be prepared.

239
00:18:35,000 --> 00:18:39,000
If we go completely down now, we can still drive up a bit.

240
00:18:39,000 --> 00:18:45,000
And finally, I would like to say goodbye to you.

241
00:18:45,000 --> 00:18:48,000
Also by the way from...

242
00:18:48,000 --> 00:18:52,000
Who are you actually?

243
00:18:52,000 --> 00:18:56,000
Sorry Anchoray, I can't tell you that.

244
00:18:56,000 --> 00:19:04,000
Oh, then we'll have to solve this riddle next time.

245
00:19:04,000 --> 00:19:06,000
See you then. Bye.

246
00:19:06,000 --> 00:19:25,000
Bye.

