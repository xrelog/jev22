1
00:00:00,000 --> 00:00:07,000
The following talk is called, Is the Metaverse Meta-Worse? by Martin Swimmer.

2
00:00:07,000 --> 00:00:13,000
Martin is a virus expert. He has been involved with viruses for decades.

3
00:00:13,000 --> 00:00:18,000
But not the Corona kind of viruses. His focus is on digital malware.

4
00:00:18,000 --> 00:00:25,000
In this talk, we will hear about the dangers for digital avatars and software-based lifeforms in the metaverse.

5
00:00:25,000 --> 00:00:32,000
To learn more about potential undesired digital complications, listen to Martin Swimmer.

6
00:00:32,000 --> 00:00:41,000
Hi, I'm Martin Swimmer and the question we want to answer today is, Is the Metaverse Meta-Worse?

7
00:00:41,000 --> 00:00:45,000
But first about our research group.

8
00:00:45,000 --> 00:00:51,000
So we're part of TrendMarker Research and some of you may know the Zero Day initiative.

9
00:00:51,000 --> 00:00:58,000
If so, you might know them through the Pwn2Own event that actually just happened recently up in Toronto.

10
00:00:58,000 --> 00:01:03,000
We are a slightly different branch of it called the Forward-Looking Threat Research Team.

11
00:01:03,000 --> 00:01:05,000
And we're kind of scouts for the company.

12
00:01:05,000 --> 00:01:14,000
We're always looking into like the latest threats that are coming around the bend in a couple of years' time.

13
00:01:14,000 --> 00:01:26,000
In particular, in the areas of e-crime, so threat actors, campaigns at the underground, as well as technology that will affect and change how security has to be dealt with.

14
00:01:26,000 --> 00:01:33,000
Anything from 5G to quantum computing is game there, as well as social aspects.

15
00:01:33,000 --> 00:01:43,000
So that may affect security, you know, fake news and metaverse, which is what we're going to talk about today, is what we deal with.

16
00:01:43,000 --> 00:01:48,000
And actually, the topic of metaverse isn't really new to me personally.

17
00:01:48,000 --> 00:01:55,000
I've been looking at this since basically 2005 when I was still in a different company.

18
00:01:55,000 --> 00:02:10,000
And back then we were looking into the way something like Second Life could be an interesting platform for studying this at that time emerging idea of social networking, social media.

19
00:02:10,000 --> 00:02:16,000
There I met people like Annie Ok, who did a lot of kinetic art like this.

20
00:02:16,000 --> 00:02:19,000
There were a lot of things going on there.

21
00:02:19,000 --> 00:02:25,000
In fact, I was very impressed by how much of the creative crowd Second Life attracted.

22
00:02:25,000 --> 00:02:30,000
This is, for instance, a kinema from Robbie Dingo.

23
00:02:30,000 --> 00:02:48,000
And I think this played very much into this idea that the Web 2.0 movement, which was only getting started around the time the term was coined by, well, actually, I don't even know who it was coined by, but it was popularized by Tim O'Reilly.

24
00:02:48,000 --> 00:02:56,000
And the idea there really was that we as users should become contributors and not just consumers.

25
00:02:56,000 --> 00:03:04,000
And this was, I guess, a very useful and very early platform to explore a lot of those ideas.

26
00:03:04,000 --> 00:03:07,000
But we still see a lot of creativity nowadays.

27
00:03:07,000 --> 00:03:17,000
If you have an Oculus Quest 2, you might have tried to tilt a brush from here, from Google.

28
00:03:17,000 --> 00:03:19,000
And I think there's a lot of possibilities out there.

29
00:03:19,000 --> 00:03:30,000
So, like the Unreal Engine that was released last year is able to produce a lot of detail and a lot of realism that goes into it.

30
00:03:30,000 --> 00:03:34,000
It's really, really impressive what they can do there.

31
00:03:34,000 --> 00:03:40,000
But the metaverse has been, of course, of great interest to the marketing crowd.

32
00:03:40,000 --> 00:03:48,000
And this stretches way back into Second Lifetimes, but is just as current now as well.

33
00:03:48,000 --> 00:03:57,000
So, companies might have their storefronts and their branding up in such a metaverse.

34
00:03:57,000 --> 00:04:05,000
Or it might go a step further, like in this case, where you can look at the contents of what you're trying to buy.

35
00:04:05,000 --> 00:04:07,000
You can learn more about it.

36
00:04:07,000 --> 00:04:16,000
You might be able to interact with somebody who gives you suggestions, maybe a vine that pairs to it, that sort of thing.

37
00:04:16,000 --> 00:04:23,000
And then you put it into your virtual basket, and then you can either pick it up or have it delivered later.

38
00:04:23,000 --> 00:04:27,000
But, of course, there's also professional use of this technology.

39
00:04:27,000 --> 00:04:35,000
The Port of Hamburg, for instance, has been experimenting with ways of visualizing the digital twin of the harbor.

40
00:04:35,000 --> 00:04:50,000
And this might lead to more interesting and creative ways of planning or monitoring or just promoting, perhaps, even the port.

41
00:04:50,000 --> 00:04:58,000
We and our team tried to use Spatial for a couple of our meetings.

42
00:04:58,000 --> 00:05:05,000
Here you can see us trying to operate a slide deck or at least look at our notes together.

43
00:05:05,000 --> 00:05:09,000
And the idea is a little bit less that we're trying to replace office work.

44
00:05:09,000 --> 00:05:17,000
We actually all work remote, so we have to meet separately anyhow.

45
00:05:17,000 --> 00:05:27,000
But the point really is that if we have to meet remotely, that we have this feeling of being closer to each other.

46
00:05:27,000 --> 00:05:29,000
So we have this shared experience.

47
00:05:29,000 --> 00:05:31,000
Now, I kind of wish we had legs.

48
00:05:31,000 --> 00:05:36,000
I'm not sure why modern virtual spaces seem to have problems with legs.

49
00:05:36,000 --> 00:05:48,000
But the real problem in the end was that the headsets that we were wearing, the Oculus Quest 2 in this case, were maybe a little bit uncomfortable after like an hour.

50
00:05:48,000 --> 00:05:52,000
And some people also kind of had the feeling of dizziness after a while.

51
00:05:52,000 --> 00:05:55,000
So it's not really quite ideal.

52
00:05:55,000 --> 00:06:00,000
Perhaps better in some environments would be things like Gather Town or Work Adventure.

53
00:06:00,000 --> 00:06:11,000
I find these 2D spaces maybe a little bit more easy to use alongside your actual work.

54
00:06:11,000 --> 00:06:19,000
They're not as immersive, obviously, but that maybe is an advantage if you're trying to use it as a work tool.

55
00:06:19,000 --> 00:06:22,000
But OK, back to the original question, what is a metaverse?

56
00:06:22,000 --> 00:06:29,000
Let me just quote probably the best definition that we've seen externally.

57
00:06:29,000 --> 00:06:49,000
So Accenture defines the metaverse as the evolution of the Internet that enables users to move beyond browsing to inhabiting in a persistent shared experience that spans the spectrum of our real world to the fully virtual and in-between.

58
00:06:49,000 --> 00:06:58,000
It is a place to go where people can meet and interact and where digital assets can be created, bought and sold.

59
00:06:58,000 --> 00:07:06,000
So we kind of simplified this to saying, OK, it's a new interactive application layer or Internet of Experiences.

60
00:07:06,000 --> 00:07:20,000
And I think the important distinction between the metaverse and individual metaverse spaces is that the metaverse will allow us to seamlessly go from one to another.

61
00:07:20,000 --> 00:07:25,000
But we do need to talk a little about user interfaces, so hardware.

62
00:07:25,000 --> 00:07:32,000
So there's a full spectrum there from more immersive to less immersive, to more intrusive and less intrusive.

63
00:07:32,000 --> 00:07:48,000
And let's start with the virtual reality glasses. We have, you know, ignoring wild ideas like Gibson's neurological interfaces to what he called the matrix that we now call the metaverse.

64
00:07:48,000 --> 00:08:00,000
At the moment, the current state of the art is we have virtual reality glasses that allow for pretty good immersion into any virtual world like this.

65
00:08:00,000 --> 00:08:09,000
But the next step along the line would be augmented reality.

66
00:08:09,000 --> 00:08:14,000
For instance, we have experimented with the Microsoft HoloLens, which is already pretty impressive.

67
00:08:14,000 --> 00:08:21,000
It kind of takes the outside world and adds a layer of inside, you know, of virtual to it.

68
00:08:21,000 --> 00:08:30,000
We also a couple of years ago had looked at Google Glass, which is more like a heads up display in some ways, although it does do some augmentation too, of course.

69
00:08:30,000 --> 00:08:36,000
And more accessible right now because augmented reality is pretty hard to get right.

70
00:08:36,000 --> 00:08:41,000
And maybe we won't see this for a long time, you know, in a practical and affordable sense.

71
00:08:41,000 --> 00:08:47,000
But what we can already do now is take our tablets or our phones and use those as our augmented reality devices.

72
00:08:47,000 --> 00:09:01,000
And that is real. But to really be inclusive and less intrusive, even if it saves a lot in immersion, is we have to be able to also boil this down to like smaller devices like watches, for instance.

73
00:09:01,000 --> 00:09:07,000
There's a little screen on a smartwatch. Why can't we find a way of interacting with the metaverse in that way?

74
00:09:07,000 --> 00:09:11,000
Maybe it'll be a different form of interaction. Who knows?

75
00:09:11,000 --> 00:09:23,000
And even spatial sound. The spatial sound gives you the opportunity to put voices in certain areas around you.

76
00:09:23,000 --> 00:09:27,000
And that could also lend itself to some form of virtual reality.

77
00:09:27,000 --> 00:09:42,000
The trick will be to be able to span this entire space between immersive and non-immersive and still make it feel natural to be able to move from one to the other without really skipping a beat.

78
00:09:42,000 --> 00:09:49,000
And that's hard. And I don't think we'll see this anytime soon. But that is the vision that we have as we see it.

79
00:09:49,000 --> 00:10:15,000
But backing up now on a more technological side, on a software stack side, we started out back in the early 90s with Web 1, which was based on Tim Berners-Lee's view of what he called then the World Wide Web, which is really just a set of interlinked texts on various servers all over the Internet.

80
00:10:15,000 --> 00:10:24,000
Then Web 2.0 that was popularized by Tim O'Reilly was really more about dynamic and user-generated content.

81
00:10:24,000 --> 00:10:45,000
And then things branched out a little bit. This was like the 2000s. And we saw at the same time the idea of the semantic web and linked data, which is more about a network of data rather than a network of text.

82
00:10:45,000 --> 00:10:59,000
So it's more about machines than actual people. But then there was also more recently the Web 3.0 slash solid movement, which talks about pods where we can store our private information on.

83
00:10:59,000 --> 00:11:05,000
These pods are decentralized but can be used by centralized services.

84
00:11:05,000 --> 00:11:24,000
On top of that though now there is a separate movement called Web 3, not Web 3.0. I know it's confusing, but stick with us. And that is mainly about fully decentralizing things and adding a layer of trust to it, at least in some cases.

85
00:11:24,000 --> 00:11:29,000
And the trust is usually in the form of some form of blockchain technology in most cases.

86
00:11:29,000 --> 00:11:43,000
It's a technology that is kind of not clear which way it's going to go.

87
00:11:43,000 --> 00:11:55,000
And when it comes to the metaverse, we then add the experiences that the other forms of technology just don't really have as such.

88
00:11:55,000 --> 00:12:13,000
And the reason why it's felt that we need this sort of technology is because we envision the scenario where you're in one metaverse space and you want to go over to another metaverse space.

89
00:12:13,000 --> 00:12:22,000
But you kind of need to bring your stuff along. Like your avatar is comprised of shapes and textures. Those are assets. Those assets have to be stored somewhere.

90
00:12:22,000 --> 00:12:28,000
The metaverse too may not actually have them. So they would have to get it from somewhere.

91
00:12:28,000 --> 00:12:31,000
In order to get them from somewhere you have to prove that they're yours.

92
00:12:31,000 --> 00:12:52,000
So in one vision of it, we'll assume that the data is stored in some asset server and NFTs, as an example, would be used to prove that I own these things and the data is then retrieved from this asset service.

93
00:12:52,000 --> 00:13:03,000
Now the same thing could also work on decentralized services like IPFS or ARWeave or whatever you want to use. BitTorrent if you feel like it.

94
00:13:03,000 --> 00:13:14,000
And you have the same situation. You have the NFTs referencing the content, proving that you own it, and therefore the metaverse space can pick it up.

95
00:13:14,000 --> 00:13:28,000
Unfortunately the whole decentralization idea is really just a huge spectrum which has multiple dimensions to it. So it's a little bit hard in a short lecture like this, a short talk like this, to talk about this in great detail.

96
00:13:28,000 --> 00:13:32,000
Just say that there are options.

97
00:13:32,000 --> 00:13:45,000
So the promise really here is that we're going to be able to have meaningful interactions, we're going to be able to take back our data, and we're going to solve our security and privacy problems all in one batch.

98
00:13:45,000 --> 00:13:47,000
Sounds fantastic, sign me up.

99
00:13:47,000 --> 00:13:49,000
Okay, back to reality.

100
00:13:49,000 --> 00:14:07,000
The reality obviously is going to look very different. We have not been able to solve the problem of large corporations dominating the Web 2.0 space, so why would we think that that's going to happen for the metaverse space or the Web 3.0 space?

101
00:14:07,000 --> 00:14:33,000
So there are other problems there too, for instance blockchains, if you want to use them, are a little bit on the slow side, and we don't really know how to convert an asset of some sort from one world that's very natural, maybe like Second Life or so, to a Minecraft-like world.

102
00:14:33,000 --> 00:14:37,000
Those things would need to be solved.

103
00:14:37,000 --> 00:14:40,000
But threats are real.

104
00:14:40,000 --> 00:14:50,000
In fact, a year ago, the security industry was dealing with the Log4J vulnerability called Log4Shell.

105
00:14:50,000 --> 00:14:55,000
And the first people who really started talking about it were in fact the Minecraft people.

106
00:14:55,000 --> 00:15:00,000
Minecraft is of course developed on Java, it's a Java application.

107
00:15:00,000 --> 00:15:05,000
Most Java applications use Log4J as a logging mechanism.

108
00:15:05,000 --> 00:15:10,000
And yes, there were vulnerabilities in that system, and they were exploitable.

109
00:15:10,000 --> 00:15:15,000
So games have already been targets, and they already have been shown to be susceptible.

110
00:15:15,000 --> 00:15:21,000
And in fact, we see that going over most of that architecture that people are proposing.

111
00:15:21,000 --> 00:15:24,000
They're still just computers in the end.

112
00:15:24,000 --> 00:15:32,000
And even if they're distributed, these distributed servers can still be attacked, TDOS attacks, vulnerabilities, whatever.

113
00:15:32,000 --> 00:15:33,000
Endpoints have the same problem.

114
00:15:33,000 --> 00:15:43,000
If we're going to use a computer as an endpoint of some sort, or a phone or whatever, again, all of the existing vulnerabilities are there.

115
00:15:43,000 --> 00:15:48,000
They don't go away just because we called it the metaverse.

116
00:15:48,000 --> 00:15:55,000
Threats that are metaverse-specific, though, are kind of where the interesting bits start.

117
00:15:55,000 --> 00:16:00,000
The headsets that we are using will have their own little quirks.

118
00:16:00,000 --> 00:16:11,000
We were using the Microsoft HoloLens recently for some experiments and realized that it's actually capable of taking an image of our iris.

119
00:16:11,000 --> 00:16:13,000
That's a bit weird.

120
00:16:13,000 --> 00:16:20,000
The other thing we notice in most of the headsets, in fact, I think all the headsets that we've been working with so far, is that they have a lot of cameras around them.

121
00:16:20,000 --> 00:16:38,000
And that is all sorts of interesting privacy concerns when it comes to, well, stuff that's just lying around on my desk, or just the view of myself, the biometric data of myself or so that may then get transmitted.

122
00:16:38,000 --> 00:16:47,000
Then on top of that, we have all of the protocols and the new file formats that we're going to have to work on.

123
00:16:47,000 --> 00:17:00,000
X3D may not have a lot of mature code around it, so we could expect to see deserialization-style vulnerabilities popping up.

124
00:17:00,000 --> 00:17:07,000
The protocols may have loopholes in it that we're not aware of and allow for DDOS attacks or data theft.

125
00:17:07,000 --> 00:17:10,000
So a lot of concerns there.

126
00:17:10,000 --> 00:17:19,000
In 2019, we already looked at the eSports industry, found a lot of malware targeting these platforms, as well as a lot of vulnerabilities in them.

127
00:17:19,000 --> 00:17:25,000
This should not come to anybody's surprise, but just trying to point that out.

128
00:17:25,000 --> 00:17:34,000
IPFS as a technology for distributed computing is also a little bit dubious.

129
00:17:34,000 --> 00:17:37,000
So far, we've seen a lot of use in it for phishing.

130
00:17:37,000 --> 00:17:42,000
We haven't seen actually a lot of other applications that are legitimately using IPFS.

131
00:17:42,000 --> 00:17:46,000
Maybe one or two, yes, sure, but mostly not.

132
00:17:46,000 --> 00:17:56,000
And actually just today, when I was recording it, we released a short blog, which is kind of like a preamble for a longer paper that will be coming in January.

133
00:17:56,000 --> 00:18:02,000
Assets can be stolen and also ransomed.

134
00:18:02,000 --> 00:18:05,000
So stealing assets is actually pretty straightforward.

135
00:18:05,000 --> 00:18:13,000
I mean, if you have an asset server based on IPFS, all you need is a CID, and then you can just read that asset, unless, of course, it's encrypted.

136
00:18:13,000 --> 00:18:21,000
But then you have to deal with PKIs, so for distributing the keys to the metafor spaces that may actually need them.

137
00:18:21,000 --> 00:18:23,000
Ransoming is a little more interesting.

138
00:18:23,000 --> 00:18:38,000
If you have a centralized server, then yes, it should be obvious that you break into the server, and then you can ransom those assets if you encrypt them or delete them after copying, of course.

139
00:18:38,000 --> 00:18:43,000
And on a distributed system like IPFS, of course, this is a little bit harder.

140
00:18:43,000 --> 00:19:00,000
You would imagine, in fact, it would be impossible, but we looked at how IPFS distributes the content, and very often we only see the content like one or two servers, which gives you very few servers that you have to go after.

141
00:19:00,000 --> 00:19:13,000
In fact, some people may not even store their own data, in which case they're relying on pinning services, and the pinning services themselves are yet again a centralized instance that can be attacked.

142
00:19:13,000 --> 00:19:18,000
So not a perfect solution here.

143
00:19:18,000 --> 00:19:21,000
Stealing assets could also mean something else.

144
00:19:21,000 --> 00:19:32,000
Let's say you have a brand, you have your logos, you have your special textures and everything, and you don't want them to be copied and spoofed by other people.

145
00:19:32,000 --> 00:19:43,000
Well, it's very easy to take an existing image, add a little bit of noise to it, and then the NFT thinks it's dealing with a different image, so it doesn't protect that image, it only protects the original one.

146
00:19:43,000 --> 00:19:49,000
This can even happen when you're converting it from one format to the other, so this is difficult.

147
00:19:49,000 --> 00:20:05,000
And you don't need to even do this much noise, you don't have to add this much noise, and you still will have a completely different hash of that value, and even adding a lot of noise, like in this case, I don't see any difference between the two images.

148
00:20:05,000 --> 00:20:12,000
So this is actually quite a difficult topic to deal with.

149
00:20:12,000 --> 00:20:25,000
We're also worried about financial fraud, if indeed Web3 is going to be the basis for the metaverse and cryptocurrencies become an integral part of it, for one reason or the other.

150
00:20:25,000 --> 00:20:35,000
We're worried that we're going to see very vendor-specific cryptocurrencies, which may not be such a problem, these are probably going to be tokens based on, let's say, Ethereum or whatever.

151
00:20:35,000 --> 00:20:57,000
But what we're worried about then is that when people get used to the idea of vendor-specific cryptocurrencies, they will have no problem with accepting some sort of criminal cryptocurrency that maybe doesn't have the same sort of security checks that, let's say, a legit cryptocurrency would have.

152
00:20:57,000 --> 00:21:11,000
So we're worried about that. Also, if cryptocurrencies are going to proliferate in the metaverse, then money laundering, of course, becomes so much easier to hide in such an area.

153
00:21:11,000 --> 00:21:20,000
And obviously, we're going to see a lot of fraud happening, because that seems to be the main purpose of a lot of cryptocurrencies at this point.

154
00:21:20,000 --> 00:21:39,000
Privacy is also an issue. So while we keep on talking about the decentralization of the metaverse, and as much as I'd like to see that sort of thing happen in a good way, most likely we're going to see vendors consolidating and re-centralizing.

155
00:21:39,000 --> 00:21:56,000
Just imagine Amazon's marketplace for vendors starting a metaverse type service, and then the whole world basically is on Amazon at that point, at least of the people who want to shop.

156
00:21:56,000 --> 00:22:05,000
So privacy really comes at the mercy of those platforms, and ubiquitous wiretap is very practical.

157
00:22:05,000 --> 00:22:16,000
We also have the ordinary data sovereignty issues, which we already deal with now, which is actually worse when you're dealing with Web3, though, if you think about it.

158
00:22:16,000 --> 00:22:26,000
Because I have no idea. If I send my data out to be pinned somewhere, I don't know where it's being pinned. I have no control over that. So that data could be anywhere in the world at that point.

159
00:22:26,000 --> 00:22:43,000
I think also one has to realize, especially in a decentralized world, that the assets are not really private. If you have the CID of a piece of content, then anybody can read it in principle.

160
00:22:43,000 --> 00:22:56,000
And also the transactions are not necessarily private, because when we're dealing with blockchain technology, it's basically everything all in the open.

161
00:22:56,000 --> 00:23:03,000
The transactions are not really private. Yes, they're a little bit obfuscated, but they're not really private in the end.

162
00:23:03,000 --> 00:23:16,000
We also are going to see a lot of misuse of this immersive experience. We saw already very early on in social networks that people trust new social networks a lot more than they should.

163
00:23:16,000 --> 00:23:37,000
Social engineering then becomes quite easy to do. But we've also seen through the fake news issues that bubble worlds are likely to appear. You might have bubble immersive spaces, basically.

164
00:23:37,000 --> 00:23:43,000
And influence campaigns are also going to be pretty predominant there.

165
00:23:43,000 --> 00:23:59,000
Brands don't really have to deal that much with protests on their websites, but in the case of the metaverse, we've already seen that sort of thing happening in Second Life, so why wouldn't it happen in other metaverse spaces?

166
00:23:59,000 --> 00:24:12,000
And of course, DOS attacks against these spaces are always going to be a thing if you're going to allow active content that can be scripted.

167
00:24:12,000 --> 00:24:18,000
And you kind of do want scripted content, because that makes the world feel just a little more real.

168
00:24:18,000 --> 00:24:23,000
So it's a dilemma that we're probably not going to have a good solution for ever.

169
00:24:23,000 --> 00:24:30,000
And then there's the darkverse. And the darkverse is what we call a number of things.

170
00:24:30,000 --> 00:24:45,000
First of all, the criminal spaces themselves are a problem. Criminals can more easily organize in spaces like the metaverse.

171
00:24:45,000 --> 00:24:57,000
And this doesn't have to be just cyber criminals. This could also be ordinary criminals that may use this area to organize, but also even rehearse a bank heist or something like that.

172
00:24:57,000 --> 00:25:07,000
So in advance, maybe even use a digital twin of some building to figure out how best to attack it.

173
00:25:07,000 --> 00:25:13,000
So criminals will probably be attracted to this if it's to their benefit.

174
00:25:13,000 --> 00:25:30,000
And the bubble worlds themselves might be relatively harmless, but might lead them to extremist spaces. And that's obviously something that we have to worry about a lot, especially in recent memory.

175
00:25:30,000 --> 00:25:36,000
It's been a problem. Harassment has pretty much always been a problem, which is a shame.

176
00:25:36,000 --> 00:25:41,000
A lot of these games are very nice, and yet they still seem to have harassment problems.

177
00:25:41,000 --> 00:25:53,000
And childhood pornography was something that in Second Life we also had to look at, and it's super hard to define what that even means in a virtual world.

178
00:25:53,000 --> 00:26:00,000
That was never fully resolved, but obviously a major concern to society.

179
00:26:00,000 --> 00:26:03,000
So yes, a lot of questions here.

180
00:26:03,000 --> 00:26:15,000
Conclusions, though. So this is a new interactive application layer on top of the Internet, and it's really going to add to the problems we already have. It doesn't solve anything.

181
00:26:15,000 --> 00:26:22,000
We're going to see all sorts of API and protocol risks. We're going to see the social engineering risks increase.

182
00:26:22,000 --> 00:26:28,000
We're going to see risks to brands. I mean, opportunities, sure, but a lot of risks, too.

183
00:26:28,000 --> 00:26:37,000
But we're going to first have to deal with individual applications, because we don't really have the metaverse yet. It's not really there.

184
00:26:37,000 --> 00:26:42,000
And so maybe we'll have some time to figure out some of these details over time.

185
00:26:42,000 --> 00:27:00,000
So I'll leave you with the question that I'm not going to answer. Is it metaverse or is it metaverse? Thank you.

