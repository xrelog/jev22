1
00:00:00,000 --> 00:00:07,000
Thirdroom. Decentralized Virtual Worlds on Matrix.

2
00:00:07,000 --> 00:00:14,000
It's only the first technical preview, but what we've already seen, 
with the appetite for more.

3
00:00:14,000 --> 00:00:21,000
Imagine you are on the Matrix, have your chat, have video or phone calls, 
and now you get a new dimension.

4
00:00:21,000 --> 00:00:26,000
Thirdroom.io Completely web-based, integrated in Matrix.

5
00:00:26,000 --> 00:00:32,000
Robert Long, Chief Developer of Thirdroom, gives us a first insight.

6
00:00:32,000 --> 00:00:36,000
Please welcome Robert Long.

7
00:00:36,000 --> 00:00:40,000
Hi everyone, my name is Robert Long 
and this is Thirdroom.

8
00:00:40,000 --> 00:00:44,000
Thirdroom is a virtual world platform powered 
by the Matrix Protocol.

9
00:00:44,000 --> 00:00:53,000
A decentralized, end-to-end, encrypted 
communications protocol used for messaging, 
voice, video, and now 3D worlds.

10
00:00:53,000 --> 00:00:58,000
You can see I'm in a virtual world right now, 
and in the world is another avatar.

11
00:00:58,000 --> 00:01:02,000
I can walk around the virtual world and talk 
with other people.

12
00:01:02,000 --> 00:01:12,000
Usually I have a couple other people in the demo 
so you can actually hear them talking, 
but for now we've got me puppeting Robert Test over here.

13
00:01:12,000 --> 00:01:24,000
In the overlay, press escape, you can 
see I've got a bunch of different worlds, including 
the world that we're currently in,

14
00:01:24,000 --> 00:01:28,000
which is a special version of an environment that 
we call Terra.

15
00:01:28,000 --> 00:01:39,000
All of these different rooms are virtual worlds 
that I can join, similar to how you might 
join a typical chat room.

16
00:01:39,000 --> 00:01:46,000
If I scroll all the way to the end here, 
you can see we've got the XR.Labs Lounge right here.

17
00:01:46,000 --> 00:01:49,000
If I scroll back, you can see all the messages 
coming in here.

18
00:01:49,000 --> 00:01:59,000
This is just a standard Matrix client, but it also 
has the ability to browse virtual worlds.

19
00:01:59,000 --> 00:02:06,000
Right now, these two clients are communicating 
over a peer-to-peer WebRTC connection established 
over Matrix.

20
00:02:06,000 --> 00:02:12,000
Everything we do in this world is happening without 
any additional servers beyond our standard Matrix 
home server.

21
00:02:12,000 --> 00:02:18,000
Anyone with a Matrix account also has a Thirdroom 
account.

22
00:02:18,000 --> 00:02:25,000
Anyway, I think this crowd is already pretty 
familiar with Matrix, so let's get into some 3D stuff.

23
00:02:25,000 --> 00:02:38,000
Thirdroom uses a couple new browser 
technologies, such as the shared array buffer 
and Atomics APIs,

24
00:02:38,000 --> 00:02:41,000
which allows us to do multithreading in the browser.

25
00:02:41,000 --> 00:02:50,000
We've built an entire brand new 
browser-based game engine on top of these primitives.

26
00:02:50,000 --> 00:02:57,000
The shared array buffer API and the Atomics APIs 
are now shipping in Firefox, Chrome, and Safari.

27
00:02:57,000 --> 00:03:05,000
Most 3D web applications out there 
today, everything is running on a single thread.

28
00:03:05,000 --> 00:03:11,000
This leads to dropped frames due to late 
WebGL submissions,

29
00:03:11,000 --> 00:03:20,000
stalling the UI thread when uploading textures 
to the GPU, and a lot less room for adding 
fun stuff like physics and animations.

30
00:03:20,000 --> 00:03:27,000
Thirdroom uses three threads, the main browser 
thread and two different web workers.

31
00:03:27,000 --> 00:03:33,000
The main thread is running our React.js UI, 
as well as our Matrix client,

32
00:03:33,000 --> 00:03:39,000
which handles the real-time audio and game 
networking over WebRTC.

33
00:03:39,000 --> 00:03:46,000
The next web worker is our game thread, 
which is running our game logic, animations, 
and physics using Rapier,

34
00:03:46,000 --> 00:03:51,000
which is a physics engine written in Rust and 
compiled to WebAssembly.

35
00:03:51,000 --> 00:04:01,000
And finally, we have our render thread, 
which is running a modified version of 3GS, 
which uses WebGL for rendering.

36
00:04:01,000 --> 00:04:11,000
Our game state is held in a lock-free data 
structure called a triple buffer, enabling us to 
run our game thread at a rate independent 
of a render thread.

37
00:04:11,000 --> 00:04:20,000
In browsers, we can't easily limit our frame 
rate, so this really helps us support high refresh 
rate monitors,

38
00:04:20,000 --> 00:04:25,000
where we can run the game thread and the render 
thread at two entirely independent rates.

39
00:04:25,000 --> 00:04:31,000
Now, let's get into some of the details of 
the scene. So, we've got the Robert Test 
avatar over here,

40
00:04:31,000 --> 00:04:39,000
and I can run around the world, and I can 
toss some cubes.

41
00:04:39,000 --> 00:04:47,000
And so, to save on rendering costs for 
this environment, we're making heavy 
use of instancing.

42
00:04:47,000 --> 00:04:53,000
You can see a lot of repeated elements in 
this scene, and those are all being rendered 
in one pass.

43
00:04:53,000 --> 00:04:59,000
So, we've also baked light maps in this 
scene and added support for reflection probes.

44
00:04:59,000 --> 00:05:07,000
If I spawn this cube and then bring it inside 
here, you can see the lighting starts to change 
as I move in and out of this doorway.

45
00:05:07,000 --> 00:05:16,000
And that's all baked. And we added that to 3GS, 
which gives us more accurate lighting for dynamic 
objects moving about the scene,

46
00:05:16,000 --> 00:05:24,000
but still saving on expensive real-time lighting 
costs across the wide array of hardware 
that we have to support.

47
00:05:24,000 --> 00:05:31,000
So, in order to produce those assets, we created 
an export pipeline from Unity, which I'll flip 
over to now.

48
00:05:31,000 --> 00:05:37,000
And here you can see that same scene that we 
had open in Thirdroom, now open in Unity.

49
00:05:37,000 --> 00:05:46,000
So, let's click on some of these reflection 
probes, like the ones that we were using in 
that scene before.

50
00:05:46,000 --> 00:05:51,000
So, there's one right there, and 
there's one right here. You can see the little 
reflections on this sphere right here.

51
00:05:51,000 --> 00:05:55,000
That's kind of what's being saved into the 
environment there.

52
00:05:55,000 --> 00:05:59,000
And you can also see in this lighting tab 
here, we can open up the preview.

53
00:05:59,000 --> 00:06:08,000
This is the light map that was baked out of 
Unity and exported through the Unity GLTF pipeline.

54
00:06:08,000 --> 00:06:17,000
So, the scene is exported as GLTF. GLTF is 
this 3D file format created by the Khronos Group.

55
00:06:17,000 --> 00:06:27,000
We are members of the Khronos Groups ourselves. 
And there's an exporter in the Unity ecosystem 
called Unity GLTF.

56
00:06:27,000 --> 00:06:33,000
And if you go, you have to install the package, 
and then it'll be available under the Assets tab.

57
00:06:33,000 --> 00:06:39,000
And you can just export the active scene as a 
GLTF or a GLB.

58
00:06:39,000 --> 00:06:49,000
And after you've installed our Thirdroom exporter, 
it'll export out all these different 
GLTF extensions that we've created.

59
00:06:49,000 --> 00:07:00,000
Some of the GLTF extensions that we've worked 
on are the Lightmap extension and the 
Reflection Probe extensions.

60
00:07:00,000 --> 00:07:07,000
And we've also worked with other 
standards bodies, such as the Khronos Group 
on the GLTF Audio extension,

61
00:07:07,000 --> 00:07:15,000
and with the Open Metaverse Interoperability Group 
on a Collider extension for physics and 
colliding with the different meshes in the scene.

62
00:07:15,000 --> 00:07:26,000
So all that gets exported out of Unity, and then 
we have another post-processing pipeline that 
we've built into a browser-based tool.

63
00:07:26,000 --> 00:07:37,000
And this compresses textures using the 
Basis Universal Texture Encoder, which makes 
it smaller for download, but then also smaller 
on the GPU.

64
00:07:37,000 --> 00:07:43,000
And then it stores it in this KTX container 
and then serves it along with the GLTF file.

65
00:07:43,000 --> 00:07:49,000
It also takes duplicate mesh references 
and converts them into instance messages.

66
00:07:49,000 --> 00:07:56,000
So that's what's happening with all these 
duplicate objects around here. All that data 
gets baked into the GLTF.

67
00:07:56,000 --> 00:08:07,000
So inside our world, you can see some portals 
to some other worlds. So let's go through 
this portal right over here.

68
00:08:07,000 --> 00:08:13,000
So here we are in the matrix in Matrix. Pretty meta.

69
00:08:13,000 --> 00:08:23,000
So in this scene, I've got an interactable TV, 
and if I click on it, then it turns everything 
turns into the code from the matrix.

70
00:08:23,000 --> 00:08:33,000
So this happened through a scriptable interaction. 
So let's go and take a look at the code for that.

71
00:08:33,000 --> 00:08:42,000
So here in my Coder there, you can see a 
little piece of JavaScript, and I am getting the 
TV object in the scene.

72
00:08:42,000 --> 00:08:47,000
I'm making it interactable by setting its 
interactable property.

73
00:08:47,000 --> 00:08:51,000
I've got a little piece of state saying whether 
or not the TV is on.

74
00:08:51,000 --> 00:09:00,000
And then every frame, we check to see if the 
interactable is pressed. We toggle the state 
and then we toggle the matrix material.

75
00:09:00,000 --> 00:09:10,000
So that's a pretty simple example. Let's check 
out something a little bit more advanced.

76
00:09:10,000 --> 00:09:21,000
So in this example, I've got two different 
materials, and if I press this button, 
it swaps the materials.

77
00:09:21,000 --> 00:09:29,000
And in here, I've got a light that can turn off and on.

78
00:09:29,000 --> 00:09:38,000
Now, you might be thinking, there's 
the scripts, they're running inside of matrix.

79
00:09:38,000 --> 00:09:46,000
How can you run this in a safe fashion? 
And we've built our scripting engine 
around WebAssembly.

80
00:09:46,000 --> 00:09:56,000
So this API we're calling WebSceneGraph, 
and it is a JavaScript and WebAssembly based API.

81
00:09:56,000 --> 00:10:04,000
All of the code runs inside of a secure 
WebAssembly context and is completely isolated 
from the rest of the content of the client.

82
00:10:04,000 --> 00:10:12,000
So the matrix client is running on a different thread. This is then inside of a WebAssembly module,

83
00:10:12,000 --> 00:10:19,000
which only has API access that we've 
exposed to it via that WebSceneGraph API.

84
00:10:19,000 --> 00:10:34,000
So let's look at the code for this scene then. 
Flip back to my code editor, and you can see 
here's the JavaScript based version of that script.

85
00:10:34,000 --> 00:10:41,000
And we can see we're querying for the buttons, 
the cubes, the different textures.

86
00:10:41,000 --> 00:10:46,000
And we also are querying for the different 
switches and lights.

87
00:10:46,000 --> 00:10:53,000
And then we can see every single frame. 
We're querying to see if that material 
button is pressed and we can toggle a state there.

88
00:10:53,000 --> 00:10:59,000
We loop through all of the mesh primitives. 
These are all the different sub meshes of that cube.

89
00:10:59,000 --> 00:11:06,000
In this case, there's only one, but we're 
looping through it. And we've flipped the 
texture there.

90
00:11:06,000 --> 00:11:13,000
For the lights, we're setting the intensity property. 
If it's on, it's 20. If it's off, it's 0.

91
00:11:13,000 --> 00:11:18,000
And then because it's WebAssembly, we can see 
that same exact example in C.

92
00:11:18,000 --> 00:11:23,000
So here we've got the same queries. We've 
got the same update function.

93
00:11:23,000 --> 00:11:32,000
And we're just flipping the material or flipping 
the intensity based on that interactable state.

94
00:11:32,000 --> 00:11:37,000
And if you've done any web development, this 
might start looking a bit familiar.

95
00:11:37,000 --> 00:11:43,000
Just like JavaScript opened up the ability 
to program rich applications against HTML with 
the DOM API,

96
00:11:43,000 --> 00:11:50,000
we believe that the WebSceneGraph API has 
the potential to add interactivity to 
GLTF documents,

97
00:11:50,000 --> 00:11:58,000
allowing you to make portable 3D experiences 
that can be executed inside existing 
3D applications or entirely new ones.

98
00:11:58,000 --> 00:12:04,000
And that's the last bit for my interactive 
piece of this demo.

99
00:12:04,000 --> 00:12:08,000
Let's go back to Firefox, go over Figma.

100
00:12:08,000 --> 00:12:14,000
And I will show you a little bit of what's 
coming next in Thirdroom.

101
00:12:14,000 --> 00:12:24,000
So for our scripting API, we'll be launching 
the rest of the API in early 2023 with 
Tech Preview 2.

102
00:12:24,000 --> 00:12:33,000
And this is going to have the entire 
GLTF scene graph exposed as this JavaScript and 
WebAssembly API.

103
00:12:33,000 --> 00:12:37,000
So you'll be able to manipulate all those objects 
in that way.

104
00:12:37,000 --> 00:12:41,000
After that, we're looking at publishing a 
high-level networking API,

105
00:12:41,000 --> 00:12:45,000
which will let you sync the state of that 
scene graph across the network.

106
00:12:45,000 --> 00:12:50,000
Right now, there's just tiny interactions 
that are synced across the network.

107
00:12:50,000 --> 00:12:54,000
And then we're going to add the physics API.

108
00:12:54,000 --> 00:13:00,000
So you'll be able to create interactable 
physics objects within the scene,

109
00:13:00,000 --> 00:13:07,000
which will respond to you colliding with them, 
or you'll be able to query for collisions with 
other objects.

110
00:13:07,000 --> 00:13:13,000
We're also going to be adding the scriptable 
avatars and objects.

111
00:13:13,000 --> 00:13:19,000
So you'll be able to take avatars and add 
behaviors for the avatar.

112
00:13:19,000 --> 00:13:26,000
Maybe it'll start to, the eyes will follow 
other users or the hair will have like spring 
bones in it.

113
00:13:26,000 --> 00:13:30,000
Lots of things that people have been able to 
accomplish in other VR platforms,

114
00:13:30,000 --> 00:13:35,000
you'll be able to accomplish in Thirdroom 
with interactable scripts.

115
00:13:35,000 --> 00:13:40,000
And then for objects, I mean, there's a 
million different things that you could do with that.

116
00:13:40,000 --> 00:13:45,000
One of my favorite things would be you could 
do like a main cabinet or something like that.

117
00:13:45,000 --> 00:13:53,000
You could have this arcade cabinet, place 
it into the world, and then play arcade games 
inside of the world.

118
00:13:53,000 --> 00:14:04,000
And then that object is then portable 
so you can bring it between the different 
scenes in different Thirdroom worlds.

119
00:14:04,000 --> 00:14:09,000
Now, another piece of the Tech Preview 2 
is going to be the discovery page.

120
00:14:09,000 --> 00:14:16,000
Here's a basic concept that we had when we 
were exploring this concept.

121
00:14:16,000 --> 00:14:20,000
The final render will look a little bit 
different from this.

122
00:14:20,000 --> 00:14:24,000
And like I said, this is launching in Tech Preview 2.

123
00:14:24,000 --> 00:14:30,000
And it's a new way to find 3D worlds to 
join and explore.

124
00:14:30,000 --> 00:14:36,000
It's also a good way to find existing 
Matrix rooms to join from within the 
Thirdroom client.

125
00:14:36,000 --> 00:14:45,000
So just as we found the XR log room or the 
Thirdroom room, we can find that in this tab now.

126
00:14:45,000 --> 00:14:48,000
And one of the biggest things is we can 
find user created scenes.

127
00:14:48,000 --> 00:14:56,000
So after you've created the scene in 
Unity or any of the other tooling, you can 
publish it to this discovery page,

128
00:14:56,000 --> 00:15:00,000
and you can find other users' contents.

129
00:15:00,000 --> 00:15:04,000
And this is completely decentralized.

130
00:15:04,000 --> 00:15:08,000
The first version is just going to be focused on our...

131
00:15:08,000 --> 00:15:16,000
It's going to be one room that will basically 
hold the content that everyone submits to it.

132
00:15:16,000 --> 00:15:22,000
And it's based on top of this spec, MSC3948, 
which is the repository room spec.

133
00:15:22,000 --> 00:15:34,000
And they're rooms, and when you join them, 
it gives you access to the content and the 
ability to submit content for them to be approved 
and featured.

134
00:15:34,000 --> 00:15:43,000
And then the goal is that the discovery page 
will then pull from multiple repository rooms, 
not just our own repository room 
like we will release.

135
00:15:43,000 --> 00:15:55,000
And every repository room that you've joined 
will feed into this discovery page and kind of 
give you this overall view of all the content 
being shared across your communities.

136
00:15:55,000 --> 00:16:03,000
So it's a really powerful concept, especially 
when you combine it with Matrix Spaces, which 
we're hoping to launch relatively soon.

137
00:16:03,000 --> 00:16:09,000
So you'll be able to join spaces and see all 
the content across all the spaces that you've joined.

138
00:16:09,000 --> 00:16:11,000
Really cool way to find content.

139
00:16:11,000 --> 00:16:23,000
And if we take the repository rooms to the 
next level, we'll be able to create a 
decentralized content marketplace.

140
00:16:23,000 --> 00:16:31,000
So this is extending repository rooms 
to add the ability to purchase and 
cryptographically sign content.

141
00:16:31,000 --> 00:16:41,000
And we're still working out the details of the spec, 
but the goal is for the marketplace to be 
fed by repository rooms so that everything 
is hosted in a federated manner.

142
00:16:41,000 --> 00:16:46,000
There's no walled gardens. There's fair pricing 
driven by competition.

143
00:16:46,000 --> 00:16:59,000
All the different marketplaces basically being 
funneled into this singular user interface to 
find content across all these different servers 
that you've joined.

144
00:16:59,000 --> 00:17:07,000
And creators can get paid for their work, 
which is super important for any sort of platform.

145
00:17:07,000 --> 00:17:14,000
We're going to be launching our store alongside 
all of this, and we'll have to compete 
with all the other people on the platform.

146
00:17:14,000 --> 00:17:17,000
But we think that's a really healthy thing.

147
00:17:17,000 --> 00:17:22,000
And Discovery will be driven by the communities 
that you join.

148
00:17:22,000 --> 00:17:36,000
So if I'm interested in, I don't know, the 
general Matrix community will probably be posting 
a whole bunch of cool Matrix related rooms.

149
00:17:36,000 --> 00:17:50,000
So I can find places to talk about Matrix 
features, but I could also join something related 
to my favorite movies or my favorite video games 
or my interests, things like that.

150
00:17:50,000 --> 00:17:58,000
And you can find people, content, rooms and 
spaces that relate to this sort of stuff.

151
00:17:58,000 --> 00:18:11,000
Yeah. What's next? So after all this sort 
of stuff, we've got Tech Preview 2, which is 
launching with scripting, the Discovery 
page and authoritative networking.

152
00:18:11,000 --> 00:18:20,000
Then we're launching custom avatars, WebXR 
and mobile gamepad support and input remapping.

153
00:18:20,000 --> 00:18:26,000
This has been something that's been wanted 
for a long time and we need to add it.

154
00:18:26,000 --> 00:18:39,000
The bot API, which is kind of like the bot 
API for Matrix chat bots, except for now you 
can have like an actual virtual agent walk 
around your world and dedicated servers.

155
00:18:39,000 --> 00:18:56,000
So similar to the bot API, except it's running 
an instance of your world over a long 
period of time, keeping it up and running and 
having some sort of additional persistence 
layer beyond what we're doing with just peer to peer.

156
00:18:56,000 --> 00:19:01,000
And then the in-world editor. This was something 
that we started on.

157
00:19:01,000 --> 00:19:12,000
If you press tilde in Thirdroom today, 
you'll see there's like a tree view and you 
can see all the different objects in the world 
and see like an outline around them when you select them.

158
00:19:12,000 --> 00:19:25,000
In order to build out the rest of the editor, 
we really needed to focus on the scripting API 
and getting some of that multi-threaded 
data structure work done.

159
00:19:25,000 --> 00:19:31,000
But now that's almost done and we'll be able to 
start work on the in-world editor.

160
00:19:31,000 --> 00:19:40,000
We want to increase our room caps and luckily 
the work on the selective forwarding unit is well 
underway and there's proof of concept out there.

161
00:19:40,000 --> 00:19:49,000
If you go look for, I believe, Waterfall is the 
name of the project, you'll find our selective 
forwarding unit.

162
00:19:49,000 --> 00:20:03,000
And then the decentralized marketplace work 
that I talked about and with that, an inventory 
so that you can actually pull out these 
pieces of content that you've purchased,

163
00:20:03,000 --> 00:20:10,000
as well as scriptable objects and avatars, 
which should be available in your inventory 
and on that marketplace as well.

164
00:20:10,000 --> 00:20:17,000
So really competing, completing the whole metaverse, 
interoperable metaverse picture there.

165
00:20:17,000 --> 00:20:27,000
And that's all I've got. So if you're interested 
in getting involved, you can find our product 
live at thirdroom.io.

166
00:20:27,000 --> 00:20:35,000
You can find us on GitHub. You can join us on 
Matrix in our thirdroom.dev room.

167
00:20:35,000 --> 00:20:39,000
We talk about everything there, not just development.

168
00:20:39,000 --> 00:20:49,000
Anyone who is interested in contributing 
should get in touch via that Matrix room if you're 
interested in art, whether it's avatars or 
environments,

169
00:20:49,000 --> 00:20:58,000
or if you're interested in development, 
we have graphics programming, front end UI stuff, 
a lot of really cool work going on there.

170
00:20:58,000 --> 00:21:03,000
And we are looking for external contributors. 
Definitely get in touch on our Matrix room.

171
00:21:03,000 --> 00:21:09,000
Anyways, thank you so much. And I look forward 
to talking to you all soon.

