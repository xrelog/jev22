1
00:00:30,000 --> 00:00:40,800
Benvinguts A La inauguració De La XRelog22!

2
00:00:40,800 --> 00:00:51,920
Hola Anchoray i hola al món exterior! Aquí us volem presentar el Que el XRelog22

3
00:00:51,920 --> 00:01:01,760
a la botiga per a tu aquest any. Què és El XRelog22 en general?

4
00:01:01,760 --> 00:01:11,360
Quins són els objectius? Volem cuidar SocialVR i SocialVR sense ximpleries

5
00:01:11,360 --> 00:01:17,000
i això és el que hem posat en el nostre lema. Lo llamamos
Visiones Para Multiversos Independientes.’’

6
00:01:17,000 --> 00:01:23,960
He sentit que també voleu integrar l'ètica dels pirates informàtics al metavers i al multivers.

7
00:01:23,960 --> 00:01:29,960
Les extensions del meta i el multivers. Absolutament! No n'hi ha prou en el

8
00:01:29,960 --> 00:01:35,480
Els Metavers no existeixen. Per exemple, acabo de notar que no veig gens el logotip del canal.

9
00:01:35,480 --> 00:01:39,680
Teniu el logo del canal? Logo del canal.

10
00:01:39,680 --> 00:01:49,440
Què passa? Què era aquest soroll?

11
00:01:49,440 --> 00:01:56,960
Sí, jo tampoc ho sé. Sí, malauradament, va ser l'escocès el que no va aguantar.

12
00:01:56,960 --> 00:02:04,960
Sí, no som els súper vídeos. Ni tan sols super professional AMB OBS.

13
00:02:04,960 --> 00:02:11,680
No som persones perfectes, així que passa que el logotip del canal desapareix.

14
00:02:11,680 --> 00:02:17,440
L'escocès, simplement, era dolent. Ha de ser de mala qualitat.

15
00:02:17,440 --> 00:02:21,760
Crec que encara hem d'adaptar una mica les nostres expectatives. Vull dir amb això,

16
00:02:21,760 --> 00:02:27,760
que la gent no ha de pensar que van a ser atesos perfectament, ni una producció de qualitat de la ARD, ZDF.

17
00:02:27,760 --> 00:02:30,520
Ningú vol ARD I ZDF.

18
00:02:30,520 --> 00:02:35,960
Però, de totes maneres, amb nosaltres, moltes coses estan inacabades, però atractives.

19
00:02:35,960 --> 00:02:44,960
No està inacabat, està en producció. És àgil. És un desenvolupament àgil.

20
00:02:44,960 --> 00:02:47,560
El 'Treball En Progrés' és el nou normal, oi?

21
00:02:47,560 --> 00:02:48,560
Sí, és clar.

22
00:02:48,560 --> 00:02:54,320
D'acord. Així que prepareu-vos per rebre molts vessaments ... vessaments …

23
00:02:54,320 --> 00:02:58,720
sí, ja ho veus, aquí tornem a anar. Prepareu-vos per veure moltes coses vessades.

24
00:02:58,720 --> 00:03:02,320
Ampolles Mate i coses vessades, però ens divertirem molt.

25
00:03:02,320 --> 00:03:04,720
Els enganxem amb la cinta adhesiva.

26
00:03:04,720 --> 00:03:05,400
Exacte.

27
00:03:05,400 --> 00:03:24,360
Però és realment pronunciat XRelog22.

28
00:03:24,360 --> 00:03:31,960
Així i això deriva DE XR.Labs Operation Group (En Anglès).

29
00:03:31,960 --> 00:03:39,800
Sóc AQUÍ A L'ICCBS, mira. Estem construint mons virtuals.

30
00:03:39,800 --> 00:03:47,800
Un d'ELLS, un vehicle que viatja pel món virtual, és EL NOSTRE ICCBS,

31
00:03:47,800 --> 00:03:55,000
Intergalactic Chaos Communication Broadcast Studio (En Anglès). I va amarrar A Overte per a aquest esdeveniment.

32
00:03:55,000 --> 00:04:02,760
Aquí, ja ho veieu, està força ben millorat. Això és perquè hem tingut alguns accidents aquí abans.

33
00:04:02,760 --> 00:04:10,600
I tot el bonic disseny interior no s'ha arreglat bé i, per tant, s'ha fet malbé una mica.

34
00:04:10,600 --> 00:04:17,120
Però estem en procés d'arreglar-ho molt bé.

35
00:04:17,120 --> 00:04:23,920
Vinga, t'ensenyaré una cosa. Perquè volem fer algunes especialitats als racons.

36
00:04:23,920 --> 00:04:31,960
No sé qui ho reconeixerà, els que ja han estat a

37
00:04:31,960 --> 00:04:39,680
La Base C el reconeixerà. Són parets de llum, compostes per ampolles de mate.

38
00:04:39,680 --> 00:04:49,760
I aquí, per exemple, estem considerant construir el que s'anomena Bessó Digital d'aquest dispositiu.

39
00:04:49,760 --> 00:04:55,120
Aquí, per cert, hi ha les instruccions per fabricar la interfície de programació d'aplicacions per saber com utilitzar-la.

40
00:04:55,120 --> 00:05:01,480
Aquí teniu aquesta pàgina, la podeu veure i llegir. I aquí, per exemple, teniu una idea de com podríem utilitzar aquest espai.

41
00:05:01,480 --> 00:05:08,920
Que instal aquesta PANTALLA LED aquí.

42
00:05:08,920 --> 00:05:15,120
Estic volant una mica. Aquí, també podem volar aquí. He de practicar una mica volar.

43
00:05:15,120 --> 00:05:21,880
Hura, ara estic volant per aquí. Mira aquí. Encara hi ha un forat aquí, veus? Oh, està trencat de nou.

44
00:05:21,880 --> 00:05:26,680
Aquí encara cal reparar una mica. Aquesta és una altra tasca. Ja veus, és així a causa dels accidents d'aterratge.

45
00:05:26,680 --> 00:05:32,520
De nou aquí. Acabo de passar un portal i aquí teniu la nostra sala de sortida.

46
00:05:32,520 --> 00:05:42,920
Allà, podem pujar al zeppelin. Per exemple, la terra gira ara mateix. Cool…

47
00:05:42,920 --> 00:05:51,920
podem fer-ho ... Molt xulo. Allà, veiem ELS ICCBS des de fora. Llavors sóc

48
00:05:51,920 --> 00:05:58,600
aquí, dins del zeppelin. També hi ha un portal. Anem a veure on condueix? Anem a dins.

49
00:05:58,600 --> 00:06:08,400
Oh, sí, et fa tornar. Va ser llavors quan van canviar de portal. És bo que en parlem.

50
00:06:08,400 --> 00:06:13,720
De fet, el portal que acabo de passar hauria de conduir aquí.

51
00:06:13,720 --> 00:06:23,080
Mirem aquí. Què passa? Mira, una fosca... Mira, és La Pols De Les Fades.

52
00:06:23,080 --> 00:06:33,160
Increïble. És la primera vegada que el veig. Vaja, això no està gens malament. De fet,

53
00:06:33,160 --> 00:06:39,880
Encara passen coses allà dalt? Ja no es tornen. Sí, giren molt lentament.

54
00:06:39,880 --> 00:06:44,960
Aquí, veiem el node de dades en diferents representacions artístiques. I tenim una foguera.

55
00:06:44,960 --> 00:06:56,560
I crepita. Aquí és realment benvingut. Espero que ens vegem sovint aquí.

56
00:06:56,560 --> 00:07:02,720
Bé, si camino per aquí, també puc anar al soterrani de Jev22.

57
00:07:02,720 --> 00:07:13,960
I el soterrani té aquest aspecte. Ei, ei, ei, ei. Mirem aquí.

58
00:07:13,960 --> 00:07:20,520
Aquests són tots els esdeveniments, és a dir, les pàgines d'inici dels esdeveniments. Podeu veure-les totes.

59
00:07:20,520 --> 00:07:27,240
N'hi ha molts. Sí, mirem aquí. Piratejar en paralel. I quan ens hi acostem, hi ha un lloc realment interactiu.

60
00:07:27,240 --> 00:07:33,000
Podem fer-hi clic i fullejar-lo. I funciona per als 38 esdeveniments.

61
00:07:33,000 --> 00:07:42,640
Podeu veure-les totes i participar-hi.

62
00:07:42,640 --> 00:07:54,120
Chili amb El Caos . Sí, aquí podeu veure El Chili con Chaos. El programa és molt interessant.

63
00:07:54,120 --> 00:08:01,480
Podem a Través de Llocs, volar en el Nostre Espai De Fabricació.

64
00:08:01,480 --> 00:08:11,680
L'Espai De Referència s'anomena Simplement XRelog22. I aquest espai de creació és ara una mena de recull de coses inacabades.

65
00:08:11,680 --> 00:08:17,120
de coses inacabades. Vam començar les coses allà, en ordre dispers.

66
00:08:17,120 --> 00:08:25,840
per exemple, tinc la idea, per a les 15 pistes, així que 15 temes, que tenim. Què tal si fem una ruta turística?

67
00:08:25,840 --> 00:08:38,800
construir 15 plantes. Sí, molt xulo. Jo també vull poder volar. Volar és una bona idea.

68
00:08:38,800 --> 00:08:45,840
En una inspecció més propera, aquests pisos semblen una mica petites sales d'embarcament.

69
00:08:45,840 --> 00:08:53,800
Molt petit i molt petit. I a cadascuna d'aquestes plantes hi ha un tema dedicat.

70
00:08:53,800 --> 00:09:02,200
Vam començar fins al tercer pis.

71
00:09:02,200 --> 00:09:09,480
Ja sigui podcasting, Ja sigui SocialVR, ja siguin comunitats XR, n'hi ha diverses, etc.

72
00:09:09,480 --> 00:09:16,680
I, per descomptat, a les primeres plantes, hi ha (H)Activisme.

73
00:09:16,680 --> 00:09:28,240
Aquest és, per descomptat, el nostre tema principal.

74
00:09:28,240 --> 00:09:36,160
Espera, estic volant allà. Està escrit allà, però el llum no està encès.

75
00:09:36,160 --> 00:09:42,040
He traslladat el llum a algun lloc. Ara no s'encén en absolut.

76
00:09:42,040 --> 00:09:48,800
En general, aquí s'encén bé. Podem llegir això.
No, l'hectivisme està a les fosques.
sobre l'ètica dels pirates informàtics.

77
00:09:48,800 --> 00:09:55,160
Fes una ullada per aquí. Aquesta és una de les primeres coses que vaig fer.

78
00:09:55,160 --> 00:10:02,920
Vaig dibuixar l'ètica del pirata informàtic en 3d en realitat virtual amb un multibrush.

79
00:10:02,920 --> 00:10:11,240
I després vaig integrar aquesta pintura com a model 3d aquí. I fins i tot vaig integrar un text a veu, en alemany i anglès.

80
00:10:11,240 --> 00:10:18,160
Bé, el que vaig descobrir en la investigació sobre l'ètica dels pirates informàtics,

81
00:10:18,160 --> 00:10:24,600
és perquè les sis primeres regles dels pirates informàtics són més antigues,

82
00:10:24,600 --> 00:10:30,440
data dels anys 60 i que només s'han afegit dues regles Per Part Del Computer Chaos Club

83
00:10:30,440 --> 00:10:36,760
En particular, no embruteu les dades d'altres persones.

84
00:10:36,760 --> 00:10:43,840
Utilitzar dades públiques, protegir dades privades. Crec que ho hem sentit abans.

85
00:10:43,840 --> 00:10:50,520
Aquests són els dos complements de L'Ordinador-Chaos-Club. Ens hem pres la llibertat d'afegir una novena regla.

86
00:10:50,520 --> 00:11:00,720
Penseu sempre en trossos i arbres. Perquè l'aspecte ecològic, encara no apareix en l'ètica dels pirates informàtics.

87
00:11:00,720 --> 00:11:07,280
Per això s'hauria de completar necessàriament.

88
00:11:07,280 --> 00:11:14,320
Aquesta era la nostra ètica pirata. En cas contrari, ja ho veieu, és un recull de tot tipus de coses.

89
00:11:14,320 --> 00:11:17,560
Veus a dalt, veiem el zeppelin allà.

90
00:11:17,560 --> 00:11:24,880
Per sobre del zeppelin, hi ha una primera versió DELS ICCBS.

91
00:11:24,880 --> 00:11:30,440
Aquí hi ha safates. Utilitzarem aquestes safates quan organitzem tallers.

92
00:11:30,440 --> 00:11:36,280
Després podem volar a temes i escoltar coses.

93
00:11:36,280 --> 00:11:43,160
Som tan senzills. Aquest és el nostre espai virtual.

94
00:11:43,160 --> 00:11:52,040
No té relació temàtica.

95
00:11:52,040 --> 00:11:59,000
Quan haguem acabat la Torre De Rastreig, aquesta serà transferida al seu propi servidor de domini.

96
00:11:59,000 --> 00:12:07,320
Després el traiem de l'espai del fabricant i construïm alguna cosa nova.

97
00:12:07,320 --> 00:12:20,400
Ara torno a L'Estudi Intergalactic Chaos Communication Broadcast. Iccbs abreujat.

98
00:12:20,400 --> 00:12:31,080
Fes un cop d'ull. D'on vinc. D'aquesta manera, qualsevol pot canviar la seva ubicació, qui hi ha al sistema?

99
00:12:31,080 --> 00:12:37,960
Sí, tot està obert. Ni tan sols cal registrar-se.

100
00:12:37,960 --> 00:12:44,560
Ni tan sols cal crear un compte. Tot el que heu de fer és descarregar el client i llançar-lo per entrar en aquest món.

101
00:12:44,560 --> 00:12:49,320
Trobareu una breu explicació de com funciona.

102
00:12:49,320 --> 00:12:55,960
A continuació, ja podeu utilitzar la funció Llocs. Aleshores pots descobrir encara més mons.

103
00:12:55,960 --> 00:13:07,080
Així, ELS ICCBS i La Pols De Fades i El XRelog o més tard també la Torre De Rastreig.

104
00:13:07,080 --> 00:13:14,040
Aquests són només tres mons de desenes de mons.

105
00:13:14,040 --> 00:13:18,240
Probablement aviat en podrem visitar centenars.

106
00:13:18,240 --> 00:13:27,520
Per descomptat, també hi ha mons que no estan oberts al públic, com per exemple Un Darling VR.

107
00:13:27,520 --> 00:13:32,520
És una mica més colorit sexualment. No volem que ningú hi pugui entrar.

108
00:13:32,520 --> 00:13:39,280
Però, en cas contrari, la majoria dels mons estan oberts al públic,

109
00:13:39,280 --> 00:13:49,200
lliurement, sense merda, SENSE nft, smart contract, cadena de blocs, cripto-truc.

110
00:13:49,200 --> 00:13:52,560
Tot això no es troba aquí. És tan bonic.

111
00:13:52,560 --> 00:14:09,880
Anchoray, ens has ensenyat tantes coses d'aquest món.

112
00:14:09,880 --> 00:14:12,960
Què penses fer al Congrés? No n'has dit res.

113
00:14:12,960 --> 00:14:25,080
Sí, és cert. Tenim 15 sessions i 15 temes.

114
00:14:25,080 --> 00:14:36,440
i també mirem els altres 38 esdeveniments DE JEV22. El tema principal és la crítica a la tècnica.

115
00:14:36,440 --> 00:14:42,240
Crítica clara i clara de la tècnica. No volem deixar Que Internet se'n vagi.faceboo Facebook

116
00:14:42,240 --> 00:14:48,800
Parlem de la xarxa 3.0. Volem fer una ullada crítica a com s'està desenvolupant.

117
00:14:48,800 --> 00:14:53,520
Us hem preparat unes sessions molt interessants.

118
00:14:53,520 --> 00:14:58,280
Aleshores volem mirar més enllà de la punta del nas i també estar interessats en altres esdeveniments DE JEV.

119
00:14:58,280 --> 00:15:04,920
Hi ha iniciatives absolutament fantàstiques, des De Londres fins A Atenes.

120
00:15:04,920 --> 00:15:12,560
A Munic n'hi ha dos, A Hamburg n'hi ha quatre en total. A Berlín, A Erlangen, A Dopotsseldorf, a Aquisgrà.

121
00:15:12,560 --> 00:15:15,800
Hi ha molt per descobrir.

122
00:15:15,800 --> 00:15:17,280
Però, també serà divertit?

123
00:15:17,280 --> 00:15:23,640
Oh, divertit! Sí, és clar. És cert que no tenim El Perill,

124
00:15:23,640 --> 00:15:29,080
ja no hem aconseguit preparar preguntes boniques per a això.

125
00:15:29,080 --> 00:15:38,400
Però estem intentant fer alguna cosa tan boja. L'anomenarem Copa Del Món Mafifa 2030. Repetirem el sorteig de la Copa del Món 2030.

126
00:15:38,400 --> 00:15:44,920
Deixa't sorprendre. Cada vegada a les 23 hores, dimecres, dijous i divendres.

127
00:15:44,920 --> 00:15:47,400
Serà interessant.

128
00:15:47,400 --> 00:15:50,600
S'ha de fer d'una manera molt corrupta.

129
00:15:50,600 --> 00:15:57,840
El mate compta com a moneda?

130
00:15:57,840 --> 00:16:05,720
Si no compta com a moneda, tampoc estem corruptes.

131
00:16:05,720 --> 00:16:12,880
El que és important és que amb nosaltres, hi ha molt, molt poques presentacions frontals.

132
00:16:12,880 --> 00:16:19,640
Amb nosaltres, gairebé tot és un taller participatiu, dissenyat com un Barcamp.

133
00:16:19,640 --> 00:16:28,080
HI ha UN barcamp XR, just després d'aquesta obertura. Però també hi ha Bits i Arbres.

134
00:16:28,080 --> 00:16:34,200
Fireside Xat, on esteu cordialment convidats a participar.

135
00:16:34,200 --> 00:16:39,760
També els Tallers Oberts, on explorem aquest món,

136
00:16:39,760 --> 00:16:41,080
també estan dissenyats per ser tallers i no conferències.

137
00:16:41,080 --> 00:16:50,760
Vols tornar a dir com accedim al món?

138
00:16:50,760 --> 00:16:57,800
Perquè veiem aquest món realment gran, però com entrar-hi.

139
00:16:57,800 --> 00:16:58,800
Et podem visitar?

140
00:16:58,800 --> 00:17:04,520
Sí, és clar. Així que el més important passa a Casa A Matrix.

141
00:17:04,520 --> 00:17:11,960
A La programació De Pretalx, s'indica un canal De Matriu per a cada presentació.

142
00:17:11,960 --> 00:17:16,760
Aquí és on ens reunim i on també intercanviem tota la informació.

143
00:17:16,760 --> 00:17:29,820
L'altra cosa és aquest Metavers, aquest Multivers Obert.

144
00:17:29,820 --> 00:17:35,520
Overte.org , aquesta ja és la pàgina d'inici.
Podeu descarregar un client allà.

145
00:17:35,520 --> 00:17:42,880
Existeix Per A Linux i Per A Finestres. Curiosament, No està disponible Per A Apple.

146
00:17:42,880 --> 00:17:51,840
També podeu utilitzar-lo per visitar-nos ALS ICCBS, al Bosc De Pols De Fades.

147
00:17:51,840 --> 00:17:57,160
o fins i tot en un espai virtual al bloc xrelog amb La Torre de Pistes.

148
00:17:57,160 --> 00:17:58,160
Podeu explorar tot això vosaltres mateixos.

149
00:17:58,160 --> 00:18:08,760
Em sembla molt interessant i crec que,

150
00:18:08,760 --> 00:18:11,600
a alguns oients també els hauria de semblar molt emocionant conèixer-vos a l'espai 3d.

151
00:18:11,600 --> 00:18:17,640
Tinc curiositat per saber-ho. A partir de quants espectadors s'enfonsaran els servidors?

152
00:18:17,640 --> 00:18:19,040
ho descobrirem junts.

153
00:18:19,040 --> 00:18:20,040
Fer proves, provar.

154
00:18:20,040 --> 00:18:26,200
També podem oferir altres servidors. Però encara no estem automatitzats.

155
00:18:26,200 --> 00:18:32,280
proporcionar altres servidors. Però encara no estem molt automatitzats.

156
00:18:32,280 --> 00:18:36,840
Es necessita una mica de temps, però estem preparats.

157
00:18:36,840 --> 00:18:38,080
Si caiem ara, encara podem fer alguna cosa.

158
00:18:38,080 --> 00:18:45,520
Finalment, m'agradaria acomiadar-me de vosaltres.

159
00:18:45,520 --> 00:18:51,240
També per cert, qui ets exactament?

160
00:18:51,240 --> 00:18:56,560
Ho Sento, Anchoray, no t'ho puc dir.

161
00:18:56,560 --> 00:19:25,560
Així que haurem de resoldre aquest enigma la propera vegada. Fins després. Adéu.