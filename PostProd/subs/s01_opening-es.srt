1
00:00:30,000 --> 00:00:40,800
¡Bienvenido a la inauguración de XRelog22!

2
00:00:40,800 --> 00:00:51,920
¡Hola Anchoray y hola al mundo exterior! Queremos presentarte en qué consiste XRelog22

3
00:00:51,920 --> 00:01:01,760
 tiene reservado para ti este año. ¿Qué es XRelog22 en general?

4
00:01:01,760 --> 00:01:11,360
 ¿Cuáles son los objetivos? Queremos tratar con SocialVR y SocialVR sin tonterías 

5
00:01:11,360 --> 00:01:17,000
y eso es lo que ponemos en nuestro lema. Lo llamamos 
''Visiones para multiversos independientes''.

6
00:01:17,000 --> 00:01:23,960
He oído que también quiere integrar la ética hacker en el metaverso y en el multiverso.

7
00:01:23,960 --> 00:01:29,960
Las extensiones del meta y el multiverso. ¡Claro que sí! Simplemente no hay suficiente en la

8
00:01:29,960 --> 00:01:35,480
El metaverso no existe. Oye, me acabo de dar cuenta de que no veo el logotipo del canal en absoluto.

9
00:01:35,480 --> 00:01:39,680
¿Tiene un logotipo del canal? ¿El logotipo del canal?

10
00:01:39,680 --> 00:01:49,440
¿Qué ha sido eso? ¿Qué ha sido ese ruido?

11
00:01:49,440 --> 00:01:56,960
Sí, yo tampoco lo sé. Sí, por desgracia, fue la cinta la que no aguantó.

12
00:01:56,960 --> 00:02:04,960
Sí, no somos los súper videastas. Ni realmente super profesional con OBS.

13
00:02:04,960 --> 00:02:11,680
No somos personas perfectas, así que a veces el logotipo del canal desaparece.

14
00:02:11,680 --> 00:02:17,440
El whisky era malo. Debe de ser de mala calidad.

15
00:02:17,440 --> 00:02:21,760
Creo que todavía tenemos que ajustar un poco nuestras expectativas. Lo digo en serio,

16
00:02:21,760 --> 00:02:27,760
que la gente no debe pensar que va a ser atendida a la perfección, ni que va a obtener una producción de calidad ARD, ZDF.

17
00:02:27,760 --> 00:02:30,520
Nadie quiere ARD y ZDF.

18
00:02:30,520 --> 00:02:35,960
Pero en fin, muchas cosas están inacabadas, pero son atractivas.

19
00:02:35,960 --> 00:02:44,960
No está inacabada, está en producción. Es ágil. Es el desarrollo ágil.

20
00:02:44,960 --> 00:02:47,560
Quieres decir que "Work in Progress" es la nueva norma, ¿no?

21
00:02:47,560 --> 00:02:48,560
Sí, claro que sí.

22
00:02:48,560 --> 00:02:54,320
Aha, vale. Así que prepárate para un montón de al revés ... al revés ...

23
00:02:54,320 --> 00:02:58,720
Sí, ya ves, aquí vamos de nuevo. Prepárate para ver muchas cosas derramadas.

24
00:02:58,720 --> 00:03:02,320
Botellas de mate y cosas derramadas, pero vamos a divertirnos.

25
00:03:02,320 --> 00:03:04,720
Los pegamos con la cinta.

26
00:03:04,720 --> 00:03:05,400
Exactamente.

27
00:03:05,400 --> 00:03:24,360
Pero en realidad se pronuncia XRelog22.

28
00:03:24,360 --> 00:03:31,960
Así que, y esto se deriva de XR.Labs Operation Group.

29
00:03:31,960 --> 00:03:39,800
Estoy aquí en el ICCBS, mira. Estamos construyendo mundos virtuales.

30
00:03:39,800 --> 00:03:47,800
Uno de ellos, un vehículo que viaja por el mundo virtual, es nuestro ICCBS, 

31
00:03:47,800 --> 00:03:55,000
Estudio Intergaláctico de Comunicación del Caos. Y ha atracado en Overte para este evento.

32
00:03:55,000 --> 00:04:02,760
Ahí lo tienes, ves, ves, está bastante maltrecho. Eso es porque hemos tenido algunos accidentes aquí.

33
00:04:02,760 --> 00:04:10,600
Y todos los bonitos accesorios interiores no estaban bien fijados, por lo que se estropearon un poco. 

34
00:04:10,600 --> 00:04:17,120
Pero lo estamos arreglando de maravilla.

35
00:04:17,120 --> 00:04:23,920
Vamos, te mostraré algo. Porque queremos hacer algunas especialidades en las esquinas.

36
00:04:23,920 --> 00:04:31,960
No sé quién va a reconocer esto, los que han estado en

37
00:04:31,960 --> 00:04:39,680
 la C-Base lo reconocerá. Son paredes de luz, hechas de botellas de mate.

38
00:04:39,680 --> 00:04:49,760
Y aquí estamos viendo, por ejemplo, la construcción de lo que se llama un Gemelo Digital de este dispositivo.

39
00:04:49,760 --> 00:04:55,120
Y aquí están las instrucciones para construir la interfaz de programación de aplicaciones para que sepas cómo usarla. 

40
00:04:55,120 --> 00:05:01,480
Aquí tienes esta página web, puedes mirarla e incluso leerla. Y aquí tienes un ejemplo de cómo podrías utilizar este espacio.

41
00:05:01,480 --> 00:05:08,920
Pongamos esta pantalla LED aquí.

42
00:05:08,920 --> 00:05:15,120
Vuelo un poco. Aquí también puedes volar. Tengo que practicar un poco el vuelo. 

43
00:05:15,120 --> 00:05:21,880
Wow, ahora estoy volando aquí. Mira aquí.  Todavía hay un agujero aquí, ¿ves? Oh, está roto otra vez. 

44
00:05:21,880 --> 00:05:26,680
Aquí tenemos que arreglarlo un poco más. Esa es otra tarea. Es así por los accidentes de aterrizaje.

45
00:05:26,680 --> 00:05:32,520
Otra vez aquí. Acabo de pasar un portal y este es nuestro vestíbulo de salida.

46
00:05:32,520 --> 00:05:42,920
Ahora podemos subir al zepelín. Oye, la tierra está girando. Guay...

47
00:05:42,920 --> 00:05:51,920
 podemos hacerlo... muy guay. Ahora podemos ver el ICCBS desde fuera. Entonces

48
00:05:51,920 --> 00:05:58,600
aquí, dentro del zepelín. Y también hay un portal. ¿A ver adónde nos lleva? Vamos adentro.

49
00:05:58,600 --> 00:06:08,400
Ah, sí, eso nos lleva de vuelta. Ah, ahí es donde cambiaron los portales. Es bueno que hayamos hablado de ello.

50
00:06:08,400 --> 00:06:13,720
En realidad, el portal que acabo de atravesar debería conducir aquí. 

51
00:06:13,720 --> 00:06:23,080
Echemos un vistazo. ¿Qué es eso? Mira, un oscuro... Mira, es el Polvo de Hadas.

52
00:06:23,080 --> 00:06:33,160
No me lo puedo creer. Es la primera vez que lo veo. Vaya, no está nada mal. De hecho, lo es, 

53
00:06:33,160 --> 00:06:39,880
¿Siguen girando las cosas ahí arriba? Ya no giran. Sí, giran muy lentamente. 

54
00:06:39,880 --> 00:06:44,960
Aquí vemos el nodo de datos en diferentes representaciones artísticas. Y tenemos una fogata.

55
00:06:44,960 --> 00:06:56,560
Y está crepitando. Esto es muy acogedor. Espero que nos encontremos aquí a menudo.

56
00:06:56,560 --> 00:07:02,720
Bueno, si ando por aquí, también puedo ir al sótano de Jev22.

57
00:07:02,720 --> 00:07:13,960
Y el sótano tiene este aspecto. Whoa, whoa, whoa, whoa. Miremos aquí. 

58
00:07:13,960 --> 00:07:20,520
Estos son todos los eventos, las páginas de inicio de los eventos. Puedes verlos todos.

59
00:07:20,520 --> 00:07:27,240
Hay muchos. Sí, miremos aquí. Hackear en paralelo. Y cuando te acercas, hay un sitio realmente interactivo.

60
00:07:27,240 --> 00:07:33,000
Puede hacer clic en este y hojearlo. Y funciona para los 38 eventos.

61
00:07:33,000 --> 00:07:42,640
Puede consultarlas todas y participar.

62
00:07:42,640 --> 00:07:54,120
Chili con caos . Sí, aquí puedes ver Chili con Chaos. El programa es muy interesante. 

63
00:07:54,120 --> 00:08:01,480
Puedes utilizar Places para volar a nuestro Makerspace.  

64
00:08:01,480 --> 00:08:11,680
El Makerspace se llama simplemente XRelog22. Y este makerspace es ahora una especie de colección de cosas inacabadas. 

65
00:08:11,680 --> 00:08:17,120
de cosas inacabadas. Empezamos las cosas ahí, en un orden disperso. 

66
00:08:17,120 --> 00:08:25,840
por ejemplo, tengo la idea, para las 15 pistas, así que 15 temas, que tenemos. ¿Y si hacemos una ronda de pistas.

67
00:08:25,840 --> 00:08:38,800
construir 15 pisos. Sí, muy guay. También quiero poder volar. Sí, volar es una buena idea.

68
00:08:38,800 --> 00:08:45,840
A primera vista, estas plantas parecen pequeñas salas de embarque. 

69
00:08:45,840 --> 00:08:53,800
Muy pequeño y muy mini. Y en cada una de estas plantas hay un tema dedicado. 

70
00:08:53,800 --> 00:09:02,200
Empezamos a subir a la tercera planta.

71
00:09:02,200 --> 00:09:09,480
Ya sea podcasting, SocialVR o comunidades XR, hay un montón de ellas y así sucesivamente.

72
00:09:09,480 --> 00:09:16,680
Y, por supuesto, en las primeras plantas está (H)Activismo.

73
00:09:16,680 --> 00:09:28,240
Este es, por supuesto, nuestro tema principal.

74
00:09:28,240 --> 00:09:36,160
Espera, estoy volando allí. Está escrito ahí, pero la lámpara no está encendida.

75
00:09:36,160 --> 00:09:42,040
Moví la lámpara a alguna parte. Ahora no se enciende en absoluto.

76
00:09:42,040 --> 00:09:48,800
En general, aquí está muy claro. Podemos leer esto.
No, el hectivismo está en la oscuridad. 
sobre la ética de los hackers.

77
00:09:48,800 --> 00:09:55,160
Mira por aquí. Esta es una de las primeras cosas que hice.

78
00:09:55,160 --> 00:10:02,920
Dibujé la ética hacker en 3D en realidad virtual con un multipincel. 

79
00:10:02,920 --> 00:10:11,240
Y luego integré esta pintura como un modelo 3D aquí. E incluso he integrado una síntesis de voz, en alemán e inglés.

80
00:10:11,240 --> 00:10:18,160
Bueno, lo que descubrí investigando sobre ética hacker,

81
00:10:18,160 --> 00:10:24,600
es que las seis primeras reglas de los hackers son más antiguas,

82
00:10:24,600 --> 00:10:30,440
datan de los años 60 y sólo dos reglas fueron añadidas por el Computer Chaos Club

83
00:10:30,440 --> 00:10:36,760
En particular, no te metas con los datos de los demás.

84
00:10:36,760 --> 00:10:43,840
Utilizar los datos públicos, proteger los datos privados. Creo que ya lo hemos oído antes. 

85
00:10:43,840 --> 00:10:50,520
Estos son los dos complementos del Computer-Chaos-Club. Nos hemos tomado la libertad de añadir una novena regla.

86
00:10:50,520 --> 00:11:00,720
Piensa siempre en trozos y árboles. Porque el aspecto ecológico aún no forma parte de la ética de los hackers. 

87
00:11:00,720 --> 00:11:07,280
Por eso debe completarse. 

88
00:11:07,280 --> 00:11:14,320
Esa era nuestra ética hacker. Si no, ya ves, así que es una colección de todo tipo de cosas.

89
00:11:14,320 --> 00:11:17,560
Puedes ver el zepelín en la parte superior.

90
00:11:17,560 --> 00:11:24,880
Sobre el zepelín, hay una primera versión del ICCBS. 

91
00:11:24,880 --> 00:11:30,440
Aquí hay bandejas. Utilizaremos estas bandejas cuando organicemos talleres.

92
00:11:30,440 --> 00:11:36,280
Entonces podemos volar a temas y escuchar cosas. 

93
00:11:36,280 --> 00:11:43,160
Jugueteamos tan sencillamente. Este es nuestro makerspace virtual.

94
00:11:43,160 --> 00:11:52,040
No tiene ningún vínculo temático. 

95
00:11:52,040 --> 00:11:59,000
Cuando terminemos el Tracktower, será transferido a su propio servidor de dominio. 

96
00:11:59,000 --> 00:12:07,320
Luego lo sacamos del makerspace y construimos algo nuevo. 

97
00:12:07,320 --> 00:12:20,400
Ahora vuelvo al Estudio Intergaláctico de Comunicación del Caos. ICCBS para abreviar. 

98
00:12:20,400 --> 00:12:31,080
Echa un vistazo. De dónde vengo. Así que cualquiera puede cambiar la ubicación, ¿quién está en el sistema?

99
00:12:31,080 --> 00:12:37,960
Sí, todo está abierto. Ni siquiera tienes que registrarte. 

100
00:12:37,960 --> 00:12:44,560
Ni siquiera necesitas crear una cuenta. Basta con descargar el cliente y ejecutarlo para entrar en este mundo.

101
00:12:44,560 --> 00:12:49,320
Recibirá una breve explicación sobre su funcionamiento.

102
00:12:49,320 --> 00:12:55,960
Entonces ya puedes utilizar la función Lugares. Así podrás descubrir aún más mundos.

103
00:12:55,960 --> 00:13:07,080
Así que ICCBS y Fairy Dust y XRelog o más tarde también Tracktower.

104
00:13:07,080 --> 00:13:14,040
Son sólo tres mundos entre docenas de mundos.

105
00:13:14,040 --> 00:13:18,240
Probablemente pronto habrá cientos de ellos para visitar.

106
00:13:18,240 --> 00:13:27,520
Por supuesto, también hay mundos que no están abiertos al público, como un Darling VR. 

107
00:13:27,520 --> 00:13:32,520
Tiene un poco más de color sexual. No queremos que pueda entrar cualquiera. 

108
00:13:32,520 --> 00:13:39,280
Por lo demás, la mayoría de los mundos están abiertos al público, 

109
00:13:39,280 --> 00:13:49,200
libremente, sin chorradas, sin NFT, smart contract, blockchain, cripto-truc. 

110
00:13:49,200 --> 00:13:52,560
Todo esto no está aquí. Es precioso.

111
00:13:52,560 --> 00:14:09,880
Anchoray, nos has enseñado tantas cosas de este mundo. 

112
00:14:09,880 --> 00:14:12,960
Pero, ¿qué piensa hacer en el Congreso? No has dicho nada al respecto.

113
00:14:12,960 --> 00:14:25,080
Sí, así es. Tenemos 15 sesiones y 15 temas.

114
00:14:25,080 --> 00:14:36,440
y también estamos viendo los otros 38 eventos JEV22. Nuestro tema principal es la crítica de la tecnología. 

115
00:14:36,440 --> 00:14:42,240
Clara crítica a la tecnología. No queremos que Facebook se salga con la suya.

116
00:14:42,240 --> 00:14:48,800
Queremos hablar de la Web 3.0. Queremos analizar críticamente cómo se está desarrollando esto.

117
00:14:48,800 --> 00:14:53,520
Hemos preparado unas sesiones muy interesantes para usted.

118
00:14:53,520 --> 00:14:58,280
A continuación, queremos mirar más allá del final de nuestra nariz y fijarnos también en otros acontecimientos de la JEV.

119
00:14:58,280 --> 00:15:04,920
Hay iniciativas absolutamente fantásticas, desde Nueva York hasta Atenas. 

120
00:15:04,920 --> 00:15:12,560
En Múnich hay dos, en Hamburgo hay cuatro en total. En Berlín, en Erlangen, en Düsseldorf, en Aquisgrán.

121
00:15:12,560 --> 00:15:15,800
Hay mucho por descubrir.

122
00:15:15,800 --> 00:15:17,280
Pero, ¿será tan divertido?

123
00:15:17,280 --> 00:15:23,640
¡Qué divertido! Sí, por supuesto. Es cierto que no tenemos Jeopardy,

124
00:15:23,640 --> 00:15:29,080
no hemos conseguido preparar más preguntas bonitas para él. 

125
00:15:29,080 --> 00:15:38,400
Pero estamos tratando de hacer algo así de loco. Vamos a llamarla Copa del Mundo Mafifa 2030. Vamos a repetir el sorteo de la Copa del Mundo de 2030.

126
00:15:38,400 --> 00:15:44,920
Deja que te sorprenda. Cada vez a las 23.00 horas el miércoles, jueves y viernes.

127
00:15:44,920 --> 00:15:47,400
Esto va a ser interesante.

128
00:15:47,400 --> 00:15:50,600
Debes estar haciendo esto de una manera muy corrupta.

129
00:15:50,600 --> 00:15:57,840
¿El mate cuenta como moneda?

130
00:15:57,840 --> 00:16:05,720
Si no cuenta como moneda, tampoco somos corruptos. 

131
00:16:05,720 --> 00:16:12,880
Lo importante es que tenemos muy, muy pocas presentaciones frontales.

132
00:16:12,880 --> 00:16:19,640
Casi todo es un taller participativo, diseñado como un Barcamp. 

133
00:16:19,640 --> 00:16:28,080
Hay un barcamp XR, justo después de esta inauguración. Pero también hay un Bits and Trees.

134
00:16:28,080 --> 00:16:34,200
Fireside Chat, donde está cordialmente invitado a participar.

135
00:16:34,200 --> 00:16:39,760
También los talleres de Overte, donde exploramos este mundo, 

136
00:16:39,760 --> 00:16:41,080
también están concebidos como talleres, no como conferencias.

137
00:16:41,080 --> 00:16:50,760
¿Quiere repetir cómo accedemos al mundo? 

138
00:16:50,760 --> 00:16:57,800
Porque vemos este mundo realmente grandioso, pero ¿cómo entramos en él? 

139
00:16:57,800 --> 00:16:58,800
¿Podemos visitarle?

140
00:16:58,800 --> 00:17:04,520
Sí, por supuesto. Así que lo principal es que estamos en casa en Matrix. 

141
00:17:04,520 --> 00:17:11,960
En la programación Pretalx, se indica un canal Matrix para cada presentación. 

142
00:17:11,960 --> 00:17:16,760
Aquí es donde nos reunimos y también intercambiamos toda la información. 

143
00:17:16,760 --> 00:17:29,820
La otra cosa es este Metaverso, este Multiverso Overte.

144
00:17:29,820 --> 00:17:35,520
Overte.org ya es la página de inicio.
 Puede descargar un cliente allí.

145
00:17:35,520 --> 00:17:42,880
Existe para Linux y para Windows. Curiosamente, no está disponible para Apple. 

146
00:17:42,880 --> 00:17:51,840
También puede utilizarlo para visitarnos en el ICCBS, en el bosque Fairy Dust. 

147
00:17:51,840 --> 00:17:57,160
o en un hackerspace virtual en el xrelog con la Track Tower. 

148
00:17:57,160 --> 00:17:58,160
Puede explorar todo esto usted mismo.

149
00:17:58,160 --> 00:18:08,760
Eso suena emocionante y creo, 

150
00:18:08,760 --> 00:18:11,600
a algunos oyentes también les resultará muy emocionante conocerle en el espacio 3D.

151
00:18:11,600 --> 00:18:17,640
Tengo curiosidad por saberlo. ¿Cuántos espectadores colapsarán los servidores? 

152
00:18:17,640 --> 00:18:19,040
lo descubriremos juntos.

153
00:18:19,040 --> 00:18:20,040
Probando, probando.

154
00:18:20,040 --> 00:18:26,200
También podemos aprovisionar otros servidores. Pero aún no estamos automatizados. 

155
00:18:26,200 --> 00:18:32,280
aprovisionar otros servidores. Pero aún no estamos muy automatizados.

156
00:18:32,280 --> 00:18:36,840
Lleva un poco de tiempo, pero estamos preparados. 

157
00:18:36,840 --> 00:18:38,080
Si caemos ahora, aún podemos hacer algo.

158
00:18:38,080 --> 00:18:45,520
Por último, me gustaría despedirme cordialmente. 

159
00:18:45,520 --> 00:18:51,240
Por cierto, también de, eh, eh, eh, ¿quién eres exactamente?

160
00:18:51,240 --> 00:18:56,560
Lo siento Anchoray, no puedo decírtelo.

161
00:18:56,560 --> 00:19:25,560
Así que tendremos que resolver este rompecabezas la próxima vez. Hasta luego. Adiós.
