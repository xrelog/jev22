1
00:00:00,000 --> 00:00:07,000
Dritter Raum. Dezentrale virtuelle Welten auf Matrix.

2
00:00:07,000 --> 00:00:14,000
Es ist nur die erste technische Vorschau, aber was wir schon gesehen haben, 
macht Appetit auf mehr.

3
00:00:14,000 --> 00:00:21,000
Stellen Sie sich vor, Sie sind in der Matrix, haben Ihren Chat, führen Video- oder Telefongespräche, 
und jetzt bekommst du eine neue Dimension.

4
00:00:21,000 --> 00:00:26,000
Thirdroom.io Vollständig webbasiert, in Matrix integriert.

5
00:00:26,000 --> 00:00:32,000
Robert Long, Chefentwickler von Thirdroom, gibt uns einen ersten Einblick.

6
00:00:32,000 --> 00:00:36,000
Bitte begrüßen Sie Robert Long.

7
00:00:36,000 --> 00:00:40,000
Hallo zusammen, mein Name ist Robert Long 
und dies ist der dritte Raum.

8
00:00:40,000 --> 00:00:44,000
Thirdroom ist eine Plattform für virtuelle Welten, die 
die auf dem Matrix-Protokoll basiert.

9
00:00:44,000 --> 00:00:53,000
Ein dezentralisiertes, Ende-zu-Ende-verschlüsseltes 
Kommunikationsprotokoll, das für Messaging, Sprache 
Sprache, Video und jetzt auch 3D-Welten.

10
00:00:53,000 --> 00:00:58,000
Wie du siehst, befinde ich mich gerade in einer virtuellen Welt, 
und in dieser Welt ist ein anderer Avatar.

11
00:00:58,000 --> 00:01:02,000
Ich kann durch die virtuelle Welt gehen und 
mit anderen Menschen sprechen.

12
00:01:02,000 --> 00:01:12,000
Normalerweise habe ich ein paar andere Leute in der Demo 
so dass man sie tatsächlich sprechen hören kann, 
aber im Moment habe ich hier Robert Test als Puppe.

13
00:01:12,000 --> 00:01:24,000
In der Überlagerung, drücken Sie Escape, können Sie 
sehen Sie, dass ich eine Reihe von verschiedenen Welten habe, einschließlich 
die Welt, in der wir uns gerade befinden,

14
00:01:24,000 --> 00:01:28,000
die eine spezielle Version einer Umgebung ist, die wir 
wir Terra nennen.

15
00:01:28,000 --> 00:01:39,000
All diese verschiedenen Räume sind virtuelle Welten 
denen ich beitreten kann, ähnlich wie man 
einem typischen Chatraum beitreten.

16
00:01:39,000 --> 00:01:46,000
Wenn ich hier ganz bis zum Ende scrolle, 
können Sie sehen, dass wir die XR.Labs Lounge genau hier haben.

17
00:01:46,000 --> 00:01:49,000
Wenn ich zurück scrolle, können Sie alle Nachrichten sehen 
die hier reinkommen.

18
00:01:49,000 --> 00:01:59,000
Dies ist nur ein Standard-Matrix-Client, aber er kann auch 
die Möglichkeit, virtuelle Welten zu durchsuchen.

19
00:01:59,000 --> 00:02:06,000
Im Moment kommunizieren diese beiden Clients 
über eine Peer-to-Peer-WebRTC-Verbindung, die über 
über Matrix.

20
00:02:06,000 --> 00:02:12,000
Alles, was wir in dieser Welt tun, geschieht ohne 
ohne zusätzliche Server über unseren Standard-Matrix 
Home-Server.

21
00:02:12,000 --> 00:02:18,000
Jeder, der ein Matrix-Konto hat, hat auch ein Thirdroom 
Konto.

22
00:02:18,000 --> 00:02:25,000
Wie auch immer, ich denke, diese Leute sind bereits ziemlich 
mit Matrix vertraut, also lasst uns ein bisschen 3D-Zeug machen.

23
00:02:25,000 --> 00:02:38,000
Thirdroom verwendet ein paar neue Browser 
Technologien, wie zum Beispiel den Shared Array Buffer 
und Atomics APIs,

24
00:02:38,000 --> 00:02:41,000
was uns erlaubt, Multithreading im Browser zu betreiben.

25
00:02:41,000 --> 00:02:50,000
Wir haben eine ganz neue, browserbasierte 
Browser-basierte Spiel-Engine auf der Grundlage dieser Primitive.

26
00:02:50,000 --> 00:02:57,000
Die Shared Array Buffer API und die Atomics APIs 
sind jetzt in Firefox, Chrome und Safari verfügbar.

27
00:02:57,000 --> 00:03:05,000
Bei den meisten 3D-Webanwendungen, die es 
läuft heute alles auf einem einzigen Thread.

28
00:03:05,000 --> 00:03:11,000
Dies führt zu unterbrochenen Frames aufgrund von verspäteten 
WebGL-Übertragungen,

29
00:03:11,000 --> 00:03:20,000
Abwürgen des UI-Threads beim Hochladen von Texturen 
Texturen auf die GPU, und viel weniger Raum für das Hinzufügen 
Spaß wie Physik und Animationen.

30
00:03:20,000 --> 00:03:27,000
Thirdroom verwendet drei Threads, den Hauptbrowser 
Thread und zwei verschiedene Webworker.

31
00:03:27,000 --> 00:03:33,000
Der Hauptthread führt unsere React.js UI aus, 
sowie unser Matrix-Client,

32
00:03:33,000 --> 00:03:39,000
der die Echtzeit-Audio- und Spielvernetzung 
über WebRTC abwickelt.

33
00:03:39,000 --> 00:03:46,000
Der nächste Webworker ist unser Spiel-Thread, 
der unsere Spiellogik, Animationen 
und Physik mit Rapier ausführt,

34
00:03:46,000 --> 00:03:51,000
eine Physik-Engine, die in Rust geschrieben und 
kompiliert zu WebAssembly.

35
00:03:51,000 --> 00:04:01,000
Und schließlich haben wir unseren Render-Thread, 
der eine modifizierte Version von 3GS ausführt, 
die WebGL für das Rendering verwendet.

36
00:04:01,000 --> 00:04:11,000
Unser Spielzustand wird in einer sperrfreien Datenstruktur 
Datenstruktur gehalten, die Triple Buffer genannt wird, was es uns ermöglicht 
unseren Spiel-Thread unabhängig von einem 
von einem Render-Thread.

37
00:04:11,000 --> 00:04:20,000
In Browsern können wir unsere Bildwiederholrate nicht einfach begrenzen. 
Bildwiederholrate nicht einfach begrenzen, daher hilft uns dies bei der Unterstützung 
Monitore mit hoher Bildwiederholrate,

38
00:04:20,000 --> 00:04:25,000
wo wir den Spielthread und den Renderthread 
Thread mit zwei völlig unabhängigen Raten laufen lassen können.

39
00:04:25,000 --> 00:04:31,000
Kommen wir nun zu einigen Details der 
der Szene. Also, wir haben den Robert Test 
Avatar hier drüben,

40
00:04:31,000 --> 00:04:39,000
und ich kann durch die Welt laufen und ich kann 
ein paar Würfel werfen.

41
00:04:39,000 --> 00:04:47,000
Um also die Renderingkosten für diese Umgebung 
dieser Umgebung zu sparen, machen wir viel 
Instanzierung verwendet.

42
00:04:47,000 --> 00:04:53,000
Ihr seht eine Menge sich wiederholender Elemente in 
dieser Szene, die alle in einem Durchgang gerendert 
in einem Durchgang gerendert.

43
00:04:53,000 --> 00:04:59,000
Wir haben auch Lichtkarten in dieser Szene gebacken 
Szene gebacken und Unterstützung für Reflexionssonden hinzugefügt.

44
00:04:59,000 --> 00:05:07,000
Wenn ich diesen Würfel spawne und ihn dann hierher bringe 
kann man sehen, wie sich die Beleuchtung ändert 
wenn ich mich in und aus dieser Tür bewege.

45
00:05:07,000 --> 00:05:16,000
Und das ist alles gebacken. Und wir haben das zu 3GS hinzugefügt, 
was uns eine genauere Beleuchtung für dynamische 
Objekte, die sich in der Szene bewegen,

46
00:05:16,000 --> 00:05:24,000
und trotzdem teure Kosten für die Echtzeit-Beleuchtung sparen 
Echtzeit-Beleuchtung über die gesamte Bandbreite der Hardware 
die wir unterstützen müssen.

47
00:05:24,000 --> 00:05:31,000
Um diese Assets zu produzieren, haben wir also eine 
eine Export-Pipeline von Unity erstellt, die ich jetzt 
umklappen.

48
00:05:31,000 --> 00:05:37,000
Und hier könnt ihr dieselbe Szene sehen, die wir 
die wir in Thirdroom geöffnet hatten, jetzt in Unity geöffnet.

49
00:05:37,000 --> 00:05:46,000
Also, lasst uns auf einige dieser Reflexionssonden klicken 
Reflexionssonden, wie die, die wir in der 
dieser Szene vorher benutzt haben.

50
00:05:46,000 --> 00:05:51,000
Also, da ist eine genau dort, und 
eine genau hier. Du kannst die kleinen 
Reflektionen auf dieser Kugel hier.

51
00:05:51,000 --> 00:05:55,000
Das ist das, was in der Umgebung gespeichert wird 
Umgebung gespeichert.

52
00:05:55,000 --> 00:05:59,000
Und auf der Registerkarte "Beleuchtung" kann man 
hier, können wir die Vorschau öffnen.

53
00:05:59,000 --> 00:06:08,000
Dies ist die Light Map, die aus Unity heraus gebacken 
Unity gebacken und durch die Unity GLTF Pipeline exportiert wurde.

54
00:06:08,000 --> 00:06:17,000
Die Szene wird also als GLTF exportiert. GLTF ist 
dieses 3D-Dateiformat, das von der Khronos Group entwickelt wurde.

55
00:06:17,000 --> 00:06:27,000
Wir sind selbst Mitglieder der Khronos Group. 
Und es gibt einen Exporter im Unity-Ökosystem 
namens Unity GLTF.

56
00:06:27,000 --> 00:06:33,000
Und wenn du gehst, musst du das Paket installieren, 
und dann wird es unter der Registerkarte Assets verfügbar sein.

57
00:06:33,000 --> 00:06:39,000
Und du kannst die aktive Szene einfach exportieren als eine 
GLTF oder eine GLB exportieren.

58
00:06:39,000 --> 00:06:49,000
Und nachdem du unseren Thirdroom-Exporter installiert hast, 
exportiert er all diese verschiedenen 
GLTF-Erweiterungen, die wir erstellt haben.

59
00:06:49,000 --> 00:07:00,000
Einige der GLTF-Erweiterungen, an denen wir gearbeitet haben 
sind die Lightmap-Erweiterung und die 
Reflection Probe Erweiterungen.

60
00:07:00,000 --> 00:07:07,000
Und wir haben auch mit anderen 
Standardisierungsgremien zusammengearbeitet, wie z.B. mit der Khronos Group 
an der GLTF-Audio-Erweiterung,

61
00:07:07,000 --> 00:07:15,000
und mit der Open Metaverse Interoperability Group 
an einer Collider-Erweiterung für Physik und 
Kollisionen mit den verschiedenen Meshes in der Szene.

62
00:07:15,000 --> 00:07:26,000
All das wird also aus Unity exportiert, und dann 
haben wir eine weitere Post-Processing-Pipeline, die 
die wir in ein browserbasiertes Tool eingebaut haben.

63
00:07:26,000 --> 00:07:37,000
Und das komprimiert Texturen mit dem 
Basis Universal Texture Encoder, wodurch die 
für den Download, aber auch für die GPU kleiner 
auf der GPU.

64
00:07:37,000 --> 00:07:43,000
Und dann wird es in diesem KTX-Container gespeichert 
und serviert ihn dann zusammen mit der GLTF-Datei.

65
00:07:43,000 --> 00:07:49,000
Es nimmt auch doppelte Mesh-Referenzen 
und wandelt sie in Instanznachrichten um.

66
00:07:49,000 --> 00:07:56,000
Das ist es also, was mit all diesen 
doppelten Objekten hier. All diese Daten 
werden in die GLTF eingebacken.

67
00:07:56,000 --> 00:08:07,000
Innerhalb unserer Welt kannst du einige Portale 
zu einigen anderen Welten. Gehen wir also durch 
dieses Portal hier drüben.

68
00:08:07,000 --> 00:08:13,000
Hier sind wir also in der Matrix in Matrix. Ziemlich meta.

69
00:08:13,000 --> 00:08:23,000
In dieser Szene habe ich also einen interaktiven Fernseher, 
und wenn ich darauf klicke, dann verwandelt sich alles 
verwandelt sich alles in den Code der Matrix.

70
00:08:23,000 --> 00:08:33,000
Dies geschah also durch eine skriptfähige Interaktion. 
Schauen wir uns also den Code dafür an.

71
00:08:33,000 --> 00:08:42,000
Hier in meinem Coder kannst du ein kleines Stück JavaScript sehen 
kleines Stück JavaScript, und ich bekomme das 
TV Objekt in der Szene.

72
00:08:42,000 --> 00:08:47,000
Ich mache es interaktiv, indem ich seine 
Eigenschaft interaktiv.

73
00:08:47,000 --> 00:08:51,000
Ich habe ein kleines Stück Status, das sagt, ob 
oder nicht, ob der Fernseher eingeschaltet ist.

74
00:08:51,000 --> 00:09:00,000
Und dann überprüfen wir in jedem Frame, ob das 
interaktive Taste gedrückt wird. Wir schalten den Status um 
und dann schalten wir das Matrixmaterial um.

75
00:09:00,000 --> 00:09:10,000
Das ist also ein ziemlich einfaches Beispiel. Schauen wir uns 
ein wenig fortgeschrittener aus.

76
00:09:10,000 --> 00:09:21,000
In diesem Beispiel habe ich also zwei verschiedene 
Materialien, und wenn ich diesen Knopf drücke, 
tauscht er die Materialien aus.


77
00:09:21,000 --> 00:09:29,000
Und hier drin habe ich ein Licht, das aus- und eingeschaltet werden kann.

78
00:09:29,000 --> 00:09:38,000
Jetzt denkst du vielleicht, da sind 
die Skripte, die laufen innerhalb der Matrix.

79
00:09:38,000 --> 00:09:46,000
Wie kann man das auf sichere Weise ausführen? 
Und wir haben unsere Skripting-Engine 
um WebAssembly herum gebaut.

80
00:09:46,000 --> 00:09:56,000
Diese API nennen wir also WebSceneGraph, 
und es ist eine JavaScript- und WebAssembly-basierte API.

81
00:09:56,000 --> 00:10:04,000
Der gesamte Code läuft innerhalb eines sicheren 
WebAssembly-Kontext und ist vollständig isoliert 
von den übrigen Inhalten des Clients isoliert.

82
00:10:04,000 --> 00:10:12,000
Der Matrix-Client läuft also in einem anderen Thread. Dieser befindet sich dann innerhalb eines WebAssembly-Moduls,

83
00:10:12,000 --> 00:10:19,000
das nur über den API-Zugriff verfügt, den wir 
die wir über die WebSceneGraph-API zur Verfügung stellen.

84
00:10:19,000 --> 00:10:34,000
Schauen wir uns also den Code für diese Szene an. 
Blättern Sie zurück zu meinem Code-Editor, und Sie können sehen 
hier ist die JavaScript-basierte Version des Skripts.

85
00:10:34,000 --> 00:10:41,000
Und wir können sehen, dass wir die Knöpfe abfragen, 
die Würfel, die verschiedenen Texturen.

86
00:10:41,000 --> 00:10:46,000
Und wir fragen auch nach den verschiedenen 
Schalter und Lichter.

87
00:10:46,000 --> 00:10:53,000
Und dann können wir jedes einzelne Bild sehen. 
Wir fragen ab, ob der Materialknopf 
Knopf gedrückt ist und wir können einen Zustand umschalten.

88
00:10:53,000 --> 00:10:59,000
Wir durchlaufen eine Schleife durch alle Mesh-Primitive. 
Das sind all die verschiedenen Submeshes dieses Würfels.

89
00:10:59,000 --> 00:11:06,000
In diesem Fall gibt es nur eines, aber wir 
durchlaufen es in einer Schleife. Und wir haben die 
Textur dort.

90
00:11:06,000 --> 00:11:13,000
Für die Lichter stellen wir die Eigenschaft Intensität ein. 
Wenn sie eingeschaltet ist, ist sie 20. Wenn sie ausgeschaltet ist, ist sie 0.

91
00:11:13,000 --> 00:11:18,000
Und da es sich um WebAssembly handelt, können wir 
genau dasselbe Beispiel in C sehen.

92
00:11:18,000 --> 00:11:23,000
Hier haben wir also die gleichen Abfragen. Wir haben 
die gleiche Aktualisierungsfunktion.

93
00:11:23,000 --> 00:11:32,000
Und wir drehen einfach das Material oder die Intensität um 
die Intensität basierend auf diesem interaktiven Zustand.

94
00:11:32,000 --> 00:11:37,000
Und wenn du dich mit Webentwicklung beschäftigt hast, wird dir das 
kommt dir das vielleicht ein bisschen bekannt vor.

95
00:11:37,000 --> 00:11:43,000
So wie JavaScript die Möglichkeit eröffnet hat 
reichhaltige Anwendungen gegen HTML zu programmieren mit 
der DOM-API,

96
00:11:43,000 --> 00:11:50,000
glauben wir, dass die WebSceneGraph API 
das Potential hat, Interaktivität zu 
GLTF-Dokumenten hinzuzufügen,

97
00:11:50,000 --> 00:11:58,000
um portable 3D-Erlebnisse zu ermöglichen 
die innerhalb bestehender oder völlig neuer 
3D-Anwendungen oder völlig neuen Anwendungen ausgeführt werden können.

98
00:11:58,000 --> 00:12:04,000
Und das ist der letzte Teil meiner interaktiven 
Teil dieser Demo.

99
00:12:04,000 --> 00:12:08,000
Gehen wir zurück zu Firefox, gehen wir über Figma.

100
00:12:08,000 --> 00:12:14,000
Und ich werde euch ein bisschen zeigen, was 
was als nächstes in Thirdroom kommt.

101
00:12:14,000 --> 00:12:24,000
Also für unsere Skripting-API werden wir den Rest der 
den Rest der API Anfang 2023 mit 
Tech Preview 2.

102
00:12:24,000 --> 00:12:33,000
Und das wird den gesamten 
GLTF-Szenengraph als JavaScript- und 
WebAssembly-API.

103
00:12:33,000 --> 00:12:37,000
Auf diese Weise können Sie all diese Objekte manipulieren 
auf diese Weise manipulieren.

104
00:12:37,000 --> 00:12:41,000
Danach planen wir die Veröffentlichung einer 
High-Level-Netzwerk-API zu veröffentlichen,

105
00:12:41,000 --> 00:12:45,000
mit der man den Zustand des Szenegraphs 
Szenengraphen über das Netzwerk synchronisieren kann.

106
00:12:45,000 --> 00:12:50,000
Im Moment gibt es nur kleine Interaktionen 
die über das Netzwerk synchronisiert werden.

107
00:12:50,000 --> 00:12:54,000
Und dann werden wir die Physik-API hinzufügen.

108
00:12:54,000 --> 00:13:00,000
So kannst du interaktive Physikobjekte 
Physikobjekte innerhalb der Szene erstellen,

109
00:13:00,000 --> 00:13:07,000
die auf Kollisionen mit Ihnen reagieren werden, 
oder Sie können nach Kollisionen mit anderen Objekten 
anderen Objekten.

110
00:13:07,000 --> 00:13:13,000
Wir werden auch die skriptfähigen 
Avatare und Objekte hinzufügen.

111
00:13:13,000 --> 00:13:19,000
Man wird also Avatare nehmen können und ihnen 
Verhaltensweisen für den Avatar hinzufügen.

112
00:13:19,000 --> 00:13:26,000
Vielleicht wird es anfangen, die Augen werden anderen Benutzern folgen 
anderen Nutzern oder die Haare werden wie Federknochen 
Gräten haben.

113
00:13:26,000 --> 00:13:30,000
Viele Dinge, die die Leute in anderen 
in anderen VR-Plattformen zu erreichen,

114
00:13:30,000 --> 00:13:35,000
werden Sie in Thirdroom erreichen können 
mit interaktiven Skripten.

115
00:13:35,000 --> 00:13:40,000
Und dann für Objekte, ich meine, es gibt eine 
Millionen verschiedene Dinge, die man damit machen kann.

116
00:13:40,000 --> 00:13:45,000
Eines meiner Lieblingsdinge wäre, dass man 
wie ein Hauptschrank oder so etwas in der Art.

117
00:13:45,000 --> 00:13:53,000
Du könntest diese Spielhallenbox haben, sie in die Welt stellen 
in der Welt aufstellen und dann Arcade-Spiele 
innerhalb der Welt spielen.

118
00:13:53,000 --> 00:14:04,000
Und dann ist dieses Objekt tragbar 
so dass man es zwischen den verschiedenen 
Szenen in verschiedenen Thirdroom-Welten mitnehmen kann.

119
00:14:04,000 --> 00:14:09,000
Ein weiterer Teil der Tech Preview 2 
wird die Entdeckungsseite sein.

120
00:14:09,000 --> 00:14:16,000
Hier ist ein grundlegendes Konzept, das wir hatten, als wir 
als wir dieses Konzept erforschten.

121
00:14:16,000 --> 00:14:20,000
Das endgültige Rendering wird ein wenig anders aussehen 
anders aussehen.

122
00:14:20,000 --> 00:14:24,000
Und wie ich schon sagte, dies ist der Start von Tech Preview 2.

123
00:14:24,000 --> 00:14:30,000
Und es ist ein neuer Weg, 3D-Welten zu finden 
beitreten und erkunden.

124
00:14:30,000 --> 00:14:36,000
Es ist auch ein guter Weg, um bestehende 
Matrix-Räume zu finden, denen man innerhalb des 
Thirdroom-Client beizutreten.

125
00:14:36,000 --> 00:14:45,000
So wie wir also den XR-Log-Raum oder den 
Thirdroom-Raum gefunden haben, können wir ihn jetzt in diesem Register finden.

126
00:14:45,000 --> 00:14:48,000
Und eines der wichtigsten Dinge ist, dass wir 
vom Benutzer erstellte Szenen finden.

127
00:14:48,000 --> 00:14:56,000
Nachdem man also die Szene in 
Unity oder einem der anderen Tools erstellt hat, kann man 
sie auf dieser Entdeckungsseite veröffentlichen,

128
00:14:56,000 --> 00:15:00,000
und du kannst die Inhalte anderer Benutzer finden.

129
00:15:00,000 --> 00:15:04,000
Und das ist komplett dezentralisiert.

130
00:15:04,000 --> 00:15:08,000
Die erste Version wird sich nur auf unsere...

131
00:15:08,000 --> 00:15:16,000
Es wird ein Raum sein, der im Grunde 
der im Grunde die Inhalte enthält, die jeder dort einreicht.

132
00:15:16,000 --> 00:15:22,000
Und er basiert auf dieser Spezifikation, MSC3948, 
das ist die Spezifikation für den Repository-Raum.

133
00:15:22,000 --> 00:15:34,000
Und das sind Räume, und wenn man ihnen beitritt, 
erhält man Zugang zu den Inhalten und die 
die Möglichkeit, Inhalte einzureichen, damit sie genehmigt 
und vorzustellen.

134
00:15:34,000 --> 00:15:43,000
Und das Ziel ist, dass die Entdeckungsseite 
aus mehreren Repositorien beziehen wird, 
nicht nur aus unserem eigenen Repositorium 
wie wir es veröffentlichen werden.

135
00:15:43,000 --> 00:15:55,000
Und jeder Repository-Raum, dem Sie beigetreten sind 
wird in diese Entdeckungsseite einfließen und so 
einen Gesamtüberblick über alle Inhalte, die 
die in Ihren Gemeinschaften geteilt werden.

136
00:15:55,000 --> 00:16:03,000
Es ist also ein wirklich leistungsfähiges Konzept, vor allem 
wenn man es mit Matrix Spaces kombiniert, das 
wir hoffen, relativ bald auf den Markt bringen zu können.

137
00:16:03,000 --> 00:16:09,000
Man kann also Räume miteinander verbinden und alle 
Inhalte in allen Spaces sehen, denen man beigetreten ist.

138
00:16:09,000 --> 00:16:11,000
Eine wirklich coole Art, Inhalte zu finden.

139
00:16:11,000 --> 00:16:23,000
Und wenn wir die Repository-Räume auf die nächste Stufe bringen 
nächste Stufe bringen, werden wir in der Lage sein 
dezentralen Marktplatz für Inhalte schaffen.

140
00:16:23,000 --> 00:16:31,000
Dies ist also eine Erweiterung der Repository Rooms 
um die Möglichkeit zu schaffen, Inhalte zu kaufen und 
Inhalte zu kaufen und kryptographisch zu signieren.

141
00:16:31,000 --> 00:16:41,000
Und wir arbeiten noch an den Details der Spezifikation, 
aber das Ziel ist, dass der Marktplatz 
von Repository-Räumen gespeist wird, so dass alles 
in einer föderierten Weise gehostet wird.

142
00:16:41,000 --> 00:16:46,000
Es gibt keine Walled Gardens. Es gibt eine faire Preisgestaltung 
die vom Wettbewerb bestimmt wird.

143
00:16:46,000 --> 00:16:59,000
All die verschiedenen Marktplätze werden im Grunde 
eine einzige Benutzeroberfläche, um Inhalte auf all diesen 
Inhalte auf all diesen verschiedenen Servern zu finden 
denen man sich angeschlossen hat.

144
00:16:59,000 --> 00:17:07,000
Und die Schöpfer können für ihre Arbeit bezahlt werden, 
was super wichtig für jede Art von Plattform ist.

145
00:17:07,000 --> 00:17:14,000
Wir werden unseren Shop parallel zu all dem starten 
und wir müssen mit all den anderen Leuten 
mit all den anderen Leuten auf der Plattform konkurrieren.

146
00:17:14,000 --> 00:17:17,000
Aber wir denken, das ist eine wirklich gesunde Sache.

147
00:17:17,000 --> 00:17:22,000
Und die Entdeckung wird von den Gemeinschaften vorangetrieben 
der man beitritt.

148
00:17:22,000 --> 00:17:36,000
Wenn ich also interessiert bin an, ich weiß nicht, der 
die allgemeine Matrix-Community wird wahrscheinlich 
einen ganzen Haufen cooler Matrix-bezogener Räume.

149
00:17:36,000 --> 00:17:50,000
Ich kann also Orte finden, an denen ich mich über Matrix 
Features zu sprechen, aber ich könnte auch etwas beitreten, das mit 
zu meinen Lieblingsfilmen oder meinen Lieblingsvideospielen 
oder meine Interessen und so weiter.

150
00:17:50,000 --> 00:17:58,000
Und du kannst Leute, Inhalte, Räume und 
Räume, die mit diesen Dingen zu tun haben.

151
00:17:58,000 --> 00:18:11,000
Und jetzt? Was kommt als Nächstes? Also nach all diesen Dingen 
haben wir die Tech Preview 2, die mit Skripting 
mit Skripting, der Discovery-Seite 
Seite und autorisierenden Netzwerken.

152
00:18:11,000 --> 00:18:20,000
Dann führen wir benutzerdefinierte Avatare ein, WebXR 
und mobile Gamepad-Unterstützung und Input-Remapping.

153
00:18:20,000 --> 00:18:26,000
Das ist etwas, das schon lange gewünscht wurde 
seit langem gewünscht und wir müssen es hinzufügen.


154
00:18:26,000 --> 00:18:39,000
Die Bot-API, die in etwa der Bot-API für 
API für Matrix Chatbots, außer dass man jetzt 
einen echten virtuellen Agenten in deiner Welt 
in deiner Welt herumlaufen lassen kann, und dedizierte Server.

155
00:18:39,000 --> 00:18:56,000
Also ähnlich wie die Bot-API, nur dass es eine Instanz deiner Welt 
eine Instanz deiner Welt über einen langen 
über einen langen Zeitraum, um sie am Laufen zu halten und 
mit einer Art zusätzlicher Persistenzschicht 
Schicht, die über das hinausgeht, was wir mit Peer-to-Peer machen.

156
00:18:56,000 --> 00:19:01,000
Und dann der In-World-Editor. Das war etwas 
mit dem wir angefangen haben.

157
00:19:01,000 --> 00:19:12,000
Wenn ihr heute in Thirdroom die Tilde drückt, 
siehst du eine Art Baumansicht und du 
und du kannst all die verschiedenen Objekte in der Welt sehen 
und man sieht einen Umriss um sie herum, wenn man sie auswählt.

158
00:19:12,000 --> 00:19:25,000
Um den Rest des Editors auszubauen, 
mussten wir uns wirklich auf die Skripting-API konzentrieren 
und einige der Multithreading 
Datenstruktur zu erledigen.

159
00:19:25,000 --> 00:19:31,000
Aber jetzt ist das fast fertig und wir können 
mit der Arbeit am In-World-Editor beginnen.

160
00:19:31,000 --> 00:19:40,000
Wir wollen unsere Raumobergrenzen erhöhen und glücklicherweise 
ist die Arbeit an der selektiven Weiterleitungseinheit gut 
in vollem Gange und es gibt bereits einen Konzeptnachweis.

161
00:19:40,000 --> 00:19:49,000
Wenn Sie nach Waterfall, so glaube ich, dem Namen des 
Name des Projekts, dann finden Sie unsere selektive 
Weiterleitungseinheit.

162
00:19:49,000 --> 00:20:03,000
Und dann die Arbeit am dezentralen Marktplatz 
über den ich gesprochen habe, und damit ein Inventar 
so dass man tatsächlich diese Inhalte 
Inhalte, die man gekauft hat, abrufen kann,

163
00:20:03,000 --> 00:20:10,000
ebenso wie skriptfähige Objekte und Avatare, 
die in Ihrem Inventar verfügbar sein sollten 
und auch auf dem Marktplatz.

164
00:20:10,000 --> 00:20:17,000
Also wirklich konkurrieren und das ganze Metaverse vervollständigen, 
interoperables Metaverse-Bild.

165
00:20:17,000 --> 00:20:27,000
Und das ist alles, was ich habe. Wenn Sie also daran interessiert sind 
mitzumachen, können Sie unser Produkt 
live unter thirdroom.io.

166
00:20:27,000 --> 00:20:35,000
Du kannst uns auf GitHub finden. Du kannst dich uns anschließen auf 
Matrix in unserem thirdroom.dev Raum.

167
00:20:35,000 --> 00:20:39,000
Wir reden dort über alles, nicht nur über Entwicklung.

168
00:20:39,000 --> 00:20:49,000
Jeder, der daran interessiert ist, einen Beitrag zu leisten 
sollte sich über diesen Matrix-Raum melden, wenn ihr 
Kunst interessiert, egal ob es Avatare oder 
Umgebungen,

169
00:20:49,000 --> 00:20:58,000
oder wenn du an der Entwicklung interessiert bist, 
wir haben Grafikprogrammierung, Front End UI Zeug, 
eine Menge wirklich cooler Arbeit, die dort stattfindet.

170
00:20:58,000 --> 00:21:03,000
Und wir sind auf der Suche nach externen Mitwirkenden. 
Melde dich auf jeden Fall in unserem Matrix-Raum.

171
00:21:03,000 --> 00:21:09,000
Wie auch immer, ich danke Ihnen vielmals. Und ich freue mich darauf 
auf ein baldiges Gespräch mit Ihnen allen.
