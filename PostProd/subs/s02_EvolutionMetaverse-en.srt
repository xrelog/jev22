1
00:00:00,000 --> 00:00:02,000
You

2
00:00:30,000 --> 00:00:39,840
Hello everyone, I'm Ankur Rai. You see me in my dark alien suite. It's a little bit

3
00:00:39,840 --> 00:00:47,560
dark. And I want to present Darling. Darling had a nice talk for us about the evolution

4
00:00:47,560 --> 00:00:56,160
of Metaverse. And Darling was kind to give this to us because Felipe is in Brazil and

5
00:00:56,160 --> 00:01:14,360
can't attend his talk live. So, so please welcome Darling. Bye bye.

6
00:01:14,360 --> 00:01:42,840
So, a late night video when I can't sleep. Had something on my mind. You know, a lot

7
00:01:42,840 --> 00:01:48,200
of people, and I'm not going to say that my opinions are better or worse than anybody

8
00:01:48,200 --> 00:01:58,680
else's. It's just a thought. Tell me in the comment section down below what you guys think.

9
00:01:58,680 --> 00:02:10,560
Why isn't the Metaverse catching on like we think it should? And maybe a little bit of,

10
00:02:10,560 --> 00:02:15,600
you know, the evolution of the Metaverse. Maybe that will be the name of the video I'm

11
00:02:15,600 --> 00:02:22,520
going to make. The evolution of the Metaverse. But I'm going to compare it to the evolution

12
00:02:22,520 --> 00:02:35,320
of the Internet, right? So, the beginning of the Internet, right? Generally it was started

13
00:02:35,320 --> 00:02:48,040
by the government. And it was basically the early Internet was links between schools,

14
00:02:48,040 --> 00:02:55,840
educational institutions, stuff like that. And it was designed in a way to be decentralized

15
00:02:55,840 --> 00:03:05,160
on purpose because at the time it was designed they were still getting over the Cold War.

16
00:03:05,160 --> 00:03:15,460
Because of it being educational institutes and funded by the government, there wasn't

17
00:03:15,460 --> 00:03:23,940
large business yet that were… The planning of the Internet was not based around money-making

18
00:03:23,940 --> 00:03:31,320
opportunities and everybody's going to get rich. That came in later. But the Internet

19
00:03:31,320 --> 00:03:42,960
had already had a foothold before companies like AOL and all that came in. Servers were

20
00:03:42,960 --> 00:03:56,720
already a thing. Anybody that was a tech geek could run a server. Email was already part

21
00:03:56,720 --> 00:04:12,360
of the Internet. But there wasn't standards yet. But if you look back the early days of

22
00:04:12,360 --> 00:04:24,720
the Internet, the first things that were really important, right? They were the Googles, the

23
00:04:24,720 --> 00:04:43,800
search engines, Alta Vista and all those kind of things. Search.com. But that's what allowed…

24
00:04:43,800 --> 00:04:53,860
You could run a server but you could not… You had to be found somehow. And even today

25
00:04:53,860 --> 00:05:00,400
we can run servers. But if you're not in a major search engine, nobody's going to

26
00:05:00,400 --> 00:05:15,680
find you. You might as well not exist. But okay, so how can we kind of compare them with

27
00:05:15,680 --> 00:05:27,120
what's going on today? In AltSpaceVR you have a section, VRChat, the same, Sansar.

28
00:05:27,120 --> 00:05:39,200
You have an explorer, a go-to, that allows you to find destinations in the world. But

29
00:05:39,200 --> 00:05:48,020
the problem with that is it's completely controlled by the company that runs it. There

30
00:05:48,020 --> 00:05:55,900
is no financial incentive for them to decentralize because that is giving up power. It might

31
00:05:55,900 --> 00:06:05,860
give away their ability to monetize later. If you could just run your own server, they

32
00:06:05,860 --> 00:06:11,260
might literally just hand you the keys to the castle. They're not doing this to make

33
00:06:11,260 --> 00:06:18,060
your life easier. They're doing this to keep you from possibly being that person that

34
00:06:18,060 --> 00:06:32,060
comes up with a good idea. They want the monetization, not you. But that takes away from us. But

35
00:06:32,060 --> 00:06:40,540
you also had browsers. You could choose what browser you wanted to use. But the Metaverse

36
00:06:40,540 --> 00:06:52,020
doesn't have that. The Metaverse didn't have that major foothold like the early Internet,

37
00:06:52,020 --> 00:06:59,860
where it was funded by the government and nobody really had to worry about monetization

38
00:06:59,860 --> 00:07:09,820
and all that kind of stuff. All these companies that are paying for this stuff, they're fully

39
00:07:09,820 --> 00:07:15,900
invested in the fact that they are going to be able to fully monetize all these systems

40
00:07:15,900 --> 00:07:29,460
later through different schemes, either advertising or selling goods or selling use of servers

41
00:07:29,460 --> 00:07:40,300
that they will control. But the Internet had the decentralization and the ability to run

42
00:07:40,300 --> 00:07:55,820
servers from almost the very beginning. That's why I use systems like Janus VR and the offshoots

43
00:07:55,820 --> 00:08:09,780
of High Fidelity. Because I think in order for us to have the Metaverse that we want,

44
00:08:09,780 --> 00:08:16,700
to a certain extent, we have to go through the same steps that allowed the Internet that

45
00:08:16,700 --> 00:08:24,740
we love today. We have to allow the Metaverse to go through the same steps. We have to allow

46
00:08:24,740 --> 00:08:34,180
there to be a foothold before companies come in and try to monetize everything. We can

47
00:08:34,180 --> 00:08:40,340
have an argument, we can have a discussion about monetization schemes and all that later,

48
00:08:40,340 --> 00:08:53,580
but that's never going to happen unless people feel comfortable before that even happens.

49
00:08:53,580 --> 00:09:05,300
But if we're going to go down this road, we must first have standardized browsers. So

50
00:09:05,300 --> 00:09:17,740
we have systems like High Fidelity. Each of these browsers must be compatible so people

51
00:09:17,740 --> 00:09:24,980
have the choice. And I know that's scary. Some people say, oh well, if they have choice,

52
00:09:24,980 --> 00:09:36,340
they'll leave us. That isn't necessarily a bad thing. If you look at Google, right, and

53
00:09:36,340 --> 00:09:45,380
you look at their upvotes and all that kind of stuff and comments, if people don't like

54
00:09:45,380 --> 00:09:52,900
your videos, you can see when you make videos what videos are liked and which videos are

55
00:09:52,900 --> 00:10:03,940
disliked. And you can change your direction in your videos to conversations that interest

56
00:10:03,940 --> 00:10:13,860
the people that you're going after. Same thing here. If people have the choice, they're going

57
00:10:13,860 --> 00:10:19,980
to make the choice and you will be forced to listen to the people that are using your

58
00:10:19,980 --> 00:10:26,860
platform. And hopefully if it's designed properly and they're all compatible, you'll see your

59
00:10:26,860 --> 00:10:34,460
numbers going down. And you have to ask yourself, well, what are we doing different? What are

60
00:10:34,460 --> 00:10:38,940
these people, what do they want from a system like this? What are they getting somewhere

61
00:10:38,940 --> 00:10:52,940
else that we might have to bring in? Choice in this matter is important. It gives power

62
00:10:52,940 --> 00:10:59,740
to the users. And especially when you don't have a lot of users, like the early days of

63
00:10:59,740 --> 00:11:07,060
the internet, that makes them comfortable. It makes them feel like they have power within

64
00:11:07,060 --> 00:11:16,900
the system that they are using.

65
00:11:16,900 --> 00:11:24,420
This is a, I'm not in darling VR today, but I thought I would show you this, which I thought

66
00:11:24,420 --> 00:11:30,060
was really cool. And this is exactly what I've been talking about for a while. And I

67
00:11:30,060 --> 00:11:42,380
kind of wanted to show it off. Whoever made this go to did a very good job. This explores

68
00:11:42,380 --> 00:11:51,540
well done. And what they did was they broke it down into places, domains, which is domain

69
00:11:51,540 --> 00:11:58,700
wide. You can see there's one person online and then you have regions, which is cool because

70
00:11:58,700 --> 00:12:08,700
you can. So any, the idea is that any organization or nonprofit or any, anybody, even companies

71
00:12:08,700 --> 00:12:23,380
that want to make money can come in and run their own backend and have more control over

72
00:12:23,380 --> 00:12:33,580
the users and all that kind of stuff. You might have some other one might give you special

73
00:12:33,580 --> 00:12:39,580
benefit. We'll run your own, we'll run your servers for you. We'll, you know, we'll charge

74
00:12:39,580 --> 00:12:43,900
you for servers. We'll do whatever. We'll kick out all the bad guys. Somebody else might

75
00:12:43,900 --> 00:12:50,500
come in and go, nah, you know, everybody wants to run your own servers right now. So we're

76
00:12:50,500 --> 00:12:55,860
just going to be chill with you and put up with, with trolls. Maybe you want to be in

77
00:12:55,860 --> 00:13:05,380
a one that's more controlled where the trolls are kicked out, but it's up to you. You get

78
00:13:05,380 --> 00:13:24,740
to vote by using the, the, the account system that appeals to you, which is good. That,

79
00:13:24,740 --> 00:13:31,420
that kind of thing, the choice, just like I can choose to use Google or search.com or

80
00:13:31,420 --> 00:13:40,380
any of the other search engines based on, on my personal preferences and my either like

81
00:13:40,380 --> 00:13:46,500
or dislike for the company of the people that run it. That's what, that's what needs to

82
00:13:46,500 --> 00:13:57,540
happen in order to make a strong metaverse, a strong community.

83
00:13:57,540 --> 00:14:12,860
And it goes back, it goes back to the video I made, the last video I made. Some of the

84
00:14:12,860 --> 00:14:25,260
things I say, I think other people, I, I know I'm not the only person, but sometimes some

85
00:14:25,260 --> 00:14:35,940
of the things I say, it's kind of like, this is, this is obvious stuff. Why aren't people

86
00:14:35,940 --> 00:15:00,100
demanding this? This is the metaverse built like the internet that you love. The, the

87
00:15:00,100 --> 00:15:05,980
way the browser looks, the rendering engine and all that kind of stuff. Those are all

88
00:15:05,980 --> 00:15:11,540
on top of it. Those can be updated. They can be changed. They can use different rendering

89
00:15:11,540 --> 00:15:24,020
methods. They can do all kinds of stuff, but I'm mainly concerned with the backend, the,

90
00:15:24,020 --> 00:15:37,740
the system's ability to empower the users and be fair and also the benefit of it.

91
00:15:37,740 --> 00:15:54,300
In a company like Sansar or Altspace or Horizons, you don't, you don't, you're never going to

92
00:15:54,300 --> 00:15:59,860
have, like I said before, you're never going to have an Amazon. You're never going to have

93
00:15:59,860 --> 00:16:11,140
a Google. Well, you might, but the thing about it is, they're, they want to own it before

94
00:16:11,140 --> 00:16:17,380
you can, anybody can ever come up with the idea that might grow into the next Amazon

95
00:16:17,380 --> 00:16:30,740
or Google. In a system like this, where it empowers the users, that, that good idea might

96
00:16:30,740 --> 00:16:38,900
be your good idea. You might be the person that comes up with the, the knock, knock it

97
00:16:38,900 --> 00:16:51,660
out of the park idea for the metaverse that actually works. You might be the lucky one

98
00:16:51,660 --> 00:17:07,360
that goes, Hmm, you know, if we just do this, the only system, the only way of doing that

99
00:17:07,360 --> 00:17:22,140
is in a system like this or a system like Janus VR. There's no other way to do it.

100
00:17:22,140 --> 00:17:29,980
And here's the thing, the Google or Amazon hasn't been made yet. It's already been made

101
00:17:29,980 --> 00:17:41,840
on the internet. It already exists. Wouldn't you rather be in a system that allows, that

102
00:17:41,840 --> 00:18:00,160
empowers you to actually succeed? Maybe I'm just crazy, ranting at nothing. Maybe we're

103
00:18:00,160 --> 00:18:18,000
just, Oh, well, until the next time.

