1
00:00:30,000 --> 00:00:40,800
Willkommen zum Opening zur xrelog22!

2
00:00:40,800 --> 00:00:51,920
Hallo Anchoray und hallo Welt da draußen! Wir wollen euch heute vorstellen, was die xrelog22

3
00:00:51,920 --> 00:01:01,760
 dieses Jahr für euch bereithält. Was ist eigentlich generell diese xrelog22? Was sind

4
00:01:01,760 --> 00:01:11,360
denn die Ziele? Wir wollen uns um SocialVR kümmern und zwar SocialVR ohne Bullshit und

5
00:01:11,360 --> 00:01:17,000
das haben wir in unser Motto reingegossen. Wir nennen es Visions for Independent Multiverses.

6
00:01:17,000 --> 00:01:23,960
Ich habe gehört ihr wollt auch die Hacker Ethik in das Meta und Multiverse in die unendlichen

7
00:01:23,960 --> 00:01:29,960
Weiten des Meta und Multiverse tragen. Unbedingt! Davon ist einfach zu wenig im

8
00:01:29,960 --> 00:01:35,480
Metaverse vorhanden. Sag mal, was mir gerade auffällt, ich sehe überhaupt kein Senderlogo.

9
00:01:35,480 --> 00:01:39,680
Habt ihr eigentlich ein Senderlogo? Senderlogo?

10
00:01:39,680 --> 00:01:49,440
Was ist das? Was war das denn jetzt für ein Krach?

11
00:01:49,440 --> 00:01:56,960
Ja, ich weiß es auch nicht. Ja, leider, ich glaube das Tesa-Crap hat nicht so gut gehalten.

12
00:01:56,960 --> 00:02:04,960
Ja, da sind wir halt nicht. Wir sind jetzt nicht so die tollen super Video OBS super

13
00:02:04,960 --> 00:02:11,680
perfekten Menschen, sondern da geht auch schon mal ein Senderlogo in die Büt.

14
00:02:11,680 --> 00:02:17,440
Das Tesa, war einfach nur schlecht. Der hat sicher nur schlechte Qualität.

15
00:02:17,440 --> 00:02:21,760
Ich glaube, wir müssen das Erwartungsmanagement noch ein bisschen anpassen. Ich meine,

16
00:02:21,760 --> 00:02:27,760
dass die Leute nicht denken, sie kriegen jetzt wirklich so ARD, ZDF-like super Perfektion

17
00:02:27,760 --> 00:02:30,520
geliefert. Niemand will ARD und ZDF.

18
00:02:30,520 --> 00:02:35,960
Aber wie es auch sein soll, also bei uns ist vieles unfertig, aber sexy.

19
00:02:35,960 --> 00:02:44,960
Das ist nicht unfertig, das ist in Production. Das ist agil. Das ist eine agile Entwicklung.

20
00:02:44,960 --> 00:02:47,560
Du meinst Work in Progress ist das neue Normal, ja?

21
00:02:47,560 --> 00:02:48,560
Ja, natürlich.

22
00:02:48,560 --> 00:02:54,320
Aha, okay. Also macht euch gefasst auf viele umflaschende, umflaschende, ja,

23
00:02:54,320 --> 00:02:58,720
siehst du, da haben wir es schon wieder. Macht euch gefasst auf viele umgefallene

24
00:02:58,720 --> 00:03:02,320
Mateflaschen und irgendwie Krams, aber wir werden Spaß haben.

25
00:03:02,320 --> 00:03:04,720
Wir kleben sie fest mit dem Tesa.

26
00:03:04,720 --> 00:03:05,400
Genau.

27
00:03:05,400 --> 00:03:24,360
Aber ausgesprochen wird das wirklich XR-RELOG22.

28
00:03:24,360 --> 00:03:31,960
So und das leitet sich ab von XR.Labs Operation Group.

29
00:03:31,960 --> 00:03:39,800
Ich stehe ja hier im ICCBS, guck mal hier. Wir bauen halt virtuelle Welten auf.

30
00:03:39,800 --> 00:03:47,800
Eine davon, ein Gefährt durch die virtuelle Welt zu reisen, ist unser ICCBS, Intergalactical

31
00:03:47,800 --> 00:03:55,000
Chaos Communication Broadcast Studio. Und das gastiert halt für diese Veranstaltung in Overte.

32
00:03:55,000 --> 00:04:02,760
So, ihr seht, seht ihr, das ist ganz schön kaputt. Das liegt daran, dass wir damit auch schon einige

33
00:04:02,760 --> 00:04:10,600
Crashes aufs Parkett gelegt haben. Und die ganze schöne Inneneinrichtung ist dabei nicht fest

34
00:04:10,600 --> 00:04:17,120
justiert gewesen und ist deswegen auch ein bisschen kaputt gegangen. Aber das bauen wir gerade wieder

35
00:04:17,120 --> 00:04:23,920
wunderschön auf. Kommt mal mit, ich zeige euch mal was. Wir wollen nämlich so in den Ecken so ein

36
00:04:23,920 --> 00:04:31,960
paar Spezialitäten zusammenstellen. Ich weiß nicht, wer das wieder erkennt, wer schon mal in der

37
00:04:31,960 --> 00:04:39,680
C-Base war, wird das wieder erkennen. Das ist nämlich so eine Lichtwand, bestehend aus Mateflaschen.

38
00:04:39,680 --> 00:04:49,760
Und da ist zum Beispiel eine Überlegung, dass wir einen sogenannten Digital Twin von diesem Gerät

39
00:04:49,760 --> 00:04:55,120
bauen. Hier ist übrigens die Bastelanleitung zur API, wie man das bedienen kann. Das ist hier

40
00:04:55,120 --> 00:05:01,480
diese Webseite, die kann man sich angucken und auch durchlesen. Und das ist so zum Beispiel eine Idee,

41
00:05:01,480 --> 00:05:08,920
wie wir diesen Raum, sagen wir mal, bespielen können. Dass wir hier diese LED-Wand aufbauen.

42
00:05:08,920 --> 00:05:15,120
Flieg mal eben. Moment, fliegen geht auch. Da muss ich ein bisschen üben, fliegen. Ui, jetzt fliege

43
00:05:15,120 --> 00:05:21,880
ich hier rum. Guck mal hier. Es ist hier noch ein Loch, siehst du? Oh, das ist noch kaputt. Hier

44
00:05:21,880 --> 00:05:26,680
müssen wir noch ein bisschen flicken. Das ist auch noch so eine Aufgabe. Ihr seht, das ist so bei den

45
00:05:26,680 --> 00:05:32,520
Bruchlandungen. Noch mal hier. Durch ein Portal bin ich gerade gegangen und hier ist unser Departure

46
00:05:32,520 --> 00:05:42,920
Hall. Da kann man nämlich hier zu dem Zeppelin hinaufgehen. Sag mal, die Erde da, die dreht sich.

47
00:05:42,920 --> 00:05:51,920
Cool, das kann man so. Sehr cool. Da sieht man so ein bisschen das ICCBS von außen. Dann bin ich

48
00:05:51,920 --> 00:05:58,600
hier in dem Zeppelin drin. Und da ist auch ein Portal. Guck mal, wo führt das denn hin? Gehen wir mal rein.

49
00:05:58,600 --> 00:06:08,400
Achso, das führt wieder zurück. Ach, da haben die die Portale geändert. Gut, dass wir mal darüber

50
00:06:08,400 --> 00:06:13,720
geredet haben. Eigentlich sollte das Portal, wo ich eben durchgegangen bin, hier hin führen. Guck

51
00:06:13,720 --> 00:06:23,080
mal hier. Was ist das denn hier? Schau mal, ein dunkler... Guck mal da, da steht die Fairy Dust.

52
00:06:23,080 --> 00:06:33,160
Unglaublich, das habe ich ja noch gar nicht gesehen. Wow, das ist ja nicht schlecht. Drehen sich eigentlich

53
00:06:33,160 --> 00:06:39,880
noch die Dinger hier oben? Drehen sich nicht mehr. Doch, ganz langsam drehen sich. Hier siehst du den

54
00:06:39,880 --> 00:06:44,960
Datenknoten in verschiedenen künstlerischen Ausprägungen. Und wir haben ein Lagerfeuer.

55
00:06:44,960 --> 00:06:56,560
Und es knistert. Es knistert. Es ist echt gemütlich hier. Ich hoffe, dass wir uns hier wirklich noch

56
00:06:56,560 --> 00:07:02,720
mal oft sehen. Na gut, wenn ich hier so rumlaufe, dann kann ich ja auch noch mal ins Basement gehen

57
00:07:02,720 --> 00:07:13,960
vom Jeff22. Und das Basement sieht dann so aus. Wow, wow, wow, wow. Schau mal hier. Das sind alle Events,

58
00:07:13,960 --> 00:07:20,520
also die Startseiten von den Events. Die kannst du dir jetzt alle angucken gehen. Da sind einige

59
00:07:20,520 --> 00:07:27,240
zusammengekommen. Ja, guck mal hier. Hacking in Parallel. Zack. Und wenn du dem näher kommst, kommt da eine

60
00:07:27,240 --> 00:07:33,000
richtig interaktive Webseite. Die kannst du hier anklicken und durchlesen. Und das geht dann mit

61
00:07:33,000 --> 00:07:42,640
allen. Insgesamt sind das 38 Events. Die kannst du dir jetzt alle angucken und mitmachen. Chili con

62
00:07:42,640 --> 00:07:54,120
Chaos. Ja, kannst du dir Chili con Chaos angucken. Ah, das Programm ist sehr interessant. Man kann hier

63
00:07:54,120 --> 00:08:01,480
nämlich über Places, kann man wunderschön in unser Makerspace reinfliegen. Der Makerspace

64
00:08:01,480 --> 00:08:11,680
nennt sich ganz schickerweise xrelog. Und dieser Makerspace, der ist jetzt so eine Ansammlung von

65
00:08:11,680 --> 00:08:17,120
unvollendeten Dingen. Also haben wir einfach so kreuz und quer angefangen. Ich habe dann zum Beispiel

66
00:08:17,120 --> 00:08:25,840
hier die Vision gehabt, wir haben 15 Tracks, also 15 Themenbereiche. Lass uns doch ein Track Tower

67
00:08:25,840 --> 00:08:38,800
bauen mit 15 Etagen. Ja, sehr cool. Da will ich auch fliegen können. Ja doch, da ist Fliegen keine

68
00:08:38,800 --> 00:08:45,840
so schlechte Idee. Wenn man mal genau hinguckt, dann wirken diese Räume so ein kleines bisschen

69
00:08:45,840 --> 00:08:53,800
wie so kleine Mini-Abflughallen. Also ganz klein und ganz mini. Und in jedem dieser Räume, wir

70
00:08:53,800 --> 00:09:02,200
haben bis zur dritten Etage angefangen, ist ein Thema gewidmet. Und diesem Thema wollen wir uns

71
00:09:02,200 --> 00:09:09,480
dann praktisch widmen. Ob das Podcasterei ist, ob das SocialVR ist, ob das die XR-Communities

72
00:09:09,480 --> 00:09:16,680
sind, da gibt es einige oder oder oder. Und natürlich einer der ersten Etagen, die erste

73
00:09:16,680 --> 00:09:28,240
Etage ist (H)Activism. Das ist natürlich unser Hauptthema. (H)Activism, unser Hauptthema. Und

74
00:09:28,240 --> 00:09:36,160
warte, ich fliege da nochmal hin. Das ist sozusagen, da steht es in Buchstaben geschrieben,

75
00:09:36,160 --> 00:09:42,040
aber die Lampe ist nicht an. Ich habe die Lampe irgendwo verschoben. Jetzt leuchtet es hier gar

76
00:09:42,040 --> 00:09:48,800
nicht. Eigentlich leuchtet das hier schön. Kann man das da ganz gut lesen. Nein, der Hectivism steht im Dunkeln. Achso,

77
00:09:48,800 --> 00:09:55,160
apropos Hackerethik. Schau mal her. Das ist einer der ersten Dinge, die ich gemacht habe. Ich habe

78
00:09:55,160 --> 00:10:02,920
in der Virtual Reality die Hackerethik in 3D gemalt mit so einem Multi-Brush. Und dieses Gemälde, das

79
00:10:02,920 --> 00:10:11,240
habe ich dann als 3D-Modell hier eingebaut. Und nicht nur das, ich habe sogar auch eine Sprachausgabe

80
00:10:11,240 --> 00:10:18,160
mit eingebaut, in Deutsch und in Englisch. Nummer one. Naja, was ich herausgefunden habe bei der

81
00:10:18,160 --> 00:10:24,600
Hackerethik-Forschung ist, dass die ersten sechs Hackerregeln eigentlich schon älter sind, schon

82
00:10:24,600 --> 00:10:30,440
aus den 60er Jahren stammen und dass nur zwei Regeln vom Computer-Chaos-Club hinzugefügt worden

83
00:10:30,440 --> 00:10:36,760
sind. Nämlich, Mülle nicht in den Daten anderer. Und der nächste ist, glaube ich, sehr berühmt.

84
00:10:36,760 --> 00:10:43,840
Öffentliche Daten nützen, private Daten schützen. Das hat man, glaube ich, schon öfters gehört. Und

85
00:10:43,840 --> 00:10:50,520
das sind zwei Ergänzungen vom Computer-Chaos-Club. Und wir waren mal so frei und haben noch eine

86
00:10:50,520 --> 00:11:00,720
neunte Ergänzung hinzugefügt. Denke immer an Bits und Bäume. Denn der ökologische Aspekt ist,

87
00:11:00,720 --> 00:11:07,280
bedingt dadurch, dass die ja auch so älter sind, in der Hackerethik noch nicht vertreten. Deswegen

88
00:11:07,280 --> 00:11:14,320
müsste man das unbedingt mal ergänzen. Das ist so die Hackerethik. Und wie du siehst, ist also

89
00:11:14,320 --> 00:11:17,560
eine Sammlung von allen möglichen Sachen. Du siehst oben, wir können mal nach oben gucken,

90
00:11:17,560 --> 00:11:24,880
da siehst du den Zeppelin. Und über den Zeppelin ist auch schon eine erste Version vom ICCBS. Und

91
00:11:24,880 --> 00:11:30,440
hier sind so, wie nennt man, Plateaus. Diese Plateaus werden wir nochmal benutzen, wenn wir

92
00:11:30,440 --> 00:11:36,280
hier Workshops halten. Dann kann man nämlich zu diesen Themengebieten dann hier was hinfliegen

93
00:11:36,280 --> 00:11:43,160
und Dinge hören. Und ja, wir basteln hier so einfach so rum. Es ist so unser virtueller Makerspace.

94
00:11:43,160 --> 00:11:52,040
Und der hat jetzt sozusagen noch keinen thematischen Zusammenhang. Weil wenn wir den

95
00:11:52,040 --> 00:11:59,000
Tracktower fertig haben, dann wird der umziehen in einen eigenen Domainserver. Dann schmeißen wir

96
00:11:59,000 --> 00:12:07,320
den aus dem Makerspace wieder raus und dann bauen wir was Neues. Jetzt fliege ich mal wieder zurück

97
00:12:07,320 --> 00:12:20,400
zum Intergalactic Chaos Communication Broadcast Studio. Kurz ICCBS. Mal ICCBS. Guck mal, wie ich

98
00:12:20,400 --> 00:12:31,080
da ankomme. Wo ich da ankomme. Und so kann dann einfach jeder wechseln, der in dem System ist?

99
00:12:31,080 --> 00:12:37,960
Ja, das ist alles offen. Du brauchst dich sogar noch nicht mal anzumelden. Du musst ja noch nicht

100
00:12:37,960 --> 00:12:44,560
mal irgendeinen Account anlegen. Du kannst einfach den Client runterladen, den starten und dann bist

101
00:12:44,560 --> 00:12:49,320
du in der Welt drin. Dann kriegst du eine kleine Erklärung, wie da so alles funktioniert. Und

102
00:12:49,320 --> 00:12:55,960
dann kannst du schon dieses Places Funktion benutzen. Und dann kannst du, es gibt noch viel

103
00:12:55,960 --> 00:13:07,080
mehr Welten. Also dieses ICCBS und das Fairy Dust und das xrelog oder später auch der Tracktower

104
00:13:07,080 --> 00:13:14,040
15. Das sind jetzt nur drei Welten von hunderten von Welten oder sagen wir mal Dutzende von Welten.

105
00:13:14,040 --> 00:13:18,240
Es werden wahrscheinlich bald hunderte, die man direkt offen besuchen kann. Es gibt natürlich

106
00:13:18,240 --> 00:13:27,520
auch Welten, die sind nicht offen, weil sie zum Beispiel ein Darling VR. Das ist dann so ein

107
00:13:27,520 --> 00:13:32,520
bisschen bunter von der sexuellen Ausrichtung her. Aber da will man nicht unbedingt, dass da halt

108
00:13:32,520 --> 00:13:39,280
jeder so direkt reinkommt. Aber ansonsten, die meisten Welten sind offen, zugänglich, frei,

109
00:13:39,280 --> 00:13:49,200
offen, ohne Bullshit, ohne NFT, Smart Contract, Blockchain, Crypto-Krams. Alles ist hier überhaupt

110
00:13:49,200 --> 00:13:52,560
nicht zu finden. Das gibt es hier nicht. Das ist so wunderschön.

111
00:13:52,560 --> 00:14:09,880
Also, Anchoray, du hast jetzt so viel über die Welt gezeigt. Aber was habt ihr jetzt eigentlich vor,

112
00:14:09,880 --> 00:14:12,960
beim Congress zu machen? Also dazu hast du noch gar nichts gesagt.

113
00:14:12,960 --> 00:14:25,080
Ja, das stimmt. Also, wir haben 15 Sessions, wir haben 15 Thementracks und wir schauen uns

114
00:14:25,080 --> 00:14:36,440
auch über 35 andere JEV22-Events an. Und unser Themenschwerpunkt dabei ist Technikkritik. Klar

115
00:14:36,440 --> 00:14:42,240
und deutlich Technikkritik. Da wollen wir Facebook nicht davon kommen lassen. Da wollen wir uns über

116
00:14:42,240 --> 00:14:48,800
Web 3.0 auslassen. Da wollen wir wirklich schauen, wie sich das alles kritisch entwickelt. Und da

117
00:14:48,800 --> 00:14:53,520
haben wir einige sehr interessante Sessions für euch vorbereitet. Und wie schon gesagt,

118
00:14:53,520 --> 00:14:58,280
wir wollen dann auch über den Tellerrand hinaus schauen und uns auch mit den anderen JEV-Events

119
00:14:58,280 --> 00:15:04,920
beschäftigen. Da gibt es ganz fantastische Initiativen von New York über Athen. In München

120
00:15:04,920 --> 00:15:12,560
gibt es zwei, in Hamburg gibt es vier insgesamt. In Berlin, in Erlangen, in Düsseldorf, in Aachen.

121
00:15:12,560 --> 00:15:15,800
Sehr viel gibt es zu entdecken. Sehr viel gibt es zu entdecken.

122
00:15:15,800 --> 00:15:17,280
Gibt es aber auch ein bisschen Spaß?

123
00:15:17,280 --> 00:15:23,640
Oh, Spaß. Ja, klar. Wir haben zwar kein Jeopardy, das haben wir nicht mehr geschafft,

124
00:15:23,640 --> 00:15:29,080
schöne Fragen zusammenzustellen. Aber wir versuchen, etwas Ähnliches Beklopptes zu

125
00:15:29,080 --> 00:15:38,400
machen. Wir nennen das mal mafifa WM 2030. Wir spielen die WM oder die Auslosung der WM 2030

126
00:15:38,400 --> 00:15:44,920
nach. Lasst euch überraschen. Jeweils um 23 Uhr, Mittwoch, Donnerstag und Freitag.

127
00:15:44,920 --> 00:15:47,400
Wird interessant.

128
00:15:47,400 --> 00:15:50,600
Ihr macht das doch sicher auch ganz korrupt.

129
00:15:50,600 --> 00:15:57,840
Aber zählt Mate als Währung?

130
00:15:57,840 --> 00:16:05,720
Also wenn das nicht als Währung zählt, dann sind wir auch nicht korrupt. Aber im Endeffekt,

131
00:16:05,720 --> 00:16:12,880
was wichtig ist zu wissen, bei uns gibt es eigentlich nur ganz, ganz wenige Frontalvorträge.

132
00:16:12,880 --> 00:16:19,640
Bei uns ist fast alles wie ein Mitmach-Workshop, wie ein Barcamp ausgelegt. Expresseswerbes gibt

133
00:16:19,640 --> 00:16:28,080
es ein XR-Barcamp, gleich im Anschluss an diesem Opening. Aber es gibt auch ein Bits&Bäume

134
00:16:28,080 --> 00:16:34,200
Fireside Chat, wo ihr auch herzlich eingeladen seid, mitzumachen. Und auch die Workshops

135
00:16:34,200 --> 00:16:39,760
von Overte, wo wir diese Welt nie erkunden, sind auch darauf ausgelegt, Workshops zu

136
00:16:39,760 --> 00:16:41,080
sein und keine Vorträge.

137
00:16:41,080 --> 00:16:50,760
Möchtest du vielleicht dann auch nochmal sagen, wie man zu der Welt kommt? Weil aktuell

138
00:16:50,760 --> 00:16:57,800
sehen wir diese richtig geile Welt, aber so richtig, wie man drauf kommt. Kann man euch

139
00:16:57,800 --> 00:16:58,800
da besuchen?

140
00:16:58,800 --> 00:17:04,520
Ja, natürlich. Also das Wichtigste findet bei uns in der Matrix statt. Wenn ihr in Fahrplan

141
00:17:04,520 --> 00:17:11,960
im Pretalx schaut, dann ist bei jedem Vortrag ein Matrix-Kanal ausgewiesen. Dort treffen

142
00:17:11,960 --> 00:17:16,760
wir uns und dort tauschen wir auch alle Informationen aus. Das ist schon mal das eine, was ganz

143
00:17:16,760 --> 00:17:29,820
interessant ist. Das andere ist hier dieses Metaverse, dieses Multiverse Overte. Overte.org,

144
00:17:29,820 --> 00:17:35,520
das ist schon die Homepage. Dort könnt ihr euch einen Client herunterladen. Den gibt

145
00:17:35,520 --> 00:17:42,880
es für Linux und für Windows. Interessanterweise nicht für Apple. Interessant. Und mit dem

146
00:17:42,880 --> 00:17:51,840
könnt ihr uns hier auch im ICCBS besuchen, im Fairy Dust Wald oder auch in so einem virtuellen

147
00:17:51,840 --> 00:17:57,160
Hackerspace auf der xrelog mit dem Track Tower. Das könnt ihr dann alles selbst auch

148
00:17:57,160 --> 00:17:58,160
erkunden.

149
00:17:58,160 --> 00:18:08,760
Das klingt auf alle Fälle spannend und ich glaube, einige Zuhörer dürften es auch richtig

150
00:18:08,760 --> 00:18:11,600
spannend finden, euch im 3D-Raum zu treffen.

151
00:18:11,600 --> 00:18:17,640
Ich bin gespannt. Ab wie viel Zuschauer dann die Server zusammenbrechen, müssen wir dann

152
00:18:17,640 --> 00:18:19,040
auch mal zusammen herausfinden.

153
00:18:19,040 --> 00:18:20,040
Testen, testen.

154
00:18:20,040 --> 00:18:26,200
Ja, mal gucken. Also wir haben da ein bisschen was vorbereitet bei Hetzner. Wir können auch

155
00:18:26,200 --> 00:18:32,280
weitere Server provisionieren. Sind da allerdings noch nicht so super automatisiert. Dauert ein

156
00:18:32,280 --> 00:18:36,840
bisschen was, aber vorbereitet wären wir. Falls wir jetzt vollkommen untergehen, können

157
00:18:36,840 --> 00:18:38,080
wir da noch ein bisschen was auffahren.

158
00:18:38,080 --> 00:18:45,520
Und zum Schluss möchte ich mich dann auch noch ganz recht herzlich verabschieden. Auch

159
00:18:45,520 --> 00:18:51,240
übrigens von, ähm, ähm, äh, wer bist du eigentlich?

160
00:18:51,240 --> 00:18:56,560
Sorry Anchoray, I can't tell you that.

161
00:18:56,560 --> 00:19:25,560
Dann müssen wir dieses Rätsel wohl beim nächsten Mal auflösen. Bis dann. Tschüss.

