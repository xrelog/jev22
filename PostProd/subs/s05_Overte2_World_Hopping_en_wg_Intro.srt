1
00:00:00,000 --> 00:00:02,000
.

2
00:00:13,260 --> 00:00:14,820
.

3
00:00:17,300 --> 00:00:19,260
.

4
00:00:21,400 --> 00:00:21,920
.

5
00:01:51,920 --> 00:01:58,920
Welcome to the second day of our xrelog22.

6
00:01:58,920 --> 00:02:03,920
Our first session takes us through the world of Overte.

7
00:02:03,920 --> 00:02:07,920
We do a little world hopping.

8
00:02:07,920 --> 00:02:14,920
The starting point is our makerspace XR-Vlog and there we meet in front of the hackerspace.

9
00:02:14,920 --> 00:02:18,920
Let's see who's already there.

10
00:02:18,920 --> 00:02:30,920
Places, XR-Vlog, there is a space.

11
00:02:30,920 --> 00:02:34,920
Yes, and there are already six people there.

12
00:02:34,920 --> 00:02:41,920
So, aha, hello, hello.

13
00:02:41,920 --> 00:02:44,920
So, welcome to our little tour.

14
00:02:44,920 --> 00:02:52,920
I have to conjure up my little Woody a little bit.

15
00:02:52,920 --> 00:03:06,920
So, okay.

16
00:03:06,920 --> 00:03:14,920
While we are waiting, we can have a short look on our hacker ethic.

17
00:03:14,920 --> 00:03:28,920
If you think you don't understand it, you can click on the voice snippets.

18
00:03:28,920 --> 00:03:36,920
You can listen in German or in English.

19
00:03:36,920 --> 00:03:44,920
The stream is started, so we are online.

20
00:03:44,920 --> 00:04:00,920
And perhaps I will also use our XR-Vlog Overte channel. There I am posting where we are going next.

21
00:04:00,920 --> 00:04:29,920
So, from here, what do you think we go next?

22
00:04:29,920 --> 00:04:38,920
Okay.

23
00:04:38,920 --> 00:04:59,920
Fantasy world at first, I think at second.

24
00:04:59,920 --> 00:05:28,920
.

25
00:05:28,920 --> 00:05:40,920
.

26
00:05:40,920 --> 00:06:09,920
.

27
00:06:09,920 --> 00:06:38,920
.

28
00:06:38,920 --> 00:06:41,920
.

29
00:06:41,920 --> 00:06:57,920
Yes, so in the audio, you can adjust music volume also.

30
00:06:57,920 --> 00:07:03,920
All right.

31
00:07:03,920 --> 00:07:21,920
And this world, when it loads, it has a lot of secrets to explore.

32
00:07:21,920 --> 00:07:27,920
Is there a sign if this world loading is ready?

33
00:07:27,920 --> 00:07:28,920
I think that...

34
00:07:28,920 --> 00:07:32,920
Ah, loading content. I see below, yeah.

35
00:07:32,920 --> 00:07:39,920
Normally it's been finished, so I don't think there will be a sign.

36
00:07:39,920 --> 00:07:43,920
But when it looks mostly okay, then we can start exploring.

37
00:07:43,920 --> 00:08:09,920
So, like, for example, that sail ship here, it should be loaded for us to start.

38
00:08:09,920 --> 00:08:14,920
Oh. Oh, so I think we can start.

39
00:08:14,920 --> 00:08:27,920
So, let's go exploring. And there's a lot of stuff to find here.

40
00:08:27,920 --> 00:08:35,920
Oh. Later it will start loading stuff again, because it streams as we explore.

41
00:08:35,920 --> 00:08:46,920
Oh. There are cow sounds also. Nice.

42
00:08:46,920 --> 00:08:50,920
Ah, there are seed scripts.

43
00:08:50,920 --> 00:09:13,920
Ah, but they don't work. Ah, they work in VR. Nice.

44
00:09:13,920 --> 00:09:28,920
Oh.

45
00:09:28,920 --> 00:09:33,920
This is only the beginning of the world, so we should go on the path here.

46
00:09:33,920 --> 00:09:36,920
And a lot of terrain, there are techniques.

47
00:09:36,920 --> 00:09:41,920
So, one is exported from Blender, it is heightmap, but a lot of stuff is made from Voxel.

48
00:09:41,920 --> 00:09:47,920
So, for example, those roadstones here are made from Voxels.

49
00:09:47,920 --> 00:09:52,920
The world is made by Pandora.

50
00:09:52,920 --> 00:10:21,920
So, we can try. Let's go this way.

51
00:10:21,920 --> 00:10:45,920
I think the road is new. So, let's see. Let's see where it leads.

52
00:10:45,920 --> 00:10:51,920
Oh, let's maybe wait. Where are others? Just a moment, I will see.

53
00:10:51,920 --> 00:11:02,920
If somebody lost, then you can open the people menu and double click on 7-4.

54
00:11:02,920 --> 00:11:07,920
I'm also anonymous. Yes, I'm not logged in.

55
00:11:07,920 --> 00:11:16,920
I will also see if... Oh, I think someone is lost, so I will help them find their way.

56
00:11:16,920 --> 00:11:22,920
Oh, we are here.

57
00:11:22,920 --> 00:11:33,920
Come with us.

58
00:11:33,920 --> 00:11:44,920
If you get lost at any moment, you can go to people tab and click on a person's name and you will automatically teleport to them.

59
00:11:44,920 --> 00:11:59,920
Oh, yes, like this. So, let's go. We should see where the road goes.

60
00:11:59,920 --> 00:12:20,920
I have no idea, to be honest, because I didn't check this way, but it will be something cool. I'm pretty sure.

61
00:12:20,920 --> 00:12:42,920
The way back we fly.

62
00:12:42,920 --> 00:12:56,920
Oh, there is something here. Oh, that's new, I think. So, let's wait for the other. Yes, let's wait for everyone.

63
00:12:56,920 --> 00:13:22,920
Oh, Moto is hidden here.

64
00:13:22,920 --> 00:13:32,920
Let's check it out. Everyone is here, so we can see what's inside.

65
00:13:32,920 --> 00:13:47,920
Be careful not to step on Moto.

66
00:13:47,920 --> 00:14:12,920
So, this is all sculpted from Voxels. It is all sculpted in game.

67
00:14:12,920 --> 00:14:18,920
Come back for a moment. Check. There is a hole there. Check it out.

68
00:14:42,920 --> 00:15:00,920
Oh, there is a sound track here. Nice. If you get close to the...

69
00:15:00,920 --> 00:15:03,920
Is it actually possible to read this?

70
00:15:03,920 --> 00:15:12,920
I think the resolution is too low for here. But generally it is possible. So, generally it should be possible.

71
00:15:12,920 --> 00:15:35,920
And we made also a shader that puts web entities on the textures. So, it should be possible to make a book where you can actually flip pages and have text in it.

72
00:15:35,920 --> 00:15:54,920
And of course, if the texture was higher resolution, it should be doubled.

73
00:15:54,920 --> 00:16:23,920
Oh, there is a sound track.

74
00:16:23,920 --> 00:16:33,920
And here you can see from the top.

75
00:16:33,920 --> 00:17:02,920
And because we go at high, it keeps a little down until it is back.

76
00:17:02,920 --> 00:17:15,920
There is a medieval town here.

77
00:17:15,920 --> 00:17:44,920
Okay. What do you think? Should we jump to the next world?

78
00:17:44,920 --> 00:17:54,920
Okay. I'd like to.

79
00:17:54,920 --> 00:17:54,920
I think we should go left.

80
00:17:54,920 --> 00:18:02,280
I will stay here for a moment and everyone will try and see if they load for them.

81
00:18:02,280 --> 00:18:08,440
So the one we would like to go is called Hytrion Junction.

82
00:18:08,440 --> 00:18:13,000
So it is like the one with black,

83
00:18:13,000 --> 00:18:18,000
white, and orange background and the yellow background,

84
00:18:18,000 --> 00:18:20,760
and this is called Hytrion Junction.

85
00:18:20,760 --> 00:18:22,680
That's the next word?

86
00:18:22,680 --> 00:18:26,440
Yes. So we should go there.

87
00:18:26,440 --> 00:18:52,960
I think it worked for everyone.

88
00:18:56,440 --> 00:19:04,800
Totally white everything.

89
00:19:04,800 --> 00:19:06,720
Okay.

90
00:19:13,160 --> 00:19:18,880
Yeah. Careful.

91
00:19:18,880 --> 00:19:21,160
There are portals nearby.

92
00:19:21,160 --> 00:19:23,280
I just stepped into one.

93
00:19:23,280 --> 00:19:29,960
No. I see it works for everyone.

94
00:19:29,960 --> 00:19:33,760
So this is like a hub of words made by Alizia,

95
00:19:33,760 --> 00:19:35,480
and some of them are really beautiful.

96
00:19:35,480 --> 00:19:38,720
So the first one I would like to show you,

97
00:19:38,720 --> 00:19:42,400
it is called Chrome Crush.

98
00:19:42,400 --> 00:19:45,920
It is like a night forest and it's absolutely beautiful.

99
00:19:45,920 --> 00:19:48,200
So let's go check this one.

100
00:19:48,200 --> 00:19:49,840
There is a lot of different ones.

101
00:19:49,840 --> 00:19:57,240
Like for example, there is one called Vox Secutor.

102
00:19:57,240 --> 00:20:00,920
So that one is like a Pac-Man like game.

103
00:20:00,920 --> 00:20:06,960
For example, this is also a game like,

104
00:20:06,960 --> 00:20:08,880
but it's mostly for VR.

105
00:20:08,880 --> 00:20:12,080
But very fun. Let's see.

106
00:20:12,080 --> 00:20:13,640
This is really cool.

107
00:20:13,640 --> 00:20:15,240
This looks like a cinema.

108
00:20:15,240 --> 00:20:23,960
Yes. Alizia has absolutely amazing talent for very unique art style.

109
00:20:23,960 --> 00:20:26,320
Her works are pretty much best,

110
00:20:26,320 --> 00:20:28,880
like most amazing ones in Uberto.

111
00:20:28,880 --> 00:20:33,000
So let's see. I don't see Chrome Crush here.

112
00:20:33,000 --> 00:20:45,200
Maybe we should go

113
00:20:45,200 --> 00:20:49,760
this way to that green one to Vilt's Junction.

114
00:20:49,760 --> 00:21:19,680
So we should go there.

115
00:21:19,680 --> 00:21:30,960
There we go.

116
00:22:30,960 --> 00:22:43,960
Oh, we should message him, because maybe he's back and I will quickly see if he's back there

117
00:22:43,960 --> 00:22:44,960
or not.

118
00:22:44,960 --> 00:22:45,960
Ah, he's here, nice.

119
00:22:45,960 --> 00:22:46,960
But he's still loading.

120
00:22:46,960 --> 00:22:47,960
Yes, so everyone is here.

121
00:22:47,960 --> 00:22:48,960
Ah, let's wait.

122
00:22:48,960 --> 00:22:49,960
Ah, so in the meantime, let's go to the next one.

123
00:22:49,960 --> 00:23:10,600
Let's go this way, because this world in itself is very unique and beautiful.

124
00:23:10,600 --> 00:23:15,560
Is there a way to see the current name of the world?

125
00:23:15,560 --> 00:23:16,560
Yes.

126
00:23:16,560 --> 00:23:22,360
So you can even copy and paste the link to the world so others can join you there.

127
00:23:22,360 --> 00:23:32,640
And let's try if we go, so it is like on the top of the menu, you can check, let me see,

128
00:23:32,640 --> 00:23:38,560
because I'm in VR right now, but at the top.

129
00:23:38,560 --> 00:23:44,360
So like at the top in navigate menu, you can copy link to current position and place and

130
00:23:44,360 --> 00:23:48,680
even give it to other users and you can also bookmark places there.

131
00:23:48,680 --> 00:23:51,680
Also, let's see.

132
00:23:51,680 --> 00:23:59,560
I think here there should be something really unique and cool.

133
00:23:59,560 --> 00:24:09,360
Oh yes, this part is gorgeous.

134
00:24:09,360 --> 00:24:19,360
Make sure you have a graphics detail set to medium or high.

135
00:24:19,360 --> 00:24:47,480
Oh, and they flip up.

136
00:24:47,480 --> 00:25:09,480
And they make popping sounds when they pop out from there.

137
00:25:09,480 --> 00:25:15,840
So the next one, also by Alizia, that is absolutely beautiful.

138
00:25:15,840 --> 00:25:22,280
So I think it is not a voxel because it uses PBR materials, but it was made, it is like

139
00:25:22,280 --> 00:25:24,800
a voxel but exported from Blender.

140
00:25:24,800 --> 00:25:29,480
But in the future I also want to add the PBR materials to voxels so then it will be possible

141
00:25:29,480 --> 00:25:37,160
to get such material quality with voxels too.

142
00:25:37,160 --> 00:25:41,040
So we should try, I think we should try to go to ChromeCruash.

143
00:25:41,040 --> 00:25:43,720
It is also via portal from here.

144
00:25:43,720 --> 00:25:45,960
So let's try.

145
00:25:45,960 --> 00:26:11,920
Oh, the side from here is nice.

146
00:26:11,920 --> 00:26:25,040
Oh, just a moment.

147
00:26:25,040 --> 00:26:31,400
Can you paste the word name where we go in the chat?

148
00:26:31,400 --> 00:26:35,320
But it is not, we cannot go there through words.

149
00:26:35,320 --> 00:26:38,320
So we have to go there through portal from here.

150
00:26:38,320 --> 00:26:39,320
Okay.

151
00:26:39,320 --> 00:26:44,280
Oh, so let's try.

152
00:26:44,280 --> 00:26:46,440
So that one will be called ChromeCruash.

153
00:26:46,440 --> 00:26:59,520
So let's follow me and I will show you where the portal is.

154
00:26:59,520 --> 00:27:21,240
So it is called ChromeCruash.

155
00:27:21,240 --> 00:27:24,800
And let's go this way.

156
00:27:24,800 --> 00:27:31,800
I will wait here just a moment more.

157
00:27:31,800 --> 00:27:56,800
Yes, I call this for a moment because then we will be able to fly if you hold space for

158
00:27:56,800 --> 00:28:11,580
just a second.

159
00:28:11,580 --> 00:28:21,120
Okay.

160
00:28:21,120 --> 00:28:41,760
This is a load for everyone else.

161
00:28:41,760 --> 00:28:44,000
Hopefully it works.

162
00:28:44,000 --> 00:28:48,040
It's a dark one here, yeah?

163
00:28:48,040 --> 00:28:51,040
Yes, everybody is already exploring there.

164
00:28:51,040 --> 00:28:52,040
So let's go.

165
00:28:52,040 --> 00:28:53,040
Ah, maybe.

166
00:28:53,040 --> 00:28:54,040
Yes.

167
00:28:54,040 --> 00:28:55,040
Oh, so come with us.

168
00:28:55,040 --> 00:29:09,120
And if you get lost at any moment, you can go to people's app and teleport to us by double

169
00:29:09,120 --> 00:29:17,960
clicking on someone's name.

170
00:29:17,960 --> 00:29:25,760
I think your raptor needs some lightning for the darkness so that we can see you.

171
00:29:25,760 --> 00:29:28,680
On the head or something like this.

172
00:29:28,680 --> 00:29:41,480
And it's possible.

173
00:29:41,480 --> 00:29:43,480
You can actually pick up mushrooms.

174
00:29:43,480 --> 00:29:44,480
Mushrooms?

175
00:29:44,480 --> 00:29:45,480
Oh.

176
00:29:45,480 --> 00:29:46,480
Yes.

177
00:29:46,480 --> 00:29:48,480
What happened then?

178
00:29:48,480 --> 00:29:57,480
I wouldn't eat them though, because the screen will become very blurry and colorful.

179
00:29:57,480 --> 00:30:02,960
Yes, eating mushrooms and you become VR even without a headset.

180
00:30:02,960 --> 00:30:05,960
Ah, so you can eat them.

181
00:30:05,960 --> 00:30:06,960
Let's try.

182
00:30:06,960 --> 00:30:12,360
No, it doesn't seem to work on this one.

183
00:30:12,360 --> 00:30:18,200
But I've seen such that it was possible.

184
00:30:18,200 --> 00:30:38,600
And there should be an ancient altar this way.

185
00:30:38,600 --> 00:30:49,960
Oh, sorry.

186
00:30:49,960 --> 00:31:02,240
Ah, there's a pig.

187
00:31:02,240 --> 00:31:05,240
So I think we need to go this way probably.

188
00:31:05,240 --> 00:31:08,240
Yes, and it is there.

189
00:31:08,240 --> 00:31:09,240
Ah, music.

190
00:31:09,240 --> 00:31:10,240
Oh, hard.

191
00:31:10,240 --> 00:31:11,240
Rock.

192
00:31:11,240 --> 00:31:39,100
So, we're pretty much done.

193
00:31:39,100 --> 00:31:45,620
oh yes

194
00:32:09,100 --> 00:32:29,760
the

195
00:32:29,760 --> 00:32:36,540
new

196
00:32:36,540 --> 00:32:45,540
Oh and if you don't play, there is also a really nice campfire there.

197
00:32:45,540 --> 00:32:50,540
Just like chill and relax by the fire.

198
00:32:50,540 --> 00:32:54,540
Oh yes, and you have pitchforks.

199
00:33:20,540 --> 00:33:32,540
So let's wait for everyone.

200
00:33:32,540 --> 00:33:39,540
Perhaps it's time for a vote with more lights.

201
00:33:39,540 --> 00:33:41,540
Oh good idea.

202
00:33:41,540 --> 00:33:42,540
So let's try.

203
00:33:42,540 --> 00:33:47,540
I suggest, let me think.

204
00:33:47,540 --> 00:33:56,540
Oh, there is also by Alicia, there is absolutely beautiful tropical island world.

205
00:33:56,540 --> 00:34:01,540
So we should go there and then I will show you something totally different.

206
00:34:01,540 --> 00:34:07,540
Because there is also absolutely amazing world by Basinski, that is called photodrametry.

207
00:34:07,540 --> 00:34:11,540
And that one shows something very unusual, so it's very worth seeing.

208
00:34:11,540 --> 00:34:17,540
So I think that we could go just back here because the portal is in the same world,

209
00:34:17,540 --> 00:34:30,540
so we cannot teleport to that summer island world.

210
00:34:30,540 --> 00:34:33,540
So we can just go back this way.

211
00:34:33,540 --> 00:34:58,540
Like we came from here and through the cave.

212
00:34:58,540 --> 00:35:00,540
This was beautiful.

213
00:35:00,540 --> 00:35:03,540
Yes, Alicia's words are absolutely amazing.

214
00:35:03,540 --> 00:35:05,540
And there is a lot more of them.

215
00:35:05,540 --> 00:35:11,540
And some are like many miles long in every direction and take a lot of time to explore.

216
00:35:11,540 --> 00:35:17,540
And for example, she even made like this absolutely huge desert world.

217
00:35:17,540 --> 00:35:25,540
When you fly there, there is even like wind sound and time of day changes.

218
00:35:25,540 --> 00:35:30,540
So the night falls. Her words are amazing.

219
00:35:56,540 --> 00:36:05,540
And Alicia also made that place.

220
00:36:05,540 --> 00:36:09,540
So now we can teleport.

221
00:36:09,540 --> 00:36:12,540
So you can teleport to us.

222
00:36:12,540 --> 00:36:17,540
But what is called?

223
00:36:17,540 --> 00:36:19,540
I'm here.

224
00:36:19,540 --> 00:36:23,540
Ah, nice.

225
00:36:23,540 --> 00:36:30,540
So we should I think that if you want like a sun, also a really beautiful world, also by Alicia.

226
00:36:30,540 --> 00:36:34,540
So this one is called Nowhere Island.

227
00:36:34,540 --> 00:36:46,540
It's right here.

228
00:36:46,540 --> 00:36:51,540
This one is also very optimized, so it will load quickly.

229
00:36:51,540 --> 00:36:56,540
In a moment. For me, it's still the skybox is still loading.

230
00:36:56,540 --> 00:36:59,540
But it should be visible soon.

231
00:36:59,540 --> 00:37:03,540
Hopefully.

232
00:37:03,540 --> 00:37:06,540
Do you see the sky?

233
00:37:06,540 --> 00:37:07,540
Yeah.

234
00:37:07,540 --> 00:37:10,540
Maybe I will need to reload content.

235
00:37:10,540 --> 00:37:12,540
I see the sky.

236
00:37:12,540 --> 00:37:13,540
The clouds.

237
00:37:13,540 --> 00:37:15,540
It's only for me.

238
00:37:15,540 --> 00:37:35,540
I reload content.

239
00:37:35,540 --> 00:37:50,540
So let's say 10 minutes before the streams end.

240
00:37:50,540 --> 00:38:05,540
There is also another world by Basinski that is also really worth seeing.

241
00:38:05,540 --> 00:38:07,540
The sky didn't load here for some reason.

242
00:38:07,540 --> 00:38:10,540
It is totally black, but I have no idea why.

243
00:38:10,540 --> 00:38:11,540
But let's try.

244
00:38:11,540 --> 00:38:14,540
So we should try to go to photogrammetry.

245
00:38:14,540 --> 00:38:17,540
So it is in places.

246
00:38:17,540 --> 00:38:20,540
It is called photogrammetry in places.

247
00:38:20,540 --> 00:38:21,540
So let's try.

248
00:38:21,540 --> 00:38:22,540
And that's really cool.

249
00:38:22,540 --> 00:38:24,540
And something very, very unique.

250
00:38:24,540 --> 00:38:28,540
Oh, nice.

251
00:38:28,540 --> 00:38:56,540
Let's click on photogrammetry in places.

252
00:38:56,540 --> 00:39:11,540
This is a single room.

253
00:39:11,540 --> 00:39:15,540
Ah, this smells like chaos.

254
00:39:15,540 --> 00:39:18,540
Where is the party room?

255
00:39:18,540 --> 00:39:33,540
So this one is very unique because it has, how to say, so those are 3D scans made by

256
00:39:33,540 --> 00:39:34,540
photogrammetry.

257
00:39:34,540 --> 00:39:40,540
So you can just take a lot of photos of a given room or given location and then turn

258
00:39:40,540 --> 00:39:44,540
it into VR location this way.

259
00:39:44,540 --> 00:39:50,540
And there are a few different examples here, but first just a moment, let me.

260
00:39:50,540 --> 00:39:56,540
It's a kind of 360 degree photographies, yeah?

261
00:39:56,540 --> 00:39:59,540
Yes, but it is more than that.

262
00:39:59,540 --> 00:40:04,540
It is like a 3D model made from such photos.

263
00:40:04,540 --> 00:40:16,540
And this one has a small secret even.

264
00:40:16,540 --> 00:40:20,540
Let's see.

265
00:40:20,540 --> 00:40:29,540
Ah, it's this way, sorry.

266
00:40:29,540 --> 00:40:39,540
So the secret is another way.

267
00:40:39,540 --> 00:41:01,540
So there's like a back alley here.

268
00:41:01,540 --> 00:41:10,540
And the rest of them will be more like, will be totally different style.

269
00:41:10,540 --> 00:41:15,540
So we can try other ones.

270
00:41:15,540 --> 00:41:18,540
So I'm curious about Kraftwerk.

271
00:41:18,540 --> 00:41:20,540
Oh, can you say again?

272
00:41:20,540 --> 00:41:21,540
Sorry?

273
00:41:21,540 --> 00:41:24,540
I'm curious about Kraftwerk because you have.

274
00:41:24,540 --> 00:41:26,540
Ah, yes.

275
00:41:26,540 --> 00:41:33,540
And it is not very, like I was getting it finished for the time and it's almost finished.

276
00:41:33,540 --> 00:41:40,540
It is visitable and it looks pretty nice, but I couldn't get shadows to work in time.

277
00:41:40,540 --> 00:41:43,540
Let's not switch world yet.

278
00:41:43,540 --> 00:41:45,540
Let's wait for everyone.

279
00:41:45,540 --> 00:41:46,540
Oh, wait.

280
00:41:46,540 --> 00:41:47,540
Wait.

281
00:41:47,540 --> 00:41:48,540
What is this platform?

282
00:41:48,540 --> 00:41:49,540
Is this a shuttle?

283
00:41:49,540 --> 00:41:50,540
Oh, just a moment.

284
00:41:50,540 --> 00:41:51,540
It will load.

285
00:41:51,540 --> 00:41:52,540
It seems like, yeah.

286
00:41:52,540 --> 00:41:57,540
So you can press buttons and something happens.

287
00:41:57,540 --> 00:41:58,540
Yes.

288
00:41:58,540 --> 00:42:02,540
And on this platform, you can switch different photogrammetry worlds.

289
00:42:02,540 --> 00:42:08,540
And if you are stuck in the walls, you need to go to places app, to people app, yes, to

290
00:42:08,540 --> 00:42:12,540
people app and click on our names and then you can teleport to us.

291
00:42:12,540 --> 00:42:14,540
If you are stuck.

292
00:42:14,540 --> 00:42:15,540
Okay.

293
00:42:15,540 --> 00:42:16,540
Nice.

294
00:42:16,540 --> 00:42:26,540
So if you are stuck, click on our names and then you will teleport to us in people app.

295
00:42:26,540 --> 00:42:29,540
Oh, yes, it works.

296
00:42:29,540 --> 00:42:30,540
Nice.

297
00:42:30,540 --> 00:42:35,540
Also, so this one looks almost like a photo.

298
00:42:35,540 --> 00:42:42,540
And there will be even better one in a moment.

299
00:42:42,540 --> 00:42:49,540
And those are done just by doing a lot of photos and then they're automatically computed

300
00:42:49,540 --> 00:42:53,540
from photos with photogrammetry software.

301
00:42:53,540 --> 00:42:55,540
Oh, let's try.

302
00:42:55,540 --> 00:42:59,540
Oh, this one is really cool.

303
00:42:59,540 --> 00:43:02,540
We need to visit this one.

304
00:43:02,540 --> 00:43:04,540
Oh, let's wait.

305
00:43:04,540 --> 00:43:07,540
Let's wait till it loads now.

306
00:43:07,540 --> 00:43:15,540
It doesn't take a moment to load because it's pretty big, but it's very worth seeing.

307
00:43:15,540 --> 00:43:18,540
Oh, it's loading.

308
00:43:18,540 --> 00:43:27,540
Let me know when it loads for you and we can take a look.

309
00:43:27,540 --> 00:43:28,540
Okay.

310
00:43:28,540 --> 00:43:32,540
So there's actually more than one room now.

311
00:43:32,540 --> 00:43:33,540
Yes.

312
00:43:33,540 --> 00:43:40,540
I can imagine how many photos it requires to make.

313
00:43:40,540 --> 00:43:52,540
And it is possible to like do, for example, make something like this from photos of home,

314
00:43:52,540 --> 00:43:55,540
for example, pretty much anything.

315
00:43:55,540 --> 00:44:01,540
So it's like a 3D scan, but with camera.

316
00:44:01,540 --> 00:44:04,540
That's even a fire extinguisher.

317
00:44:04,540 --> 00:44:12,540
Yes, because I'm pretty sure it was from some kind of a museum.

318
00:44:12,540 --> 00:44:41,540
Because there's also like a light, a fire light at the top or emergency light.

319
00:44:41,540 --> 00:44:58,540
Oh, let's see what the first ones are here.

320
00:44:58,540 --> 00:44:59,540
We didn't check this one.

321
00:44:59,540 --> 00:45:05,540
So let's see.

322
00:45:05,540 --> 00:45:13,540
I have to click show scene, so I will browse for a moment and let's see what else we have here.

323
00:45:13,540 --> 00:45:16,540
Ah, that one was simple, but really cool.

324
00:45:16,540 --> 00:45:25,540
And it will load quickly.

325
00:45:25,540 --> 00:45:27,540
Oh, it is not loading.

326
00:45:27,540 --> 00:45:33,540
Maybe I have to click show scene again.

327
00:45:33,540 --> 00:45:35,540
Ah, it doesn't show.

328
00:45:35,540 --> 00:45:36,540
Okay.

329
00:45:36,540 --> 00:45:44,540
So I think that another world that will be worth seeing that is very interesting is called...

330
00:45:44,540 --> 00:45:54,540
So another world that is really worth seeing and is very interesting is...

331
00:45:54,540 --> 00:45:56,540
We could visit...

332
00:45:56,540 --> 00:45:57,540
Just a moment.

333
00:45:57,540 --> 00:46:02,540
I have to check if it's available.

334
00:46:02,540 --> 00:46:04,540
Oh, I think it's available.

335
00:46:04,540 --> 00:46:05,540
That's good.

336
00:46:05,540 --> 00:46:06,540
Let's have a look.

337
00:46:06,540 --> 00:46:07,540
Okay.

338
00:46:07,540 --> 00:46:08,540
So let's go to this.

339
00:46:08,540 --> 00:46:09,540
This is a very interesting world.

340
00:46:09,540 --> 00:46:10,540
This is a very interesting world.

341
00:46:10,540 --> 00:46:11,540
And the first one, it's called Mother's Place.

342
00:46:11,540 --> 00:46:12,540
It's a very interesting world, and it's also called Mother's Place.

343
00:46:12,540 --> 00:46:13,540
And it's also called Mother's Place.

344
00:46:13,540 --> 00:46:14,540
Because there is more than one world.

345
00:46:14,540 --> 00:46:15,540
So, let's go there.

346
00:46:15,540 --> 00:46:16,540
And this is a world called Mother's Place.

347
00:46:16,540 --> 00:46:17,540
Like Mother's with two D.

348
00:46:17,540 --> 00:46:18,540
Mother's Place.

349
00:46:18,540 --> 00:46:19,540
So, let's go there.

350
00:46:19,540 --> 00:46:20,540
And that one has a very unique feature.

351
00:46:20,540 --> 00:46:21,540
So...

352
00:46:21,540 --> 00:46:22,540
Okay.

353
00:46:22,540 --> 00:46:23,540
We have to go to the places menu.

354
00:46:23,540 --> 00:46:24,540
Yes.

355
00:46:24,540 --> 00:46:25,540
Yes, places.

356
00:46:25,540 --> 00:46:46,660
yes, places and in places many mothers place. And we are very pretty as girls.

357
00:46:55,540 --> 00:47:02,540
And we are very pretty as girls.

358
00:47:02,540 --> 00:47:09,540
And we are very pretty as girls.

359
00:47:09,540 --> 00:47:16,540
And we are very pretty as girls.

360
00:47:16,540 --> 00:47:23,540
And this one has something very unusual, because there is the gallery that is here. It can be processed.

361
00:47:23,540 --> 00:47:28,540
And I think that the interface can be randomized.

362
00:47:28,540 --> 00:47:36,540
And I think that the interface can be randomized.

363
00:47:36,540 --> 00:47:45,540
Here in this place the audio is a bit broken.

364
00:47:45,540 --> 00:47:53,540
I am not sure. Can you hear me here?

365
00:48:15,540 --> 00:48:20,540
Yes, this is very interesting.

366
00:48:20,540 --> 00:48:30,540
Maybe let's go to another place.

367
00:48:30,540 --> 00:48:34,540
So I said hyperspace.

368
00:48:34,540 --> 00:48:37,540
Can you post in the chat hyperspace?

369
00:48:37,540 --> 00:48:58,540
Hyperspace, okay.

370
00:49:07,540 --> 00:49:18,540
So this is Basinski's world.

371
00:49:18,540 --> 00:49:23,540
And it should have games there.

372
00:49:23,540 --> 00:49:30,540
And there is also an art gallery by Basinski.

373
00:49:30,540 --> 00:49:41,540
So we'll be able, let's wait for everyone to load and then we could try.

374
00:49:41,540 --> 00:49:51,540
Ah, there is, so it has some secrets to find. Nice.

375
00:49:51,540 --> 00:49:58,540
There is a lot of stuff here to explore.

376
00:49:58,540 --> 00:50:02,540
But I see that it is still loading.

377
00:50:02,540 --> 00:50:14,540
So when it loads, we could go to gallery, because it is also a really nice example what you can do, how you can create virtual art galleries this way.

378
00:50:14,540 --> 00:50:21,540
And the ice looks nice.

379
00:50:21,540 --> 00:50:31,540
So let's wait until everyone is loaded and we could try a gallery.

380
00:50:31,540 --> 00:51:00,540
Really nice launch, yes.

381
00:51:02,540 --> 00:51:15,540
And of course it is possible to also make a scene that you can just give a resolution when you get close to them.

382
00:51:15,540 --> 00:51:18,540
Or to animate it, I guess.

383
00:51:18,540 --> 00:51:47,540
Yes.

384
00:51:49,540 --> 00:51:59,540
Let me know when.

385
00:51:59,540 --> 00:52:04,540
So, okay, the time for the streaming is coming.

386
00:52:04,540 --> 00:52:12,540
So for all the people out there, let me wait.

387
00:52:12,540 --> 00:52:16,540
Is it possible to hide the menu bar at the bottom?

388
00:52:16,540 --> 00:52:18,540
Yes.

389
00:52:18,540 --> 00:52:24,540
So the easiest way would be, just a moment.

390
00:52:24,540 --> 00:52:28,540
You had to add the developer menu first.

391
00:52:28,540 --> 00:52:33,540
Yes, so you have to go to settings and developer menu.

392
00:52:33,540 --> 00:52:46,540
So for now, let's say for the streamer, let's say, wave it perhaps in my direction where I'm looking.

393
00:52:46,540 --> 00:52:48,540
There is a stream camera.

394
00:52:48,540 --> 00:52:50,540
And say goodbye.

395
00:52:50,540 --> 00:52:52,540
Thank you for watching.

396
00:52:52,540 --> 00:52:57,540
Bye bye.

397
00:52:57,540 --> 00:52:58,540
Really cool.

398
00:52:58,540 --> 00:53:00,540
We need to visit this one.

399
00:53:00,540 --> 00:53:03,540
Oh, let's wait.

400
00:53:03,540 --> 00:53:07,540
Let's wait until it loads now.

401
00:53:07,540 --> 00:53:13,540
It doesn't take a moment to load because it's pretty big, but it's very worth seeing.

402
00:53:13,540 --> 00:53:20,540
Oh, it's loading.

403
00:53:20,540 --> 00:53:27,540
Let me know when it loads for you and we can take a look.

404
00:53:27,540 --> 00:53:30,540
There's actually more than one room now.

405
00:53:30,540 --> 00:53:32,540
Yes.

406
00:53:32,540 --> 00:53:41,540
I can't imagine how many photos it requires to make.

407
00:53:41,540 --> 00:53:53,540
And it is possible to like do, for example, make something like this from photos of home, for example, pretty much anything.

408
00:53:53,540 --> 00:54:00,540
It's like a 3D scan, but with camera.

409
00:54:00,540 --> 00:54:02,540
That's even a fire extinguisher.

410
00:54:02,540 --> 00:54:11,540
Yes, because I'm pretty sure it was from some kind of a museum.

411
00:54:11,540 --> 00:54:39,540
There's also like a light, a fire light at the top or emergency light.

412
00:54:39,540 --> 00:54:42,540
Let's see.

413
00:54:42,540 --> 00:54:56,540
The different ones are here.

414
00:54:56,540 --> 00:54:57,540
We didn't check this one.

415
00:54:57,540 --> 00:55:03,540
So let's see.

416
00:55:03,540 --> 00:55:10,540
I will browse for a moment and let's see what else we have here.

417
00:55:10,540 --> 00:55:13,540
Ah, that one was simple, but really cool.

418
00:55:13,540 --> 00:55:22,540
And it will load quickly.

419
00:55:22,540 --> 00:55:24,540
Oh, it is not loading.

420
00:55:24,540 --> 00:55:30,540
Maybe I have to click short screen again.

421
00:55:30,540 --> 00:55:33,540
It doesn't show.

422
00:55:33,540 --> 00:55:41,540
I think that another world that will be worth seeing, that is very interesting, is called...

423
00:55:41,540 --> 00:55:51,540
Another world that is really worth seeing and is very interesting is...

424
00:55:51,540 --> 00:55:53,540
We could visit...

425
00:55:53,540 --> 00:55:55,540
Just a moment.

426
00:55:55,540 --> 00:55:59,540
I have to check if it's available.

427
00:55:59,540 --> 00:56:01,540
Yes.

428
00:56:01,540 --> 00:56:05,540
I'm not sure why it is not available in Belarus.

429
00:56:05,540 --> 00:56:10,540
Where did I see this?

430
00:56:10,540 --> 00:56:13,540
Oh, somewhere.

431
00:56:13,540 --> 00:56:14,540
I also have a website.

432
00:56:14,540 --> 00:56:17,540
And that one has a very unique feature.

433
00:56:17,540 --> 00:56:21,540
Okay, we have to go to the places menu.

434
00:56:21,540 --> 00:56:23,540
Yes, places.

435
00:56:23,540 --> 00:56:26,540
And in places menu, mothers place.

436
00:56:26,540 --> 00:56:40,540
And it may sound sloppy.

437
00:56:40,540 --> 00:56:54,540
You may find faults, but it will be very pretty.

438
00:56:54,540 --> 00:57:08,780
Oh, for me it is loading.

439
00:57:08,780 --> 00:57:36,780
Here in this place the audio is a bit broken.

440
00:57:36,780 --> 00:57:46,780
Not sure, can you hear me here?

441
00:57:46,780 --> 00:58:14,780
Hmm.

442
00:58:14,780 --> 00:58:17,780
Yes, this is very...

443
00:58:17,780 --> 00:58:28,780
Maybe let's go to a different place.

444
00:58:28,780 --> 00:58:31,780
So I set hyperspace.

445
00:58:31,780 --> 00:58:34,780
Can you post in the chat hyperspace?

446
00:58:34,780 --> 00:59:03,780
Hyperspace, ok.

447
00:59:04,780 --> 00:59:11,780
Oh.

448
00:59:11,780 --> 00:59:16,780
So this is Basinski's world.

449
00:59:16,780 --> 00:59:20,780
And it should have games there.

450
00:59:20,780 --> 00:59:27,780
So I think there is also an art gallery by Basinski.

451
00:59:27,780 --> 00:59:31,780
So we'll be able, let's wait for everyone to load.

452
00:59:31,780 --> 00:59:38,780
And then we could try.

453
00:59:38,780 --> 00:59:42,780
Ah, there is, so it has some secrets to find.

454
00:59:42,780 --> 00:59:49,780
Nice.

455
00:59:49,780 --> 00:59:55,780
There is a lot of stuff here to explore.

456
00:59:55,780 --> 00:59:58,780
But I see that it is still loading.

457
00:59:58,780 --> 01:00:02,780
So when it loads, we could go to a gallery.

458
01:00:02,780 --> 01:00:06,780
Because it is also a really nice example of what you can do,

459
01:00:06,780 --> 01:00:11,780
how you can create virtual art galleries this way.

460
01:00:11,780 --> 01:00:18,780
And the ice looks nice.

461
01:00:18,780 --> 01:00:28,780
So let's wait until everyone is loaded and we could try a gallery.

462
01:00:48,780 --> 01:01:03,780
And of course it is possible to also make a scene.

463
01:01:03,780 --> 01:01:08,780
That images get a bigger resolution when you get close to them.

464
01:01:08,780 --> 01:01:15,780
Or to animate it, I guess.

465
01:01:15,780 --> 01:01:18,780
Yes.

466
01:01:46,780 --> 01:01:56,780
Let me know when.

467
01:01:56,780 --> 01:02:01,780
So, ok, the time for the streaming is coming.

468
01:02:01,780 --> 01:02:09,780
So for all the people out there, let me wait.

469
01:02:09,780 --> 01:02:13,780
Is it possible to hide the menu bar at the bottom?

470
01:02:13,780 --> 01:02:16,780
Yes.

471
01:02:16,780 --> 01:02:22,780
So the easiest way would be, just a moment.

472
01:02:22,780 --> 01:02:25,780
You have to add the developer menu first.

473
01:02:25,780 --> 01:02:33,780
Yes, so you have to go to settings and developer menu.

474
01:02:33,780 --> 01:02:39,780
So for now, let's say for the streamer, let's say,

475
01:02:39,780 --> 01:02:43,780
wave it perhaps in my direction where I'm looking.

476
01:02:43,780 --> 01:02:45,780
There is a stream camera.

477
01:02:45,780 --> 01:02:47,780
And say goodbye.

478
01:02:47,780 --> 01:02:49,780
Thank you for watching.

479
01:02:49,780 --> 01:03:11,780
Bye-bye.

480
01:03:19,780 --> 01:03:21,780
Bye-bye.

