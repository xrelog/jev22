1
00:00:30,000 --> 00:00:40,800
Bem-vindo à abertura do XRelog22!

2
00:00:40,800 --> 00:00:51,920
Olá Anchoray e olá para o mundo exterior! Queremos apresentar-lhe o que é o XRelog22

3
00:00:51,920 --> 00:01:01,760
 tem reservado para si este ano. O que é o XRelog22 em geral?

4
00:01:01,760 --> 00:01:11,360
 Quais são os objectivos? Queremos lidar com SocialVR e SocialVR sem tretas 

5
00:01:11,360 --> 00:01:17,000
e é isso que pomos no nosso lema. Nós chamamos-lhe 
""Visões para Multiverses Independentes"".

6
00:01:17,000 --> 00:01:23,960
Ouvi dizer que também querem integrar a ética hacker no metaverso e no multiverso.

7
00:01:23,960 --> 00:01:29,960
As extensões da meta e do multiverso. Absolutamente! Só não há o suficiente no

8
00:01:29,960 --> 00:01:35,480
O Metaverso não existe. Digamos, acabei de reparar que não vejo de todo o logótipo do canal.

9
00:01:35,480 --> 00:01:39,680
Tem um logotipo do canal? Logotipo do canal?

10
00:01:39,680 --> 00:01:49,440
O que foi isso? Que barulho foi esse?

11
00:01:49,440 --> 00:01:56,960
Sim, eu também não sei. Sim, infelizmente, foi a fita que não aguentou.

12
00:01:56,960 --> 00:02:04,960
Sim, não somos os super-vídeos. Nem realmente super profissional com OBS.

13
00:02:04,960 --> 00:02:11,680
Não somos pessoas perfeitas, por isso por vezes o logotipo do canal desaparece.

14
00:02:11,680 --> 00:02:17,440
O whisky, era apenas mau. Deve ser de má qualidade.

15
00:02:17,440 --> 00:02:21,760
Penso que ainda precisamos de ajustar um pouco as nossas expectativas. Refiro-me a isso,

16
00:02:21,760 --> 00:02:27,760
que as pessoas não devem pensar que vão ser servidas na perfeição, nem devem pensar que vão receber ARD, ZDF produção de qualidade.

17
00:02:27,760 --> 00:02:30,520
Ninguém quer ARD e ZDF.

18
00:02:30,520 --> 00:02:35,960
Mas de qualquer modo, muitas coisas estão inacabadas, mas são atraentes.

19
00:02:35,960 --> 00:02:44,960
Não está inacabado, está em produção. É ágil. É um desenvolvimento ágil.

20
00:02:44,960 --> 00:02:47,560
Quer dizer que 'Trabalho em progresso' é o novo padrão, certo?

21
00:02:47,560 --> 00:02:48,560
Sim, claro que é.

22
00:02:48,560 --> 00:02:54,320
Aha, está bem. Portanto, prepare-se para muita coisa de cabeça para baixo ... de cabeça para baixo ...

23
00:02:54,320 --> 00:02:58,720
Sim, aqui vamos nós outra vez. Prepare-se para ver muitas coisas derramadas.

24
00:02:58,720 --> 00:03:02,320
Garrafas de mate e coisas entornadas, mas vamos divertir-nos.

25
00:03:02,320 --> 00:03:04,720
Colamo-los com a fita adesiva.

26
00:03:04,720 --> 00:03:05,400
Exactamente.

27
00:03:05,400 --> 00:03:24,360
Mas é realmente pronunciado XRelog22.

28
00:03:24,360 --> 00:03:31,960
Assim, e isto é derivado do XR.Labs Operation Group.

29
00:03:31,960 --> 00:03:39,800
Estou aqui no ICCBS, veja. Estamos a construir mundos virtuais.

30
00:03:39,800 --> 00:03:47,800
Um deles, um veículo que viaja através do mundo virtual, é o nosso ICCBS, 

31
00:03:47,800 --> 00:03:55,000
Estúdio Intergalactical Chaos Communication Broadcast. E atracou em Overte para este evento.

32
00:03:55,000 --> 00:04:02,760
Aí está, estás a ver, estás a ver, está bastante espancado. Isso é porque tivemos aqui alguns acidentes.

33
00:04:02,760 --> 00:04:10,600
E todos os belos acessórios interiores não estavam bem arranjados, pelo que ficaram um pouco danificados. 

34
00:04:10,600 --> 00:04:17,120
Mas estamos a consertá-lo lindamente.

35
00:04:17,120 --> 00:04:23,920
Anda, vou mostrar-te uma coisa. Porque queremos fazer algumas especialidades nos cantos.

36
00:04:23,920 --> 00:04:31,960
Não sei quem vai reconhecer isto, aqueles que já estiveram em

37
00:04:31,960 --> 00:04:39,680
 a C-Base irá reconhecê-la. São paredes de luz, feitas de garrafas de mate.

38
00:04:39,680 --> 00:04:49,760
E aqui estamos a olhar, por exemplo, para a construção do que se chama um Gémeo Digital deste dispositivo.

39
00:04:49,760 --> 00:04:55,120
E aqui estão as instruções para a construção da interface de programação da aplicação, para que saiba como utilizá-la. 

40
00:04:55,120 --> 00:05:01,480
Aqui está este sítio web, pode olhá-lo e até lê-lo. E aqui está um exemplo de como se poderia utilizar este espaço.

41
00:05:01,480 --> 00:05:08,920
Vamos colocar aqui este ecrã LED.

42
00:05:08,920 --> 00:05:15,120
Voo um bocadinho. Aqui também se pode voar. Tenho de praticar um pouco de voo. 

43
00:05:15,120 --> 00:05:21,880
Uau, agora estou a voar aqui. Olha para aqui.  Ainda há aqui um buraco, vês? Está novamente partido. 

44
00:05:21,880 --> 00:05:26,680
Aqui temos de o consertar um pouco mais. Essa é outra tarefa. É assim, devido aos acidentes de aterragem.

45
00:05:26,680 --> 00:05:32,520
Mais uma vez aqui. Acabo de passar por um portal e esta é a nossa sala de partida.

46
00:05:32,520 --> 00:05:42,920
Agora podemos ir até ao zepelim. A terra está a girar. Fixe...

47
00:05:42,920 --> 00:05:51,920
 podemos fazer isto ... muito fixe. Agora podemos ver o ICCBS a partir do exterior. Então eu sou

48
00:05:51,920 --> 00:05:58,600
aqui, dentro do zepelim. E há também um portal. Vejamos onde isso nos leva? Vamos para dentro.

49
00:05:58,600 --> 00:06:08,400
Ah sim, isso leva-nos de volta. Ah, foi aí que eles mudaram os portais. É bom que tenhamos falado sobre isso.

50
00:06:08,400 --> 00:06:13,720
Na verdade, o portal pelo qual acabei de passar deveria conduzir aqui. 

51
00:06:13,720 --> 00:06:23,080
Vamos dar uma vista de olhos aqui. O que é isso? Olha, uma escuridão... Olha, é o Pó de Fada.

52
00:06:23,080 --> 00:06:33,160
Não posso acreditar. Esta é a primeira vez que a vejo. Uau, não é nada mau. De facto, é, 

53
00:06:33,160 --> 00:06:39,880
As coisas ainda estão a girar lá em cima? Já não giram. Sim, giram, giram muito lentamente. 

54
00:06:39,880 --> 00:06:44,960
Aqui vemos o nó de dados em diferentes representações artísticas. E temos uma fogueira de acampamento.

55
00:06:44,960 --> 00:06:56,560
E está a crepitar. É realmente acolhedor aqui. Espero que nos encontremos aqui frequentemente.

56
00:06:56,560 --> 00:07:02,720
Bem, se ando por aqui, também posso ir à cave do Jev22.

57
00:07:02,720 --> 00:07:13,960
E a cave tem este aspecto. Whoa, whoa, whoa, whoa, whoa. Vamos ver aqui. 

58
00:07:13,960 --> 00:07:20,520
Estes são todos os eventos, as páginas iniciais do evento. Pode olhar para todos eles.

59
00:07:20,520 --> 00:07:27,240
Há muitos deles. Sim, vamos ver aqui. Hacking em paralelo. E quando se chega perto, há um site realmente interactivo.

60
00:07:27,240 --> 00:07:33,000
Pode clicar sobre este e folheá-lo. E funciona para todos os 38 eventos.

61
00:07:33,000 --> 00:07:42,640
Pode olhar para todos eles e participar.

62
00:07:42,640 --> 00:07:54,120
Chili con Chaos . Sim, aqui pode ver o Chili con Chaos. O programa é muito interessante. 

63
00:07:54,120 --> 00:08:01,480
Podemos usar Lugares para voar para o nosso espaço Maker.  

64
00:08:01,480 --> 00:08:11,680
O Makerspace é simplesmente chamado XRelog22. E este espaço de criação é agora uma espécie de colecção de coisas inacabadas. 

65
00:08:11,680 --> 00:08:17,120
de coisas inacabadas. Começámos lá as coisas, numa ordem dispersa. 

66
00:08:17,120 --> 00:08:25,840
por exemplo, tenho a ideia, para as 15 faixas, assim 15 temas, que nós temos. E se fizéssemos uma ronda de pistas.

67
00:08:25,840 --> 00:08:38,800
construir 15 andares. Sim, muito fixe. Também quero ser capaz de voar. Sim, voar é uma boa ideia.

68
00:08:38,800 --> 00:08:45,840
Numa inspecção mais atenta, estes pisos parecem-se um pouco com pequenos salões de partida. 

69
00:08:45,840 --> 00:08:53,800
Muito pequeno e muito mini. E em cada um destes pisos, há um tema dedicado. 

70
00:08:53,800 --> 00:09:02,200
Começámos até ao terceiro andar.

71
00:09:02,200 --> 00:09:09,480
Quer seja podcasting, quer seja SocialVR, quer sejam comunidades XR, há muitas delas e assim por diante.

72
00:09:09,480 --> 00:09:16,680
E, claro, nos primeiros andares, há (H)Activismo.

73
00:09:16,680 --> 00:09:28,240
Este é, naturalmente, o nosso tema principal.

74
00:09:28,240 --> 00:09:36,160
Espera, estou a voar para lá. Está aí escrito, mas a lâmpada não está acesa.

75
00:09:36,160 --> 00:09:42,040
Mudei a lâmpada para algum lado. Agora não se acende de todo.

76
00:09:42,040 --> 00:09:48,800
Em geral, aqui é muito claro. Podemos ler isto.
Não, o hectivismo está no escuro. 
sobre a ética dos hackers.

77
00:09:48,800 --> 00:09:55,160
Olha à tua volta. Esta é uma das primeiras coisas que fiz.

78
00:09:55,160 --> 00:10:02,920
Desenhei a ética hacker em 3D em realidade virtual com um pincel múltiplo. 

79
00:10:02,920 --> 00:10:11,240
E depois integrei aqui esta pintura como modelo 3D. E até integrei uma síntese de voz, em alemão e inglês.

80
00:10:11,240 --> 00:10:18,160
Bem, o que descobri na investigação sobre ética hacker,

81
00:10:18,160 --> 00:10:24,600
é que as primeiras seis regras dos hackers são mais antigas,

82
00:10:24,600 --> 00:10:30,440
datam dos anos 60 e apenas duas regras foram acrescentadas pelo Computer Chaos Club

83
00:10:30,440 --> 00:10:36,760
Em particular, não mexer com os dados de outras pessoas.

84
00:10:36,760 --> 00:10:43,840
Utilizar dados públicos, proteger dados privados. Penso que já o ouvimos antes. 

85
00:10:43,840 --> 00:10:50,520
Estes são os dois complementos do Computer-Chaos-Club. Tomámos a liberdade de acrescentar uma nona regra.

86
00:10:50,520 --> 00:11:00,720
Pense sempre em pedaços e árvores. Porque o aspecto ecológico ainda não faz parte da ética dos hackers. 

87
00:11:00,720 --> 00:11:07,280
É por isso que deve ser concluída. 

88
00:11:07,280 --> 00:11:14,320
Essa era a nossa ética de hacker. Caso contrário, é uma colecção de todo o tipo de coisas.

89
00:11:14,320 --> 00:11:17,560
Pode ver o zepelim no topo.

90
00:11:17,560 --> 00:11:24,880
Acima do zepelim, existe uma primeira versão do ICCBS. 

91
00:11:24,880 --> 00:11:30,440
Aqui há bandejas. Utilizaremos estes tabuleiros quando organizarmos workshops.

92
00:11:30,440 --> 00:11:36,280
Depois podemos voar para temas e ouvir coisas. 

93
00:11:36,280 --> 00:11:43,160
Nós mexemos de forma tão simples. Este é o nosso espaço criador virtual.

94
00:11:43,160 --> 00:11:52,040
Não tem qualquer ligação temática. 

95
00:11:52,040 --> 00:11:59,000
Quando terminarmos a Tracktower, esta será transferida para o seu próprio servidor de domínio. 

96
00:11:59,000 --> 00:12:07,320
Depois retiramo-lo do makerspace e construímos algo novo. 

97
00:12:07,320 --> 00:12:20,400
Agora vou voltar ao Estúdio de Comunicação do Caos Intergaláctico. ICCBS para abreviar. 

98
00:12:20,400 --> 00:12:31,080
Dêem uma vista de olhos. De onde venho. Então qualquer pessoa pode simplesmente mudar o local, quem está no sistema?

99
00:12:31,080 --> 00:12:37,960
Sim, tudo está aberto. Nem sequer tem de se registar. 

100
00:12:37,960 --> 00:12:44,560
Nem sequer precisa de criar uma conta. Basta descarregar o cliente e executá-lo para entrar neste mundo.

101
00:12:44,560 --> 00:12:49,320
Receberá uma breve explicação sobre o seu funcionamento.

102
00:12:49,320 --> 00:12:55,960
Então já pode utilizar a função Lugares. Poderá então descobrir ainda mais mundos.

103
00:12:55,960 --> 00:13:07,080
Assim, ICCBS e Fairy Dust e XRelog ou mais tarde também Tracktower.

104
00:13:07,080 --> 00:13:14,040
Isso são apenas três mundos em dezenas de mundos.

105
00:13:14,040 --> 00:13:18,240
Haverá provavelmente centenas deles para visitar em breve.

106
00:13:18,240 --> 00:13:27,520
Claro que também há mundos que não estão abertos ao público, como um Darling VR. 

107
00:13:27,520 --> 00:13:32,520
É um pouco mais colorido sexualmente. Não queremos que apenas qualquer pessoa possa entrar. 

108
00:13:32,520 --> 00:13:39,280
Mas, caso contrário, a maioria dos mundos estão abertos ao público, 

109
00:13:39,280 --> 00:13:49,200
livremente, sem tretas, sem NFT, contrato inteligente, cadeia de bloqueios, cripto-trucção. 

110
00:13:49,200 --> 00:13:52,560
Tudo isto não está aqui. É tão bonito.

111
00:13:52,560 --> 00:14:09,880
Anchoray, mostrou-nos tantas coisas deste mundo. 

112
00:14:09,880 --> 00:14:12,960
Mas o que tenciona fazer no Congresso? Não disse nada sobre isso.

113
00:14:12,960 --> 00:14:25,080
Sim, é isso mesmo. Temos 15 sessões, e 15 tópicos.

114
00:14:25,080 --> 00:14:36,440
e estamos também a olhar para os outros 38 eventos JEV22. O nosso tema principal é a crítica da tecnologia. 

115
00:14:36,440 --> 00:14:42,240
Críticas claras à tecnologia. Não queremos deixar o Facebook escapar impune.

116
00:14:42,240 --> 00:14:48,800
Queremos falar sobre a Web 3.0. Queremos olhar criticamente para a forma como isto se está a desenvolver.

117
00:14:48,800 --> 00:14:53,520
Preparámos algumas sessões muito interessantes para si.

118
00:14:53,520 --> 00:14:58,280
Depois queremos olhar para além do fim do nosso nariz e também olhar para outros eventos JEV.

119
00:14:58,280 --> 00:15:04,920
Há algumas iniciativas absolutamente fantásticas, de Nova Iorque a Atenas. 

120
00:15:04,920 --> 00:15:12,560
Em Munique há dois, em Hamburgo há quatro no total. Em Berlim, em Erlangen, em Dusseldorf, em Aachen.

121
00:15:12,560 --> 00:15:15,800
Há muito a descobrir.

122
00:15:15,800 --> 00:15:17,280
Mas será tão divertido quanto isso?

123
00:15:17,280 --> 00:15:23,640
Oh, divertido! Sim, é claro. É verdade que não temos o Jeopardy,

124
00:15:23,640 --> 00:15:29,080
não conseguimos preparar mais perguntas agradáveis para ele. 

125
00:15:29,080 --> 00:15:38,400
Mas estamos a tentar fazer algo tão louco. Vamos chamar-lhe o Campeonato do Mundo de Mafifa 2030. Vamos repetir o sorteio para o Campeonato do Mundo de 2030.

126
00:15:38,400 --> 00:15:44,920
Deixe-se surpreender. Todas as vezes às 23h00 de quarta-feira, quinta-feira e sexta-feira.

127
00:15:44,920 --> 00:15:47,400
Isto vai ser interessante.

128
00:15:47,400 --> 00:15:50,600
Deve estar a fazer isto de uma forma muito corrupta.

129
00:15:50,600 --> 00:15:57,840
O mate conta como moeda?

130
00:15:57,840 --> 00:16:05,720
Se não conta como moeda, então também não somos corruptos. 

131
00:16:05,720 --> 00:16:12,880
O importante é que temos muito, muito poucas apresentações frontais.

132
00:16:12,880 --> 00:16:19,640
Quase tudo é uma oficina participativa, concebida como um Barcamp. 

133
00:16:19,640 --> 00:16:28,080
Há um acampamento de barras XR, logo após esta abertura. Mas também há um Bits e Árvores.

134
00:16:28,080 --> 00:16:34,200
Fireside Chat, onde é cordialmente convidado a participar.

135
00:16:34,200 --> 00:16:39,760
Também os workshops da Overte, onde exploramos este mundo, 

136
00:16:39,760 --> 00:16:41,080
são também concebidos para serem seminários e não palestras.

137
00:16:41,080 --> 00:16:50,760
Quer dizer novamente como é que temos acesso ao mundo? 

138
00:16:50,760 --> 00:16:57,800
Porque vemos este mundo realmente grande, mas como é que entramos nele? 

139
00:16:57,800 --> 00:16:58,800
Podemos visitá-lo?

140
00:16:58,800 --> 00:17:04,520
Sim, é claro. Portanto, o principal é que estamos em casa na Matrix. 

141
00:17:04,520 --> 00:17:11,960
Na programação do Pretalx, é indicado um canal Matrix para cada apresentação. 

142
00:17:11,960 --> 00:17:16,760
É aqui que nos encontramos e também trocamos toda a informação. 

143
00:17:16,760 --> 00:17:29,820
A outra coisa é este Metaverso, este Overte Multiverso.

144
00:17:29,820 --> 00:17:35,520
Overte.org já é a página inicial.
 Pode descarregar um cliente lá.

145
00:17:35,520 --> 00:17:42,880
Existe para Linux e para Windows. Curiosamente, não está disponível para a Apple. 

146
00:17:42,880 --> 00:17:51,840
Pode também utilizá-lo para nos visitar no ICCBS, na floresta da Fada do Pó. 

147
00:17:51,840 --> 00:17:57,160
ou num espaço hacker virtual no xrelog com a Torre de Pista. 

148
00:17:57,160 --> 00:17:58,160
Pode explorar tudo isto você mesmo.

149
00:17:58,160 --> 00:18:08,760
Isso parece excitante e penso eu, 

150
00:18:08,760 --> 00:18:11,600
alguns ouvintes também devem achar muito excitante encontrá-lo no espaço 3D.

151
00:18:11,600 --> 00:18:17,640
Estou curioso por saber. Quantos telespectadores irão os servidores entrar em colapso? 

152
00:18:17,640 --> 00:18:19,040
vamos descobrir juntos.

153
00:18:19,040 --> 00:18:20,040
Testes, testes.

154
00:18:20,040 --> 00:18:26,200
Podemos também fornecer outros servidores. Mas ainda não estamos automatizados. 

155
00:18:26,200 --> 00:18:32,280
provisão de outros servidores. Mas ainda não estamos muito automatizados. 

156
00:18:32,280 --> 00:18:36,840
Demora um pouco, mas estamos prontos. 

157
00:18:36,840 --> 00:18:38,080
Se cairmos agora, ainda podemos fazer alguma coisa.

158
00:18:38,080 --> 00:18:45,520
Finalmente, gostaria de dizer um caloroso adeus. 

159
00:18:45,520 --> 00:18:51,240
A propósito, também de, uh, uh, uh, quem é você exactamente?

160
00:18:51,240 --> 00:18:56,560
Desculpe Anchoray, não lhe posso dizer.

161
00:18:56,560 --> 00:19:25,560
Por isso, teremos de resolver este puzzle da próxima vez. Vejo-vos mais tarde. Adeusinho.


