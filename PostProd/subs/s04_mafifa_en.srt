1
00:00:00,000 --> 00:00:23,000
We have Mr.Capture here, but we are still missing Harris.

2
00:00:23,000 --> 00:00:30,000
We have to look where Harris is.

3
00:00:30,000 --> 00:00:35,000
Ah, now I can hear you.

4
00:00:35,000 --> 00:00:40,000
Capture is there, Harris is there. Hey, we're almost complete, I would say.

5
00:00:40,000 --> 00:00:48,000
Now I just have to get your audio into the stream.

6
00:00:48,000 --> 00:00:56,000
Harris, can you say something? Something a little longer, one after the other.

7
00:00:56,000 --> 00:00:58,000
You can hear me, okay.

8
00:00:58,000 --> 00:01:03,000
Yes, now you can be heard in the stream. Say something again.

9
00:01:03,000 --> 00:01:04,000
Test test, one, two, three.

10
00:01:04,000 --> 00:01:05,000
Ah, that looks very good.

11
00:01:05,000 --> 00:01:09,000
I have the opportunity to adjust the name again.

12
00:01:09,000 --> 00:01:14,000
The world hears you, us or something.

13
00:01:14,000 --> 00:01:32,000
Now I'll see if I can get that into the Jitsi stream.

14
00:01:32,000 --> 00:01:42,000
You see, something is flashing up there. That's what I'm transmitting to the stream plus your audio signal.

15
00:01:42,000 --> 00:01:45,000
That's all really exciting, what's going on.

16
00:01:45,000 --> 00:01:55,000
And now I'm going to switch to ICC BS, if I find the right button.

17
00:01:55,000 --> 00:01:59,000
I think I have to turn this off.

18
00:01:59,000 --> 00:02:03,000
Welcome to ICC BS.

19
00:02:03,000 --> 00:02:04,000
Look, I can jump.

20
00:02:04,000 --> 00:02:06,000
I thought, wait a minute, now I can jump.

21
00:02:06,000 --> 00:02:11,000
You see, jump, jump, jump, jump.

22
00:02:11,000 --> 00:02:14,000
Yes, FIFA World Cup 2030.

23
00:02:14,000 --> 00:02:20,000
I thought, let's make something funny out of it.

24
00:02:20,000 --> 00:02:29,000
But I just found out that our Amiga Tipkick emulator V2.1 is smeared.

25
00:02:29,000 --> 00:02:36,000
I was able to save the program, but I don't know how to get it uploaded to this virtual web Amiga.

26
00:02:36,000 --> 00:02:41,000
So a first question to all beings out there listening to us.

27
00:02:41,000 --> 00:02:51,000
Does anyone know Amiga Commodore, no, not C64, just with the Amiga?

28
00:02:51,000 --> 00:02:55,000
Because otherwise we would have to improvise.

29
00:02:55,000 --> 00:02:59,000
Oh, that could work for us.

30
00:02:59,000 --> 00:03:09,000
But you can see here on the right of me, no, the other way around, so if you look at it, on the right of me, you can actually see the 18 finalists.

31
00:03:09,000 --> 00:03:13,000
They actually made it to the World Cup final round.

32
00:03:13,000 --> 00:03:27,000
And here on the left side you can see a screenshot of what a game transmission would look like if our Amiga Tipkick transmission software were to run.

33
00:03:27,000 --> 00:03:32,000
You see, life is complicated.

34
00:03:32,000 --> 00:03:39,000
At least we're not the FIFA, uh, the UEFA, because at UEFA you can't even make a solution.

35
00:03:39,000 --> 00:03:44,000
So we're better than the UEFA.

36
00:03:44,000 --> 00:03:49,000
But I see a problem, someone has panned.

37
00:03:49,000 --> 00:03:59,000
We have to build four pots with 16 teams, but if I count that correctly, that's 18.

38
00:03:59,000 --> 00:04:08,000
We'll have to correct the numbers, I don't know how we can get that out, but somehow we'll get it fair and democratic.

39
00:04:08,000 --> 00:04:12,000
Okay, fair and democratic. Do you have a cube?

40
00:04:12,000 --> 00:04:18,000
You'll find something for sure.

41
00:04:18,000 --> 00:04:22,000
Wait, we're still professional, we'll solve it with Excel.

42
00:04:22,000 --> 00:04:34,000
You can just generate random values or random area and then you can get a very correct result.

43
00:04:34,000 --> 00:04:37,000
Who gets kicked?

44
00:04:37,000 --> 00:04:41,000
Okay, then you have to help me now.

45
00:04:41,000 --> 00:04:48,000
With all the streams here and all the sources and all the windows, I don't dare to open anything else now.

46
00:04:48,000 --> 00:04:53,000
I'm here today, so I can support Neste.

47
00:04:53,000 --> 00:04:56,000
Ah, sober.

48
00:04:56,000 --> 00:05:00,000
Do you just want to make the list alphabetical?

49
00:05:00,000 --> 00:05:04,000
I've also ticked them off somewhere.

50
00:05:04,000 --> 00:05:07,000
Let's see if I can find it under the many windows.

51
00:05:07,000 --> 00:05:11,000
I thought if the Amiga thing is broken, we have to do something else.

52
00:05:11,000 --> 00:05:14,000
So I saved the finalists.

53
00:05:14,000 --> 00:05:18,000
Wait a minute, I'll do it dry.

54
00:05:18,000 --> 00:05:26,000
I'll just copy the list in the concept pad.

55
00:05:26,000 --> 00:05:31,000
And boldly, I'll just post it in the matrix chat.

56
00:05:31,000 --> 00:05:34,000
Look here, attention, that's the list.

57
00:05:34,000 --> 00:05:43,000
Maybe you can sort them with Excel or something like that.

58
00:05:43,000 --> 00:05:52,000
So, a random generator says from these 18 entries,

59
00:05:52,000 --> 00:05:55,000
Who has to go?

60
00:05:55,000 --> 00:05:58,000
exit.

61
00:05:58,000 --> 00:06:01,000
Entry number 6.

62
00:06:01,000 --> 00:06:04,000
Cameroon.

63
00:06:04,000 --> 00:06:08,000
I think it's one that lands in Africa, that has to go.

64
00:06:08,000 --> 00:06:14,000
Can we maybe change that to Scotland?

65
00:06:14,000 --> 00:06:19,000
Let's see what happens.

66
00:06:19,000 --> 00:06:23,000
And number 13.

67
00:06:23,000 --> 00:06:26,000
Denmark.

68
00:06:26,000 --> 00:06:29,000
I'll take that out.

69
00:06:29,000 --> 00:06:36,000
In good German tradition we can also say, Denmark, Germany, it's all the same anyway.

70
00:06:36,000 --> 00:06:43,000
But we didn't enter the war yet, did we?

71
00:06:43,000 --> 00:06:46,000
I think we got the wrong one.

72
00:06:46,000 --> 00:06:50,000
So Denmark I've gone through.

73
00:06:50,000 --> 00:06:53,000
And Cameroon.

74
00:06:53,000 --> 00:06:56,000
Sorry, but we have it legally correct.

75
00:06:56,000 --> 00:06:59,000
Shouldn't we dice again?

76
00:06:59,000 --> 00:07:02,000
Because the only African team.

77
00:07:02,000 --> 00:07:06,000
Excitement.

78
00:07:06,000 --> 00:07:09,000
Second throw.

79
00:07:09,000 --> 00:07:12,000
Entry number 14.

80
00:07:12,000 --> 00:07:14,000
Mexico.

81
00:07:14,000 --> 00:07:18,000
Mexico is a shame, but better than Cameroon.

82
00:07:18,000 --> 00:07:21,000
That's the whole objective.

83
00:07:21,000 --> 00:07:26,000
Absolutely, objectivity is our second name after corruption.

84
00:07:26,000 --> 00:07:30,000
I'm sorry, but the payments weren't high enough.

85
00:07:30,000 --> 00:07:34,000
You have to say what it is.

86
00:07:34,000 --> 00:07:38,000
It's an exciting country.

87
00:07:38,000 --> 00:07:45,000
There's a reason why the Formula 1 race is called the Mexico City Grand Prix.

88
00:07:45,000 --> 00:07:49,000
Because the city is stealing the money, which the country doesn't want to give.

89
00:07:49,000 --> 00:07:51,000
Good.

90
00:07:51,000 --> 00:07:54,000
So we have 16 finalists.

91
00:07:54,000 --> 00:07:57,000
4 dice.

92
00:07:57,000 --> 00:08:00,000
And do we do it like they always do?

93
00:08:00,000 --> 00:08:05,000
That we distribute the dice with the number 1?

94
00:08:05,000 --> 00:08:16,000
So that it's not in a group of France, Germany, England, Spain or something?

95
00:08:16,000 --> 00:08:28,000
Or is it just a race of continents?

96
00:08:28,000 --> 00:08:31,000
We could do it randomly.

97
00:08:31,000 --> 00:08:34,000
Dice for continents.

98
00:08:34,000 --> 00:08:37,000
Oh, we do dice for continents?

99
00:08:37,000 --> 00:08:40,000
Or against it.

100
00:08:40,000 --> 00:08:43,000
So that we say there should be as few of them as possible.

101
00:08:43,000 --> 00:08:46,000
Dice for continents.

102
00:08:46,000 --> 00:08:51,000
Then each continent will come after the group phase 1.

103
00:08:51,000 --> 00:08:54,000
The problem is Africa.

104
00:08:54,000 --> 00:08:57,000
We don't have that many.

105
00:08:57,000 --> 00:09:00,000
You have to jump over the shadow.

106
00:09:00,000 --> 00:09:08,000
But you can see that you can throw together the European ones.

107
00:09:08,000 --> 00:09:14,000
Germany, Italy, England, Netherlands.

108
00:09:14,000 --> 00:09:18,000
Someone in the editorial team seems to be wrong.

109
00:09:18,000 --> 00:09:21,000
Or the software is wrong.

110
00:09:21,000 --> 00:09:24,000
But we're not at UEFA.

111
00:09:24,000 --> 00:09:27,000
We don't make software errors.

112
00:09:27,000 --> 00:09:30,000
But the software is from 1990.

113
00:09:30,000 --> 00:09:35,000
I think back then you were allowed to say Holland.

114
00:09:35,000 --> 00:09:38,000
Morocco is also in it.

115
00:09:38,000 --> 00:09:41,000
We have a watcher.

116
00:09:41,000 --> 00:09:44,000
Who do we throw out?

117
00:09:44,000 --> 00:09:47,000
No, it's fine.

118
00:09:47,000 --> 00:09:52,000
How do we get to 4 pots?

119
00:09:52,000 --> 00:09:56,000
Just dice, just shuffle and see?

120
00:09:56,000 --> 00:09:59,000
Let's do a random principle.

121
00:09:59,000 --> 00:10:02,000
We want to keep it democratic.

122
00:10:02,000 --> 00:10:05,000
We want to follow all the rules.

123
00:10:05,000 --> 00:10:10,000
We could ask the NSA if they have random generators.

124
00:10:10,000 --> 00:10:13,000
They can spiel them all out.

125
00:10:13,000 --> 00:10:16,000
Not just one.

126
00:10:16,000 --> 00:10:19,000
Okay, then the Americans have us.

127
00:10:19,000 --> 00:10:23,000
They support us with us and we support them with them.

128
00:10:23,000 --> 00:10:30,000
As far as random generators are concerned, we are world champions.

129
00:10:30,000 --> 00:10:36,000
Cheers, I'm drinking a cold drink.

130
00:10:36,000 --> 00:10:43,000
I can join in with a Kölsch.

131
00:10:43,000 --> 00:10:46,000
Okay, my bad.

132
00:10:46,000 --> 00:10:49,000
The cast doesn't want to open.

133
00:10:49,000 --> 00:10:53,000
Okay, we'll do that later.

134
00:10:53,000 --> 00:10:56,000
How do we do that?

135
00:10:56,000 --> 00:11:01,000
Do we put in an Excel-speller and do it all over again?

136
00:11:01,000 --> 00:11:07,000
The problem is, we wanted to have a special guest today.

137
00:11:07,000 --> 00:11:10,000
John Masternon should be here today.

138
00:11:10,000 --> 00:11:13,000
He's not coming, is he?

139
00:11:13,000 --> 00:11:17,000
Unfortunately, he has to cancel because he's overworked.

140
00:11:17,000 --> 00:11:22,000
He has to do a lot of work and set up a lot of servers.

141
00:11:22,000 --> 00:11:26,000
He's so busy with servers that he doesn't have time to join us.

142
00:11:26,000 --> 00:11:29,000
We're sorry about that.

143
00:11:29,000 --> 00:11:32,000
But he'll be invited next time.

144
00:11:32,000 --> 00:11:37,000
John, if you're listening, feel honored.

145
00:11:37,000 --> 00:11:40,000
We're sorry you can't join us today.

146
00:11:40,000 --> 00:11:43,000
And get some rest.

147
00:11:43,000 --> 00:11:47,000
Even though Elon Musk is giving you more work than ever.

148
00:11:47,000 --> 00:11:50,000
That's Elon Musk again.

149
00:11:50,000 --> 00:11:55,000
Okay, so we don't have John Masternon to help us out of the mess.

150
00:11:55,000 --> 00:11:58,000
We'll have to do it ourselves.

151
00:11:58,000 --> 00:12:02,000
That's true, but...

152
00:12:02,000 --> 00:12:10,000
Where do we have a pot where we can do it professionally?

153
00:12:10,000 --> 00:12:13,000
We can...

154
00:12:13,000 --> 00:12:16,000
Yes.

155
00:12:19,000 --> 00:12:22,000
We can just type in a name.

156
00:12:22,000 --> 00:12:25,000
I'll write it out here.

157
00:12:25,000 --> 00:12:28,000
Group A.

158
00:12:28,000 --> 00:12:31,000
Group A.

159
00:12:31,000 --> 00:12:34,000
Then I'll take over the drawing.

160
00:12:34,000 --> 00:12:36,000
Exactly.

161
00:12:36,000 --> 00:12:41,000
Then I won't let the money we've put aside irritate you.

162
00:12:41,000 --> 00:12:48,000
Nobody wanted to take anything from us.

163
00:12:48,000 --> 00:12:51,000
Nobody wanted to build a wall.

164
00:12:51,000 --> 00:12:54,000
We never intended to build a wall.

165
00:12:54,000 --> 00:12:58,000
That was the wrong historical context.

166
00:12:58,000 --> 00:13:01,000
I'll write down the four groups.

167
00:13:01,000 --> 00:13:03,000
Group A.

168
00:13:03,000 --> 00:13:05,000
Group B.

169
00:13:05,000 --> 00:13:10,000
Group C.

170
00:13:10,000 --> 00:13:14,000
Group D.

171
00:13:14,000 --> 00:13:18,000
The basics are laid out.

172
00:13:18,000 --> 00:13:20,000
So.

173
00:13:20,000 --> 00:13:26,000
We'll prepare all our systems.

174
00:13:26,000 --> 00:13:42,000
Our consultant, Mr. Kaptscher, is very careful about all the wrong things.

175
00:13:42,000 --> 00:13:45,000
So, let's get started.

176
00:13:45,000 --> 00:13:46,000
Yes.

177
00:13:46,000 --> 00:13:49,000
Thanks for writing along.

178
00:13:49,000 --> 00:14:02,000
One moment. I'll rearrange this a bit.

179
00:14:02,000 --> 00:14:09,000
Cold drink is good.

180
00:14:09,000 --> 00:14:11,000
Yes, so group A.

181
00:14:11,000 --> 00:14:20,000
We'll start with group A.

182
00:14:20,000 --> 00:14:26,000
Group A is Germany, the Netherlands, Italy and England.

183
00:14:26,000 --> 00:14:28,000
Germany.

184
00:14:28,000 --> 00:14:30,000
I pressed return too fast.

185
00:14:30,000 --> 00:14:36,000
No, it's just the group that's starting.

186
00:14:36,000 --> 00:14:38,000
One moment.

187
00:14:38,000 --> 00:14:42,000
So, group A is Germany.

188
00:14:42,000 --> 00:14:44,000
Yes, I noted that.

189
00:14:44,000 --> 00:14:46,000
From group 2.

190
00:14:46,000 --> 00:14:48,000
In group B then?

191
00:14:48,000 --> 00:14:52,000
Brazil, Cameroon, Argentina and the USSR.

192
00:14:52,000 --> 00:14:55,000
Brazil, Brazil.

193
00:14:55,000 --> 00:14:58,000
Then comes Cameroon.

194
00:14:58,000 --> 00:15:00,000
Then group B is Cameroon.

195
00:15:00,000 --> 00:15:01,000
Group A.

196
00:15:01,000 --> 00:15:02,000
Group A.

197
00:15:02,000 --> 00:15:04,000
Now I understand how you do it.

198
00:15:04,000 --> 00:15:09,000
Yes, very good. I noted Cameroon.

199
00:15:09,000 --> 00:15:14,000
From group C, from group 3, comes group A.

200
00:15:14,000 --> 00:15:16,000
Scotland.

201
00:15:16,000 --> 00:15:19,000
Scotland. I'm noting Scotland.

202
00:15:19,000 --> 00:15:21,000
One moment, I was distracted.

203
00:15:21,000 --> 00:15:26,000
Scotland.

204
00:15:26,000 --> 00:15:35,000
And the last part, group A, from group 4, Belgium.

205
00:15:35,000 --> 00:15:38,000
Belgium, that's not bad.

206
00:15:38,000 --> 00:15:42,000
We have very good German experience with that.

207
00:15:42,000 --> 00:15:45,000
How many times have we fought?

208
00:15:45,000 --> 00:15:48,000
Both in the military and in football.

209
00:15:48,000 --> 00:15:50,000
I don't want to go into the military.

210
00:15:50,000 --> 00:15:59,000
I think we fought 8 times in the military.

211
00:15:59,000 --> 00:16:01,000
We have to hand that over to the fact checker.

212
00:16:01,000 --> 00:16:03,000
He can check that afterwards.

213
00:16:03,000 --> 00:16:05,000
Let's move on to group G.

214
00:16:05,000 --> 00:16:09,000
Before I get caught in embarrassing ignorance.

215
00:16:09,000 --> 00:16:15,000
Group B, from pot number 1, comes Italy.

216
00:16:15,000 --> 00:16:19,000
Italy.

217
00:16:19,000 --> 00:16:22,000
Ciao, Bella, ciao, ciao, ciao.

218
00:16:22,000 --> 00:16:25,000
Pot number 2, Cameroon.

219
00:16:25,000 --> 00:16:34,000
Cameroon.

220
00:16:34,000 --> 00:16:38,000
Pot number 3, Turkey.

221
00:16:38,000 --> 00:16:40,000
Turkey.

222
00:16:40,000 --> 00:16:43,000
We need one more.

223
00:16:43,000 --> 00:16:46,000
And pot number 4, Poland.

224
00:16:46,000 --> 00:16:49,000
Not a bad and uninteresting group.

225
00:16:49,000 --> 00:16:52,000
That could be an exciting game.

226
00:16:52,000 --> 00:16:54,000
Now we come to group C.

227
00:16:54,000 --> 00:16:57,000
What do we have there?

228
00:16:57,000 --> 00:17:05,000
One moment.

229
00:17:05,000 --> 00:17:11,000
Group C is coming now.

230
00:17:11,000 --> 00:17:16,000
One moment.

231
00:17:16,000 --> 00:17:19,000
We have already given up.

232
00:17:19,000 --> 00:17:22,000
Germany, group A?

233
00:17:22,000 --> 00:17:30,000
From the air, pot number 1, we have Germany and Italy.

234
00:17:30,000 --> 00:17:39,000
In group A we have Cameroon, Scotland and Belgium.

235
00:17:39,000 --> 00:17:42,000
That sounds a bit wrong.

236
00:17:42,000 --> 00:17:45,000
We have to go to pot number 2.

237
00:17:45,000 --> 00:17:48,000
Okay, yes.

238
00:17:48,000 --> 00:17:52,000
Because we seem to have listed twice.

239
00:17:52,000 --> 00:17:54,000
Right, Cameroon is listed twice.

240
00:17:54,000 --> 00:17:56,000
I think that's right.

241
00:17:56,000 --> 00:17:59,000
I think I did that wrong here.

242
00:17:59,000 --> 00:18:02,000
Then we change group A or group B.

243
00:18:02,000 --> 00:18:04,000
Should we do it piece by piece?

244
00:18:04,000 --> 00:18:07,000
We change group A.

245
00:18:07,000 --> 00:18:11,000
Group A with the USSR.

246
00:18:11,000 --> 00:18:18,000
Okay, luckily it's not Russia, but the USSR.

247
00:18:18,000 --> 00:18:22,000
I wrote it wrong.

248
00:18:22,000 --> 00:18:25,000
Good, we have quickly fixed the small faux pas.

249
00:18:25,000 --> 00:18:28,000
Nobody noticed.

250
00:18:28,000 --> 00:18:30,000
Then we can now very smoothly...

251
00:18:30,000 --> 00:18:34,000
Unfortunately we don't have experts here who would redo the draw.

252
00:18:34,000 --> 00:18:39,000
Exactly, our Mr. Katscha has already fled,

253
00:18:39,000 --> 00:18:42,000
after he noticed that.

254
00:18:42,000 --> 00:18:47,000
Let's try to put group C and group D together.

255
00:18:47,000 --> 00:18:53,000
Group C, Holland.

256
00:18:53,000 --> 00:18:55,000
Brazil.

257
00:18:55,000 --> 00:19:00,000
Oh, Holland and Brazil, very nice.

258
00:19:00,000 --> 00:19:04,000
Spain.

259
00:19:04,000 --> 00:19:08,000
And Colombia.

260
00:19:08,000 --> 00:19:12,000
Now we should have got group D as a gift.

261
00:19:12,000 --> 00:19:17,000
It can no longer be unknown what is left.

262
00:19:17,000 --> 00:19:20,000
Then we stay with England.

263
00:19:20,000 --> 00:19:22,000
England.

264
00:19:22,000 --> 00:19:29,000
Argentina.

265
00:19:29,000 --> 00:19:32,000
Or Argentina.

266
00:19:32,000 --> 00:19:35,000
I have noted that.

267
00:19:35,000 --> 00:19:37,000
France.

268
00:19:37,000 --> 00:19:40,000
And the USA.

269
00:19:40,000 --> 00:19:45,000
Great, we have a group draw.

270
00:19:45,000 --> 00:19:48,000
Fantastic.

271
00:19:48,000 --> 00:19:50,000
Let's save that.

272
00:19:50,000 --> 00:19:52,000
Everything is zinked.

273
00:19:52,000 --> 00:19:54,000
Spain.

274
00:19:54,000 --> 00:19:57,000
No, Spain is correct.

275
00:19:57,000 --> 00:20:00,000
Wonderful, that sounds good.

276
00:20:00,000 --> 00:20:02,000
Now we have four groups.

277
00:20:02,000 --> 00:20:11,000
For tomorrow's evening we have to create a group game plan.

278
00:20:11,000 --> 00:20:16,000
Is there an Excel template for something like that?

279
00:20:16,000 --> 00:20:21,000
Tournament Excel template?

280
00:20:21,000 --> 00:20:26,000
I'm just asking in the comments.

281
00:20:26,000 --> 00:20:29,000
We have prepared nice graphics.

282
00:20:29,000 --> 00:20:33,000
That would be great.

283
00:20:33,000 --> 00:20:37,000
If we had the group game there.

284
00:20:37,000 --> 00:20:42,000
Because it's about the games we play.

285
00:20:42,000 --> 00:20:51,000
Then we know who will be in the knockout stages.

286
00:20:51,000 --> 00:20:57,000
The game will be played by the group winners.

287
00:20:57,000 --> 00:21:00,000
Group A and B, C and D.

288
00:21:00,000 --> 00:21:07,000
The first from group A will play against the second from group B.

289
00:21:07,000 --> 00:21:10,000
The first from group B will play against the second from group A.

290
00:21:10,000 --> 00:21:14,000
And the same for group C and D.

291
00:21:14,000 --> 00:21:22,000
And that's how the games start.

292
00:21:22,000 --> 00:21:34,000
Group A and B win one of the two quarterfinals.

293
00:21:34,000 --> 00:21:40,000
And the group C and D play against the group winners.

294
00:21:40,000 --> 00:21:46,000
And that's how it goes from quarter to semi-final to final.

295
00:21:46,000 --> 00:21:56,000
Tomorrow is the group stage at 11pm.

296
00:21:56,000 --> 00:22:04,000
And the next day at 12am is the knockout stage.

297
00:22:04,000 --> 00:22:12,000
And the big final at 11pm.

298
00:22:12,000 --> 00:22:16,000
We can also play the small final.

299
00:22:16,000 --> 00:22:19,000
If we want to.

300
00:22:19,000 --> 00:22:23,000
Depending on the alcohol level.

301
00:22:23,000 --> 00:22:28,000
There is no game in the European Championships.

302
00:22:28,000 --> 00:22:34,000
But we have to play one in the World Cup.

303
00:22:34,000 --> 00:22:38,000
We have a template office.

304
00:22:38,000 --> 00:22:44,000
To decide the winners and losers.

305
00:22:44,000 --> 00:22:46,000
Wonderful.

306
00:22:46,000 --> 00:22:50,000
We have reached the end of the draw.

307
00:22:50,000 --> 00:22:54,000
I wish you a good night.

308
00:22:54,000 --> 00:22:58,000
And all out there who listen to the stream.

309
00:22:58,000 --> 00:23:00,000
Bye.

310
00:23:00,000 --> 00:23:29,000
Bye.

