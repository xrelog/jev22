1
00:00:00,000 --> 00:00:07,000
Thirdroom. Mondes virtuels décentralisés sur Matrix.

2
00:00:07,000 --> 00:00:14,000
Ce n'est que le premier aperçu technique, mais ce que nous avons déjà vu, 
et l'appétit pour plus.

3
00:00:14,000 --> 00:00:21,000
Imaginez que vous êtes sur la Matrice, que vous discutez, que vous passez des appels vidéo ou téléphoniques, 
et maintenant vous avez une nouvelle dimension.

4
00:00:21,000 --> 00:00:26,000
Thirdroom.io Entièrement basé sur le web, intégré dans Matrix.

5
00:00:26,000 --> 00:00:32,000
Robert Long, développeur en chef de Thirdroom, nous donne un premier aperçu.

6
00:00:32,000 --> 00:00:36,000
Veuillez accueillir Robert Long.

7
00:00:36,000 --> 00:00:40,000
Bonjour tout le monde, mon nom est Robert Long 
et voici la troisième salle.

8
00:00:40,000 --> 00:00:44,000
Thirdroom est une plateforme de monde virtuel alimentée 
par le protocole Matrix.

9
00:00:44,000 --> 00:00:53,000
Un protocole de communication décentralisé, crypté, de bout en bout. 
utilisé pour la messagerie, 
la voix, la vidéo, et maintenant les mondes 3D.

10
00:00:53,000 --> 00:00:58,000
Vous pouvez voir que je suis dans un monde virtuel en ce moment, 
et dans ce monde se trouve un autre avatar.

11
00:00:58,000 --> 00:01:02,000
Je peux me promener dans le monde virtuel et parler 
avec d'autres personnes.

12
00:01:02,000 --> 00:01:12,000
Habituellement, j'ai quelques autres personnes dans la démo. 
pour que vous puissiez les entendre parler, 
mais pour l'instant nous avons une marionnette de Robert Test ici.

13
00:01:12,000 --> 00:01:24,000
Dans la superposition, appuyez sur Echap, vous pouvez 
voir que j'ai un tas de mondes différents, incluant 
le monde dans lequel nous sommes actuellement,

14
00:01:24,000 --> 00:01:28,000
qui est une version spéciale d'un environnement que 
que nous appelons Terra.

15
00:01:28,000 --> 00:01:39,000
Toutes ces différentes pièces sont des mondes virtuels 
que je peux rejoindre, de la même manière que vous pourriez 
rejoindre un salon de discussion typique.

16
00:01:39,000 --> 00:01:46,000
Si je fais défiler tout le chemin jusqu'à la fin ici, 
vous pouvez voir que nous avons le XR.Labs Lounge juste ici.

17
00:01:46,000 --> 00:01:49,000
Si je fais défiler vers l'arrière, vous pouvez voir tous les messages 
qui arrivent ici.

18
00:01:49,000 --> 00:01:59,000
C'est juste un client Matrix standard, mais il a également 
mais il a aussi la capacité de naviguer dans les mondes virtuels.

19
00:01:59,000 --> 00:02:06,000
En ce moment, ces deux clients communiquent 
via une connexion WebRTC peer-to-peer établie 
sur Matrix.

20
00:02:06,000 --> 00:02:12,000
Tout ce que nous faisons dans ce monde se passe sans 
aucun serveur supplémentaire en dehors de notre serveur standard Matrix 
standard de Matrix.

21
00:02:12,000 --> 00:02:18,000
Toute personne ayant un compte Matrix a aussi un compte Thirdroom. 
Thirdroom.

22
00:02:18,000 --> 00:02:25,000
Quoi qu'il en soit, je pense que cette foule est déjà assez 
familier avec Matrix, alors passons à des trucs en 3D.

23
00:02:25,000 --> 00:02:38,000
Thirdroom utilise quelques nouvelles technologies de navigateur 
comme le tampon de tableau partagé 
et les API Atomics,

24
00:02:38,000 --> 00:02:41,000
ce qui nous permet de faire du multithreading dans le navigateur.

25
00:02:41,000 --> 00:02:50,000
Nous avons construit un tout nouveau 
moteur de jeu par navigateur au dessus de ces primitives.

26
00:02:50,000 --> 00:02:57,000
L'API de tampon de tableau partagé et les API Atomics 
sont maintenant disponibles dans Firefox, Chrome et Safari.

27
00:02:57,000 --> 00:03:05,000
La plupart des applications web 3D existantes 
aujourd'hui, tout fonctionne sur un seul thread.

28
00:03:05,000 --> 00:03:11,000
Cela conduit à des images perdues en raison de retards 
WebGL en retard,

29
00:03:11,000 --> 00:03:20,000
l'arrêt du thread de l'interface utilisateur lors du téléchargement des textures 
au GPU, et beaucoup moins de place pour ajouter des 
des choses amusantes comme la physique et les animations.

30
00:03:20,000 --> 00:03:27,000
Thirdroom utilise trois threads, le thread principal du navigateur 
et deux web workers différents.

31
00:03:27,000 --> 00:03:33,000
Le thread principal exécute notre interface utilisateur React.js, 
ainsi que notre client Matrix,

32
00:03:33,000 --> 00:03:39,000
qui gère l'audio en temps réel et le jeu 
de jeu en temps réel sur WebRTC.

33
00:03:39,000 --> 00:03:46,000
Le prochain travailleur web est notre thread de jeu, 
qui exécute notre logique de jeu, les animations, 
et la physique en utilisant Rapier,

34
00:03:46,000 --> 00:03:51,000
qui est un moteur physique écrit en Rust et 
compilé en WebAssembly.

35
00:03:51,000 --> 00:04:01,000
Et enfin, nous avons notre thread de rendu, 
qui exécute une version modifiée de 3GS, 
qui utilise WebGL pour le rendu.

36
00:04:01,000 --> 00:04:11,000
L'état de notre jeu est conservé dans une structure de données 
structure de données sans verrou appelée triple tampon, ce qui nous permet 
d'exécuter notre thread de jeu à un rythme indépendant 
d'un thread de rendu.

37
00:04:11,000 --> 00:04:20,000
Dans les navigateurs, nous ne pouvons pas facilement limiter notre taux de rafraîchissement. 
taux de rafraîchissement, donc cela nous aide vraiment à supporter les moniteurs 
élevé,

38
00:04:20,000 --> 00:04:25,000
où nous pouvons exécuter le thread de jeu et le thread de rendu 
à deux taux entièrement indépendants.

39
00:04:25,000 --> 00:04:31,000
Maintenant, entrons dans certains détails de la 
la scène. Donc, nous avons l'avatar de Robert Test 
ici,

40
00:04:31,000 --> 00:04:39,000
et je peux courir autour du monde, et je peux 
lancer des cubes.


41
00:04:39,000 --> 00:04:47,000
Et donc, pour économiser sur les coûts de rendu pour 
cet environnement, nous faisons un usage intensif 
de l'instanciation.

42
00:04:47,000 --> 00:04:53,000
Vous pouvez voir beaucoup d'éléments répétés dans 
cette scène, et ceux-ci sont tous rendus 
en un seul passage.

43
00:04:53,000 --> 00:04:59,000
Donc, nous avons également fait des cartes de lumière dans cette scène. 
scène et ajouté le support pour les sondes de réflexion.

44
00:04:59,000 --> 00:05:07,000
Si je fais apparaître ce cube et que je l'amène à l'intérieur de la scène. 
ici, vous pouvez voir que l'éclairage commence à changer 
comme je me déplace dans et hors de cette porte.

45
00:05:07,000 --> 00:05:16,000
Et c'est tout cuit. Et nous l'avons ajouté à 3GS, 
ce qui nous donne un éclairage plus précis pour les objets dynamiques 
dynamiques se déplaçant dans la scène,

46
00:05:16,000 --> 00:05:24,000
mais toujours en économisant sur l'éclairage en temps réel coûteux 
à travers le large éventail de matériel 
que nous devons supporter.

47
00:05:24,000 --> 00:05:31,000
Donc, afin de produire ces actifs, nous avons créé 
un pipeline d'exportation depuis Unity, que je vais basculer 
maintenant.

48
00:05:31,000 --> 00:05:37,000
Et ici vous pouvez voir la même scène que nous 
ouverte dans Thirdroom, maintenant ouverte dans Unity.

49
00:05:37,000 --> 00:05:46,000
Donc, nous allons cliquer sur certaines de ces sondes de réflexion. 
de réflexion, comme celles que nous utilisions dans 
cette scène avant.

50
00:05:46,000 --> 00:05:51,000
Donc, il y en a une juste là, et 
il y en a une juste ici. Vous pouvez voir les petites 
réflexions sur cette sphère juste ici.

51
00:05:51,000 --> 00:05:55,000
C'est en quelque sorte ce qui est enregistré dans l'environnement 
l'environnement ici.

52
00:05:55,000 --> 00:05:59,000
Et vous pouvez également voir dans cet onglet d'éclairage 
ici, nous pouvons ouvrir l'aperçu.

53
00:05:59,000 --> 00:06:08,000
Il s'agit de la carte de la lumière qui a été élaborée à partir de 
Unity et exportée par le pipeline GLTF de Unity.

54
00:06:08,000 --> 00:06:17,000
Ainsi, la scène est exportée au format GLTF. Le GLTF est 
ce format de fichier 3D créé par le Khronos Group.

55
00:06:17,000 --> 00:06:27,000
Nous sommes nous-mêmes membres du groupe Khronos. 
Et il y a un exportateur dans l'écosystème Unity 
appelé Unity GLTF.

56
00:06:27,000 --> 00:06:33,000
Et si vous y allez, vous devez installer le paquet, 
et ensuite il sera disponible sous l'onglet Assets.

57
00:06:33,000 --> 00:06:39,000
Et vous pouvez simplement exporter la scène active comme un 
GLTF ou un GLB.

58
00:06:39,000 --> 00:06:49,000
Et après avoir installé notre exportateur Thirdroom, 
il va exporter toutes ces différentes 
extensions GLTF que nous avons créées.

59
00:06:49,000 --> 00:07:00,000
Certaines des extensions GLTF sur lesquelles nous avons travaillé 
sont l'extension Lightmap et les extensions 
les extensions Reflection Probe.

60
00:07:00,000 --> 00:07:07,000
Et nous avons également travaillé avec d'autres 
organismes de normalisation, tels que le Khronos Group 
sur l'extension GLTF Audio,

61
00:07:07,000 --> 00:07:15,000
et avec l'Open Metaverse Interoperability Group 
sur une extension Collider pour la physique et 
les collisions avec les différents maillages de la scène.

62
00:07:15,000 --> 00:07:26,000
Donc tout ça est exporté hors de Unity, et ensuite 
nous avons un autre pipeline de post-traitement que 
que nous avons construit dans un outil basé sur un navigateur.

63
00:07:26,000 --> 00:07:37,000
Et cela compresse les textures en utilisant le 
Basis Universal Texture Encoder, ce qui les rend 
plus petit pour le téléchargement, mais aussi plus petit 
sur le GPU.

64
00:07:37,000 --> 00:07:43,000
Et ensuite il le stocke dans ce conteneur KTX 
et le sert ensuite avec le fichier GLTF.

65
00:07:43,000 --> 00:07:49,000
Il prend aussi les références de maillage dupliquées 
et les convertit en messages d'instance.

66
00:07:49,000 --> 00:07:56,000
Donc c'est ce qui se passe avec tous ces 
objets dupliqués ici. Toutes ces données 
sont intégrées dans le GLTF.

67
00:07:56,000 --> 00:08:07,000
Donc à l'intérieur de notre monde, vous pouvez voir quelques portails 
vers d'autres mondes. Alors passons par 
ce portail juste ici.

68
00:08:07,000 --> 00:08:13,000
Donc nous sommes ici dans la matrice de Matrix. Plutôt méta.

69
00:08:13,000 --> 00:08:23,000
Donc, dans cette scène, j'ai une TV interactif, 
et si je clique dessus, alors tout se transforme 
se transforme en code de la matrice.

70
00:08:23,000 --> 00:08:33,000
Donc cela s'est passé par une interaction scriptable. 
Donc, allons jeter un coup d'oeil au code pour cela.

71
00:08:33,000 --> 00:08:42,000
Donc ici dans mon codeur, vous pouvez voir un 
un petit morceau de JavaScript, et je récupère l'objet 
l'objet TV dans la scène.

72
00:08:42,000 --> 00:08:47,000
Je le rends interactif en fixant sa propriété 
propriété interactable.

73
00:08:47,000 --> 00:08:51,000
J'ai un petit morceau d'état qui dit si 
si la télé est allumée ou non.

74
00:08:51,000 --> 00:09:00,000
Et ensuite à chaque frame, nous vérifions si l'interactable 
interactable est pressé. Nous basculons l'état 
et ensuite nous basculons le matériau de la matrice.

75
00:09:00,000 --> 00:09:10,000
C'est donc un exemple assez simple. Vérifions 
quelque chose d'un peu plus avancé.

76
00:09:10,000 --> 00:09:21,000
Donc dans cet exemple, j'ai deux matériaux différents. 
matériaux, et si j'appuie sur ce bouton, 
ça échange les matériaux.


77
00:09:21,000 --> 00:09:29,000
Et ici, j'ai une lumière qui peut s'éteindre et s'allumer.

78
00:09:29,000 --> 00:09:38,000
Maintenant, vous pourriez penser, il y a 
les scripts, ils sont exécutés à l'intérieur de la matrice.

79
00:09:38,000 --> 00:09:46,000
Comment pouvez-vous exécuter cela en toute sécurité ? 
Et nous avons construit notre moteur de script 
autour de WebAssembly.

80
00:09:46,000 --> 00:09:56,000
Donc cette API que nous appelons WebSceneGraph, 
et c'est une API basée sur JavaScript et WebAssembly.

81
00:09:56,000 --> 00:10:04,000
Tout le code s'exécute à l'intérieur d'un contexte sécurisé 
WebAssembly et est complètement isolé 
du reste du contenu du client.

82
00:10:04,000 --> 00:10:12,000
Le client de la matrice s'exécute donc sur un thread différent. C'est ensuite à l'intérieur d'un module WebAssembly,

83
00:10:12,000 --> 00:10:19,000
qui n'a que l'accès à l'API que nous avons 
que nous lui avons exposé via l'API WebSceneGraph.

84
00:10:19,000 --> 00:10:34,000
Alors regardons le code de cette scène. 
Retournez à mon éditeur de code, et vous pouvez voir 
voici la version JavaScript de ce script.

85
00:10:34,000 --> 00:10:41,000
Et nous pouvons voir que nous interrogeons les boutons, 
les cubes, les différentes textures.

86
00:10:41,000 --> 00:10:46,000
Et nous faisons aussi des requêtes pour les différents 
interrupteurs et les lumières.

87
00:10:46,000 --> 00:10:53,000
Et puis nous pouvons voir chaque image. 
Nous interrogeons pour voir si ce bouton de matériel 
est pressé et nous pouvons basculer un état là.

88
00:10:53,000 --> 00:10:59,000
Nous bouclons à travers toutes les primitives du maillage. 
Ce sont tous les différents sous-maillages de ce cube.

89
00:10:59,000 --> 00:11:06,000
Dans ce cas, il n'y en a qu'un seul, mais nous allons 
bouclons à travers lui. Et nous avons inversé la 
texture ici.

90
00:11:06,000 --> 00:11:13,000
Pour les lumières, nous définissons la propriété d'intensité. 
Si elle est allumée, elle est de 20. Si elle est éteinte, c'est 0.

91
00:11:13,000 --> 00:11:18,000
Et puis, parce que c'est WebAssembly, nous pouvons voir 
le même exemple exact en C.

92
00:11:18,000 --> 00:11:23,000
Donc ici nous avons les mêmes requêtes. Nous avons 
Nous avons la même fonction de mise à jour.

93
00:11:23,000 --> 00:11:32,000
Et nous changeons juste la matière ou l'intensité 
l'intensité basée sur cet état interactif.

94
00:11:32,000 --> 00:11:37,000
Et si vous avez fait du développement web, ceci 
pourrait commencer à vous sembler un peu familier.

95
00:11:37,000 --> 00:11:43,000
Tout comme JavaScript a ouvert la possibilité 
de programmer des applications riches contre HTML avec 
l'API DOM,

96
00:11:43,000 --> 00:11:50,000
nous pensons que l'API WebSceneGraph possède 
le potentiel d'ajouter de l'interactivité aux 
documents du GLTF,

97
00:11:50,000 --> 00:11:58,000
vous permettant de créer des expériences 3D portables 
qui peuvent être exécutées dans des applications 
applications 3D existantes ou entièrement nouvelles.

98
00:11:58,000 --> 00:12:04,000
Et c'est le dernier morceau de ma partie interactive 
interactive de cette démo.

99
00:12:04,000 --> 00:12:08,000
Revenons à Firefox et passons à Figma.

100
00:12:08,000 --> 00:12:14,000
Et je vais vous montrer un peu de ce qui est 
à venir dans Thirdroom.

101
00:12:14,000 --> 00:12:24,000
Donc pour notre API de script, nous allons lancer 
le reste de l'API au début de 2023 avec 
Tech Preview 2.

102
00:12:24,000 --> 00:12:33,000
Et cela va avoir l'ensemble du 
le graphe de scène GLTF exposé en tant que JavaScript et 
WebAssembly API.

103
00:12:33,000 --> 00:12:37,000
Donc vous serez en mesure de manipuler tous ces objets 
de cette façon.

104
00:12:37,000 --> 00:12:41,000
Après cela, nous envisageons de publier une 
une API réseau de haut niveau,

105
00:12:41,000 --> 00:12:45,000
qui vous permettra de synchroniser l'état de ce 
l'état de ce graphe de scène à travers le réseau.

106
00:12:45,000 --> 00:12:50,000
Pour l'instant, il n'y a que de petites interactions 
qui sont synchronisées à travers le réseau.

107
00:12:50,000 --> 00:12:54,000
Et puis nous allons ajouter l'API physique.

108
00:12:54,000 --> 00:13:00,000
Donc vous serez capable de créer des objets physiques 
interactif dans la scène,

109
00:13:00,000 --> 00:13:07,000
qui réagiront si vous entrez en collision avec eux, 
ou vous serez en mesure de demander des collisions avec 
d'autres objets.

110
00:13:07,000 --> 00:13:13,000
Nous allons également ajouter des avatars et des objets 
avatars et objets scriptables.

111
00:13:13,000 --> 00:13:19,000
Donc vous serez en mesure de prendre des avatars et d'ajouter 
des comportements pour l'avatar.

112
00:13:19,000 --> 00:13:26,000
Peut-être que ça va commencer à, les yeux vont suivre 
d'autres utilisateurs ou les cheveux auront comme un ressort 
des os à ressort.

113
00:13:26,000 --> 00:13:30,000
Beaucoup de choses que les gens ont été en mesure d'accomplir 
accomplir dans d'autres plateformes VR,

114
00:13:30,000 --> 00:13:35,000
vous serez en mesure d'accomplir dans Thirdroom 
avec des scripts interactifs.

115
00:13:35,000 --> 00:13:40,000
Et puis pour les objets, je veux dire, il y a un 
millions de choses différentes que vous pourriez faire avec ça.

116
00:13:40,000 --> 00:13:45,000
Une de mes choses préférées serait que vous pourriez 
faire comme une armoire principale ou quelque chose comme ça.


117
00:13:45,000 --> 00:13:53,000
Vous pourriez avoir cette armoire d'arcade, la placer 
dans le monde, et ensuite jouer à des jeux d'arcade 
à l'intérieur du monde.

118
00:13:53,000 --> 00:14:04,000
Et puis cet objet est ensuite portable 
donc vous pouvez l'amener entre les différentes 
scènes dans les différents mondes de Thirdroom.

119
00:14:04,000 --> 00:14:09,000
Maintenant, une autre pièce de la Tech Preview 2 
va être la page de découverte.

120
00:14:09,000 --> 00:14:16,000
Voici un concept de base que nous avions lorsque nous 
explorions ce concept.

121
00:14:16,000 --> 00:14:20,000
Le rendu final sera un peu 
différent de celui-ci.

122
00:14:20,000 --> 00:14:24,000
Et comme je l'ai dit, ceci est lancé dans Tech Preview 2.

123
00:14:24,000 --> 00:14:30,000
Et c'est une nouvelle façon de trouver des mondes 3D à 
rejoindre et explorer.

124
00:14:30,000 --> 00:14:36,000
C'est aussi un bon moyen de trouver des pièces existantes de la 
Matrix à rejoindre depuis le client 
client Thirdroom.

125
00:14:36,000 --> 00:14:45,000
Donc, de la même manière que nous avons trouvé le XR log room ou le 
Thirdroom, nous pouvons les trouver dans cet onglet maintenant.

126
00:14:45,000 --> 00:14:48,000
Et l'une des plus grandes choses est que nous pouvons 
trouver les scènes créées par les utilisateurs.

127
00:14:48,000 --> 00:14:56,000
Donc, après avoir créé la scène dans 
Unity ou n'importe quel autre outil, vous pouvez 
la publier sur cette page de découverte,

128
00:14:56,000 --> 00:15:00,000
et vous pouvez trouver les contenus des autres utilisateurs.

129
00:15:00,000 --> 00:15:04,000
Et ceci est complètement décentralisé.

130
00:15:04,000 --> 00:15:08,000
La première version va juste se concentrer sur notre...

131
00:15:08,000 --> 00:15:16,000
Il s'agira d'une pièce qui va essentiellement 
le contenu que tout le monde lui soumet.

132
00:15:16,000 --> 00:15:22,000
Et c'est basé sur cette spécification, MSC3948, 
qui est la spécification de la salle de dépôt.

133
00:15:22,000 --> 00:15:34,000
Et ce sont des salles, et quand vous les rejoignez, 
ça vous donne accès au contenu et la 
la possibilité de soumettre du contenu pour qu'il soit approuvé 
et mis en avant.

134
00:15:34,000 --> 00:15:43,000
Et puis le but est que la page de découverte 
provienne de plusieurs espaces de stockage, 
pas seulement notre propre salle de dépôt 
comme nous allons le faire.

135
00:15:43,000 --> 00:15:55,000
Et chaque dépôt que vous avez rejoint 
va alimenter cette page de découverte et en quelque sorte 
vous donner cette vue d'ensemble de tout le contenu 
partagé dans vos communautés.

136
00:15:55,000 --> 00:16:03,000
Donc c'est un concept vraiment puissant, surtout 
quand vous le combinez avec Matrix Spaces, qui 
que nous espérons lancer assez rapidement.

137
00:16:03,000 --> 00:16:09,000
Donc vous serez en mesure de rejoindre des espaces et de voir tout 
le contenu de tous les espaces que vous avez rejoints.

138
00:16:09,000 --> 00:16:11,000
Une façon vraiment cool de trouver du contenu.

139
00:16:11,000 --> 00:16:23,000
Et si nous amenons les salles de dépôt au niveau suivant. 
au prochain niveau, nous serons en mesure de créer un 
marché de contenu décentralisé.

140
00:16:23,000 --> 00:16:31,000
Donc c'est l'extension des salles de dépôt 
pour ajouter la possibilité d'acheter et 
de signer cryptographiquement du contenu.

141
00:16:31,000 --> 00:16:41,000
Et nous sommes encore en train de travailler sur les détails de la spécification, 
mais le but est que la place de marché soit 
alimentée par des salles de dépôt de sorte que tout 
soit hébergé d'une manière fédérée.

142
00:16:41,000 --> 00:16:46,000
Il n'y a pas de jardins clos. Il y a des prix équitables 
conduit par la concurrence.

143
00:16:46,000 --> 00:16:59,000
Toutes les différentes places de marché sont essentiellement 
canalisés dans cette interface utilisateur unique pour 
trouver du contenu sur tous ces différents serveurs 
que vous avez rejoints.

144
00:16:59,000 --> 00:17:07,000
Et les créateurs peuvent être payés pour leur travail, 
ce qui est super important pour toute sorte de plateforme.

145
00:17:07,000 --> 00:17:14,000
Nous allons lancer notre boutique en même temps que 
tout cela, et nous allons devoir rivaliser 
avec toutes les autres personnes sur la plate-forme.

146
00:17:14,000 --> 00:17:17,000
Mais nous pensons que c'est une chose très saine.

147
00:17:17,000 --> 00:17:22,000
Et la découverte sera conduite par les communautés 
que vous rejoignez.

148
00:17:22,000 --> 00:17:36,000
Donc si je suis intéressé par, je ne sais pas, la 
la communauté générale de Matrix va probablement poster 
tout un tas de pièces cool liées à Matrix.

149
00:17:36,000 --> 00:17:50,000
Donc je peux trouver des endroits pour parler de Matrix. 
de Matrix, mais je peux aussi rejoindre quelque chose en rapport 
à mes films préférés ou mes jeux vidéo préférés 
ou mes intérêts, des choses comme ça.

150
00:17:50,000 --> 00:17:58,000
Et vous pouvez trouver des gens, du contenu, des pièces et des 
espaces qui se rapportent à ce genre de choses.

151
00:17:58,000 --> 00:18:11,000
Ouais. Et ensuite ? Donc, après toutes ces sortes 
de trucs, on a le Tech Preview 2, qui est 
le lancement avec les scripts, la page de découverte 
et le réseau autoritaire.

152
00:18:11,000 --> 00:18:20,000
Ensuite, nous lançons des avatars personnalisés, WebXR 
et le support de la manette de jeu mobile et le remappage des entrées.

153
00:18:20,000 --> 00:18:26,000
C'est quelque chose qui a été voulu 
depuis longtemps et nous devons l'ajouter.


154
00:18:26,000 --> 00:18:39,000
L'API pour les robots, qui est un peu comme l'API pour les robots de Matrix. 
API pour les bots de chat de Matrix, sauf que maintenant vous 
vous pouvez avoir comme un agent virtuel réel qui marche 
autour de votre monde et des serveurs dédiés.

155
00:18:39,000 --> 00:18:56,000
Donc similaire à l'API des robots, sauf qu'il s'agit de faire tourner 
une instance de votre monde sur une longue 
période de temps, le garder en place et le faire fonctionner et 
en ayant une sorte de couche de persistance supplémentaire 
au-delà de ce que nous faisons avec le peer to peer.

156
00:18:56,000 --> 00:19:01,000
Et puis l'éditeur in-world. C'est quelque chose 
sur lequel nous avons commencé.

157
00:19:01,000 --> 00:19:12,000
Si vous appuyez sur tilde dans Thirdroom aujourd'hui, 
vous verrez qu'il y a comme une vue arborescente et vous 
et vous pouvez voir tous les différents objets du monde 
et voir comme un contour autour d'eux quand vous les sélectionnez.

158
00:19:12,000 --> 00:19:25,000
Afin de construire le reste de l'éditeur, 
nous devions vraiment nous concentrer sur l'API de scripting 
et obtenir une partie de ce multi-threaded 
de la structure de données.

159
00:19:25,000 --> 00:19:31,000
Mais maintenant c'est presque fait et nous allons pouvoir 
commencer à travailler sur l'éditeur in-world.

160
00:19:31,000 --> 00:19:40,000
Nous voulons augmenter nos plafonds de chambres et heureusement 
le travail sur l'unité de transfert sélectif est bien avancé. 
en cours et il y a une preuve de concept.

161
00:19:40,000 --> 00:19:49,000
Si vous cherchez, je crois que Waterfall est le nom du projet. 
nom du projet, vous trouverez notre unité de transmission 
unité de transfert sélectif.

162
00:19:49,000 --> 00:20:03,000
Et puis le travail sur le marché décentralisé 
dont j'ai parlé et avec ça, un inventaire 
de sorte que vous pouvez effectivement sortir ces 
morceaux de contenu que vous avez achetés,

163
00:20:03,000 --> 00:20:10,000
ainsi que les objets scriptables et les avatars, 
qui devraient être disponibles dans votre inventaire 
et sur cette place de marché également.

164
00:20:10,000 --> 00:20:17,000
Donc vraiment en concurrence, complétant l'ensemble du metaverse, 
l'image interopérable du metaverse ici.

165
00:20:17,000 --> 00:20:27,000
Et c'est tout ce que j'ai. Donc si vous êtes intéressés 
de vous impliquer, vous pouvez trouver notre produit 
en direct sur thirdroom.io.

166
00:20:27,000 --> 00:20:35,000
Vous pouvez nous trouver sur GitHub. Vous pouvez nous rejoindre sur 
Matrix dans notre salle thirdroom.dev.

167
00:20:35,000 --> 00:20:39,000
Nous y parlons de tout, pas seulement de développement.

168
00:20:39,000 --> 00:20:49,000
Toute personne intéressée à contribuer 
devrait prendre contact via cette salle Matrix si vous êtes 
intéressé par l'art, que ce soit des avatars ou des 
environnements,

169
00:20:49,000 --> 00:20:58,000
ou si vous êtes intéressés par le développement, 
nous avons de la programmation graphique, des trucs de front-end UI, 
beaucoup de travail vraiment cool qui se passe ici.

170
00:20:58,000 --> 00:21:03,000
Et nous recherchons des contributeurs externes. 
N'hésitez pas à nous contacter sur notre salle Matrix.

171
00:21:03,000 --> 00:21:09,000
Quoi qu'il en soit, merci beaucoup. Et j'ai hâte 
de vous parler à tous bientôt.






