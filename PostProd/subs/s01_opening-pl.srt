1
00:00:30,000 --> 00:00:40,800
Zapraszamy na otwarcie XRelog22!

2
00:00:40,800 --> 00:00:51,920
Witaj Anchoray i witaj w świecie zewnętrznym! Chcemy przedstawić Ci, czym jest XRelog22

3
00:00:51,920 --> 00:01:01,760
 ma dla ciebie w tym roku. Czym w ogóle jest XRelog22?

4
00:01:01,760 --> 00:01:11,360
 Jakie są cele? Chcemy zajmować się SocialVR i SocialVR bez bzdur. 

5
00:01:11,360 --> 00:01:17,000
i to jest to, co umieszczamy w naszym motto. Nazywamy to 
''Visions for Independent Multiverses''.

6
00:01:17,000 --> 00:01:23,960
Słyszałem, że chcesz również zintegrować etykę hakerską z metawersją i z multiwersum.

7
00:01:23,960 --> 00:01:29,960
Ekspansje meta i multiwersum. Absolutnie! Po prostu nie ma jej wystarczająco dużo w

8
00:01:29,960 --> 00:01:35,480
Metaverse nie istnieje. Powiedzmy, że właśnie zauważyłem, że w ogóle nie widzę logo kanału.

9
00:01:35,480 --> 00:01:39,680
Czy masz logo kanału? Logo kanału?

10
00:01:39,680 --> 00:01:49,440
Co to było? Co to był za hałas?

11
00:01:49,440 --> 00:01:56,960
Tak, ja też nie wiem. Tak, niestety to była taśma, która nie trzymała.

12
00:01:56,960 --> 00:02:04,960
Tak, nie jesteśmy super videastami. Nor naprawdę super profesjonalny z OBS.

13
00:02:04,960 --> 00:02:11,680
Nie jesteśmy idealnymi ludźmi, więc czasami logo kanału znika.

14
00:02:11,680 --> 00:02:17,440
Szkotka była po prostu zła. Musi być złej jakości.

15
00:02:17,440 --> 00:02:21,760
Myślę, że musimy jeszcze trochę dostosować nasze oczekiwania. Chodzi mi o to, że,

16
00:02:21,760 --> 00:02:27,760
że ludzie nie powinni myśleć, że zostaną obsłużeni idealnie, ani że dostaną produkcję jakości ARD, ZDF.

17
00:02:27,760 --> 00:02:30,520
Nikt nie chce ARD i ZDF.

18
00:02:30,520 --> 00:02:35,960
Ale i tak wiele rzeczy jest niedokończonych, ale atrakcyjnych.

19
00:02:35,960 --> 00:02:44,960
To nie jest niedokończone, to jest w produkcji. Jest zwinny. To jest zwinny rozwój.

20
00:02:44,960 --> 00:02:47,560
Chodzi Ci o to, że "Work in Progress" to nowy standard, tak?

21
00:02:47,560 --> 00:02:48,560
Tak, oczywiście, że tak.

22
00:02:48,560 --> 00:02:54,320
Aha, ok. Więc przygotuj się na wiele... do góry nogami...

23
00:02:54,320 --> 00:02:58,720
Tak, widzisz, znowu się zaczyna. Przygotuj się na widok wielu rozlanych rzeczy.

24
00:02:58,720 --> 00:03:02,320
Butelki z mate i rozlane rzeczy, ale będziemy się dobrze bawić.

25
00:03:02,320 --> 00:03:04,720
Przyklejamy je za pomocą taśmy.

26
00:03:04,720 --> 00:03:05,400
Dokładnie.

27
00:03:05,400 --> 00:03:24,360
Ale tak naprawdę to wymawia się XRelog22.

28
00:03:24,360 --> 00:03:31,960
Tak więc i to pochodzi z grupy operacyjnej XR.Labs.

29
00:03:31,960 --> 00:03:39,800
Jestem tu w ICCBS, spójrz. Budujemy wirtualne światy.

30
00:03:39,800 --> 00:03:47,800
Jednym z nich, pojazdem podróżującym po wirtualnym świecie, jest nasz ICCBS, 

31
00:03:47,800 --> 00:03:55,000
Międzygalaktyczne Studio Nadawcze Komunikacji Chaosu. I zadokował w Overte na to wydarzenie.

32
00:03:55,000 --> 00:04:02,760
Proszę bardzo, widać, że jest nieźle poobijany. To dlatego, że mieliśmy tu kilka wypadków.

33
00:04:02,760 --> 00:04:10,600
A wszystkie ładne elementy wyposażenia wnętrza nie były dobrze zamocowane, więc trochę się zniszczyły. 

34
00:04:10,600 --> 00:04:17,120
Ale my to pięknie naprawiamy.

35
00:04:17,120 --> 00:04:23,920
Chodź, pokażę ci coś. Bo chcemy zrobić kilka specjałów na rogach.

36
00:04:23,920 --> 00:04:31,960
Nie wiem, kto to rozpozna, ci, którzy byli w

37
00:04:31,960 --> 00:04:39,680
 C-Base go rozpozna. To ściany światła, wykonane z butelek mate.

38
00:04:39,680 --> 00:04:49,760
A tutaj patrzymy na przykład na budowę czegoś, co nazywa się cyfrowym bliźniakiem tego urządzenia.

39
00:04:49,760 --> 00:04:55,120
A oto instrukcje budowania interfejsu programowania aplikacji, abyś wiedział, jak go używać. 

40
00:04:55,120 --> 00:05:01,480
Tutaj jest ta strona, można na nią spojrzeć, a nawet przeczytać. A oto przykład, jak można wykorzystać tę przestrzeń.

41
00:05:01,480 --> 00:05:08,920
Umieśćmy tutaj ten ekran LED.

42
00:05:08,920 --> 00:05:15,120
Trochę latam. Tutaj też można latać. Muszę trochę poćwiczyć latanie. 

43
00:05:15,120 --> 00:05:21,880
Wow, teraz ja tu lecę. Spójrz tutaj.  Tu nadal jest dziura, widzisz? O, znowu się zepsuło. 

44
00:05:21,880 --> 00:05:26,680
Tutaj musimy jeszcze trochę poprawić. To kolejne zadanie. Widzisz, tak jest z powodu wypadków przy lądowaniu.

45
00:05:26,680 --> 00:05:32,520
Jeszcze raz tutaj. Właśnie minąłem portal i to jest nasza hala odlotów.

46
00:05:32,520 --> 00:05:42,920
Teraz możemy wejść na zeppelina. Hej, ziemia się obraca. Fajnie...

47
00:05:42,920 --> 00:05:51,920
 możemy to zrobić... bardzo fajnie. Teraz możemy zobaczyć ICCBS z zewnątrz. Więc jestem

48
00:05:51,920 --> 00:05:58,600
tutaj, we wnętrzu zeppelina. Jest też portal. Zobaczymy, dokąd to prowadzi? Wejdźmy do środka.

49
00:05:58,600 --> 00:06:08,400
Ach tak, to nas przenosi w przeszłość. Ach, to tam zmienili portale. Dobrze, że o tym rozmawialiśmy.

50
00:06:08,400 --> 00:06:13,720
Właściwie portal, przez który właśnie przeszedłem, powinien prowadzić tutaj. 

51
00:06:13,720 --> 00:06:23,080
Spójrzmy tutaj. Co to jest? Spójrz, ciemna... Spójrzcie, to wróżkowy pył.

52
00:06:23,080 --> 00:06:33,160
Nie mogę w to uwierzyć. Pierwszy raz to widzę. Wow, to wcale nie jest złe. W rzeczywistości tak jest, 

53
00:06:33,160 --> 00:06:39,880
Czy tam jeszcze wszystko się kręci? Już się nie kręcą. Owszem, obracają się bardzo powoli. 

54
00:06:39,880 --> 00:06:44,960
Tutaj widzimy węzeł danych w różnych reprezentacjach artystycznych. I mamy ognisko.

55
00:06:44,960 --> 00:06:56,560
I to z trzaskiem. Jest tu naprawdę gościnnie. Mam nadzieję, że będziemy się tu często spotykać.

56
00:06:56,560 --> 00:07:02,720
Skoro już tu chodzę, to mogę też pójść do piwnicy Jev22.

57
00:07:02,720 --> 00:07:13,960
A piwnica wygląda tak. Whoa, whoa, whoa, whoa. Spójrzmy tutaj. 

58
00:07:13,960 --> 00:07:20,520
To są wszystkie wydarzenia, strony główne wydarzeń. Można je wszystkie obejrzeć.

59
00:07:20,520 --> 00:07:27,240
Jest ich bardzo dużo. Tak, spójrzmy tutaj. Hakowanie równoległe. A gdy się zbliżysz, jest naprawdę interaktywna strona.

60
00:07:27,240 --> 00:07:33,000
Możesz kliknąć na ten i przerzucić się nim. I działa dla wszystkich 38 wydarzeń.

61
00:07:33,000 --> 00:07:42,640
Można je wszystkie obejrzeć i wziąć w nich udział.

62
00:07:42,640 --> 00:07:54,120
Chili con Chaos . Tak, tutaj możesz obejrzeć Chili con Chaos. Program jest bardzo ciekawy. 

63
00:07:54,120 --> 00:08:01,480
Możesz użyć Places, aby wlecieć do naszej Makerspace.  

64
00:08:01,480 --> 00:08:11,680
Makerspace nazywa się po prostu XRelog22. I ten makerspace jest teraz swego rodzaju zbiorem niedokończonych rzeczy. 

65
00:08:11,680 --> 00:08:17,120
niedokończonych spraw. Tam rozpoczęliśmy sprawy w rozproszonym porządku. 

66
00:08:17,120 --> 00:08:25,840
na przykład mam pomysł, na 15 utworów, czyli 15 tematów, które mamy. A gdybyśmy zrobili rundę po torach.

67
00:08:25,840 --> 00:08:38,800
zbudować 15 pięter. Tak, bardzo fajne. Chcę też umieć latać. Tak, latanie to dobry pomysł.

68
00:08:38,800 --> 00:08:45,840
Po bliższym przyjrzeniu się, te podłogi wyglądają trochę jak małe salony odlotów. 

69
00:08:45,840 --> 00:08:53,800
Bardzo małe i bardzo mini. A na każdym z tych pięter znajduje się dedykowany temat. 

70
00:08:53,800 --> 00:09:02,200
Zaczęliśmy wchodzić na trzecie piętro.

71
00:09:02,200 --> 00:09:09,480
Czy to podcasting, czy to SocialVR, czy to społeczności XR, jest ich mnóstwo i tak dalej.

72
00:09:09,480 --> 00:09:16,680
No i oczywiście na pierwszych piętrach znajduje się (H)Activism.

73
00:09:16,680 --> 00:09:28,240
To jest oczywiście nasz główny temat.

74
00:09:28,240 --> 00:09:36,160
Czekaj, ja tam lecę. Tam jest napisane, ale lampa nie jest zapalona.

75
00:09:36,160 --> 00:09:42,040
Przeniosłem gdzieś lampę. Teraz w ogóle nie świeci.

76
00:09:42,040 --> 00:09:48,800
Ogólnie rzecz biorąc, jest tu bardzo jasno. Czy możemy przeczytać to.
Nie, heteryzm jest w ciemności. 
o etyce hakerów.

77
00:09:48,800 --> 00:09:55,160
Rozejrzyj się tutaj. To jedna z pierwszych rzeczy, które zrobiłem.

78
00:09:55,160 --> 00:10:02,920
Etykę hakera narysowałem w 3D w wirtualnej rzeczywistości za pomocą multibrush'a. 

79
00:10:02,920 --> 00:10:11,240
A potem zintegrowałem ten obraz jako model 3D tutaj. I nawet zintegrowałem syntezę głosu, po niemiecku i angielsku.

80
00:10:11,240 --> 00:10:18,160
Cóż, czego dowiedziałem się w badaniach nad etyką hakerów,

81
00:10:18,160 --> 00:10:24,600
jest to, że pierwsze sześć zasad hakerów jest starszych,

82
00:10:24,600 --> 00:10:30,440
pochodzą z lat 60. i tylko dwie zasady zostały dodane przez Computer Chaos Club

83
00:10:30,440 --> 00:10:36,760
W szczególności nie należy zadzierać z danymi innych osób.

84
00:10:36,760 --> 00:10:43,840
Korzystaj z danych publicznych, chroń dane prywatne. Chyba już to słyszeliśmy. 

85
00:10:43,840 --> 00:10:50,520
Są to dwa uzupełnienia Klubu Komputerowego-Chaos-Club. Pozwoliliśmy sobie dodać dziewiątą zasadę.

86
00:10:50,520 --> 00:11:00,720
Zawsze myśl o bitach i drzewach. Bo aspekt ekologiczny nie jest jeszcze wpisany w etykę hakerów. 

87
00:11:00,720 --> 00:11:07,280
Dlatego należy ją uzupełnić. 

88
00:11:07,280 --> 00:11:14,320
To była nasza etyka hakerska. Inaczej widzisz, więc jest to zbiór różnego rodzaju rzeczy.

89
00:11:14,320 --> 00:11:17,560
Na górze widać zeppelina.

90
00:11:17,560 --> 00:11:24,880
Nad zeppelinem znajduje się pierwsza wersja ICCBS. 

91
00:11:24,880 --> 00:11:30,440
Tu są tace. Tace te wykorzystamy podczas organizowania warsztatów.

92
00:11:30,440 --> 00:11:36,280
Wtedy możemy polecieć na tematy i usłyszeć różne rzeczy. 

93
00:11:36,280 --> 00:11:43,160
Tak zwyczajnie majstrujemy. To jest nasz wirtualny makerspace.

94
00:11:43,160 --> 00:11:52,040
Nie ma ona żadnego związku tematycznego. 

95
00:11:52,040 --> 00:11:59,000
Gdy skończymy Tracktower, zostanie on przeniesiony na własny serwer domenowy. 

96
00:11:59,000 --> 00:12:07,320
Następnie zabieramy go z makerspace i budujemy coś nowego. 

97
00:12:07,320 --> 00:12:20,400
Teraz wracam do studia nadawczego Intergalactic Chaos Communication. W skrócie ICCBS. 

98
00:12:20,400 --> 00:12:31,080
Spójrzcie. Skąd pochodzę. Czyli każdy może po prostu zmienić lokalizację, kto jest w systemie?

99
00:12:31,080 --> 00:12:37,960
Tak, wszystko jest otwarte. Nie musisz się nawet rejestrować. 

100
00:12:37,960 --> 00:12:44,560
Nie trzeba nawet zakładać konta. Wystarczy pobrać klienta i uruchomić go, aby wejść do tego świata.

101
00:12:44,560 --> 00:12:49,320
Otrzymasz krótkie wyjaśnienie, jak to działa.

102
00:12:49,320 --> 00:12:55,960
Wtedy można już korzystać z funkcji Places. Możesz wtedy odkryć jeszcze więcej światów.

103
00:12:55,960 --> 00:13:07,080
Czyli ICCBS i Fairy Dust oraz XRelog lub później także Tracktower.

104
00:13:07,080 --> 00:13:14,040
To tylko trzy światy z dziesiątek światów.

105
00:13:14,040 --> 00:13:18,240
Zapewne niedługo będą ich setki do odwiedzenia.

106
00:13:18,240 --> 00:13:27,520
Oczywiście są też światy, które nie są ogólnodostępne, jak np. Darling VR. 

107
00:13:27,520 --> 00:13:32,520
Jest nieco bardziej zabarwiony seksualnie. Nie chcemy, żeby każdy mógł wejść. 

108
00:13:32,520 --> 00:13:39,280
Ale poza tym większość światów jest otwarta dla zwiedzających, 

109
00:13:39,280 --> 00:13:49,200
swobodnie, bez bzdur, bez NFT, smart contract, blockchain, kryptowalut. 

110
00:13:49,200 --> 00:13:52,560
Tego wszystkiego tu nie ma. To takie piękne.

111
00:13:52,560 --> 00:14:09,880
Anchoray, pokazałeś nam tak wiele rzeczy z tego świata. 

112
00:14:09,880 --> 00:14:12,960
Ale co zamierza pan zrobić w Kongresie? Nie powiedziałeś nic na ten temat.

113
00:14:12,960 --> 00:14:25,080
Tak, to prawda. Mamy 15 sesji i 15 tematów.

114
00:14:25,080 --> 00:14:36,440
i przyglądamy się także pozostałym 38 wydarzeniom JEV22. Naszym głównym tematem jest krytyka technologii. 

115
00:14:36,440 --> 00:14:42,240
Wyraźna krytyka technologii. Nie chcemy dopuścić do tego, by Facebookowi się upiekło.

116
00:14:42,240 --> 00:14:48,800
Chcemy rozmawiać o Web 3.0. Chcemy krytycznie przyjrzeć się temu, jak to się rozwija.

117
00:14:48,800 --> 00:14:53,520
Przygotowaliśmy dla Was kilka bardzo ciekawych sesji.

118
00:14:53,520 --> 00:14:58,280
Następnie chcemy spojrzeć poza koniec naszego nosa i przyjrzeć się również innym wydarzeniom związanym z JEV.

119
00:14:58,280 --> 00:15:04,920
Jest kilka absolutnie fantastycznych inicjatyw, od Nowego Jorku po Ateny. 

120
00:15:04,920 --> 00:15:12,560
W Monachium są dwa, w Hamburgu w sumie cztery. W Berlinie, w Erlangen, w Düsseldorfie, w Aachen.

121
00:15:12,560 --> 00:15:15,800
Jest wiele do odkrycia.

122
00:15:15,800 --> 00:15:17,280
Ale czy będzie to równie dobra zabawa?

123
00:15:17,280 --> 00:15:23,640
Och, zabawa! Tak, oczywiście. To prawda, że nie mamy Jeopardy,

124
00:15:23,640 --> 00:15:29,080
nie udało nam się przygotować do niego żadnych fajniejszych pytań. 

125
00:15:29,080 --> 00:15:38,400
Ale próbujemy zrobić coś tak szalonego. Będziemy go nazywać Mafifa World Cup 2030. Powtórzymy losowanie Mistrzostw Świata w 2030 roku.

126
00:15:38,400 --> 00:15:44,920
Daj się zaskoczyć. Każdorazowo o godzinie 23:00 w środę, czwartek i piątek.

127
00:15:44,920 --> 00:15:47,400
To będzie ciekawe.

128
00:15:47,400 --> 00:15:50,600
Musisz to robić w bardzo skorumpowany sposób.

129
00:15:50,600 --> 00:15:57,840
Czy kolega liczy się jako waluta?

130
00:15:57,840 --> 00:16:05,720
Jeśli nie liczy się jako waluta, to my też nie jesteśmy skorumpowani. 

131
00:16:05,720 --> 00:16:12,880
Ważne jest to, że mamy bardzo, bardzo mało prezentacji przednich.

132
00:16:12,880 --> 00:16:19,640
Prawie wszystko jest warsztatem partycypacyjnym, zaprojektowanym jako Barcamp. 

133
00:16:19,640 --> 00:16:28,080
Jest barcamp XR, zaraz po tym otwarciu. Ale jest też Bits and Trees.

134
00:16:28,080 --> 00:16:34,200
Fireside Chat, do udziału w którym serdecznie zapraszamy.

135
00:16:34,200 --> 00:16:39,760
Także warsztaty Overte'a, gdzie odkrywamy ten świat, 

136
00:16:39,760 --> 00:16:41,080
są również pomyślane jako warsztaty, a nie wykłady.

137
00:16:41,080 --> 00:16:50,760
Czy chcesz jeszcze raz powiedzieć, jaki mamy dostęp do świata? 

138
00:16:50,760 --> 00:16:57,800
Bo widzimy ten naprawdę wspaniały świat, ale jak się do niego dostać? 

139
00:16:57,800 --> 00:16:58,800
Możemy cię odwiedzić?

140
00:16:58,800 --> 00:17:04,520
Tak, oczywiście. Najważniejsze jest więc to, że w The Matrix jesteśmy w domu. 

141
00:17:04,520 --> 00:17:11,960
W harmonogramie Pretalx dla każdej prezentacji wskazany jest kanał Matrix. 

142
00:17:11,960 --> 00:17:16,760
To właśnie tam się spotykamy, a także wymieniamy wszelkie informacje. 

143
00:17:16,760 --> 00:17:29,820
Inna sprawa to to Metaverse, to Multiverse Overte.

144
00:17:29,820 --> 00:17:35,520
Overte.org jest już stroną główną.
 Można tam pobrać klienta.

145
00:17:35,520 --> 00:17:42,880
Istnieje dla systemu Linux i dla systemu Windows. Co ciekawe, nie jest ona dostępna dla Apple. 

146
00:17:42,880 --> 00:17:51,840
Możecie też za jego pomocą odwiedzić nas w ICCBS, w lesie Fairy Dust. 

147
00:17:51,840 --> 00:17:57,160
Albo w wirtualnym hackerspace'ie na xrelogu z Wieżą Tropów. 

148
00:17:57,160 --> 00:17:58,160
Wszystko to można zbadać samemu.

149
00:17:58,160 --> 00:18:08,760
To brzmi ekscytująco i myślę, 

150
00:18:08,760 --> 00:18:11,600
niektórzy słuchacze powinni też uznać, że spotkanie z Tobą w przestrzeni 3D jest naprawdę ekscytujące.

151
00:18:11,600 --> 00:18:17,640
Jestem ciekawa. Ilu widzów zawali serwery? 

152
00:18:17,640 --> 00:18:19,040
dowiemy się razem.

153
00:18:19,040 --> 00:18:20,040
Testowanie, testowanie.

154
00:18:20,040 --> 00:18:26,200
Możemy również udostępnić inne serwery. Ale nie jesteśmy jeszcze zautomatyzowani. 

155
00:18:26,200 --> 00:18:32,280
dostarczanie innych serwerów. Ale nie jesteśmy jeszcze bardzo zautomatyzowani. 

156
00:18:32,280 --> 00:18:36,840
Trochę to trwa, ale jesteśmy gotowi. 

157
00:18:36,840 --> 00:18:38,080
Jeśli teraz upadniemy, możemy jeszcze coś zrobić.

158
00:18:38,080 --> 00:18:45,520
Na koniec chciałbym się serdecznie pożegnać. 

159
00:18:45,520 --> 00:18:51,240
A tak w ogóle, to kim ty jesteś, hm, hm, dokładnie?

160
00:18:51,240 --> 00:18:56,560
Przepraszam Anchoray, nie mogę ci powiedzieć.

161
00:18:56,560 --> 00:19:25,560
Będziemy więc musieli rozwiązać tę zagadkę następnym razem. Do zobaczenia później. Do widzenia.