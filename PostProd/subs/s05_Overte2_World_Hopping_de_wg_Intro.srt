1
00:00:00,000 --> 00:00:03,660
 

2
00:00:06,680 --> 00:00:12,600
 

3
00:00:15,600 --> 00:00:21,200
 

4
00:00:25,080 --> 00:00:27,360
 

5
00:00:27,360 --> 00:00:28,700
 

6
00:01:28,700 --> 00:01:58,660
Willkommen zum zweiten Tag unserer xrelog22.

7
00:01:58,660 --> 00:02:03,340
Unsere erste Session führt uns durch die Welt von Overte.

8
00:02:03,340 --> 00:02:10,620
Wir machen ein kleines World-Hopping. Startpunkt ist unser Makerspace

9
00:02:10,620 --> 00:02:18,740
xrelog und dort treffen wir uns vor der Hacker Ethik. Mal schauen wer schon da ist.

10
00:02:18,740 --> 00:02:40,180
Und es sind schon sechs Leute da.

11
00:02:40,180 --> 00:02:49,180
Hallo. Willkommen zu unserer kleinen Tour. Ich muss meinen kleinen Woody noch ein bisschen wegzaubern.

12
00:02:49,180 --> 00:03:12,180
Okay. While we are waiting we can have a short look on our Hacker Ethic.

13
00:03:12,180 --> 00:03:28,180
If you think you don't understand it you can click on the voice snippets.

14
00:03:28,180 --> 00:03:36,180
You can listen in German or in English.

15
00:03:36,180 --> 00:03:44,180
The stream is started so we are online.

16
00:03:44,180 --> 00:04:00,180
And perhaps I will also use our xrelog22:overte channel. There I am posting where we are going next.

17
00:04:00,180 --> 00:04:29,180
So from here what do you think we go next?

18
00:04:29,180 --> 00:04:38,180
Okay.

19
00:04:38,180 --> 00:04:59,180
Fantasy World at first. I think at second.

20
00:04:59,180 --> 00:05:09,180
Okay.

21
00:05:29,180 --> 00:05:49,180
Okay.

22
00:05:49,180 --> 00:05:59,180
Okay.

23
00:06:19,180 --> 00:06:39,180
Okay.

24
00:06:39,180 --> 00:06:57,180
Yes. Ah yes. In the audio you can adjust music volume also.

25
00:06:57,180 --> 00:07:22,180
Alright.

26
00:07:22,180 --> 00:07:29,180
Is there a sign if this world loading is ready?

27
00:07:29,180 --> 00:07:40,180
Ah, loading content. I see.

28
00:07:40,180 --> 00:07:55,180
Okay.

29
00:08:10,180 --> 00:08:39,180
Oh.

30
00:08:39,180 --> 00:08:47,180
The cow sounds also nice.

31
00:08:47,180 --> 00:08:51,180
Ah, there are C-Scripts.

32
00:08:51,180 --> 00:09:14,180
Ah, but they don't work. Ah, they work in VR. Nice.

33
00:09:14,180 --> 00:09:29,180
Oh.

34
00:09:29,180 --> 00:09:53,180
Ah.

35
00:09:53,180 --> 00:10:10,180
So we can try. Let's go this way.

36
00:10:10,180 --> 00:10:31,180
Hm.

37
00:10:31,180 --> 00:10:46,180
I think the route is new. So let's see. Let's see where it leads.

38
00:10:46,180 --> 00:10:49,180
Oh, let's maybe wait.

39
00:10:49,180 --> 00:10:52,180
Where are others? Just a moment. I will see.

40
00:10:52,180 --> 00:11:01,180
Oh, we are here.

41
00:11:01,180 --> 00:11:08,180
Hm.

42
00:11:08,180 --> 00:11:13,180
Ah, I will also see if...

43
00:11:13,180 --> 00:11:23,180
I think someone is lost. So I will help them find the way.

44
00:11:23,180 --> 00:11:34,180
Oh, coming fast.

45
00:11:34,180 --> 00:11:45,180
Ah, and if you get lost at any moment, you can go to people tab and click on a person's name and you will automatically teleport to them.

46
00:11:45,180 --> 00:12:00,180
Oh yes, like this. So let's go. We should see where the route will go.

47
00:12:00,180 --> 00:12:21,180
I have no idea, to be honest, because I didn't check this way, but it will be something cool. I'm pretty sure.

48
00:12:21,180 --> 00:12:43,180
Ah, the way back we fly.

49
00:12:43,180 --> 00:12:45,180
Oh, there is something here.

50
00:12:45,180 --> 00:12:48,180
Oh, that's new, I think.

51
00:12:48,180 --> 00:12:50,180
So let's wait for the other.

52
00:12:50,180 --> 00:12:57,180
Yes, let's wait for everyone.

53
00:13:21,180 --> 00:13:32,180
Oh, let's check it out. Everyone is here so we can see what's inside.

54
00:13:32,180 --> 00:13:47,180
Be careful not to step on water.

55
00:13:47,180 --> 00:14:12,180
So this is all sculpted from voxels. It is all sculpted in game.

56
00:14:12,180 --> 00:14:17,180
Come back for a moment. Check. There is a hole there.

57
00:14:17,180 --> 00:14:42,180
Check it out. That's pretty nice.

58
00:14:42,180 --> 00:15:00,180
Oh, there is a sound track here. Nice. If you get close to the...

59
00:15:00,180 --> 00:15:03,180
Is it actually possible to read this?

60
00:15:03,180 --> 00:15:12,180
I think the resolution is too low for here. But generally it is possible.

61
00:15:12,180 --> 00:15:17,180
A friend made also a shader that puts web entities on the textures.

62
00:15:17,180 --> 00:15:35,180
So it should be possible to make a book where you can actually flip pages and have text in it.

63
00:15:35,180 --> 00:15:55,180
And of course if the texture was higher resolution, it should be doubled.

64
00:15:55,180 --> 00:16:07,180
Oh, check.

65
00:16:25,180 --> 00:16:34,180
And here you can see from the top.

66
00:16:34,180 --> 00:17:03,180
Because when you go outside, it keeps a certain zone until it is back.

67
00:17:04,180 --> 00:17:15,180
There is a medieval town here.

68
00:17:15,180 --> 00:17:20,180
Okay. What do you think? Should we jump to the next world?

69
00:17:20,180 --> 00:17:27,180
Maybe we should see... There is more stuff here. But we could also try a different world.

70
00:17:27,180 --> 00:17:36,180
But later, here is also an abandoned cathedral to discover. But we can leave it to players for later.

71
00:17:36,180 --> 00:17:39,180
And right now we could try... I think we could try Alicia's world.

72
00:17:39,180 --> 00:17:43,180
Because those are absolutely beautiful. But we will have to try.

73
00:17:43,180 --> 00:17:52,180
So first we should try to... Because they don't always load for everyone.

74
00:17:52,180 --> 00:18:02,180
So I will stay here for a moment. And everyone will try and see if they load for them.

75
00:18:02,180 --> 00:18:08,180
So the one we would like to go is called Hytrion Junction.

76
00:18:08,180 --> 00:18:16,180
So it is like the one with black, white and orange background.

77
00:18:16,180 --> 00:18:20,180
And this is called Hytrion Junction.

78
00:18:20,180 --> 00:18:22,180
That is the next world?

79
00:18:22,180 --> 00:18:25,180
Yes, yes, yes. So we should go there.

80
00:18:25,180 --> 00:18:50,180
And...

81
00:18:50,180 --> 00:19:03,180
I think it worked for everyone.

82
00:19:03,180 --> 00:19:14,180
Totally white everything. Ah, okay.

83
00:19:14,180 --> 00:19:23,180
Yeah. Careful. There are portals nearby. I just stepped into one.

84
00:19:23,180 --> 00:19:28,180
No.

85
00:19:28,180 --> 00:19:34,180
I see it works for everyone. So this is like a hub world of worlds made by Alicia.

86
00:19:34,180 --> 00:19:36,180
And some of them are really beautiful.

87
00:19:36,180 --> 00:19:42,180
So the first one I would like to show you will be called... It is called Chrome Kruash.

88
00:19:42,180 --> 00:19:46,180
It is like a night forest. It is absolutely beautiful.

89
00:19:46,180 --> 00:19:50,180
So let's go check this one. And there is a lot of different ones.

90
00:19:50,180 --> 00:19:57,180
Like for example there is one called Vox Secutor.

91
00:19:57,180 --> 00:20:03,180
So that one is like a Pac-Man-like game. For example.

92
00:20:03,180 --> 00:20:05,180
There is a lot of other different ones.

93
00:20:05,180 --> 00:20:09,180
Like for example this is also game-like. But it is mostly for VR.

94
00:20:09,180 --> 00:20:15,180
This is really cool. This looks like a cinema.

95
00:20:15,180 --> 00:20:23,180
Yes. And Alicia has absolutely amazing talent for like very unique art style.

96
00:20:23,180 --> 00:20:29,180
And her worlds are pretty much best, like most amazing ones in Oberta.

97
00:20:29,180 --> 00:20:42,180
So let's see. I don't see Chrome Kruash here. Maybe.

98
00:20:42,180 --> 00:20:50,180
So maybe we should go. We should go like if we go this way to that green one to Giltz Junction.

99
00:20:50,180 --> 00:21:01,180
We should go there. And then...

100
00:21:01,180 --> 00:21:30,180
Maybe we should go this way to there. And we need to walk into...

101
00:21:30,180 --> 00:21:45,180
Maybe we should go this way to there. And we need to walk into...

102
00:21:45,180 --> 00:22:00,180
Maybe we should go this way to there. And we need to walk into...

103
00:22:00,180 --> 00:22:15,180
Maybe we should go this way to there. And we need to walk into...

104
00:22:15,180 --> 00:22:40,180
Oh, we should message him. Because maybe he is back.

105
00:22:40,180 --> 00:22:46,180
And I will quickly see if he is back there or not.

106
00:22:46,180 --> 00:22:53,180
Ah, he is here. Nice. But it's still loading.

107
00:22:53,180 --> 00:23:00,180
Yes. So everyone is here. Let's wait.

108
00:23:00,180 --> 00:23:11,180
And in the meantime, let's go this way, because this world in itself is very unique and beautiful.

109
00:23:11,180 --> 00:23:15,180
Is there a way to see the current name of the world?

110
00:23:15,180 --> 00:23:22,180
Yes. So you can even copy and paste the link to the world so others can join you there.

111
00:23:22,180 --> 00:23:32,180
And let's try if we go... So it is like on the top of the menu, you can check...

112
00:23:32,180 --> 00:23:35,180
Let me see, because I'm in VR right now.

113
00:23:35,180 --> 00:23:38,180
At the top.

114
00:23:38,180 --> 00:23:46,180
So like at the top in navigate menu, you can copy link to current position and place and even give it to other users.

115
00:23:46,180 --> 00:23:49,180
And you can also bookmark places there.

116
00:23:49,180 --> 00:23:52,180
Also, let's see.

117
00:23:52,180 --> 00:24:00,180
I think here there should be something really unique and cool.

118
00:24:00,180 --> 00:24:09,180
Oh yes. This part is gorgeous.

119
00:24:09,180 --> 00:24:13,180
Make sure you have a graphic detail set to medium or high.

120
00:24:13,180 --> 00:24:17,180
High is of course better and medium is also good.

121
00:24:17,180 --> 00:24:25,180
Also this part is very pretty.

122
00:24:25,180 --> 00:24:38,180
Oh, and there is a cup.

123
00:24:38,180 --> 00:25:07,180
And they make popping sounds when they pop out from there.

124
00:25:07,180 --> 00:25:14,180
So the next one, also by Alizia, that is absolutely beautiful.

125
00:25:14,180 --> 00:25:21,180
So I think it is not a voxel because it uses PBR materials, but it was made...

126
00:25:21,180 --> 00:25:24,180
It is like a voxel, but exported from Blender.

127
00:25:24,180 --> 00:25:36,180
But in the future I also want to add the PBR materials to voxels, so then it will be possible to get such material quality with voxels.

128
00:25:36,180 --> 00:25:40,180
So we should try... I think we should try to go to Chrome Crush.

129
00:25:40,180 --> 00:25:43,180
It is also via portal from here.

130
00:25:43,180 --> 00:25:45,180
So let's try.

131
00:25:45,180 --> 00:26:07,180
Oh, the sight from here is nice.

132
00:26:07,180 --> 00:26:25,180
Oh, just a moment.

133
00:26:25,180 --> 00:26:31,180
Can you paste the word name where we go in the chat?

134
00:26:31,180 --> 00:26:38,180
But it is not... We cannot go there through words, so we have to go there through portal from here.

135
00:26:38,180 --> 00:26:43,180
Okay.

136
00:26:43,180 --> 00:26:46,180
So let's try. So that one will be called Chrome Crush.

137
00:26:46,180 --> 00:27:15,180
So let's follow me and I will show you where the portal is.

138
00:27:15,180 --> 00:27:21,180
So it is called Chrome Crush.

139
00:27:21,180 --> 00:27:24,180
And let's go this way.

140
00:27:24,180 --> 00:27:53,180
I will wait here just a moment more.

141
00:27:53,180 --> 00:28:20,180
Oh.

142
00:28:20,180 --> 00:28:40,180
Oh.

143
00:28:40,180 --> 00:28:43,180
This is not for everyone else.

144
00:28:43,180 --> 00:28:46,180
Hopefully for...

145
00:28:46,180 --> 00:28:48,180
It is a dark one here, yeah?

146
00:28:48,180 --> 00:28:51,180
But everybody is already exploring there.

147
00:28:51,180 --> 00:28:54,180
So let's go.

148
00:28:54,180 --> 00:28:58,180
Maybe.

149
00:28:58,180 --> 00:29:03,180
Yes.

150
00:29:03,180 --> 00:29:25,180
I think you need some lightning for the darkness so that we can't see you.

151
00:29:25,180 --> 00:29:42,180
Oh, yes.

152
00:29:42,180 --> 00:29:44,180
You can actually pick up mushrooms.

153
00:29:44,180 --> 00:29:46,180
Mushrooms? Oh.

154
00:29:46,180 --> 00:29:48,180
Yes.

155
00:29:48,180 --> 00:29:52,180
What happened then?

156
00:29:52,180 --> 00:29:58,180
I'm eating them though because the screen will become very blurry and colorful.

157
00:29:58,180 --> 00:29:59,180
Yes.

158
00:29:59,180 --> 00:30:03,180
Eating mushrooms and you become VR even without a headset.

159
00:30:03,180 --> 00:30:05,180
So you can eat them.

160
00:30:05,180 --> 00:30:09,180
Let's try.

161
00:30:09,180 --> 00:30:10,180
No.

162
00:30:10,180 --> 00:30:12,180
It doesn't seem to work on this one.

163
00:30:12,180 --> 00:30:19,180
But I've seen such that it was possible.

164
00:30:19,180 --> 00:30:22,180
This is like an ancient...

165
00:30:50,180 --> 00:31:01,180
Oh, sorry.

166
00:31:01,180 --> 00:31:04,180
So I think we need to go this way.

167
00:31:04,180 --> 00:31:06,180
Probably.

168
00:31:06,180 --> 00:31:19,180
Yes, and it is there.

169
00:31:19,180 --> 00:31:25,180
Ah, music.

170
00:31:25,180 --> 00:31:45,180
Oh.

171
00:31:45,180 --> 00:32:05,180
Oh, yes.

172
00:32:05,180 --> 00:32:31,180
Oh.

173
00:32:31,180 --> 00:32:45,180
Oh, and if we go this way, there is also a really nice campfire there.

174
00:32:45,180 --> 00:33:04,180
So just like chill and relax by the fire.

175
00:33:04,180 --> 00:33:30,180
Oh, yes. And you have this screen.

176
00:33:30,180 --> 00:33:31,180
So.

177
00:33:31,180 --> 00:33:33,180
Let's wait for everyone. Yes.

178
00:33:33,180 --> 00:33:40,180
Perhaps it's time for a word with more lights.

179
00:33:40,180 --> 00:33:41,180
Oh, good idea.

180
00:33:41,180 --> 00:33:43,180
So let's try.

181
00:33:43,180 --> 00:33:48,180
I suggest, let me think.

182
00:33:48,180 --> 00:33:52,180
Oh, like this also by Alicia.

183
00:33:52,180 --> 00:33:56,180
There is absolutely beautiful like tropical island word.

184
00:33:56,180 --> 00:33:57,180
Tropical island.

185
00:33:57,180 --> 00:33:59,180
So we should go there.

186
00:33:59,180 --> 00:34:01,180
And then I will show you something totally different.

187
00:34:01,180 --> 00:34:07,180
Because there is also absolutely amazing word by Basinski that is called photodrametry.

188
00:34:07,180 --> 00:34:09,180
And that one shows something very unusual.

189
00:34:09,180 --> 00:34:11,180
So it's very worth seeing.

190
00:34:11,180 --> 00:34:17,180
So I think that we could go just back here because the portal is in the same world.

191
00:34:17,180 --> 00:34:30,180
So we cannot teleport to, how to say, to that like summer island world.

192
00:34:30,180 --> 00:34:34,180
So we can just go back this way.

193
00:34:34,180 --> 00:34:58,180
Like we came from here and through the cave.

194
00:34:58,180 --> 00:35:00,180
This was beautiful.

195
00:35:00,180 --> 00:35:02,180
Yes.

196
00:35:02,180 --> 00:35:04,180
Alicia's words are absolutely amazing.

197
00:35:04,180 --> 00:35:06,180
And there is a lot more of them.

198
00:35:06,180 --> 00:35:12,180
And some are like many miles long in every direction and take a lot of time to explore.

199
00:35:12,180 --> 00:35:18,180
And for example, she even made like this absolutely huge desert world.

200
00:35:18,180 --> 00:35:25,180
When you fly there, there is even like wind sound and time of day changes.

201
00:35:25,180 --> 00:35:28,180
So the night falls.

202
00:35:56,180 --> 00:36:05,180
And Alicia also made that place.

203
00:36:05,180 --> 00:36:09,180
So now we can teleport.

204
00:36:09,180 --> 00:36:12,180
So you can teleport to us.

205
00:36:12,180 --> 00:36:17,180
But what is called?

206
00:36:17,180 --> 00:36:19,180
I'm here.

207
00:36:19,180 --> 00:36:23,180
Ah, nice.

208
00:36:23,180 --> 00:36:30,180
So we should, I think that if you want like a sun, also a really beautiful world, also by Alicia.

209
00:36:30,180 --> 00:36:34,180
So this one is called Nowhere Island.

210
00:36:34,180 --> 00:36:46,180
It's right here.

211
00:36:46,180 --> 00:36:48,180
This one is also very optimized.

212
00:36:48,180 --> 00:36:56,180
So if you load quickly in a moment for me, it's still the skybox is still loading.

213
00:36:56,180 --> 00:36:59,180
But it should be visible soon.

214
00:36:59,180 --> 00:37:04,180
Hopefully.

215
00:37:04,180 --> 00:37:06,180
Do you see the sky?

216
00:37:06,180 --> 00:37:07,180
Yeah.

217
00:37:07,180 --> 00:37:11,180
Maybe I will need to reload content.

218
00:37:11,180 --> 00:37:13,180
I see the sky.

219
00:37:13,180 --> 00:37:35,180
It's only for me.

220
00:37:35,180 --> 00:37:43,180
So let's say 10 minutes before the streams end.

221
00:37:43,180 --> 00:37:46,180
I think let's go to the Basinski world.

222
00:37:46,180 --> 00:37:49,180
Yes, to photogrammetry.

223
00:37:49,180 --> 00:37:50,180
Yes.

224
00:37:50,180 --> 00:37:55,180
And there is also another world by Basinski that is also really worth seeing.

225
00:37:55,180 --> 00:38:02,180
So that another world is with art gallery.

226
00:38:02,180 --> 00:38:05,180
So we should try for me.

227
00:38:05,180 --> 00:38:08,180
The sky didn't load here for some reason.

228
00:38:08,180 --> 00:38:11,180
It is totally black, but I have no idea why.

229
00:38:11,180 --> 00:38:12,180
But let's try.

230
00:38:12,180 --> 00:38:15,180
So we should try to go to photogrammetry.

231
00:38:15,180 --> 00:38:17,180
So it is in places.

232
00:38:17,180 --> 00:38:20,180
And it is called photogrammetry in places.

233
00:38:20,180 --> 00:38:22,180
So let's try.

234
00:38:22,180 --> 00:38:23,180
And that's really cool.

235
00:38:23,180 --> 00:38:25,180
And something very, very unique.

236
00:38:25,180 --> 00:38:28,180
Oh, nice.

237
00:38:28,180 --> 00:38:56,180
So let's click on photogrammetry in places.

238
00:38:56,180 --> 00:39:12,180
This is a single room.

239
00:39:12,180 --> 00:39:15,180
This smells like chaos.

240
00:39:15,180 --> 00:39:18,180
Where is the party room?

241
00:39:18,180 --> 00:39:31,180
So this one is very unique because it has, how to say,

242
00:39:31,180 --> 00:39:35,180
so those are 3D scans made by photogrammetry.

243
00:39:35,180 --> 00:39:40,180
So you can just take a lot of photos of a given room or given location

244
00:39:40,180 --> 00:39:46,180
and then turn it into VR location this way.

245
00:39:46,180 --> 00:39:49,180
And there are a few different examples here.

246
00:39:49,180 --> 00:39:51,180
But first, just a moment, let me...

247
00:39:51,180 --> 00:39:57,180
It's a kind of 360 degree photographies, yeah?

248
00:39:57,180 --> 00:40:00,180
Yes, but it is more than that.

249
00:40:00,180 --> 00:40:11,180
It is like a 3D model made from such photos.

250
00:40:11,180 --> 00:40:30,180
Oh, and this one has a small secret even.

251
00:40:30,180 --> 00:40:32,180
Let's see.

252
00:40:32,180 --> 00:40:34,180
Ah, it's not this way, sorry.

253
00:40:34,180 --> 00:41:01,180
So the secret is another way.

254
00:41:01,180 --> 00:41:04,180
So this is like a back alley here.

255
00:41:04,180 --> 00:41:08,180
And the rest of them will be more like,

256
00:41:08,180 --> 00:41:10,180
will be totally different style.

257
00:41:10,180 --> 00:41:16,180
So we can try other ones.

258
00:41:16,180 --> 00:41:19,180
So I'm curious about Kraftwerk.

259
00:41:19,180 --> 00:41:21,180
Oh, can you say again? Sorry.

260
00:41:21,180 --> 00:41:24,180
I'm curious about Kraftwerk because you have...

261
00:41:24,180 --> 00:41:26,180
Ah, yes.

262
00:41:26,180 --> 00:41:32,180
But it is not very, like, I was getting it finished for the time

263
00:41:32,180 --> 00:41:34,180
and it's almost finished.

264
00:41:34,180 --> 00:41:36,180
It is visitable and it looks pretty nice.

265
00:41:36,180 --> 00:41:42,180
But I couldn't get shadows to work in time.

266
00:41:42,180 --> 00:41:44,180
Let's not switch world yet.

267
00:41:44,180 --> 00:41:46,180
Let's wait for everyone.

268
00:41:46,180 --> 00:41:49,180
Wait, what is this platform?

269
00:41:49,180 --> 00:41:51,180
Is this a shuttle?

270
00:41:51,180 --> 00:41:53,180
Oh, just a moment.

271
00:41:53,180 --> 00:41:56,180
It seems like, yeah.

272
00:41:56,180 --> 00:41:58,180
So you can press buttons and something happens.

273
00:41:58,180 --> 00:42:02,180
Yes, and on this platform you can switch different photogrammetry worlds.

274
00:42:02,180 --> 00:42:08,180
And if you are stacking the walls, you need to go to places app, to people app.

275
00:42:08,180 --> 00:42:13,180
Yes, to people app and click on our names and then you can teleport to us.

276
00:42:13,180 --> 00:42:14,180
If you are stuck.

277
00:42:14,180 --> 00:42:17,180
Okay.

278
00:42:17,180 --> 00:42:21,180
Nice.

279
00:42:21,180 --> 00:42:28,180
If you are stuck, click on our names and then you will teleport to us.

280
00:42:28,180 --> 00:42:32,180
Oh, yes, it works. Nice.

281
00:42:32,180 --> 00:42:40,180
Also, so this one looks almost like a photo.

282
00:42:40,180 --> 00:42:45,180
And there will be even better one in a moment.

283
00:42:45,180 --> 00:42:48,180
And those are done just by doing a lot of photos

284
00:42:48,180 --> 00:42:58,180
and then they're automatically computed from photos with photogrammetry software.

285
00:42:58,180 --> 00:43:01,180
Oh, let's try. Oh, this one is really cool.

286
00:43:01,180 --> 00:43:03,180
We need to visit this one.

287
00:43:03,180 --> 00:43:05,180
Oh, let's wait.

288
00:43:05,180 --> 00:43:10,180
Let's wait till it loads now.

289
00:43:10,180 --> 00:43:16,180
This one take a moment to load because it's pretty big, but it's very worth seeing.

290
00:43:16,180 --> 00:43:23,180
Oh, it's loading.

291
00:43:23,180 --> 00:43:29,180
Let me know when it loads for you and we can take a look.

292
00:43:29,180 --> 00:43:32,180
Okay, so there's actually more than one room now.

293
00:43:32,180 --> 00:43:34,180
Yes.

294
00:43:34,180 --> 00:43:44,180
I can't imagine how many photos this requires to make.

295
00:43:44,180 --> 00:43:56,180
And it is possible to do, for example, make something like this from photos of home, for example, pretty much anything.

296
00:43:56,180 --> 00:44:03,180
So it's like a 3D scan, but with camera.

297
00:44:03,180 --> 00:44:05,180
That's even a fire extinguisher.

298
00:44:05,180 --> 00:44:14,180
Yes, because I'm pretty sure it was from some kind of museum.

299
00:44:14,180 --> 00:44:42,180
There's also like a light, a fire light at the top or emergency light.

300
00:44:42,180 --> 00:44:59,180
Let's check what the first ones are here.

301
00:44:59,180 --> 00:45:06,180
We didn't check this one. So let's see.

302
00:45:06,180 --> 00:45:13,180
I will browse for a moment and let's see what else we have here.

303
00:45:13,180 --> 00:45:17,180
Ah, that one was simple, but really cool.

304
00:45:17,180 --> 00:45:25,180
And it will load quickly.

305
00:45:25,180 --> 00:45:27,180
Oh, it is not loading.

306
00:45:27,180 --> 00:45:34,180
Maybe I have to click short screen again.

307
00:45:34,180 --> 00:45:44,180
Okay, so I think that another world that will be worth seeing, that is very interesting, is called...

308
00:45:44,180 --> 00:45:54,180
So another world that is really worth seeing, is very interesting, is...

309
00:45:54,180 --> 00:45:57,180
We could visit... Just a moment.

310
00:45:57,180 --> 00:46:02,180
I have to check if it's available.

311
00:46:02,180 --> 00:46:05,180
I have to rename that, so that's gonna be possible too.

312
00:46:05,180 --> 00:46:10,180
We have heard about today this place, it's called France.

313
00:46:10,180 --> 00:46:24,180
So, let's do another world, which will build first.

314
00:46:24,180 --> 00:46:29,180
Ja, die Orte. Und in Orten, wo viele Mütter sind.

315
00:46:29,180 --> 00:46:49,180
Und, er in einen Schuben, wo zum Beispiel die Überfläche von Osz.

316
00:46:59,180 --> 00:47:02,740
Ja teils wird das Lernen im Auto begleitet.

317
00:47:06,480 --> 00:47:12,240
Die Steuerung von Didnnius für Maps hat sich verändert.

318
00:47:16,160 --> 00:47:21,460
Sie können Signale abstellen oder sie kurz тогда wiederholen.

319
00:47:22,120 --> 00:47:24,800
Das ist das cooler Shape-Shifting.

320
00:47:54,800 --> 00:48:00,800
...

321
00:48:00,800 --> 00:48:04,800
...

322
00:48:04,800 --> 00:48:10,800
...

323
00:48:10,800 --> 00:48:17,800
...

324
00:48:17,800 --> 00:48:20,800
...

325
00:48:20,800 --> 00:48:26,800
...

326
00:48:26,800 --> 00:48:32,800
...

327
00:48:32,800 --> 00:48:38,800
...

328
00:48:38,800 --> 00:48:44,800
...

329
00:48:44,800 --> 00:48:49,800
...

330
00:48:49,800 --> 00:48:54,800
...

331
00:48:54,800 --> 00:48:57,800
...

332
00:48:57,800 --> 00:49:00,800
...

333
00:49:00,800 --> 00:49:03,800
...

334
00:49:03,800 --> 00:49:06,800
...

335
00:49:06,800 --> 00:49:09,800
...

336
00:49:09,800 --> 00:49:14,800
...

337
00:49:14,800 --> 00:49:19,800
...

338
00:49:19,800 --> 00:49:22,800
...

339
00:49:22,800 --> 00:49:25,800
...

340
00:49:25,800 --> 00:49:30,800
...

341
00:49:30,800 --> 00:49:33,800
...

342
00:49:33,800 --> 00:49:36,800
...

343
00:49:36,800 --> 00:49:39,800
...

344
00:49:39,800 --> 00:49:42,800
...

345
00:49:42,800 --> 00:49:47,800
...

346
00:49:47,800 --> 00:49:50,800
...

347
00:49:50,800 --> 00:49:55,800
...

348
00:49:55,800 --> 00:49:58,800
...

349
00:49:58,800 --> 00:50:01,800
...

350
00:50:01,800 --> 00:50:04,800
...

351
00:50:04,800 --> 00:50:12,800
Es ist ein sehr schönes Beispiel, wie man virtuellen Kunstwerke so kreieren kann.

352
00:50:13,800 --> 00:50:15,800
Und die Eisbäume sehen schön aus.

353
00:50:20,800 --> 00:50:26,800
Also warten wir, bis alle aufgeladen sind, und wir können die Galerie probieren.

354
00:50:26,800 --> 00:50:30,800
Ein wirklich schönes Launch, ja.

355
00:50:57,800 --> 00:51:06,800
Und natürlich ist es möglich, auch ein Bild zu machen, dass die Bilder eine größere Resolution haben, wenn man sie umschaut.

356
00:51:12,800 --> 00:51:14,800
Oder sie animieren, glaube ich.

357
00:51:14,800 --> 00:51:26,800
Ja.

358
00:51:44,800 --> 00:52:04,800
Und es ist Zeit für das Streaming.

359
00:52:04,800 --> 00:52:14,800
Also, für alle Leute da draußen, warte mal.

360
00:52:14,800 --> 00:52:16,800
Ist es möglich, die Menü-Bar unten zu verstecken?

361
00:52:16,800 --> 00:52:18,800
Ja.

362
00:52:18,800 --> 00:52:24,800
Ja, also der einfachste Weg wäre, nur einen Moment.

363
00:52:24,800 --> 00:52:28,800
Du musst zuerst das Entwickler-Menü einlegen.

364
00:52:28,800 --> 00:52:52,800
Also, jetzt, zuerst sagen wir für den Streamer, vielleicht in meine Richtung, wo ich schaue, da ist eine Streamkamera, und sagen wir, tschüss, danke fürs Zuschauen, tschüss.

365
00:52:52,800 --> 00:52:58,800
Tschüss.

366
00:52:58,800 --> 00:53:00,800
Wir müssen diesen besuchen.

367
00:53:00,800 --> 00:53:07,800
Oh, warten wir mal, bis es lautet.

368
00:53:07,800 --> 00:53:13,800
Dieser dauert einen Moment, weil er ziemlich groß ist, aber sehr wertvoll zu sehen.

369
00:53:13,800 --> 00:53:20,800
Oh, es lautet.

370
00:53:20,800 --> 00:53:25,800
Und es dauert einen Moment, bis es lautet.

371
00:53:25,800 --> 00:53:27,800
Danke für das Zuschauen.

372
00:53:27,800 --> 00:53:30,800
Vater hat auf der Bank geschoben.

373
00:53:30,800 --> 00:53:34,800
Kollege Robert hat auf der Bar geschoben, Machbararbeit.

374
00:53:34,800 --> 00:53:43,800
Die Widebrandfamilie zum Beispiel, die jasen stark.

375
00:53:43,800 --> 00:53:51,320
Man kann zum Beispiel so etwas aus Fotos von Zuhause machen.

376
00:53:53,320 --> 00:53:55,640
Es ist wie ein 3D-Scan, aber mit Kamera.

377
00:54:00,120 --> 00:54:02,120
Es ist sogar ein Feuerzünder.

378
00:54:02,760 --> 00:54:10,120
Ja, ich bin mir ziemlich sicher, dass es aus einem Museum kam.

379
00:54:10,120 --> 00:54:20,520
Denn es ist auch eine L chuy... Feuerzeuge, mit ste немmber dem fluss.

380
00:54:20,520 --> 00:54:46,520
Oh, let's see what the first ones are here.

381
00:54:46,520 --> 00:55:02,520
Ah, we didn't check this one, so let's see.

382
00:55:02,520 --> 00:55:10,520
Ah, I have to click Strobe Scene 2, so I will browse for a moment and let's see what else we have here.

383
00:55:10,520 --> 00:55:13,520
Ah, that one was simple but really cool.

384
00:55:13,520 --> 00:55:22,520
And it will load quickly.

385
00:55:22,520 --> 00:55:24,520
Oh, it is not loading.

386
00:55:24,520 --> 00:55:30,520
Maybe I have to click Short Scene again.

387
00:55:30,520 --> 00:55:31,520
Ah, it doesn't show.

388
00:55:31,520 --> 00:55:41,520
Okay, so I think that another world that will be worth seeing that is very interesting is called...

389
00:55:41,520 --> 00:55:51,520
So another world that is really worth seeing and is very interesting is...

390
00:55:51,520 --> 00:55:54,520
We could visit... Just a moment.

391
00:55:54,520 --> 00:55:58,520
I have to check if it is available.

392
00:55:58,520 --> 00:56:06,520
So like something more like futuristic, there is a world called Mother's Place.

393
00:56:06,520 --> 00:56:12,520
Like Mother's with two D. Mother's Place.

394
00:56:12,520 --> 00:56:17,520
So let's go there and that one has a very unique feature.

395
00:56:17,520 --> 00:56:20,520
Okay, we have to go to the Places menu.

396
00:56:20,520 --> 00:56:22,520
Yes.

397
00:56:22,520 --> 00:56:38,520
Yes, Places, and in Places menu, Mother's Place.

398
00:56:38,520 --> 00:57:07,520
Oh, and it is loading but it will be very pretty as it was.

399
00:57:07,520 --> 00:57:34,520
Oh, for me it is loading.

400
00:57:34,520 --> 00:57:42,520
In this place the audio is a bit broken.

401
00:57:42,520 --> 00:58:11,520
Not sure. Can you hear me here?

402
00:58:11,520 --> 00:58:35,520
Yes, this is very...

403
00:58:35,520 --> 00:59:04,520
Yes.

404
00:59:05,520 --> 00:59:12,520
Oh.

405
00:59:12,520 --> 00:59:16,520
So this is Basinski's world.

406
00:59:16,520 --> 00:59:20,520
And it should have games there.

407
00:59:20,520 --> 00:59:28,520
So I think there is also an art gallery by Basinski.

408
00:59:28,520 --> 00:59:31,520
So we'll be able... Let's wait for everyone to load.

409
00:59:31,520 --> 00:59:38,520
And then we could try.

410
00:59:38,520 --> 00:59:49,520
Ah, there is... So it has some secrets to find. Nice.

411
00:59:49,520 --> 00:59:55,520
There is a lot of stuff here.

412
00:59:55,520 --> 00:59:59,520
But I see that it is still loading.

413
00:59:59,520 --> 01:00:02,520
So when it loads, we could go to gallery.

414
01:00:02,520 --> 01:00:06,520
Because it is also a really nice example of what you can do,

415
01:00:06,520 --> 01:00:11,520
how you can create virtual art galleries this way.

416
01:00:11,520 --> 01:00:18,520
And the ice looks nice.

417
01:00:18,520 --> 01:00:28,520
Also, let's wait until everyone is loaded and we could try gallery.

418
01:00:28,520 --> 01:00:30,520
Yes, really nice launch.

419
01:00:59,520 --> 01:01:03,520
And of course it is possible to also make a speech.

420
01:01:03,520 --> 01:01:08,520
That images get a bigger resolution when you get close to them.

421
01:01:08,520 --> 01:01:15,520
Or to animate it, I guess.

422
01:01:15,520 --> 01:01:44,520
Yes.

423
01:01:46,520 --> 01:01:56,520
Let me know when...

424
01:01:56,520 --> 01:02:01,520
So, okay. The time for the streaming is coming.

425
01:02:01,520 --> 01:02:09,520
So for all the people out there, let me wait.

426
01:02:09,520 --> 01:02:13,520
Is it possible to hide the menu bar at the bottom?

427
01:02:13,520 --> 01:02:15,520
Yes.

428
01:02:15,520 --> 01:02:22,520
So the easiest way would be... Just a moment.

429
01:02:22,520 --> 01:02:25,520
You have to add the developer menu first.

430
01:02:25,520 --> 01:02:33,520
Yes. So you have to go to settings and developer menu.

431
01:02:33,520 --> 01:02:35,520
So for now...

432
01:02:35,520 --> 01:02:43,520
Let's say for the streamer, let's say wave it perhaps in my direction where I'm looking.

433
01:02:43,520 --> 01:02:45,520
There is a stream camera.

434
01:02:45,520 --> 01:02:47,520
And say goodbye.

435
01:02:47,520 --> 01:02:49,520
Thank you for watching.

436
01:02:49,520 --> 01:03:18,520
Bye-bye.

437
01:03:19,520 --> 01:03:21,520
Bye-bye.

