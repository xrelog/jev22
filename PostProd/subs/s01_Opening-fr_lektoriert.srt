1
00:00:30,000 --> 00:00:40,800
Bienvenue à l'ouverture du XRelog22 !

2
00:00:40,800 --> 00:00:51,920
Bonjour Anchoray et bonjour le monde extérieur ! Nous voulons vous présenter ici ce que le XRelog22

3
00:00:51,920 --> 00:01:01,760
 vous réserve cette année. Qu'est-ce que le XRelog22 en général ?

4
00:01:01,760 --> 00:01:11,360
 Quels sont les objectifs ? Nous voulons nous occuper de SocialVR et de SocialVR sans bullshit 

5
00:01:11,360 --> 00:01:17,000
et c'est ce que nous avons mis dans notre devise. Nous l'appelons 
‘’Visions for Independent Multiverses.’’

6
00:01:17,000 --> 00:01:23,960
J'ai entendu dire que vous vouliez aussi intégrer l'éthique hacker dans le métavers et dans les multivers.

7
00:01:23,960 --> 00:01:29,960
Les étendues du méta et du multivers. Absolument ! Il n'y en a tout simplement pas assez dans le

8
00:01:29,960 --> 00:01:35,480
Métavers n'existe pas. Dis, je viens de remarquer que je ne vois pas du tout le logo de la chaîne.

9
00:01:35,480 --> 00:01:39,680
Vous avez un logo de chaîne ? Logo de la chaîne ?

10
00:01:39,680 --> 00:01:49,440
Qu'est-ce que c'est ? C'était quoi ce bruit ?

11
00:01:49,440 --> 00:01:56,960
Oui, je ne sais pas non plus. Oui, malheureusement, c'est le scotch qui n'a pas tenu.

12
00:01:56,960 --> 00:02:04,960
Oui, nous ne sommes pas les super videasts. Ni vraiment super professionnelles avec OBS.

13
00:02:04,960 --> 00:02:11,680
Nous ne sommes pas des gens parfaits, aussi il arrive que le logo de la chaîne disparaît.

14
00:02:11,680 --> 00:02:17,440
Le scotch, était tout simplement mauvais. Il doit être de mauvaise qualité.

15
00:02:17,440 --> 00:02:21,760
Je pense qu'il faut encore un peu adapter las attentes. Je veux dire par là,

16
00:02:21,760 --> 00:02:27,760
que les gens ne doivent pas penser qu'ils vont être servis à la perfection, ni une qualité de production à la ARD, ZDF.

17
00:02:27,760 --> 00:02:30,520
Personne ne veut d'ARD et de ZDF.

18
00:02:30,520 --> 00:02:35,960
Mais quoi qu'il en soit, chez nous, beaucoup de choses sont inachevées, mais séduisantes.

19
00:02:35,960 --> 00:02:44,960
Ce n'est pas inachevé, c'est en production. C'est agile. C'est un développement agile.

20
00:02:44,960 --> 00:02:47,560
Tu veux dire que ‘Work in Progress’ est la nouvelle norme, c'est ça ?

21
00:02:47,560 --> 00:02:48,560
Oui, bien sûr.

22
00:02:48,560 --> 00:02:54,320
Aha, d'accord. Alors préparez-vous à en recevoir beaucoup de renversés … de renversants …

23
00:02:54,320 --> 00:02:58,720
oui, tu vois, nous y revoilà. Préparez-vous à voir des tas de trucs renversés.

24
00:02:58,720 --> 00:03:02,320
Des bouteilles de maté et des trucs renversés, mais on va bien s'amuser.

25
00:03:02,320 --> 00:03:04,720
On les colle avec le scotch.

26
00:03:04,720 --> 00:03:05,400
Exactement.

27
00:03:05,400 --> 00:03:24,360
Mais cela se prononce vraiment XRelog22.

28
00:03:24,360 --> 00:03:31,960
Ainsi et cela dérive de XR.Labs Operation Group.

29
00:03:31,960 --> 00:03:39,800
Je suis ici à l'ICCBS, regarde. Nous construisons des mondes virtuels.

30
00:03:39,800 --> 00:03:47,800
L'un d'entre eux, un véhicule voyageant à travers le monde virtuel, est notre ICCBS, 

31
00:03:47,800 --> 00:03:55,000
Intergalactical Chaos Communication Broadcast Studio. Et il s'est amarré dans Overte pour cet événement.

32
00:03:55,000 --> 00:04:02,760
Voilà, vous voyez, vous voyez, c'est plutôt bien amoché. C'est parce que nous avons déjà eu quelques accidents ici.

33
00:04:02,760 --> 00:04:10,600
Et tout le bel aménagement intérieur n'a pas été bien fixé et s'est donc un peu abîmé. 

34
00:04:10,600 --> 00:04:17,120
Mais nous sommes en train de le réparer magnifiquement.

35
00:04:17,120 --> 00:04:23,920
Venez, je vais vous montrer quelque chose. Parce que nous voulons en faire quelques spécialités dans les coins.

36
00:04:23,920 --> 00:04:31,960
Je ne sais pas qui reconnaîtra ça, ceux qui sont déjà allés à

37
00:04:31,960 --> 00:04:39,680
 la C-Base le reconnaîtront. C'est  murs de lumière, composé de bouteilles de maté.

38
00:04:39,680 --> 00:04:49,760
Et là, nous envisageons par exemple de construire ce que l'on appelle un Digital Twin de cet appareil.

39
00:04:49,760 --> 00:04:55,120
Voici d'ailleurs les instructions de fabrication de l' interface de programmation d’application pour savoir comment s'en servir. 

40
00:04:55,120 --> 00:05:01,480
Voici ce site web, vous pouvez le regarder et même le lire. Et voici par exemple une idée de la manière dont on pourrait utiliser cet espace.

41
00:05:01,480 --> 00:05:08,920
Que nous installions cet écran LED ici.

42
00:05:08,920 --> 00:05:15,120
Je vole un peu. Tiens, ici on peut aussi voler. Il faut que je m'entraîne un peu à voler. 

43
00:05:15,120 --> 00:05:21,880
Houlà, maintenant je vole ici. Regarde par ici.  Il y a encore un trou ici, tu vois ? Oh, c'est encore cassé. 

44
00:05:21,880 --> 00:05:26,680
Ici il faut encore réparer un peu. Ça encore, c'est une autre tâche. Vous voyez, c'est comme ça à cause des accidents d'atterrissage.

45
00:05:26,680 --> 00:05:32,520
Encore une fois ici. Je viens de passer un portal et voici notre hall de départ.

46
00:05:32,520 --> 00:05:42,920
Là, on peut monter vers le zeppelin. Dis donc, la terre, là, elle tourne. Cool…

47
00:05:42,920 --> 00:05:51,920
 on peut faire ça … Très cool. Là, on voit l'ICCBS de l'extérieur. Ensuite, je suis

48
00:05:51,920 --> 00:05:58,600
ici, à l'intérieur du zeppelin. Et il y a aussi un portal. Voyons où il mène ? Allons à l'intérieur.

49
00:05:58,600 --> 00:06:08,400
Ah oui, ça ramène en arrière. Ah, c'est là qu'ils ont changé les portails. C'est bien qu'on en ait parlé.

50
00:06:08,400 --> 00:06:13,720
En fait, le portail que je viens de traverser devrait mener ici. 

51
00:06:13,720 --> 00:06:23,080
Regardons ici. Qu'est-ce ? Regarde, un sombre... Regarde, c'est la Fairy Dust.

52
00:06:23,080 --> 00:06:33,160
Incroyable. Ça, c’est la première fois que je le vois. Wow, c'est pas mal du tout. En fait, 

53
00:06:33,160 --> 00:06:39,880
Est-ce que les choses tournent encore là-haut ? Elles ne tournent plus. Si, elles tournent très lentement. 

54
00:06:39,880 --> 00:06:44,960
Ici, on voit le nœud de données sous différentes représentations artistiques. Et nous avons un feu de camp.

55
00:06:44,960 --> 00:06:56,560
Et il crépite. C'est vraiment accueillant ici. J'espère que nous nous reverrons souvent ici.

56
00:06:56,560 --> 00:07:02,720
Bon, si je me promène ici, je peux aussi aller au sous-sol de Jev22.

57
00:07:02,720 --> 00:07:13,960
Et le sous-sol ressemble à ça. Ouah, ouah, ouah, ouah. Regardons ici. 

58
00:07:13,960 --> 00:07:20,520
Ce sont tous les événements, c'est-à-dire les pages d'accueil des événements. Tu peux tous les regarder.

59
00:07:20,520 --> 00:07:27,240
Il y en a beaucoup. Oui, regardons ici. Hacking en parallèle. Et quand on s'en approche, il y a un site vraiment interactif.

60
00:07:27,240 --> 00:07:33,000
On peut cliquer sur celle-ci et la feuilleter. Et cela marche pour tous les 38 événements.

61
00:07:33,000 --> 00:07:42,640
Tu peux les regarder tous et y participer.

62
00:07:42,640 --> 00:07:54,120
Chili con Chaos . Oui, ici tu peux regarder le Chili con Chaos. Le programme est très intéressant. 

63
00:07:54,120 --> 00:08:01,480
On peut via Places, voler dans notre Makerspace.  

64
00:08:01,480 --> 00:08:11,680
Le Makerspace s'appelle simplment XRelog22. Et ce makerspace, c'est maintenant une sorte de collection de de choses inachevées. 

65
00:08:11,680 --> 00:08:17,120
de choses inachevées. Nous y avons commencé des choses, en ordre dispersé. 

66
00:08:17,120 --> 00:08:25,840
par exemple, j'ai l'idée, pour les 15 tracks, donc 15 thèmes, que nous avons. Et si on faisait une tour de tracks.

67
00:08:25,840 --> 00:08:38,800
construire 15 étages. Oui, très cool. Je veux  aussi pouvoir voler. Oui, voler est une bonne idée.

68
00:08:38,800 --> 00:08:45,840
En y regardant de plus près, ces étages ressemblent un peu à des petits halls d'embarquement. 

69
00:08:45,840 --> 00:08:53,800
Tout petit et tout mini. Et à chacun de ces étages, il y a un thème dédié. 

70
00:08:53,800 --> 00:09:02,200
On a commencé jusqu'au troisième étage.

71
00:09:02,200 --> 00:09:09,480
Que ce soit le podcasting, que ce soit le SocialVR, que ce soit les communautés XR, il y en a plusieurs et cetera.

72
00:09:09,480 --> 00:09:16,680
Et bien sûr, dans les premiers étages, il y a (H)Activism.

73
00:09:16,680 --> 00:09:28,240
C'est bien sûr notre thème principal.

74
00:09:28,240 --> 00:09:36,160
Attends, j'y vole. C'est écrit là, mais la lampe n'est pas allumée.

75
00:09:36,160 --> 00:09:42,040
J'ai déplacé la lampe quelque part. Maintenant, elle éclaire pas du tout.

76
00:09:42,040 --> 00:09:48,800
En général, ça bien éclaire ici. Est-ce qu'on peut lire ça.
Non, l'hectivisme est dans le noir. 
à propos de l'éthique des hackers.

77
00:09:48,800 --> 00:09:55,160
Regarde un peu par ici. C'est l'une des premières choses que j'ai faites.

78
00:09:55,160 --> 00:10:02,920
J'ai dessiné l'éthique du hacker en 3D dans la réalité virtuelle avec un multibrush. 

79
00:10:02,920 --> 00:10:11,240
Et j'ai ensuite intégré cette peinture comme modèle 3D ici. Et j'ai même intégré une synthèse vocale, en allemand et en anglais.

80
00:10:11,240 --> 00:10:18,160
Eh bien, ce que j'ai découvert en recherche sur l'éthique des hackers,

81
00:10:18,160 --> 00:10:24,600
c'est que les six premières règles des hackers sont plus anciennes,

82
00:10:24,600 --> 00:10:30,440
datent des années 60 et que seules deux règles ont été ajoutées par le Computer Chaos Club

83
00:10:30,440 --> 00:10:36,760
Notamment, Ne salissez pas les données des autres.

84
00:10:36,760 --> 00:10:43,840
Utiliser les données publiques, protéger les données privées. Je crois qu'on l'a déjà entendu. 

85
00:10:43,840 --> 00:10:50,520
Ce sont les deux compléments du Computer-Chaos-Club. Nous avons pris la liberté d'ajouter une neuvième règle.

86
00:10:50,520 --> 00:11:00,720
Pense toujours aux bits et aux arbres. Car l'aspect écologique, n'apparaît pas encore dans l'éthique des hackers. 

87
00:11:00,720 --> 00:11:07,280
C'est pourquoi il faudrait forcément le compléter. 

88
00:11:07,280 --> 00:11:14,320
C'était notre éthique de hacker. Sinon tu vois, c'est donc une collection de toutes sortes de choses.

89
00:11:14,320 --> 00:11:17,560
Tu vois en haut, on y voit le zeppelin.

90
00:11:17,560 --> 00:11:24,880
Au-dessus du zeppelin, il y a une première version de l'ICCBS. 

91
00:11:24,880 --> 00:11:30,440
Ici, il y a des plateaux. Nous utiliserons ces plateaux lorsque nous organiserons des ateliers.

92
00:11:30,440 --> 00:11:36,280
Alors, on pourra voler vers des thèmes et entendre des choses. 

93
00:11:36,280 --> 00:11:43,160
Nous bricolons si simplement. C'est notre makerspace virtuel.

94
00:11:43,160 --> 00:11:52,040
Il n'a pas de lien thématique. 

95
00:11:52,040 --> 00:11:59,000
Lorsque nous aurons terminé la Tracktower, elle sera transférée dans son propre serveur de domaine. 

96
00:11:59,000 --> 00:12:07,320
Ensuite, nous le retirons du makerspace et construisons quelque chose de nouveau. 

97
00:12:07,320 --> 00:12:20,400
Maintenant, je retourne au Intergalactic Chaos Communication Broadcast Studio. En abrégé ICCBS. 

98
00:12:20,400 --> 00:12:31,080
Regarde un peu. Où j'arrive. Comme ça, n'importe qui peut simplement changer de lieu, qui est dans le système ?

99
00:12:31,080 --> 00:12:37,960
Oui, tout est ouvert. Tu n'as même pas besoin de t'inscrire. 

100
00:12:37,960 --> 00:12:44,560
Tu n'as même pas besoin de créer un compte. Il te suffit de télécharger le client et de le lancer pour entrer dans ce monde.

101
00:12:44,560 --> 00:12:49,320
Tu recevras une petite explication sur le fonctionnement.

102
00:12:49,320 --> 00:12:55,960
Ensuite, tu peux déjà utiliser la fonction Places. Tu peux alors découvrir encore plus de mondes.

103
00:12:55,960 --> 00:13:07,080
Donc l'ICCBS et la Fairy Dust et le XRelog ou plus tard aussi la Tracktower.

104
00:13:07,080 --> 00:13:14,040
Ce ne sont que trois mondes sur des dizaines de mondes.

105
00:13:14,040 --> 00:13:18,240
Il y en aura probablement bientôt des centaines que l'on pourra visiter.

106
00:13:18,240 --> 00:13:27,520
Il y a bien sûr aussi des mondes qui ne sont pas ouverts au public, comme par exemple un Darling VR. 

107
00:13:27,520 --> 00:13:32,520
C'est un peu plus coloré sexuellement. On ne veut pas que n'importe qui puisse y entrer. 

108
00:13:32,520 --> 00:13:39,280
Mais sinon, la plupart des mondes sont ouverts au public, 

109
00:13:39,280 --> 00:13:49,200
librement, sans bullshit, sans NFT, smart contract, blockchain, crypto-truc. 

110
00:13:49,200 --> 00:13:52,560
Tout cela ne se trouve pas ici. C'est tellement beau.

111
00:13:52,560 --> 00:14:09,880
Anchoray, tu nous as montré tant de choses de ce monde. 

112
00:14:09,880 --> 00:14:12,960
Mais qu'avez-vous l'intention de faire sur Congress ? Tu n'as rien dit à ce sujet.

113
00:14:12,960 --> 00:14:25,080
Oui, c'est vrai. Nous avons 15 sessions, et 15 sujets.

114
00:14:25,080 --> 00:14:36,440
et nous regardons aussi les 38 autres événements JEV22. Notre thème principal est la critique de la technique. 

115
00:14:36,440 --> 00:14:42,240
Critique claire et nette de la technique. Nous ne voulons pas laisser Facebook s'en tirer.

116
00:14:42,240 --> 00:14:48,800
Nous voulons parler du Web 3.0. Nous voulons regarder de manière critique comment cela se développe.

117
00:14:48,800 --> 00:14:53,520
Nous avons préparé quelques sessions très intéressantes pour vous.

118
00:14:53,520 --> 00:14:58,280
Nous voulons ensuite regarder plus loin que le bout de notre nez et nous intéresser aussi aux autres événements JEV.

119
00:14:58,280 --> 00:15:04,920
Il y a des initiatives tout à fait fantastiques, de New York à Athènes. 

120
00:15:04,920 --> 00:15:12,560
A Munich, il y en a deux, à Hambourg, il y en a quatre en tout. A Berlin, à Erlangen, à Düsseldorf, à Aix-la-Chapelle.

121
00:15:12,560 --> 00:15:15,800
Il y a beaucoup à découvrir.

122
00:15:15,800 --> 00:15:17,280
Mais, est-ce que ce sera aussi amusant ?

123
00:15:17,280 --> 00:15:23,640
Oh, du plaisir ! Oui, bien sûr. C'est vrai qu'on n'a pas le Jeopardy,

124
00:15:23,640 --> 00:15:29,080
nous n'avons plus réussi à préparer de belles questions pour cela. 

125
00:15:29,080 --> 00:15:38,400
Mais nous essayons de faire quelque chose d'aussi fou. On va appeler ça la Coupe du monde Mafifa 2030. On va rejouer le tirage au sort de la Coupe du monde 2030.

126
00:15:38,400 --> 00:15:44,920
Laissez-vous surprendre. Chaque fois à 23 heures, mercredi, jeudi et vendredi.

127
00:15:44,920 --> 00:15:47,400
Ça va être intéressant.

128
00:15:47,400 --> 00:15:50,600
Vous devez faire ça de manière très corrompue.

129
00:15:50,600 --> 00:15:57,840
Le maté compte-t-il comme monnaie ?

130
00:15:57,840 --> 00:16:05,720
Si cela ne compte pas comme monnaie, alors nous ne sommes pas corrompus non plus. 

131
00:16:05,720 --> 00:16:12,880
Ce qui est important, c'est que chez nous, il y a très, très peu de présentations frontales.

132
00:16:12,880 --> 00:16:19,640
Chez nous, presque tout est un atelier participatif, conçu comme un Barcamp. 

133
00:16:19,640 --> 00:16:28,080
Il y a un barcamp XR, juste après cette ouverture. Mais il y a aussi un Bits and arbres.

134
00:16:28,080 --> 00:16:34,200
Fireside Chat, où vous êtes cordialement invités à participer.

135
00:16:34,200 --> 00:16:39,760
Aussi les ateliers d'Overte, où nous explorons ce monde, 

136
00:16:39,760 --> 00:16:41,080
sont également conçus pour être des ateliers et non des conférences.

137
00:16:41,080 --> 00:16:50,760
Tu veux redire comment on accède au monde ? 

138
00:16:50,760 --> 00:16:57,800
Parce que nous voyons ce monde vraiment génial, mais comment y entrer. 

139
00:16:57,800 --> 00:16:58,800
Est-ce qu'on peut vous rendre visite ?

140
00:16:58,800 --> 00:17:04,520
Oui, bien sûr. Donc l'essentiel se passe chez nous dans Matrix. 

141
00:17:04,520 --> 00:17:11,960
Dans l'horaire sur Pretalx, un canal Matrix est indiqué pour chaque exposé. 

142
00:17:11,960 --> 00:17:16,760
C'est là que nous nous rencontrons et que nous échangeons aussi toutes les informations. 

143
00:17:16,760 --> 00:17:29,820
L'autre chose, c'est ce Metaverse, ce Multiverse Overte.

144
00:17:29,820 --> 00:17:35,520
Overte.org, c'est déjà la page d'accueil.
 Vous pouvez y télécharger un client.

145
00:17:35,520 --> 00:17:42,880
Il existe pour Linux et pour Windows. Curieusement, il n'est pas disponible pour Apple. 

146
00:17:42,880 --> 00:17:51,840
Vous pouvez aussi l'utiliser pour nous rendre visite à l'ICCBS, dans la forêt de Fairy Dust. 

147
00:17:51,840 --> 00:17:57,160
ou encore dans un hackerspace virtuel sur le xrelog avec la Track Tower. 

148
00:17:57,160 --> 00:17:58,160
Vous pouvez explorer tout cela vous-même.

149
00:17:58,160 --> 00:18:08,760
Cela a l'air passionnant et je pense que, 

150
00:18:08,760 --> 00:18:11,600
certains auditeurs devraient aussi trouver vraiment excitant de vous rencontrer dans l'espace 3D.

151
00:18:11,600 --> 00:18:17,640
Je suis curieux de savoir. A partir de combien de spectateurs les serveurs vont-ils s'effondrer ? 

152
00:18:17,640 --> 00:18:19,040
nous allons le découvrir ensemble.

153
00:18:19,040 --> 00:18:20,040
Faire des tests, tester.

154
00:18:20,040 --> 00:18:26,200
Nous pouvons aussi provisionner d'autres serveurs. Mais nous ne sommes pas encore automatisés. 

155
00:18:26,200 --> 00:18:32,280
provisionner d'autres serveurs. Mais nous ne sommes pas encore très automatisés. 

156
00:18:32,280 --> 00:18:36,840
Cela prend un peu de temps, mais nous sommes prêts. 

157
00:18:36,840 --> 00:18:38,080
Si on tombe maintenant, on peut encore faire quelque chose.

158
00:18:38,080 --> 00:18:45,520
Pour finir, je voudrais chaleureusement vous dire au revoir. 

159
00:18:45,520 --> 00:18:51,240
Au fait aussi de, euh, euh, euh, qui es-tu au juste ?

160
00:18:51,240 --> 00:18:56,560
Désolé Anchoray, je ne peux pas te le dire.

161
00:18:56,560 --> 00:19:25,560
Nous devrons donc résoudre cette énigme la prochaine fois. À plus tard. Au revoir.

