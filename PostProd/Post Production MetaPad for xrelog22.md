---
title: Post Production MetaPad for xrelog22
tags: xrelog22, postprod, meta
description: Post Production MetaPad for xrelog22
---

# Post Production MetaPad for xrelog22

Dieses Pad dient als zentrales Sammelpad für die Post-Produktion der xrelog22 Videos.

## Struktur
Folgendes ist angedacht:
- Übersicht aller Sessions 
    - inkl. Links
        - PostProd-Pad
        - Content-Pad
        - pretalx-seite
        - c3voc Seiten
        - ...
    - Overall-Status (max. 1)
    - ID's (UUID, Slug, XML-No, ...)
- Allgemeine Tipps, Kontakte, Tools

## Sessions
Hier die Sessions für die Post-Production in der Übersicht:

| Nr | Session | Author | Orig |
| -- | -------- | -------- | -------- |
1|Opening|XR.Labs|**[de](https://tube.lab.nrw/w/ozJb749bt2dMnDihrz4ugQ)**
2|The Evolution of Metaverse|Darling|**[en](https://tube.lab.nrw/w/5ZkYKGw5TQ7zyDSijcrFiU)**
3|Overte: In-world creation with Voxels, Physics, etc.|Overte-Team|en
4|mafifa - WM 2030 (1: Auslosung)|CodeFreezR|de
5|Overte: World hopping|Overte-Team|en
6|The Web 3.0 House of Cards|aestetix|en
7|Is the Metaverse, Metaworse?|Morton Swimmer|**[en](https://tube.lab.nrw/w/gfRp8PWLfE8YGAkP9L17LS)**
7|Third Room: Decentralized Virtual Worlds on Matrix|Robert Long|**[en](https://tube.lab.nrw/w/6sozpV5qfQwP42S4phmj91)**
8|Menschenrechtsverletzungen durch Facebook/Meta|Steffen|de
9|Overte: Logic using the JavaScript API|Overte-Team|en
10|Poetry-Paella-Party incl. jev22 Recap|XR.Labs & Friends|en

In der Spalte **Nr** wird das Post-Prod Pad für die Session verlinkt.
In der Spalte **Session** kommen die Links zum Pretalx hin.
In der Spalte **Orig** sind die raw/prelive Videos der Originalsprache verlinkt falls schon hochgeladen.


## Formate
Für Audio können wir diese Formate: flac, wav oder mp3 direkt verarbeiten.
Stereo oder Mono ist für Sprachspur erstmal egal.
Bitauflösung: 128 oder 192
Alles andere sollten wir aber auch nach Rücksprache konvertieren können.


## Tools & Tipps

## Links